#!/bin/bash
ml gcc/8.2.0
ml zlib/1.2.11
ml cmake/3.9.4
ml matlab/R2018a
ml swig/3.0.12
ml python/3.6.3
ml mkl/2019.2.187
echo "Setting compiler variables CC and CXX"
export CC=/home/comm/swstack/core/gcc/8.2.0/bin/gcc
export CXX=/home/comm/swstack/core/gcc/8.2.0/bin/g++