#!/bin/bash
#SBATCH --job-name=BHW
#SBATCH --output=slurm-%A_%a.txt
#SBATCH --partition=q16
#SBATCH --mem=4G
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --time=25:00:00
#SBATCH --array=1-100%100

programname=sga_grape

echo "========= Job started  at `date` =========="

echo $SLURM_SUBMIT_DIR

outputfolder="output_${SLURM_JOB_NAME}"

mkdir -p $SLURM_SUBMIT_DIR/../${outputfolder}

# Go to the directory where this job was submitted
cd $SLURM_SUBMIT_DIR
cd ../

# copy inputdata and the executable to the scratch-directory
cp $programname /scratch/$SLURM_JOB_ID

# change directory to the local scratch-directory, and run:
cd /scratch/$SLURM_JOB_ID
#mkdir output
export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK:-1}
./$programname ${SLURM_ARRAY_TASK_ID}  > out
#
# copy home the outputdata:
cp out   $SLURM_SUBMIT_DIR/../${outputfolder}/seed${SLURM_ARRAY_TASK_ID}.txt
#rm $SLURM_SUBMIT_DIR/*.out
cp *.mat $SLURM_SUBMIT_DIR/../${outputfolder}

cd $SLURM_SUBMIT_DIR
mv slurm-${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}.txt ../${outputfolder}/seed${SLURM_ARRAY_TASK_ID}_slurm.txt

echo "========= Job finished at `date` =========="
#