/*
 This program was used to generate F(T) (where T - control process duration) curves for the Bring Home Water problem.
 The Bring Home Water problem is defined in the Systems.h file (functions: bhw_setup, bhw) 
 and propagated to this file with the use of macros from the header file HelperMacros.h

 The main routine iterates over a specified range of process 'durations' (TMin, TMin + dT, ..., TMax)
 and generates control seeds with lengths corresponding to those durations (with points spaced by 'dt' times).
 The seeding strategy used to generate the seed 'u' is RS (by default).

 Seed 'u' is then optimized with the use of a selected optimization algorithm. 
 The algorithm is selected using the 'optimizerType' enum (GRAPE, SGA, SGA_BUCKET).
 The optimization algorithm's stopping conditions (fidelity, fpp, time out etc.)
 and data captured are specified in this file (variables fidThreshold, time threshold etc.) and the header files: 
    for GRAPE: "optimizer_settings.bp.h",  "optimize_and_save.bp.h" 
    for SGA/SGA_BUCKET:  "optimizer_settings_sga.bp.h",  "optimize_and_save_sga.bp.h".
 The data is saved in .mat files with names specified in the header file  "boilerplate/save_name.bp.h".
*/

#include <tuple>
#include <iostream>
#include "Tasks.h"
#include <chrono>
#include <random>
#include "qengine/qengine.h"
#include "Boilerplate.h"
#include "HelperMacros.h"
#include "OptimizerTypes.h"
#include "Systems.h"
#include "ControlFactories.h"
#include "RAII.h"
#include "armadillo"
#include "SeedingStrategies/RandomSineSeeding.h"
#include "SeedingStrategies/LinearSeeding.h"
#include "DifferentialEvolution.h"
#include "SeedingStrategies/RandomizedSineSeedingStrategy.h"
#include "SeedingStrategies/RandomSeeding.h"

#include <SGA.h>
using namespace qengine;

#define BHW_REF_CANDIDATES

typedef std::chrono::duration<double, std::ratio<3600> > hours;
typedef std::chrono::duration<double, std::ratio< 60> >  minutes;
typedef std::chrono::duration<double                   > seconds;
typedef std::chrono::duration<double, std::milli>        milliseconds;
typedef std::chrono::duration<double, std::micro>        microseconds;


int main(int argc, char *argv[])
{
    arma::arma_rng::set_seed_random();
    using namespace qengine;

    int runNumber = 1;
    if(argv[1]) runNumber = std::stoi(argv[1]);

    constexpr real TMin = 0.2;
    constexpr real TMax = 0.3;
    constexpr real dT= 0.005;
    constexpr size_t triesPerT = 5;
    const auto Ts = makeTimeControlWithFixedEnd(TMin,TMax,dT);
    const auto nTs  = Ts.size();

    constexpr size_t dim = 256;
    constexpr real    dt = 3.5e-4;


    constexpr SeedingType seedType = SeedingType::RS;
    constexpr OptimizerType optimizerType = OptimizerType::SGA;

    // optimization parameters
    auto printInterval       = 1;
    constexpr auto iterationThreshold  = -1;
    constexpr auto fidelityThreshold   = 0.999;
    constexpr auto stepsizeThreshold   = 1e-7;
    constexpr auto superiteration_threshold = 10;
    constexpr auto fppThreshold        = 1e7;     
    const auto fppCheckpoint           = std::make_pair(round(fppThreshold/2.0),0.8);

    const auto walltimeThresholdAll = hours( 24 );
    auto walltimeThreshold   = walltimeThresholdAll/(nTs * triesPerT) - minutes(1);
    if (walltimeThreshold.count() < 0)
    {
        std::cerr << "Negative time (" << walltimeThreshold.count() << ") per seed. Aborting the program.\n";
        exit(1);
    }
    std::cout << "Total time:" << walltimeThresholdAll.count() << "\nTime per seed: " << walltimeThreshold.count() << std::endl;
    // step size finder
    const auto linesearchEpsilon   = 1e-8;
    const auto stepsizemax         = 10.0;
    const auto stepsizeinitial     = 1.0;
    const auto unrollThreshold     = 3000000;
    const auto unrolls             = 2;
    const auto sgaBuckets = 40;

    const auto regularization      = 1e-6;

    const auto qThreshold    = 1e-14;
    const auto bfgsRestarter = makeStepTimesYBfgsRestarter(qThreshold, false);


    #include "boilerplate/save_name.bp.h"


    std::cout << "BHW sweep" << std::endl;
    const auto start = std::chrono::high_resolution_clock::now();
    BHW(dim);

    auto elapsedHoursTotal = [&start]()
    {
        auto e = std::chrono::high_resolution_clock::now() - start;
        return std::chrono::duration_cast<hours>(e);
    };

    auto elapsedMsTotal = [&start]()
    {
        auto e = std::chrono::high_resolution_clock::now() - start;
        return std::chrono::duration_cast<milliseconds>(e);
    };

    DataContainer metadata;
    metadata["system_type"] = systemType;
    metadata["dim"] = dim;
    metadata["dt"]  = dt;
    metadata["Ts"]  = Ts.mat();
    metadata["x"]   = x.vec();
    metadata["psi_0"] = psi_0.vec();
    metadata["psi_t"] = psi_t.vec();
    metadata["n_preselection_seeds"] = nPreselectionSeeds;
    metadata["regularization"] = regularization;
    metadata["threshold_fidelity"] = fidelityThreshold;
    metadata["threshold_fpp"] = fppThreshold;
    metadata["threshold_stepsize"] = stepsizeThreshold;        
    metadata["threshold_stepsize"] = stepsizeThreshold;
    metadata["stepsize_max"] = stepsizemax;
    metadata["stepsize_initial"] = stepsizeinitial;
    metadata["optimizertype"] = TranslateOptimizer( optimizerType );
    metadata["leftBoundaryPhysical"] = leftBoundaryPhysical;
    metadata["rightBoundaryPhysical"] = rightBoundaryPhysical;
    metadata["leftBoundary"] = leftBoundary;
    metadata["rightBoundary"] = rightBoundary;    
    metadata["boundaryStrength"] = boundaryStrength;        
    metadata["BFGS_restart_threshold"] = qThreshold;   
    metadata["sga_buckets"] = sgaBuckets;
    metadata.save("metadata.mat");

    // sga stuff - put outside the T loop on purpose (no need to recompute the caches)
    Control cacheDiscretized = makeDiscretizedControls( DiscreteControlInfo{ {makeDiscreteDimInfo(leftBoundaryPhysical.at(0), rightBoundaryPhysical.at(0), 128), 
                                                         makeDiscreteDimInfo(leftBoundaryPhysical.at(1), -130.0, 1)} , dt}) ;
    cacheDiscretized = controlScaling.MapScale(cacheDiscretized, utility::ScalingType::FORWARD);
    auto leftCachingStepper = makeDirectExponentCachingTimeStepper(cacheDiscretized, H, psi_0, dt);
    auto rightCachingStepper = makeDirectExponentCachingTimeStepper(cacheDiscretized, H, psi_t, -dt);
  
    try
    {
    for (size_t T_subindex = 0 ; T_subindex < triesPerT ; ++T_subindex)
    {
        count_t T_index = 1;
        for(count_t i = 0; i < Ts.size() ; i++)
        {

            walltimeThreshold = walltimeThresholdAll / ((triesPerT - T_subindex) * (nTs - i)) - minutes(1);
            if ( walltimeThreshold.count() < 0)
            {
                continue;
            }
            std::cout << "Recalculating time left per seed: " << walltimeThreshold.count() << std::endl;
            DataContainer dc;
            std::string saveName = saveNameMacro;

            const real duration = Ts.get(i).at(0);
            const auto n_steps = floor(duration / dt) + 1;
            const auto t = makeTimeControl(n_steps, dt);

            auto makeProblem = [H{H},psi_0{psi_0},psi_t{psi_t},regularization, leftBoundary, rightBoundary, boundaryStrength](const auto& u_in)
            {
                return makeStateTransferProblem(H,psi_0,psi_t,u_in) + regularization*Regularization(u_in) + boundaryStrength * Boundaries(u_in, leftBoundary, rightBoundary);
            };  

            auto elapsedHours = [start{std::chrono::high_resolution_clock::now()}]()
            {
                auto e = std::chrono::high_resolution_clock::now() - start;
                return std::chrono::duration_cast<hours>(e);
            };            

            qengine::Control u;

            switch(seedType)
            {
                case SeedingType::RS:
                {
                    RandomSeedingStrategy rs(leftBoundary, rightBoundary, n_steps, initialControl, finalControl, dt);
                    u = rs();
                    
                    break;
                }
            }

            dc["ui"] = u.mat();
               
            #include "boilerplate/optimizer_all.bp.h"

            T_index++;

            if( elapsedHoursTotal().count() > std::chrono::duration_cast<hours>(walltimeThresholdAll).count() )  
            {
                break;
            }
        }
    }
}
catch (const std::exception & err)
{
    std::cerr << "ERROR: " << err.what() << std::endl;
}

    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "Program execution time: " << elapsed.count();       
    return 0;   
}
