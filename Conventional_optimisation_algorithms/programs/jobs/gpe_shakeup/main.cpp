/*
 This program was used to generate F(T) (where T - control process duration) curves for the GPE ShakeUp problem.
 The GPE ShakeUp problem is defined in the Systems.h file (functions: gpe_shakeup_setup, gpe_shakeup) 
 and propagated to this file with the use of macros from the header file HelperMacros.h

 The main routine iterates over a specified range of process 'durations' (TMin, TMin + dT, ..., TMax)
 and generates control seeds with lengths corresponding to those durations (with points spaced by 'dt' times).
 The seeding strategy used to generate the seed 'u' is RS (by default).

 Seed 'u' is then optimized with the use of a selected optimization algorithm. 
 The algorithm is selected using the 'optimizerType' enum (GRAPE, SGA, SGA_BUCKET).
 The optimization algorithm's stopping conditions (fidelity, fpp, time out etc.)
 and data captured are specified in this file (variables fidThreshold, time threshold etc.) and the header files: 
    for GRAPE: "optimizer_settings.bp.h",  "optimize_and_save.bp.h" 
    for SGA/SGA_BUCKET:  "optimizer_settings_sga.bp.h",  "optimize_and_save_sga.bp.h".
 The data is saved in .mat files with names specified in the header file  "boilerplate/save_name.bp.h".
*/

#include <tuple>
#include <iostream>
#include <random>
#include "Tasks.h"
#include <chrono>
#include "qengine/qengine.h"
#include "Boilerplate.h"
#include "HelperMacros.h"
#include "OptimizerTypes.h"
#include "Systems.h"
#include "ControlFactories.h"
#include "RAII.h"
#include "armadillo"
#include "DifferentialEvolution.h"
#include "SeedingStrategies/RandomSineSeeding.h"
#include "SeedingStrategies/LinearSeeding.h"
#include "SeedingStrategies/RandomSeeding.h"

#include "SeedingStrategies/RandomizedSineSeedingStrategy.h"

#include <SGA_GPE.h>

using namespace qengine;

typedef std::chrono::duration<double, std::ratio<3600> > hours;
typedef std::chrono::duration<double, std::ratio< 60> >  minutes;
typedef std::chrono::duration<double                   > seconds;
typedef std::chrono::duration<double, std::milli>        milliseconds;
typedef std::chrono::duration<double, std::micro>        microseconds;

int main(int argc, char *argv[])
{
    using namespace qengine;
    arma::arma_rng::set_seed_random();
    int runNumber = 1;
    if(argv[1]) runNumber = std::stoi(argv[1]);

    constexpr size_t triesPerT = 5;
    constexpr real TMin = 0.8;
    constexpr real TMax = 1.0;
    constexpr real dT= 0.01;
    const auto Ts = makeTimeControlWithFixedEnd(TMin,TMax,dT);
    const auto nTs  = Ts.size();

    constexpr size_t dim = 256;
    constexpr real    dt = 0.001;

    constexpr SeedingType seedType = SeedingType::RS;
    constexpr OptimizerType optimizerType = OptimizerType::SGA_BUCKET;

    // optimization parameters
    constexpr auto printInterval       = 1;
    constexpr auto iterationThreshold  = -1;
    constexpr auto fidelityThreshold   = 0.999;
    constexpr auto stepsizeThreshold   = 1e-7;
    constexpr auto fppThreshold        = 1e7;  
    const auto fppCheckpoint           = std::make_pair(round(fppThreshold/2.0),0.8);

    const auto walltimeThresholdAll = hours( 24 );
    auto walltimeThreshold   = walltimeThresholdAll/(nTs * triesPerT) - minutes(1);
    if (walltimeThreshold.count() < 0)
    {
        std::cerr << "Negative time (" << walltimeThreshold.count() << ") per seed. Aborting the program.\n";
        exit(1);
    }

    std::cout << "Total time:" << walltimeThresholdAll.count() << "\nTime per seed: " << walltimeThreshold.count() << std::endl;
    // step size finder
    const auto linesearchEpsilon   = 1e-8;
    const auto stepsizemax         = 10.0;
    const auto stepsizeinitial     = 1.0;
    const size_t sgaBuckets = 40;
    const auto regularization      = 1e-6;

    const auto qThreshold    = 1e-14;
    const auto bfgsRestarter = makeStepTimesYBfgsRestarter(qThreshold, false);

    #include "boilerplate/save_name.bp.h"

    std::cout << "GPE shakeup sweep" << std::endl;
    const auto start = std::chrono::high_resolution_clock::now();
    GPE_SHAKEUP(dim);

    auto elapsedMsTotal = [&start]()
    {
        auto e = std::chrono::high_resolution_clock::now() - start;
        return std::chrono::duration_cast<milliseconds>(e);
    };

    auto elapsedHoursTotal = [&start]()
    {
        auto e = std::chrono::high_resolution_clock::now() - start;
        return std::chrono::duration_cast<hours>(e);
    };

    DataContainer metadata;
    metadata["system_type"] = systemType;
    metadata["dim"] = dim;
    metadata["dt"]  = dt;
    metadata["Ts"]  = Ts.mat();
    metadata["x"]   = x.vec();
    metadata["psi_0"] = psi_0.vec();
    metadata["psi_t"] = psi_t.vec();
    metadata["optimizertype"] = TranslateOptimizer( optimizerType );
    metadata["n_preselection_seeds"] = nPreselectionSeeds;
    metadata["regularization"] = regularization;
    metadata["threshold_fidelity"] = fidelityThreshold;
    metadata["threshold_fpp"] = fppThreshold;
    metadata["threshold_stepsize"] = stepsizeThreshold;        
    metadata["stepsize_max"] = stepsizemax;
    metadata["stepsize_initial"] = stepsizeinitial;
    metadata["leftBoundaryPhysical"] = leftBoundaryPhysical;
    metadata["rightBoundaryPhysical"] = rightBoundaryPhysical;
    metadata["leftBoundary"] = leftBoundary;
    metadata["rightBoundary"] = rightBoundary;    
    metadata["boundaryStrength"] = boundaryStrength;
    metadata["BFGS_restart_threshold"] = qThreshold;
    metadata.save("metadata.mat");

    // sga stuff - put outside the T loop on purpose (no need to recompute the caches)
    Control cacheDiscretized = makeDiscretizedControls( DiscreteControlInfo{ {makeDiscreteDimInfo(leftBoundaryPhysical, rightBoundaryPhysical, 128)} , dt}) ;
    cacheDiscretized = controlScaling.MapScale(cacheDiscretized, utility::ScalingType::FORWARD);


    try 
    {    
    for (size_t T_subindex = 0 ; T_subindex < triesPerT ; ++T_subindex)
    {
        count_t T_index = 1;        
        for(count_t i = 0; i < Ts.size() ; i++)
        {

            const real duration = Ts.get(i).at(0);
            fprintf(stderr, "Running for T = %f\n", duration);
            walltimeThreshold = walltimeThresholdAll / ((triesPerT - T_subindex) * (nTs - i)) - minutes(1);
            if ( walltimeThreshold.count() < 0)
            {
                continue;
            }
            std::cout << "Recalculating time left per seed: " << walltimeThreshold.count() << std::endl;
            DataContainer dc;
            std::string saveName = saveNameMacro;

            const auto n_steps = floor(duration / dt) + 1;
            const auto t = makeTimeControl(n_steps, dt);

            auto makeProblem = [H{H},psi_0{psi_0},psi_t{psi_t},regularization, leftBoundary, rightBoundary, boundaryStrength](const auto& u_in)
            {
                return makeStateTransferProblem(H,psi_0,psi_t,u_in) + regularization*Regularization(u_in) + boundaryStrength * Boundaries(u_in, leftBoundary, rightBoundary);
            };  
            auto elapsedHours = [start{std::chrono::high_resolution_clock::now()}]()
            {
                auto e = std::chrono::high_resolution_clock::now() - start;
                return std::chrono::duration_cast<hours>(e);
            };   

            qengine::Control u;

            switch(seedType)
            {
                case SeedingType::RS:
                {
                    RandomSeedingStrategy rs(leftBoundary, rightBoundary, n_steps, initialControl, finalControl, dt);
                    u = rs();
                    
                    break;
                }
            }

            dc["ui"] = u.mat();

            switch (optimizerType)
            {
                case OptimizerType::GRAPE:
                {
                    auto stepSizeFinder = makeInterpolatingStepSizeFinder(stepsizemax,stepsizeinitial,linesearchEpsilon);
                    #include "boilerplate/optimizer_settings.bp.h"

                    auto problem = makeProblem(u);
                    auto opt = makeGrape_bfgs_L2(problem,stopper,collector,stepSizeFinder,bfgsRestarter);
                    #include "boilerplate/optimize_and_save.bp.h"
                    break;
                }
                case OptimizerType::SGA:
                {
                    const sga::AccessOrder order = sga::AccessOrder::Random;

                    #include "boilerplate/optimizer_settings_sga.bp.h"

                    auto dcPack = sga::makeDiscretizedControlPack(u, cacheDiscretized);
                    u = dcPack.mDiscretizedReference;

                    SGA_GPE <decltype(psi_0), decltype(H), decltype(costFun), decltype(stopper), decltype(collectorSGA)> opt(H, dcPack, psi_0, psi_t, stopper, collectorSGA, order, costFun);
                    #include "boilerplate/optimize_and_save_sga.bp.h"
                    break;
                }                
                case OptimizerType::SGA_BUCKET:
                {
                    const sga::AccessOrder order = sga::AccessOrder::Random;

                    #include "boilerplate/optimizer_settings_sga.bp.h"

                    auto dcPack = sga::makeDiscretizedControlPack(u, cacheDiscretized);
                    u = dcPack.mDiscretizedReference;

                    SGA_GPE <decltype(psi_0), decltype(H), decltype(costFun), decltype(stopper), decltype(collectorSGA)> opt(H, dcPack, psi_0, psi_t, stopper, collectorSGA, order, costFun, sgaBuckets);
                    #include "boilerplate/optimize_and_save_sga.bp.h"
                    break;
                }                          
                default:
                {
                    throw std::runtime_error("Unknown optimizer type.");
                }
            }
            }
            T_index++;

            if( elapsedHoursTotal().count() > std::chrono::duration_cast<hours>(walltimeThresholdAll).count() )  
            {
                break;
            }
        }
    }
    catch (const std::exception & err)
    {
        std::cerr << err.what() << std::endl;
    }    

    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "Program execution time: " << elapsed.count();       
    return 0;   
}
