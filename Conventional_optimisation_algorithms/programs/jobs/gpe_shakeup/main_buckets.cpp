/*
 This program was used to generate F(T) (where T - control process duration) curves for the GPE ShakeUp problem for the Quantum Moves 2 game.
 The Bring Home Water problem is defined in the Systems.h file (functions: gpe_shakeup_setup, gpe_shakeup) 
 and propagated to this file with the use of macros from the header file HelperMacros.h

 The main routine iterates over a specified range of process 'durations' which are computed as multipliers of a specific T_QSL value (T=0.89).
 These multipliers are {0.05, 0.15, ..., 1.15}.
 Afterwards the program generates control seeds with lengths corresponding to those durations (with points spaced by 'dt' times).
 The seeding strategy used to generate the seed 'u' is generated using a reference control, modification with a randomized sine basis and a preselection scheme.

 
 Seed 'u' is then optimized with the use of the GRAPE optimization algorithm. 
 The optimization algorithm's stopping conditions (fidelity, fpp, time out etc.)
 and data captured are specified in this file (variables fidThreshold, time threshold etc.) and the header files: 
    for GRAPE: "optimizer_settings.bp.h",  "optimize_and_save.bp.h" 
 The data is saved in .mat files with names specified in the "saveNameMacro" macro.
*/


#include <tuple>
#include <iostream>
#include "Tasks.h"
#include <chrono>
#include "qengine/qengine.h"
#include "Boilerplate.h"
#include "HelperMacros.h"
#include "OptimizerTypes.h"
#include "Systems.h"
#include "ControlFactories.h"
#include "RAII.h"
#include "armadillo"
#include "SeedingStrategies/RandomSineSeeding.h"
#include "SeedingStrategies/LinearSeeding.h"

#include "TimeBuckets.h"
#include "SeedingStrategies/RandomizedSineSeedingStrategy.h"


using namespace qengine;

typedef std::chrono::duration<double, std::ratio<3600> > hours;
typedef std::chrono::duration<double, std::ratio< 60> >  minutes;
typedef std::chrono::duration<double                   > seconds;

int main(int argc, char *argv[])
{
    using namespace qengine;

    int runNumber = 1;
    if(argv[1]) runNumber = std::stoi(argv[1]);

    size_t triesPerT = 2;

    constexpr size_t dim = 256;
    constexpr real    dt = 0.001;

    // Bucket parameters
    constexpr real dT = 0.1;
    constexpr real leftTMultiplier = 0.05;
    constexpr real rightTMultiplier = 1.2;    
    constexpr real T_QSL = 0.89;

    BucketsInfo bucketInfo {dT, leftTMultiplier, rightTMultiplier, T_QSL};
    PrintBucketsInfo( bucketInfo );
    const auto Ts = ConstructTBucketControl( bucketInfo );

    fprintf(stderr, "T in bucket: ");
    for (size_t i = 0 ; i < Ts.size() ; ++i)
    {
        fprintf(stderr, "%f ", Ts._vec().at(i));
    }
    fprintf(stderr, "\n");
    
    const auto nTs  = Ts.size();

    OptimizerType optimizerType = OptimizerType::GRAPE;

    // optimization parameters
    const auto nPreselectionSeeds  = 7500 ;
    constexpr size_t nReferenceCandidates = 1;
    constexpr size_t nRandomizedReimprovements = 1;

    constexpr auto printInterval       = 100;
    constexpr auto iterationThreshold  = -1;
    constexpr auto fidelityThreshold   = 0.999;
    constexpr auto stepsizeThreshold   = 1e-7;
    constexpr auto fppThreshold        = 1e7;
    const auto fppCheckpoint           = std::make_pair(round(fppThreshold/2.0),0.8);

    const auto walltimeThresholdAll= hours(22);
    auto walltimeThreshold   = walltimeThresholdAll/(nTs * triesPerT) - minutes(2);
    if (walltimeThreshold.count() < 0)
    {
        std::cerr << "Negative time (" << walltimeThreshold.count() << ") per seed. Aborting the program.\n";
        exit(1);
    }
    std::cout << "Total time:" << walltimeThresholdAll.count() << "\nTime per seed: " << walltimeThreshold.count() << std::endl;
    // step size finder
    const auto linesearchEpsilon   = 1e-8;
    const auto stepsizemax         = 10.0;
    const auto stepsizeinitial     = 1.0;
    const auto unrollThreshold     = 30000;
    const auto unrolls             = 2;

    const auto regularization      = 1e-6;

    const auto qThreshold    = 1e-14;
    const auto bfgsRestarter = makeStepTimesYBfgsRestarter(qThreshold, false);

    // set up the seeding engine
    auto makeSeeding = [] (const auto & t, const auto u_reference) 
    { 
        return RandomizedSineSeedingStrategy{t, u_reference, 0.01}; 
    };

    auto adjustSeeding = [=] (auto seeding)
    {
        // example use of the new seed generation mechanism
        seeding.SetSineBasisRandomized({{1, 100}, {1, 100}});
        seeding.SetMaxAbsAmplitudeRandomized({{0.5, 1}, {0.5, 1}});
        seeding.SetAmplitudeDecayDistribution(CoeffDistribution::Normal);
        seeding.SetMaxAbsHarmonicOffsetFixed({{0.5}, {0.5}});
        seeding.SetFluenceRandomized({{1, 1e2}, {1, 5e3}});
        return seeding;
    };

    #define saveNameMacro \
    "seed_T" + std::to_string(T_index) + "_" + std::to_string(runNumber) + ".mat"

    std::cout << "GPE shakeup sweep" << std::endl;
    const auto start = std::chrono::high_resolution_clock::now();
    GPE_SHAKEUP(dim);

    auto elapsedHoursTotal = [&start]()
    {
        auto e = std::chrono::high_resolution_clock::now() - start;
        return std::chrono::duration_cast<hours>(e);
    };

    DataContainer metadata;
    metadata["system_type"] = systemType;
    metadata["dim"] = dim;
    metadata["dt"]  = dt;
    metadata["Ts"]  = Ts.mat();
    metadata["x"]   = x.vec();
    metadata["psi_0"] = psi_0.vec();
    metadata["psi_t"] = psi_t.vec();
    metadata["optimizertype"] = TranslateOptimizer( optimizerType );    
    metadata["n_preselection_seeds"] = nPreselectionSeeds;
    metadata["regularization"] = regularization;
    metadata["threshold_fidelity"] = fidelityThreshold;
    metadata["threshold_fpp"] = fppThreshold;
    metadata["threshold_stepsize"] = stepsizeThreshold;
    metadata["stepsize_max"] = stepsizemax;
    metadata["stepsize_initial"] = stepsizeinitial;
    metadata["unrollThreshold"]  = unrollThreshold;
    metadata["unrolls"]          = unrolls;
    metadata["leftBoundaryPhysical"] = leftBoundaryPhysical;
    metadata["rightBoundaryPhysical"] = rightBoundaryPhysical;
    metadata["leftBoundary"] = leftBoundary;
    metadata["rightBoundary"] = rightBoundary;    
    metadata["boundaryStrength"] = boundaryStrength;
    metadata["BFGS_restart_threshold"] = qThreshold;        
    metadata.save("metadata.mat");

    try 
    {    
        for (size_t T_subindex = 0 ; T_subindex < triesPerT ; ++T_subindex)
        {
            count_t T_index = 1;   
            for(count_t i = 0; i < Ts.size() ; i++)
            {

                walltimeThreshold = walltimeThresholdAll / ((triesPerT - T_subindex) * (nTs - i)) - minutes(2);
                if ( walltimeThreshold.count() < 0)
                {
                    continue;
                }
                std::cout << "Recalculating time left per seed: " << walltimeThreshold.count() << std::endl;

                DataContainer dc;
                std::string saveName = saveNameMacro;

                const real duration = Ts.get(i).at(0);
                fprintf(stderr, "Running for T = %f\n", duration);

                const auto n_steps = floor(duration / dt) + 1;
                const auto t = makeTimeControl(n_steps, dt);
                const auto initialAmplitude = 0.5;
                auto u_reference = initialAmplitude*sin(PI/duration * t);
                auto constructRefControl = [u_reference] () { return u_reference; };
                constexpr bool u_ref_Rescaled = false;        

                auto seeder = makeSeeding(t, u_reference);
                seeder = adjustSeeding( seeder );

                auto makeProblem = [H{H},psi_0{psi_0},psi_t{psi_t},regularization, leftBoundary, rightBoundary, boundaryStrength](const auto& u_in)
                {
                    return makeStateTransferProblem(H,psi_0,psi_t,u_in) + regularization*Regularization(u_in) + boundaryStrength * Boundaries(u_in, leftBoundary, rightBoundary);
                }; 

                switch (optimizerType)
                {
                    case OptimizerType::GRAPE:
                    {
                            auto stepSizeFinder = experimental::makeQuickStepHeuristic(makeInterpolatingStepSizeFinder(stepsizemax,stepsizeinitial,linesearchEpsilon),Control(),unrollThreshold,unrolls);

                            #include "boilerplate/seed_preselection.bp.h"
                            #include "boilerplate/optimizer_settings.bp.h"
                            auto problem = makeProblem(u);
                            auto opt = makeGrape_bfgs_L2(problem,stopper,collector,stepSizeFinder,bfgsRestarter);
                            #include "boilerplate/optimize_and_save.bp.h"
                            break;
                    }
                    default :
                    {
                        throw std::runtime_error("Unknown optimizer type.");
                    }
                }

                T_index++;

                if( elapsedHoursTotal().count() > std::chrono::duration_cast<hours>(walltimeThresholdAll).count() )  
                {
                    break;
                }
            }
        }
    }
    catch (const std::runtime_error & err)
    {
        std::cerr << err.what() << std::endl;
    }    

    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "Program execution time: " << elapsed.count();       
    return 0;   
}
