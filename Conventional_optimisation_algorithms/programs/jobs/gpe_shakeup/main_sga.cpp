/*
 This program was used to generate F(t) (where t - wall time) curves for comparing GRAPE, SGA and SGA_BUCKET algorithms in their efficiency in optimizing a controls seed.
 The optimization problem solved in this program is GPE ShakeUp.

 The GPE ShakeUp problem is defined in the Systems.h file (functions: gpe_shakeup_setup, gpe_shakeup) 
 and propagated to this file with the use of macros from the header file HelperMacros.h

 The main routine generates control seeds with lengths corresponding to a duration of T=0.255 with 'dt' spacing between subsequent control points.
 The seeding strategy used to generate the seed 'u' is RS (by default).

 Seed 'u' is generated and adjusted for the SGA algorithm for optimization (the values are readjusted to belong in the set of 128 predefined control values). 
 The seed 'u' is then copied and the copy optimized by SGA algorithm, with the optimization data being captured. 
 Similar routine is followed with the use of the SGA_BUCKET and GRAPE algorithms.

 The optimization algorithm's stopping conditions (fidelity, fpp, time out etc.)
 and data captured are specified in this file (variables fidThreshold, time threshold etc.).
 The data is saved in .mat files with names partially specified by the script running this program (as input program arguments).
*/

#include <qengine/qengine.h>
#include <DiscretizedControls.h>

#include <tuple>
#include <iostream>
#include "Tasks.h"
#include <chrono>
#include "qengine/qengine.h"
#include "Boilerplate.h"
#include "HelperMacros.h"
#include "OptimizerTypes.h"
#include "Systems.h"
#include "ControlFactories.h"
#include "RAII.h"
#include "armadillo"
#include "SeedingStrategies/RandomSineSeeding.h"
#include "SeedingStrategies/LinearSeeding.h"
#include "DifferentialEvolution.h"
#include "SeedingStrategies/RandomizedSineSeedingStrategy.h"
#include "SeedingStrategies/RandomSeeding.h"

#include <SGA_GPE.h>

typedef std::chrono::duration<double, std::ratio<3600> > hours;
typedef std::chrono::duration<double, std::ratio< 60> >  minutes;
typedef std::chrono::duration<double                   > seconds;
typedef std::chrono::duration<double, std::milli>        milliseconds;
typedef std::chrono::duration<double, std::micro>        microseconds;
using namespace qengine;

int main(int argc, char ** argv)
{
    arma::arma_rng::set_seed_random();
    sga::AccessOrder order = sga::AccessOrder::Random;
    constexpr size_t sgaBuckets = 40;
    const auto start = std::chrono::high_resolution_clock::now();
    std::string outputFileName  = "./seed_T.mat";
    int maxMinutes = 3;
    if (argc >= 3)
    {
        outputFileName = std::string(argv[1]);
        maxMinutes = std::stoi(argv[2]);
    }

    fprintf(stderr, "Output file name: %s\n", outputFileName.c_str());

    // Control
    const auto dt = 1e-3;
    const auto duration = 0.89;
    const auto n_steps = floor(duration / dt) + 1;
    const auto t = makeTimeControl(n_steps, dt);

    GPE_SHAKEUP(256);

    //  optimization parameters
    const size_t millisecondsMax = (size_t) 1e3 * 60 * maxMinutes;
    constexpr size_t maxIters = 1e2;
    constexpr double regularization = 1e-6;
    constexpr double fidThreshold = 0.999;

    const auto qThreshold    = 1e-14;
    constexpr double stepsizeThreshold = 1e-7;

    // seeding mechanisms
    auto makeSeeding = [] (const auto & t, const auto u_reference) 
    { 
        return RandomizedSineSeedingStrategy{t, u_reference, 0.01}; 
    };

    auto adjustSeeding = [=] (auto seeding)
    {
        // example use of the new seed generation mechanism
        seeding.SetSineBasisRandomized({{5, 85}});
        seeding.SetMaxAbsAmplitudeFixed({ {1}});
        seeding.SetAmplitudeDecayDistribution(CoeffDistribution::Uniform);
        seeding.SetMaxAbsHarmonicOffsetFixed({{0.5}});
        seeding.SetFluenceRandomized({{1, 1e2}});
        return seeding;
    };

    const auto produceSigmoid = [] (const auto x1, const auto x2, const auto y1, const auto y2)
    {
        const auto B = 1 / (x2 - x1) * log( ((1 - y1) / y1) * (y2 / (1 - y2)));
        const auto  A = (1 - y2) / y2 * exp(x2 * B);
        return [=] (const auto val)
        {
            return 1 / (1 + A * exp(- B * val)) ;
        };
    };

    auto u_reference = constructLinearControl(duration, dt, {initialControlPhysical.at(0)}, {finalControlPhysical.at(0)});

    // Construct the RS controls
    RandomSeedingStrategy rs(leftBoundary, rightBoundary, u_reference.size(), initialControl, finalControl, dt);
    Control u = rs();

    DataContainer dc;
    dc["u_prediscretized"] = u.mat();

    // Solver
    auto solver = makeFixedTimeStepper(H, psi_0, dt);

    // check the initial fidelity of our guess
    for(auto i = 1; i < u.size(); i++)
    {
        solver.step(u.get(i));

    }

    fprintf(stderr, "Forward propagation: Final fidelity for given u: %.15f\n", fidelity(solver.state(), psi_t));

    // Solver-backward
    auto solverb = makeFixedTimeStepper(H, psi_t, -dt);

    // check the initial fidelity of our guess
    for(int i = u.size() - 2; i >= 0; --i)
    {
        solverb.step(u.get(i));
    }    
    fprintf(stderr, "Backward propagation: Final fidelity for given u: %.15f\n", fidelity(solverb.state(), psi_0));


    // SGA requires us to 'cast' our control guess to the set of available, discretized controls
    Control tmp;
    sga::DiscretizedControlPack dcPack;
    try
    {
        // sga stuff - put outside the T loop on purpose (no need to recompute the caches)
        Control cacheDiscretized = makeDiscretizedControls( DiscreteControlInfo{ {makeDiscreteDimInfo(leftBoundaryPhysical, rightBoundaryPhysical, 128)} , dt}) ;
        cacheDiscretized = controlScaling.MapScale(cacheDiscretized, utility::ScalingType::FORWARD);

        dcPack = sga::makeDiscretizedControlPack(u, cacheDiscretized);
        u = dcPack.mDiscretizedReference;
    }
    catch (const std::exception & err)
    {
        std::cerr << err.what() << std::endl;
        exit(1);
    }

    // check the initial fidelity of our discretized guess
    {
        auto solver = makeFixedTimeStepper(H, psi_0, dt);

        // Propagation over control
        for(auto i = 0; i < n_steps; i++)
        {
            if(i < n_steps-1) solver.step(u.get(i+1));

        }
        fprintf(stderr, "Final fidelity for given u (discretized): %.15f\n", fidelity(solver.state(), psi_t));     
    }

    auto costFun = 
    [regularization] (const auto & lhs, const auto & rhs, const auto & u_in) 
    { 
        const double fidelity = qengine::fidelity(lhs, rhs);
        return 1.0 - fidelity;
    };


    auto startSGA = std::chrono::high_resolution_clock::now();
    auto elapsedMsTotalSGA = [&startSGA]()
    {
        auto e = std::chrono::high_resolution_clock::now() - startSGA;
        return std::chrono::duration_cast<milliseconds>(e);
    };    

    int stopCode = 0;
    auto stopper = [costFun, maxIters, fidThreshold, &stopCode, elapsedMsTotalSGA, millisecondsMax] (auto & alg) mutable
    { 
        if (alg.GetIter() > maxIters)
        {
            stopCode = 0;
            return true;
        }
        if (alg.GetFidelity() >= fidThreshold)
        {
            stopCode = 2;
            return true;
        }

        if (alg.GetDefaultStopCriterion()) 
        {        
            stopCode = 6; 
            return false;
        } 

        if(elapsedMsTotalSGA().count() > millisecondsMax)  
        {
            stopCode = 5 ; 
            std::cout << "walltime threshold exceeded. Stopping."  << std::endl; 
            return true;
        }
        return false;
    };

    std::vector<double> fidelityHistory;
    std::vector<double> costHistory;
    std::vector<double> regHistory;
    std::vector<size_t> evalsHistory;
    std::vector<size_t> timeMsHistory;

    std::vector<size_t> timeMsSGA_GlobalIterHistory;
    std::vector<double> fidelitySGA_GlobalIterHistory;    


    auto collectorSGA = [&fidelityHistory, &costHistory, regularization, &regHistory, &evalsHistory, &timeMsHistory, &timeMsSGA_GlobalIterHistory, &fidelitySGA_GlobalIterHistory, elapsedMsTotalSGA] 
    (auto & alg) mutable
    {
        switch(alg.GetIterState())
        {
            case sga::IterState::Global:
            {
                timeMsSGA_GlobalIterHistory.emplace_back( (size_t) elapsedMsTotalSGA().count() );
                fidelitySGA_GlobalIterHistory.emplace_back(alg.GetFidelity());                
                break;
            }
            case sga::IterState::Sub:
            {
                evalsHistory.emplace_back(alg.GetCostEvals());
                fidelityHistory.emplace_back(alg.GetFidelity());
                costHistory.emplace_back(alg.GetCost());
                timeMsHistory.emplace_back( (size_t) elapsedMsTotalSGA().count() );
                break;
            }
            default:
            {
                throw std::runtime_error("Invalid interation state of SGA.");
            }
        }
    };

    SGA_GPE sga(H, dcPack, psi_0, psi_t, stopper, collectorSGA, order, costFun);
    startSGA = std::chrono::high_resolution_clock::now();
    evalsHistory.emplace_back(sga.GetCostEvals());
    fidelityHistory.emplace_back(sga.GetFidelity());
    costHistory.emplace_back(sga.GetCost());
    timeMsHistory.emplace_back( (size_t) elapsedMsTotalSGA().count() );
    timeMsSGA_GlobalIterHistory.emplace_back((size_t) elapsedMsTotalSGA().count());
    fidelitySGA_GlobalIterHistory.emplace_back(sga.GetFidelity());

    try
    {
        sga.optimize();
        dc["ui"] = u.mat();
        dc["uf_sga"] = sga.GetOptimizedControl().mat();
        dc["dt"] = dt;
        dc["stopCode"] = stopCode;
        dc["Ff_sga"] = sga.GetFidelity();
        dc["Cf_sga"] = sga.GetCost();
        dc["Fi"] = sga.GetInitialFidelity();
        dc["iters_sga"] = sga.GetIter();
        dc["access_order_sga"] = static_cast<int> (sga.GetAccessOrder());

        RVec fidelityHistoryRVec(fidelityHistory.size());
        for (size_t i = 0 ; i < fidelityHistory.size() ; ++i)
        {
            fidelityHistoryRVec.at(i) = fidelityHistory.at(i);
        }
        dc["fidelity_history_sga"] = fidelityHistoryRVec;

        RVec costHistoryRVec(costHistory.size());
        for (size_t i = 0 ; i < costHistoryRVec.size() ; ++i)
        {
            costHistoryRVec.at(i) = costHistory.at(i);
        }
        dc["cost_history_sga"] = costHistoryRVec;  

        RVec evalsHistoryRVec(evalsHistory.size());
        for (size_t i = 0 ; i < evalsHistory.size() ; ++i)
        {
            evalsHistoryRVec.at(i) = evalsHistory.at(i);
        }
        dc["evals_history_sga"] = evalsHistoryRVec; 

        RVec timeMsHistoryRVec(timeMsHistory.size());
        for (size_t i = 0  ; i < timeMsHistory.size() ; ++i)
        {
            timeMsHistoryRVec.at( i ) = timeMsHistory.at( i );
        }                       
        dc["time_ms_history_sga"] = timeMsHistoryRVec;

        RVec timeMsSGA_GlobalIterHistoryRVec(timeMsSGA_GlobalIterHistory.size());
        for (size_t i = 0  ; i < timeMsSGA_GlobalIterHistory.size() ; ++i)
        {
            timeMsSGA_GlobalIterHistoryRVec.at( i ) = timeMsSGA_GlobalIterHistory.at( i );
        }                       
        dc["time_ms_history_global_iter_sga"] = timeMsSGA_GlobalIterHistoryRVec;    

        RVec fidelityHistoryGlobalIterRVec(fidelitySGA_GlobalIterHistory.size());
        for (size_t i = 0 ; i < fidelitySGA_GlobalIterHistory.size() ; ++i)
        {
            fidelityHistoryGlobalIterRVec.at(i) = fidelitySGA_GlobalIterHistory.at(i);
        }
        dc["fidelity_history_global_iter_sga"] = fidelityHistoryGlobalIterRVec;

    }
    catch (const std::exception & err)
    {
        std::cerr << "ERROR: " << err.what() << std::endl;
    }


    // BFGS stuff (for comparison with SGA)
    auto stepSizeFinder = makeInterpolatingStepSizeFinder(10, 1, 1e-8);

    auto makeProblem = [H{H},psi_0{psi_0},psi_t{psi_t},regularization, leftBoundary, rightBoundary, boundaryStrength](const auto& u_in)
    {
        return makeStateTransferProblem(H,psi_0,psi_t,u_in) + regularization*Regularization(u_in) + boundaryStrength * Boundaries(u_in, leftBoundary, rightBoundary);
    };   
 

    fidelityHistory.clear();
    costHistory.clear();
    regHistory.clear();
    evalsHistory.clear();
    timeMsHistory.clear();
    timeMsSGA_GlobalIterHistory.clear();
    fidelitySGA_GlobalIterHistory.clear();

    // take the guess, optimize it
    {
        auto problem = makeProblem(u);

        {
            auto printProblem = problem;
            printProblem.update(sga.GetOptimizedControl());
            fprintf(stderr, "SGA controls (according to makeProblem): F = %.9f\n", printProblem.fidelity());
        }

        auto startBFGS = std::chrono::high_resolution_clock::now();
        auto elapsedMsTotalBFGS = [&startBFGS]()
        {
            auto e = std::chrono::high_resolution_clock::now() - startBFGS;
            return std::chrono::duration_cast<milliseconds>(e);
        };    

        const auto stopper_bfgs = makeStopper([maxIters, &sga, stepsizeThreshold, fidThreshold, elapsedMsTotalBFGS, millisecondsMax](auto& opt) -> bool
        {
                bool stop = false;
                if (opt.iteration() == 1e4)                              {stop = true; std::cout << "Iteration threshold exceeded. Stopping." << std::endl; }
                
                if (opt.problem().fidelity() >= fidThreshold)            {stop = true; std::cout << "fidelity threshold exceeded. Stopping."  << std::endl; }
                 if (opt.stepSize() < stepsizeThreshold) {stop = true; std::cout << "stepsize threshold exceeded. Stopping."  << std::endl; } 

                if(elapsedMsTotalBFGS().count() > millisecondsMax)  
                {
                    stop = true;
                    std::cout << "walltime threshold exceeded. Stopping."  << std::endl; 
                }

                return stop;
        });          

        const auto collector = makeCollector( [&fidelityHistory, &costHistory, regularization, &regHistory, &evalsHistory, &timeMsHistory, elapsedMsTotalBFGS] 
            (auto& optimizer)
        {
            fidelityHistory.emplace_back(optimizer.problem().fidelity());
            costHistory.emplace_back(optimizer.problem().cost());
            // regHistory.emplace_back(  regularization * Regularization( alg.GetCurrentControl() ).cost() );
            timeMsHistory.emplace_back( (size_t) elapsedMsTotalBFGS().count() );

            auto n_steps = optimizer.problem().control().size();
            std::cout <<
                 "I "       << optimizer.iteration() << " | " <<
                 "F : " << optimizer.problem().fidelity() << "\t " <<
                 "Cost: " << optimizer.problem().cost() << "\t " <<
                 "step : " << optimizer.stepSize()   << "\t " <<
                 "fpp : "      << round(optimizer.problem().nPropagationSteps()/n_steps)   << "\t " <<
                 "bfgsRestart : " << optimizer.didRestart() << "\t " <<
                 "|g| : " << optimizer.problem().gradient().norm() << "\t " <<
            std::endl;

        });    

        const auto bfgsRestarter = makeStepTimesYBfgsRestarter(qThreshold, false);    

        auto opt = makeGrape_bfgs_L2(problem,stopper_bfgs,collector,stepSizeFinder,bfgsRestarter);
        startBFGS = std::chrono::high_resolution_clock::now();
        fidelityHistory.emplace_back(opt.problem().fidelity());
        costHistory.emplace_back(opt.problem().cost());
        // regHistory.emplace_back(  regularization * Regularization( alg.GetCurrentControl() ).cost() );
        timeMsHistory.emplace_back( (size_t) elapsedMsTotalBFGS().count() );    
        opt.optimize();
        {
            RVec fidelityHistoryRVec(fidelityHistory.size());
            for (size_t i = 0 ; i < fidelityHistory.size() ; ++i)
            {
                fidelityHistoryRVec.at(i) = fidelityHistory.at(i);
            }
            dc["fidelity_history_bfgs_l2"] = fidelityHistoryRVec;

            RVec costHistoryRVec(costHistory.size());
            for (size_t i = 0 ; i < costHistoryRVec.size() ; ++i)
            {
                costHistoryRVec.at(i) = costHistory.at(i);
            }
            dc["cost_history_bfgs_l2"] = costHistoryRVec;  

            RVec timeMsHistoryRVec(timeMsHistory.size());
            for (size_t i = 0  ; i < timeMsHistory.size() ; ++i)
            {
                timeMsHistoryRVec.at( i ) = timeMsHistory.at( i );
            }                       
            dc["time_ms_history_bfgs_l2"] = timeMsHistoryRVec;    
            dc["uf_bfgs_l2"] = opt.problem().control().mat();
            dc["Ff_bfgs_l2"] = opt.problem().fidelity();
            dc["iters_bfgs_l2"] = opt.iteration();
        }    
    }    

    {
        fidelityHistory.clear();
        costHistory.clear();
        evalsHistory.clear();
        timeMsHistory.clear();        
        timeMsSGA_GlobalIterHistory.clear();
        fidelitySGA_GlobalIterHistory.clear();

        auto collectorSGA = [&fidelityHistory, &costHistory, regularization, &regHistory, &evalsHistory, &timeMsHistory, &timeMsSGA_GlobalIterHistory, &fidelitySGA_GlobalIterHistory, elapsedMsTotalSGA] 
        (auto & alg) mutable
        {
            switch(alg.GetIterState())
            {
                case sga::IterState::Global:
                {
                    timeMsSGA_GlobalIterHistory.emplace_back( (size_t) elapsedMsTotalSGA().count() );
                    fidelitySGA_GlobalIterHistory.emplace_back(alg.GetFidelity()); 
                    break;
                }
                case sga::IterState::Sub:
                {
                    evalsHistory.emplace_back(alg.GetCostEvals());
                    fidelityHistory.emplace_back(alg.GetFidelity());
                    costHistory.emplace_back(alg.GetCost());
                    timeMsHistory.emplace_back( (size_t) elapsedMsTotalSGA().count() );
                    break;
                }
                default:
                {
                    throw std::runtime_error("Invalid interation state of SGA.");
                }
            }
        };

        SGA_GPE sga(H, dcPack, psi_0, psi_t, stopper, collectorSGA, order, costFun, sgaBuckets);
        startSGA = std::chrono::high_resolution_clock::now();
        evalsHistory.emplace_back(sga.GetCostEvals());
        fidelityHistory.emplace_back(sga.GetFidelity());
        costHistory.emplace_back(sga.GetCost());
        timeMsHistory.emplace_back( (size_t) elapsedMsTotalSGA().count() );
        timeMsSGA_GlobalIterHistory.emplace_back((size_t) elapsedMsTotalSGA().count());
        fidelitySGA_GlobalIterHistory.emplace_back(sga.GetFidelity());

        try
        {
            sga.optimize();
            dc["uf_sga_bucket"] = sga.GetOptimizedControl().mat();
            dc["dt"] = dt;
            dc["stopCode"] = stopCode;
            dc["Ff_sga_bucket"] = sga.GetFidelity();
            dc["Cf_sga_bucket"] = sga.GetCost();
            dc["Fi"] = sga.GetInitialFidelity();
            dc["iters_sga_bucket"] = sga.GetIter();
            dc["access_order_sga_bucket"] = static_cast<int> (sga.GetAccessOrder());

            RVec fidelityHistoryRVec(fidelityHistory.size());
            for (size_t i = 0 ; i < fidelityHistory.size() ; ++i)
            {
                fidelityHistoryRVec.at(i) = fidelityHistory.at(i);
            }
            dc["fidelity_history_sga_bucket"] = fidelityHistoryRVec;

            RVec costHistoryRVec(costHistory.size());
            for (size_t i = 0 ; i < costHistoryRVec.size() ; ++i)
            {
                costHistoryRVec.at(i) = costHistory.at(i);
            }
            dc["cost_history_sga_bucket"] = costHistoryRVec;  

            RVec evalsHistoryRVec(evalsHistory.size());
            for (size_t i = 0 ; i < evalsHistory.size() ; ++i)
            {
                evalsHistoryRVec.at(i) = evalsHistory.at(i);
            }
            dc["evals_history_sga_bucket"] = evalsHistoryRVec; 

            RVec timeMsHistoryRVec(timeMsHistory.size());
            for (size_t i = 0  ; i < timeMsHistory.size() ; ++i)
            {
                timeMsHistoryRVec.at( i ) = timeMsHistory.at( i );
            }                       
            dc["time_ms_history_sga_bucket"] = timeMsHistoryRVec;

            RVec timeMsSGA_GlobalIterHistoryRVec(timeMsSGA_GlobalIterHistory.size());
            for (size_t i = 0  ; i < timeMsSGA_GlobalIterHistory.size() ; ++i)
            {
                timeMsSGA_GlobalIterHistoryRVec.at( i ) = timeMsSGA_GlobalIterHistory.at( i );
            }                       
            dc["time_ms_history_global_iter_sga_bucket"] = timeMsSGA_GlobalIterHistoryRVec;  


            RVec fidelityHistoryGlobalIterRVec(fidelitySGA_GlobalIterHistory.size());
            for (size_t i = 0 ; i < fidelitySGA_GlobalIterHistory.size() ; ++i)
            {
                fidelityHistoryGlobalIterRVec.at(i) = fidelitySGA_GlobalIterHistory.at(i);
            }
            dc["fidelity_history_global_iter_sga_bucket"] = fidelityHistoryGlobalIterRVec;

        }
        catch (const std::exception & err)
        {
            std::cerr << "ERROR: " << err.what() << std::endl;
        }
    }

    dc.save( outputFileName );

    auto end = std::chrono::high_resolution_clock::now();
    std::cout << "---------------" << std::endl;
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "Program execution time: " << elapsed.count() << "s";
}
