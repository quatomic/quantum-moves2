#pragma once

#include <qengine/qengine.h>
#include <SequenceCache.h>

namespace qengine
{
	template <typename WaveFunction, typename Cache, typename Hamiltonian>
	class CachingExponentTimeStepper
	{
	public:

		CachingExponentTimeStepper(	const Cache & cache, 
									const WaveFunction & psi,
									const Hamiltonian & H) : 	
													mCache( cache ),
													mState(psi.vec()),
													mHamiltonian(H) {}

		CachingExponentTimeStepper(const CachingExponentTimeStepper & other) : 	mCache(other.mCache), 
																				mState(other.mState), 
																				mHamiltonian(other.mHamiltonian) {}

		WaveFunction state() const 
		{
			return makeFunctionOfX(mHamiltonian._H0()._serializedHilbertSpace(), mState);
		}

		void step(const size_t cacheKey)
		{
			mState = mCache.at(cacheKey) * mState;	
		}

		void resetState(const WaveFunction & psi)
		{
			mState = psi.vec();
		}

	private:
		Cache mCache;

		qengine::Vector<complex> mState;
		Hamiltonian mHamiltonian;
	};


	template <typename WaveFunction, typename Cache, typename Hamiltonian>
	class SCETS
	{

	public:
		SCETS(const Cache & cacheInit, const WaveFunction & psi, const Hamiltonian & H): 	mCache(cacheInit), 
																							mState(psi), 
																							mHamiltonian(H) { }

		WaveFunction state() const
		{
			return makeFunctionOfX(mHamiltonian._H0()._serializedHilbertSpace(), mState);
		}

		void step(const size_t controlIndex, const size_t subSteps)
		{
			mState = mCache.at( std::make_pair(controlIndex, subSteps) ) * mState;
		}

		void resetState(const WaveFunction & psi)
		{
			mState = psi.vec();
		}

	private:
		Cache mCache;
		qengine::Vector<complex> mState;
		Hamiltonian mHamiltonian;
	};


	template <typename Sequence, typename Hamiltonian>
	auto makeDirectExponentCache(	const Sequence & seq, 
									const Hamiltonian & H, 
									const double dt)
	{
		using namespace std::complex_literals;
		auto foo = [H, dt] (const auto & param) -> auto
		{
			auto Hx = H(param);
			return exp(-1i * dt * Hx._hamiltonianMatrix());
		};
		return SequenceCache<Sequence, decltype(foo)> (seq, foo);
	}

	template <typename Control, typename Hamiltonian>
	auto makeDEMCache(	const Control & seq, const Hamiltonian & H, 
						const double dt, std::vector<size_t> multipliers)
	{
		assert( !multipliers.empty() );
		std::sort(multipliers.begin(), multipliers.end());

		using namespace std::complex_literals;
		auto foo = [H, dt] ( const auto & param) -> auto
		{
			auto Hx = H(param);
			return exp(-1i * dt * Hx._hamiltonianMatrix());
		};

		using MatrixType = decltype(std::declval<decltype(foo)>()());
		std::unordered_map<std::pair<size_t, size_t>, MatrixType> cacheMap;

		for (size_t ctrlIndex = 0 ; ctrlIndex < seq.size() ; ++ctrlIndex)
		{
			auto exponent = foo( seq.at(ctrlIndex) );
			size_t mulDone = 0;
			for (const auto mulNum : multipliers)
			{
				for (; mulDone < mulNum ; ++mulDone)
				{
					exponent = exponent * exponent;
				}
				cacheMap.emplace(std::make_pair( std::make_pair(ctrlIndex, mulNum), exponent));
			}
		}
		return cacheMap; 
	}


	template <typename WaveFunction, typename Sequence, typename Hamiltonian>
	auto makeDirectExponentCachingTimeStepper(	const Sequence & seq, 
												const Hamiltonian & H, 
												const WaveFunction & psi, 
												const double dt)
	{
		auto cache = makeDirectExponentCache(seq, H, dt);
		return CachingExponentTimeStepper<WaveFunction, decltype(cache), Hamiltonian> (cache, psi, H);
	}
}