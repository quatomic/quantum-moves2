#pragma once
#include <qengine/qengine.h>
#include <armadillo>

using namespace qengine;

inline void ImposeBoundaries(Control & ctrl, const RVec leftlower, const RVec rightUpper)
{
    auto & ctrlMat = ctrl.mat();
    for (size_t i = 0 ; i < ctrlMat.n_rows() ; ++i)
    {
        for (size_t j = 0 ; j < ctrlMat.n_cols() ; ++j)
        {
            if (ctrlMat.at(i, j) < leftlower.at( i ))
            {
                ctrlMat.at(i, j) = leftlower.at( i );   
            }
            else if (ctrlMat.at(i, j) > rightUpper.at( i ))
            {
                ctrlMat.at(i, j) = rightUpper.at( i );   
            }            
        }
    }
}

enum class Distribution {Normal, Uniform};

auto constructLinearControl(const count_t n_steps, const real dt, const RVec& initialParam,const RVec& finalParam)
{
    std::vector<Control> controls;

    for(count_t i=0; i < initialParam.size(); ++i)
    {
        controls.emplace_back(makeLinearControl(initialParam.at(i),finalParam.at(i),n_steps,dt));
    }
    const auto u = makeControl(controls);

    return u;
}

auto constructLinearControl(const real duration,const real dt, const RVec& initialParam,const RVec& finalParam)
{
    const count_t n_steps = floor(duration/dt) + 1;
    return constructLinearControl(n_steps,dt,initialParam,finalParam);
}


/// control decomposition onto basis
auto decomposeControlOnBasis(const Control& u,const Basis& b)
{
    auto N = b.controlSize();
    auto M = b._controls().size();
    auto nParam = u.paramCount();

    // perform least squares fit for the coefficients
    arma::mat coefficients(nParam,M);
    for(auto q = 0; q < nParam ; q++)
    {
        arma::mat fs(N,M,arma::fill::zeros);

        for(auto m = 0; m < M ; m++)
        {
            fs.col(m) = arma::trans(b._controls().at(m).mat()._mat().row(q));
        }
        arma::vec coefficients_q = arma::inv(arma::trans(fs)*fs)*arma::trans(fs)*arma::trans(u.mat()._mat().row(q));
        coefficients.row(q) = arma::trans(coefficients_q);
    }
    BasisControl basisCoefficients(coefficients);

    Control u_fit = basisCoefficients*b;

    // residual of the least square is put into u0
    Control u0 = u - u_fit;

    return std::make_tuple(u0,basisCoefficients);
}



auto extendControlByLinearTail(const Control& u, const count_t nTailSteps,const RVec& endParam)
{
    if(nTailSteps == 0) return u;

    auto uTail = constructLinearControl(nTailSteps,u.dt(),u.getBack(),endParam);
    auto uExtended = u;
    return uExtended.glue(uTail);
}


auto extendControlByConstantTail(const Control& u, const count_t nTailSteps)
{
    if(nTailSteps == 0) return u;

    auto uTail = constructLinearControl(nTailSteps,u.dt(),u.getBack(),u.getBack());
    auto uExtended = u;
    return uExtended.glue(uTail);
}

RMat makeRandomizedCoefficients(const count_t nBasisElements, const count_t paramCount, const real maxRandVal)
{
    arma::arma_rng::set_seed_random();
    auto armamat = arma::mat((arma::randu(paramCount, nBasisElements) - 0.5)*2*maxRandVal);
    return RMat(std::move(armamat));
}

RMat makeGaussRandomizedCoefficients(const count_t nBasisElements, const count_t paramCount, const real maxRandVal, const real distAverage = 0.0, const real distVariance = 1.0)
{
    arma::arma_rng::set_seed_random();
    auto armamat = arma::mat((arma::randu(paramCount, nBasisElements) - 0.5)*2 * maxRandVal);
    const auto pi = 3.141592653589793238462643;
    const auto dist = [distAverage, distVariance, pi] (const auto x) -> double
    {
        return exp(-std::pow(x - distAverage, 2)/(2 * std::pow(distVariance, 2))) / (std::sqrt(2 * pi) * distVariance); 
    };

    const real renormalizer = 1 / dist(0);

    for (size_t row = 0 ; row < paramCount ; ++row)
    {
        for (size_t col = 0 ; col < nBasisElements ; ++col)
        {
            armamat.at(row, col) = armamat.at(row, col) * dist( col ) * renormalizer; // i.e. current_value * dist(basis_element_number)
        }
    }
    return RMat(std::move(armamat));
}


Control makeNewSigmoidShapeFunction(const Control& t, const real a = 0.03, const real S1 = 0.001, const real S2 = 1 - 0.001)
{
    // Creates a sigmoid shape function that is initially satisfies:
    // S(0) = S1        (is later set to exactly zero)
    // S(a*T) = S2

    const real duration = (t.getBack().at(0)-t.getFront().at(0));
    const real q = std::log(1.0/S1 - 1.0);
    const real k = -1.0/(a*duration)*(std::log(1.0/S2 -1.0)  - q);
    auto shapeFunction = 1.0 / (1.0 + qengine::exp(-(k*t - q)));
    shapeFunction.at(0) = 0.0;
    return shapeFunction * invert(shapeFunction);
}

auto makeTimeControlWithFixedEnd(real from,real to,real dt)
{
    auto n_steps = floor((to-from)/dt) + 1;
    auto t = makeTimeControl(n_steps,dt,from);
    if(t.getBack().at(0) < to) t.append(Control(RVec{to},1,dt));
    return t;
}

RVec linearlySpacedVector(real from, real to,real increment)
{
    std::vector<real> vec;

    real current = from;
    while(current <= to)
    {
        vec.emplace_back(current);
        current = current + increment;
    }
    return RVec(vec);
}
