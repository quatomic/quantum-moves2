
#pragma once

#include <vector>
#include <algorithm>

#include <qengine/qengine.h>

namespace qengine
{
	struct DiscreteDimInfo;
	struct DiscreteControlInfo;
	
	namespace sga
	{
		using DCPair = std::pair<double, double>;
		std::vector<RVec> appendDiscreteDimension(const std::vector<RVec> & input, const DiscreteDimInfo & newDim);
	}

	struct DiscreteDimInfo
	{	
		DiscreteDimInfo(const sga::DCPair range, const double spacing) : mRange(range), mSpacing(spacing) {}
		const sga::DCPair mRange;
		const double mSpacing;
	};


	DiscreteDimInfo makeDiscreteDimInfo(const double left, const double right, const size_t points)
	{
		qengine_assert(points && left <= right, "Invalid information for making discrete dimension info.");
		const double spacing = (right - left) / points;
		return DiscreteDimInfo( sga::DCPair{left, right}, spacing);
	}

	struct DiscreteControlInfo
	{
		DiscreteControlInfo(const std::vector<DiscreteDimInfo> & infos, const double dt) : mInfos(infos), mDt(dt) {}
		const std::vector<DiscreteDimInfo> mInfos;
		const double mDt;
	};


	Control makeDiscretizedControls(const DiscreteControlInfo & info)
	{
		std::vector<Control> tmpOut;
		tmpOut.reserve( info.mInfos.size() );
		std::vector<RVec> tmp;
		for (size_t i = 0 ; i < info.mInfos.size() ; ++i)
		{
			tmp = std::move( sga::appendDiscreteDimension(tmp, info.mInfos.at(i)) );
		}

		if (tmp.empty())
		{
			return {};
		}

		// sizes sanity check
		const size_t firstSize = tmp.at(0).size();
		if ( !std::all_of(tmp.cbegin(), tmp.cend(), [firstSize] (const auto & item) { return item.size() == firstSize; }))
		{
			throw std::runtime_error("Not all of discretized control points have the same dimensionality. ");
		} 

		Control output = Control::zeros(tmp.size(), firstSize, info.mDt);
		for (size_t i = 0 ; i < tmp.size() ; ++i)
		{
			output.set(i,  tmp.at(i));
		}
		return output;
	}

	namespace sga
	{
		std::vector<RVec> appendDiscreteDimension(const std::vector<RVec> & input, const DiscreteDimInfo & newDim)
		{
			qengine_assert(newDim.mSpacing > 0, "Invalid spacing for creating discretized controls");
			qengine_assert( newDim.mRange.second >= newDim.mRange.first, "Invalid ranges for creating discretized controls");
			qengine_assert( (newDim.mRange.second - newDim.mRange.first) >= newDim.mSpacing, "Invalid scale of spacing with relation to the dimension range of values.");
			const size_t newDimPoints = (newDim.mRange.second - newDim.mRange.first) / newDim.mSpacing;
			std::vector<RVec> output;
			output.reserve(input.size() * newDimPoints);
			if (input.empty())
			{
				for (size_t newDimValIndex = 0 ; newDimValIndex < newDimPoints ; ++newDimValIndex)
				{
					const double newDimVal = newDim.mRange.first + newDimValIndex * newDim.mSpacing;
					qengine_assert(newDimVal <= newDim.mRange.second, "Invalid point in constructing discretized dimensions");
					output.emplace_back( RVec(1, newDimVal) );
				}
			}
			else
			{			
				for (size_t newDimValIndex = 0; newDimValIndex < newDimPoints; ++newDimValIndex)
				{
					const double newDimVal = newDim.mRange.first + newDimValIndex * newDim.mSpacing;
					qengine_assert(newDimVal <= newDim.mRange.second, "Invalid point in constructing discretized dimensions");
					for (const auto & point : input)
					{
						RVec newPoint(point.size() + 1);
						for (size_t i = 0; i < point.size(); ++i)
						{
							newPoint.at(i) = point.at(i);
						}
						newPoint.at(point.size()) = newDim.mRange.first + newDimVal * newDimValIndex;
						output.emplace_back(std::move(newPoint));
					}
				}
			}
			return output;
		}

		Control ApplyDiscretizedControl(const Control & ref, const Control & discretized)
		{
			qengine_assert(discretized.size(), "Discretized controls are empty for ApplyDiscretizedControl.");
			Control output = Control::zeros(ref.size(), ref.paramCount(), ref.dt());
			const double dt = ref.dt();
			output.set(0, ref.getFront());
			output.set(output.size() - 1, ref.getBack());
			for (size_t i = 1 ; i < output.size() - 1 ; ++i)
			{
				const RVec currentCtrl = ref.get( i );
				RVec bestDisc = discretized.get( 0 );
				double bestDistance = 1e16;
				for (size_t trialInd = 0 ; trialInd < discretized.size() ; ++trialInd)
				{
					const RVec currentDisc = discretized.get( trialInd );
					const double dist = (currentDisc - currentCtrl).norm();
					if (dist < bestDistance)
					{
						bestDistance = dist;
						bestDisc = currentDisc;
					}
				}
				output.set(i, bestDisc);
			}
			output.setDt( ref.dt() );
			return output;
		}

		struct DiscretizedControlPack
		{
			Control mReference;
			Control mDiscretizedReference;
			Control mAvailableValues;
			std::vector<size_t> mIndexOrdering;
		};

		DiscretizedControlPack makeDiscretizedControlPack(const Control & ref, const Control & discretized)
		{
			qengine_assert(discretized.size(), "Discretized controls are empty for ApplyDiscretizedControl.");
			Control output = Control::zeros(ref.size(), ref.paramCount(), ref.dt());
			std::vector<size_t> indexOrdering;
			indexOrdering.reserve(ref.size());
			for (size_t i = 0 ; i < ref.size() ; ++i)
			{
				indexOrdering.push_back( 0 );
			}

			const double dt = ref.dt();
			output.set(0, ref.getFront());
			output.set(output.size() - 1, ref.getBack());
			for (size_t i = 1 ; i < output.size() - 1 ; ++i)
			{
				const RVec currentCtrl = ref.get( i );
				RVec bestDisc = discretized.get( 0 );
				double bestDistance = 1e16;
				size_t bestIndex = 0;
				for (size_t trialInd = 0 ; trialInd < discretized.size() ; ++trialInd)
				{
					const RVec currentDisc = discretized.get( trialInd );
					const double dist = (currentDisc - currentCtrl).norm();
					if (dist < bestDistance)
					{
						bestDistance = dist;
						bestDisc = currentDisc;
						bestIndex = trialInd;
					}
				}
				output.set(i, bestDisc);
				indexOrdering.at(i) = bestIndex;

			}
			output.setDt( ref.dt() );
			return {ref, output, discretized, indexOrdering};
		}
	}
}