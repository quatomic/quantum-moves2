#pragma once

// RETURN LISTS

#define GPE_BHW_SETUP_RETURN_LIST \
initialControl,finalControl,width,controlScaling,initialControlPhysical,finalControlPhysical,xLimitsPhysical,yLimitsPhysical

#define GPE_BHW_RETURN_LIST \
systemType,s,x,V_dynamic,V_static,H,psi_0,psi_t,staticTweezerParams,leftBoundaryPhysical,leftBoundary,rightBoundary,rightBoundaryPhysical,boundaryStrength

#define BHW_SETUP_RETURN_LIST \
initialControl,finalControl,width,controlScaling,initialControlPhysical,finalControlPhysical,xLimitsPhysical,yLimitsPhysical

#define BHW_RETURN_LIST \
systemType,s,x,V_dynamic,V_static,H,psi_0,psi_t,staticTweezerParams,leftBoundaryPhysical,leftBoundary,rightBoundary,rightBoundaryPhysical,boundaryStrength

#define GPE_SHAKEUP_SETUP_RETURN_LIST \
Vext_maker,initialControl,finalControl,initialControlPhysical,finalControlPhysical,controlScaling

#define GPE_SHAKEUP_RETURN_LIST \
systemType,s,x,V,H,psi_0,psi_t,leftBoundaryPhysical,leftBoundary,rightBoundary,rightBoundaryPhysical,boundaryStrength

#define GPE_SPLIT_SETUP_RETURN_LIST \
Vext_maker,initialControl,finalControl,controlScaling,initialControlPhysical,finalControlPhysical

#define GPE_SPLIT_RETURN_LIST \
systemType,V,H,psi_0,psi_t,x,s,leftBoundaryPhysical,leftBoundary,rightBoundary,rightBoundaryPhysical,boundaryStrength

#define OPTIMIZER_MAKER_RETURN_LIST \
GRAPEMaker, 0


// SETUP

#define GPE_BHW_SETUP \
auto setupTuple = gpe_bhw_setup(); \
auto & [GPE_BHW_SETUP_RETURN_LIST] = setupTuple;

#define GPE_BHW(dim) \
GPE_BHW_SETUP; \
auto bhwTuple = gpe_bhw(setupTuple, dim); \
auto & [GPE_BHW_RETURN_LIST] = bhwTuple; \
auto optimizerMakers = makeOptimizerMakers(H,psi_0,psi_t); \
auto& [OPTIMIZER_MAKER_RETURN_LIST] = optimizerMakers;

#define BHW_SETUP \
auto setupTuple = bhw_setup(); \
auto & [GPE_BHW_SETUP_RETURN_LIST] = setupTuple;

#define BHW(dim) \
BHW_SETUP; \
auto bhwTuple = bhw(setupTuple, dim); \
auto & [BHW_RETURN_LIST] = bhwTuple; \
auto optimizerMakers = makeOptimizerMakers(H,psi_0,psi_t); \
auto& [OPTIMIZER_MAKER_RETURN_LIST] = optimizerMakers;

#define GPE_SHAKEUP_Setup \
auto setupTuple = gpe_shakeup_setup(); \
auto & [GPE_SHAKEUP_SETUP_RETURN_LIST] = setupTuple;

#define GPE_SHAKEUP(dim) \
GPE_SHAKEUP_Setup \
auto gpeTuple = gpe_shakeup(setupTuple, dim); \
auto & [GPE_SHAKEUP_RETURN_LIST] = gpeTuple;  \
auto optimizerMakers = makeOptimizerMakers(H, psi_0, psi_t); \
auto & [OPTIMIZER_MAKER_RETURN_LIST] = optimizerMakers;

#define GPE_SPLIT_Setup \
auto setupTuple = gpe_split_setup(); \
auto & [GPE_SPLIT_SETUP_RETURN_LIST] = setupTuple;

#define GPE_SPLIT(dim) \
GPE_SPLIT_Setup \
auto gpeTuple = gpe_split(setupTuple, dim); \
auto & [GPE_SPLIT_RETURN_LIST] = gpeTuple;  \
auto optimizerMakers = makeOptimizerMakers(H, psi_0, psi_t); \
auto & [OPTIMIZER_MAKER_RETURN_LIST] = optimizerMakers;


// VARIABLE LABELS

namespace qengine
{
    // This macro takes an argument 'label' and constructs
    // 1) the struct that wraps a variable of type T and defines conversion operators from the struct to the wrapped type
    // 2) a make function for the struct (for type deduction)
    // Then one obtains syntactic sugar for passing parameters into functions.

    #define MAKE_VARIABLE_LABEL(label) \
        template<class T> \
        struct VariableLabel_##label \
        { \
            VariableLabel_##label(const T& var) : var_(var) {}; \
            VariableLabel_##label(const T&& var) : var_(std::move(var)) {}; \
            operator T() const{return var_;}; \
            operator T&() {return var_;}; \
            T var_; \
        };\
        \
        template<class T> \
        auto label(T&& var) \
        { \
            return VariableLabel_##label<T>(std::forward<T>(var)); \
        }

    MAKE_VARIABLE_LABEL(Dim);
    MAKE_VARIABLE_LABEL(Dt);
    MAKE_VARIABLE_LABEL(ArtificialCouplingStrength);
    MAKE_VARIABLE_LABEL(NEigenStates);
    MAKE_VARIABLE_LABEL(Duration);
    MAKE_VARIABLE_LABEL(SeedNumber);
    MAKE_VARIABLE_LABEL(SeedData)
    MAKE_VARIABLE_LABEL(SeedMetaData)
    MAKE_VARIABLE_LABEL(RecordInterval)
    MAKE_VARIABLE_LABEL(AdditionalRecording)

}

// FUNCITON BODIES
#define Playback(recordInterval,additionalRecording) \
auto ui = Control(seeddata.getRMat("ui"),dt);\
auto uf = Control(seeddata.getRMat("uf"),dt);\
DataContainer dc;\
\
const auto n_steps = uf.size();\
const auto constSteps = n_steps/5;\
uf = extendControlByConstantTail(uf,constSteps);\
auto t = makeTimeControl(n_steps,dt);\
t = extendControlByLinearTail(t,constSteps,t.getBack() + constSteps*dt);\
auto propagator = makeFixedTimeStepper(H,psi_0,dt);\
\
dc["psi_t"] = psi_t.absSquare().vec();\
dc["seed"] = seedMatPath;\
dc["dim"] = dim;\
\
for(int i = 0; i < uf.size() ; ++i)\
{\
    if(i%recordInterval == 0)\
    {\
        dc["x"] = x.vec();\
        dc["u"].append(uf.get(i));\
        dc["t"].append(t.get(i));\
        dc["w"].append(propagator.state().absSquare().vec());\
        dc["V"].append(Vext(uf.get(i)).vec());\
        dc["F"].append(fidelity(propagator.state(),psi_t));\
        additionalRecording(i,dc,uf);\
    }\
    if(i < n_steps-1) propagator.step(uf.get(i+1));\
}\

