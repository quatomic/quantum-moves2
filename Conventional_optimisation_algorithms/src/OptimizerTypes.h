#pragma once
#include "Utilities.h"
#include "qengine/qengine.h"
#include <tuple>

namespace qengine
{
    /// http://www.cplusplus.com/forum/general/223816/
    ///  NEED FOR WRAPPING LAMBDAS
    namespace detail // EDIT: make it an implementation detail
    {
        template < typename T > struct deduce_type ;

        template < typename RETURN_TYPE, typename CLASS_TYPE, typename... ARGS >
        struct deduce_type< RETURN_TYPE(CLASS_TYPE::*)(ARGS...) const >
        { using type = std::function< RETURN_TYPE(ARGS...) > ; };
    }

    template < typename CLOSURE > auto wrap( const CLOSURE& fn ) // EDIT: give it a better name
    { return typename detail::deduce_type< decltype( &CLOSURE::operator() ) >::type(fn) ; }
    ///



    template<class Hamiltonian, class WaveFunction>
    inline decltype(auto) makeOptimizerMakers(Hamiltonian& H, WaveFunction& psi_0,WaveFunction& psi_t)
    {

        auto GRAPEMaker = [H, psi_0, psi_t](Control u, int maxIteration)
        {
            auto problem  = makeStateTransferProblem(H,psi_0,psi_t,u) + 1e-6*Regularization(u);

            const auto stopper =
                    makeIterationStopper(maxIteration) +
                    makeFidelityStopper(0.999) +
                    makeStepsizeStopper(1e-7);

            const auto collector = makeCollector([recordInterval{50}](auto& optimizer)
            {
                    if(optimizer.iteration()%recordInterval == 0)
            {
                    auto n_steps = optimizer.problem().control().size();
                    std::cout <<
                         "ITER "       << optimizer.iteration() << " | " <<
                         "fidelity : " << optimizer.problem().fidelity() << "\t " <<
                         "stepSize : " << optimizer.stepSize()   << "\t " <<
                         "fpp : "      << round(optimizer.problem().nPropagationSteps()/n_steps)   << "\t " <<
                    std::endl;
            }
            });

            const auto stepSizeFinder = makeInterpolatingStepSizeFinder(10.0,1.0);

            auto GRAPE = makeGrape_bfgs_L2(problem,stopper,collector,stepSizeFinder);
            collector(GRAPE);
            return GRAPE;
        };

        return std::make_tuple(GRAPEMaker, 0);
    };
}
