#pragma once
#include "qengine/qengine.h"

using namespace qengine;

template<class Propagator, class Collector>
auto Propagate(Propagator& propagator, Control u, Collector& collector)
{
    for(int i = 0; i < u.size() ; ++i)
    {
        collector(i);
        if(i < u.size()-1) propagator.step(u.get(i+1));
    }
}
