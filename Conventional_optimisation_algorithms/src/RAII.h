#pragma once
#include <exception>

template<class F1,class F2>
class RAII
{
public:
    RAII(const F1& f1, const F2& f2) : try_(f1), finally_(f2)
    {};

    void launch()
    {
        try_()
    ;}

    ~RAII()
    {
        finally_();
    };

    F1 try_;
    F2 finally_;
};

template<class F1,class F2>
auto makeRAII(const F1& f1, const F2& f2)
{
    return RAII<F1,F2>(f1,f2);
}
