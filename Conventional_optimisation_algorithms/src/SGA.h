	#pragma once

	#include <vector>
	#include <unordered_map>
	#include <chrono>
	#include <random>
	#include <type_traits>

	#include <future>
	#include <iterator>

	#include <qengine/qengine.h>
	#include <WaveFunctionCache.h>


	#include "SeedingStrategies/RandomizedSineSeedingStrategy.h"

	#include <SequenceCache.h>
	#include <CachingDirectExpTimeStepper.h>
	#include <DiscretizedControls.h>

	#include "SGA_helpers.h"

	namespace qengine
	{

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
		class SGA
		{
		public:

		SGA (	const Hamiltonian & H, 
				const sga::DiscretizedControlPack & dcPack,
				const WaveFunction & initialState, 
				const WaveFunction & targetState, 
				Stopper & stopper, 
				Collector & collector, 
				const sga::AccessOrder order, 
				const CostFunction & cost, 
				const CachingStepper & leftStepper, 
				const CachingStepper & rightStepper)  : 	
																								mHamiltonian(H), 
																								mInitialControl(dcPack.mDiscretizedReference), 
																								mInitialState(initialState), 
																								mTargetState(targetState), 
																								mStopper(stopper),
																								mAccessOrder(order), 
																								mCost(cost), 
																								mDt(dcPack.mDiscretizedReference.dt()), 
																								mCurrentControl(dcPack.mDiscretizedReference), 
																								mDiscretizedAvailableControls(dcPack.mAvailableValues), 
																								mLeftWaveFunctionCache(wfcache::Ordering::Forward), 
																								mDiscretizedIndexOrdering(dcPack.mIndexOrdering),
																								mRightWaveFunctionCache(wfcache::Ordering::Backward), 
																								mGen( std::chrono::system_clock::now().time_since_epoch().count() ),
																								mCollector( collector ),
																								mLeftStepper( qengine::makeFixedTimeStepper(mHamiltonian, mInitialState, mDt) ),
																								mRightStepper( qengine::makeFixedTimeStepper(mHamiltonian, mTargetState, -mDt) ),
																								mLeftWaveFunctionCacheStepper(leftStepper),
																								mRightWaveFunctionCacheStepper(rightStepper),
																								mIndexType( sga::IndexType::Singular )

		{ 
			CheckDiscretizedControls();
			ConstructAccessOrder();
			mLeftWaveFunctionCache.UpdateCache(0, mInitialState);
			mRightWaveFunctionCache.UpdateCache(mInitialControl.size() - 1, mTargetState);
		}

		SGA (	const Hamiltonian & H, 
				const sga::DiscretizedControlPack & dcPack,
				const WaveFunction & initialState, 
				const WaveFunction & targetState, 
				Stopper & stopper, 
				Collector & collector, 
				const sga::AccessOrder order, 
				const CostFunction & cost, 
				const CachingStepper & leftStepper, 
				const CachingStepper & rightStepper,
				const size_t numOfBuckets)  : 	
																								mHamiltonian(H), 
																								mInitialControl(dcPack.mDiscretizedReference), 
																								mInitialState(initialState), 
																								mTargetState(targetState), 
																								mStopper(stopper),
																								mAccessOrder(order), 
																								mCost(cost), 
																								mDt(dcPack.mDiscretizedReference.dt()), 
																								mCurrentControl(dcPack.mDiscretizedReference), 
																								mDiscretizedAvailableControls(dcPack.mAvailableValues), 
																								mLeftWaveFunctionCache(wfcache::Ordering::Forward), 
																								mDiscretizedIndexOrdering(dcPack.mIndexOrdering),
																								mRightWaveFunctionCache(wfcache::Ordering::Backward), 
																								mGen( std::chrono::system_clock::now().time_since_epoch().count() ),
																								mCollector( collector ),
																								mLeftStepper( qengine::makeFixedTimeStepper(mHamiltonian, mInitialState, mDt) ),
																								mRightStepper( qengine::makeFixedTimeStepper(mHamiltonian, mTargetState, -mDt) ),
																								mLeftWaveFunctionCacheStepper(leftStepper),
																								mRightWaveFunctionCacheStepper(rightStepper),
																								mIndexType( sga::IndexType::Bucket ),
																								mBucketNumber( numOfBuckets )
		{ 
			CheckDiscretizedControls();
			mIndexBuckets = ConstructBuckets(mInitialControl.size(), mBucketNumber);
			ConstructAccessOrderBuckets();
			BucketizeControls();
			mLeftWaveFunctionCache.UpdateCache(0, mInitialState);
			mRightWaveFunctionCache.UpdateCache(mInitialControl.size() - 1, mTargetState);
		}

		Control GetOptimizedControl();

		Control GetInitialControl()
		{
			return mInitialControl;
		}

		double GetFidelity();

		double GetCost();

		size_t GetCostEvals() const noexcept;

		double GetInitialFidelity() const;

		sga::IterState GetIterState() const noexcept;

		sga::AccessOrder GetAccessOrder() const;
		
		bool GetDefaultStopCriterion() const;

		void optimize();

		size_t GetIter() const;

		size_t iteration() const
		{
			return GetIter();
		}

		std::vector<size_t> GetAccessOrderInd() const;
		
		Control GetCurrentControl() const;

	 	virtual ~SGA() {}

	 	
		private:

		bool OptimizerIter();

		bool OptimizerSubiter(const size_t controlIndex);

		bool OptimizerSubiterBucket(const size_t controlIndex);

		bool CheckStopConditions();

		void ConstructAccessOrder();


	    std::vector<std::pair<size_t, size_t>> ConstructBuckets(const size_t pointsNumDefault, const size_t bucketNum)
	    {
	    	qengine_assert(pointsNumDefault > bucketNum, "Invalid number of buckets.");
	        const int pointsInBuckets = pointsNumDefault - 2; // subtract initial and final control, as they are not influenced by the optimization

			double pointsPerBucketRaw = (double) pointsInBuckets / (double) bucketNum;

	        std::vector<std::pair<size_t, size_t>> bucketIndexRanges;


			int pointsPerBucket = floor(pointsPerBucketRaw);

			
			for (size_t bucketIndex = 0; bucketIndex < bucketNum - 1; ++bucketIndex)
			{
				size_t num1 = 1 + bucketIndex * pointsPerBucket;
				size_t num2 = num1 + pointsPerBucket;
				bucketIndexRanges.emplace_back(std::make_pair(num1, num2));
			}

			const size_t lastInd = 1 + (bucketNum - 1) * pointsPerBucket;
			bucketIndexRanges.emplace_back(std::make_pair(lastInd, pointsNumDefault - 1));
			return bucketIndexRanges;
	    }

	    void ConstructAccessOrderBuckets()
		{
			mAccessOrderInd.reserve(mIndexBuckets.size());
			for (size_t i = 0 ; i < mIndexBuckets.size()  ; ++i) 
			{
				mAccessOrderInd.emplace_back( i );
			}

			switch (mAccessOrder)
			{
				case sga::AccessOrder::Forward:
				{
					// starts out sorted in ascending order (constructor)
					break;
				}
				case sga::AccessOrder::Backward:
				{
					std::sort(mAccessOrderInd.begin(), mAccessOrderInd.end(), [] (const auto lhs, const auto rhs) { return lhs > rhs; });
					break;
				}
				case sga::AccessOrder::Random:
				{
					std::shuffle(mAccessOrderInd.begin(), mAccessOrderInd.end(), mGen);				
					break;
				}
			}
		}	

		void BucketizeControls()
		{
			for (const auto range : mIndexBuckets)
			{
				const size_t start = range.first;
				const size_t stop = range.second;
				for (size_t i = start ; i < stop ; ++i)
				{
					mInitialControl.set(i, mInitialControl.get(start));
					mDiscretizedIndexOrdering.at(i) = mDiscretizedIndexOrdering.at(start);
				}
			}
			mCurrentControl = mInitialControl;
		}

		void RemakeAccessOrder();

		void CheckDiscretizedControls();


		// propagation / cache retrieval
		WaveFunction GetWaveFunctionLeft(const size_t index);

		WaveFunction GetWaveFunctionRight(const size_t index);	

		bool CheckCacheAvailableLeft(const size_t index);

		bool CheckCacheAvailableRight(const size_t index);

		WaveFunction GetCachedResultLeft(const size_t index);

		WaveFunction GetCachedResultRight(const size_t index);

		WaveFunction PropagateLeftFrom(const size_t index, const WaveFunction & ref, const size_t startIndex);

		WaveFunction PropagateRightFrom(const size_t index, const WaveFunction & ref, const size_t startIndex);

		private:

		const double mDt;
		// the algorithm starts in 'unconverged' state. 
		// False value implies that no changes were made to the controls in the most recent iteration => we've reached a local optimum
		bool mChangedControls = true; 
		bool mStopRequest = false;

		size_t mIters = 0;
		size_t mCostEvals = 0;

		Control mInitialControl;
		Control mCurrentControl;

		// Indices of currently selected (available) discrete control values
		std::vector<size_t> mDiscretizedIndexOrdering;

		// Available discretized control values
		const Control mDiscretizedAvailableControls;

		// Cost function
		const CostFunction mCost;

		// States
		const WaveFunction mInitialState;
		const WaveFunction mTargetState;

		Hamiltonian mHamiltonian;

		// Type of ordering
		// Access ordering for one optimizer iteration (every access within an optimizer iteration is a 'subiteration')
		// (1, ..., N-1) for Forward ordering)
		// (N-1, ..., 1) for Backward ordering
		// shuffle(1, ..., N-1) for Random ordering - reshuffled every optmizer iteration 
		const sga::AccessOrder mAccessOrder;
		std::vector<size_t> mAccessOrderInd;

		// User defined collector
		Collector mCollector;

		// User defined stopper
		Stopper mStopper;

		// Iteration state (for data collection)
		sga::IterState mIterState = sga::IterState::Global;

		// Random number generator
		std::mt19937 mGen;

		// WaveFunction caches
		enum class WFSide {Left, Right};
		WaveFunctionCache<size_t, WaveFunction> mLeftWaveFunctionCache;
		WaveFunctionCache<size_t, WaveFunction> mRightWaveFunctionCache;

		// Split-step time steppers (for convenience/comparison with matrix-caching method)
		using Stepper = decltype(qengine::makeFixedTimeStepper(mHamiltonian, mInitialState, mDt));
		Stepper mLeftStepper;
		Stepper mRightStepper;

		// Matrix-caching steppers
		CachingStepper mLeftWaveFunctionCacheStepper;
		CachingStepper mRightWaveFunctionCacheStepper;

		sga::IndexType mIndexType;
		std::vector<std::pair<size_t, size_t>> mIndexBuckets;
		size_t mBucketNumber;
		};
	}

	// Include the implementation of SGA's functions
	#include "SGA.impl.h"
