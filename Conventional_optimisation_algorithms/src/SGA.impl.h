	#pragma once

	#include <vector>
	#include <unordered_map>
	#include <chrono>
	#include <random>
	#include <type_traits>

	#include <future>
	#include <iterator>

	#include <qengine/qengine.h>
	#include <WaveFunctionCache.h>


	#include "SeedingStrategies/RandomizedSineSeedingStrategy.h"

	#include <SequenceCache.h>
	#include <CachingDirectExpTimeStepper.h>
	#include <DiscretizedControls.h>

	namespace qengine
	{

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    Control SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::GetOptimizedControl()
		{
			return mCurrentControl;
		}

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    double SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::GetFidelity()
		{
			mLeftStepper.reset(mInitialState, mCurrentControl.get(0));
			for (int i = 1 ; i < mCurrentControl.size() ; ++i)
			{
				mLeftStepper.step( mCurrentControl.get(i) );
			}
			return qengine::fidelity(mLeftStepper.state(), mTargetState);	
		}

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    double SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::GetCost() 
		{
			mLeftStepper.reset(mInitialState, mCurrentControl.get(0));
			for (int i = 1 ; i < mCurrentControl.size() ; ++i)
			{
				mLeftStepper.step( mCurrentControl.get(i) );
			}
			return mCost(mLeftStepper.state(), mTargetState, mCurrentControl);		
		}

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    size_t SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::GetCostEvals() const noexcept
		{
			return mCostEvals;
		}

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    double SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::GetInitialFidelity() const
		{
			auto solverLeft = qengine::makeFixedTimeStepper(mHamiltonian, mInitialState, mDt); // todo:: instantiate only once per class (reuse with reset)

			for (int i = 1 ; i < mInitialControl.size() ; ++i)
			{
				solverLeft.step( mInitialControl.get(i) );
			}
			return qengine::fidelity(solverLeft.state(), mTargetState);
		}


		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    sga::IterState SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::GetIterState() const noexcept
		{
			return mIterState;
		}

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    sga::AccessOrder SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::GetAccessOrder() const
		{
			return mAccessOrder;
		}

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    void SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::optimize()
		{
			fprintf(stderr, "Iter: %llu: F: %.10f Cost: %.10f\n", GetIter(), GetFidelity(), GetCost());
			while(!CheckStopConditions())
			{
				OptimizerIter();
				mIterState = sga::IterState::Global;
				mCollector( *this );
				RemakeAccessOrder();
				++mIters;
				fprintf(stderr, "Iter: %llu: F: %.10f Cost: %.10f\n", GetIter(), GetFidelity(), GetCost());
				if ( GetDefaultStopCriterion() ) // stop criterion according to Dries
				{
					fprintf(stderr, "Stopping the SGA optimizer due to no changes in controls.\n");
				}
			}
		}

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    bool SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::GetDefaultStopCriterion() const
		{
			return !mChangedControls;
		}


		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    size_t SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::GetIter() const
		{
			return mIters;
		}

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    std::vector<size_t> SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::GetAccessOrderInd() const
		{
			return mAccessOrderInd;
		}
		
		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    Control SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::GetCurrentControl() const
		{
			return mCurrentControl;
		}

	    template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    bool SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::CheckCacheAvailableLeft(const size_t index)
		{
			return mLeftWaveFunctionCache.HasKey(index);
		}

	    template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    bool SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::CheckCacheAvailableRight(const size_t index)
		{
			return mRightWaveFunctionCache.HasKey(index);
		}

	    template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    WaveFunction SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::GetCachedResultLeft(const size_t index)
		{
			return mLeftWaveFunctionCache.GetCachedWF(index);
		}

	    template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    WaveFunction SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::GetCachedResultRight(const size_t index)
		{
			return mRightWaveFunctionCache.GetCachedWF(index);
		}    

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    bool SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>:: OptimizerIter()
		{
			mIterState = sga::IterState::Sub;
			mChangedControls = false;
			for (const auto index : mAccessOrderInd)
			{
				switch(mIndexType)
				{
					case sga::IndexType::Bucket:
					{
						mChangedControls |= OptimizerSubiterBucket( index );
						break;
					}
					case sga::IndexType::Singular:
					{
						mChangedControls |= OptimizerSubiter( index );
						break;
					}
				}
				mCollector( *this );

				if ( mStopper(*this) )
				{
					break;
				}
			}
			return mChangedControls;
		}

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    bool SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::OptimizerSubiter(const size_t controlIndex)
		{
			const auto & lhs = GetWaveFunctionLeft(controlIndex - 1);
			const auto & rhs = GetWaveFunctionRight(controlIndex + 1);

			WaveFunction bestState = GetWaveFunctionLeft(controlIndex);

			double bestCost = mCost(bestState, rhs, mCurrentControl);
			++mCostEvals;

			const double prevBestCost = bestCost;
			size_t bestControlIndex = 0;
			bool changed = false;
			
			Control tmp = mCurrentControl;

			for (size_t i = 0 ; i < mDiscretizedAvailableControls.size() ; ++i)
			{
				mLeftWaveFunctionCacheStepper.resetState(lhs);
				mLeftWaveFunctionCacheStepper.step( i );
				const auto & newStateLeft = mLeftWaveFunctionCacheStepper.state();

				mRightWaveFunctionCacheStepper.resetState(rhs);

				const auto & newStateRight = mRightWaveFunctionCacheStepper.state();

				tmp.set(controlIndex, mDiscretizedAvailableControls.get(i));
				const double cost = mCost(newStateLeft, newStateRight, tmp);
				++mCostEvals;
				if (cost < bestCost)
				{
					changed = true;
					bestControlIndex = i;
					bestCost = cost;
					bestState = newStateLeft;
				}
			}

			if (changed)
			{
				qengine_assert(bestCost < prevBestCost, "Disallowed increase in cost.");

				// alter the current controls with the chosen point
				mCurrentControl.at(controlIndex) = mDiscretizedAvailableControls.at(bestControlIndex);
				mDiscretizedIndexOrdering.at(controlIndex) = bestControlIndex;

				// update the cache
				mLeftWaveFunctionCache.UpdateCache(controlIndex, bestState);
				if (controlIndex  < mInitialControl.size() - 1)
				{
					mRightWaveFunctionCache.EraseCacheFrom( controlIndex );
				}
			}
			
			return changed;
		}

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    bool SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::OptimizerSubiterBucket(const size_t bucketIndex)
		{
			const auto bucketRange = mIndexBuckets.at( bucketIndex );
			const auto controlIndexStart = bucketRange.first;
			const auto controlIndexStop = bucketRange.second;

			const auto & lhs = GetWaveFunctionLeft(controlIndexStart - 1);
			const auto & rhs = GetWaveFunctionRight(controlIndexStop);

			WaveFunction bestState = GetWaveFunctionLeft(controlIndexStop - 1);

			double bestCost = mCost(bestState, rhs, mCurrentControl);
			++mCostEvals;

			const double prevBestCost = bestCost;
			size_t bestControlIndex = 0;
			bool changed = false;
			
			Control tmp = mCurrentControl;

			for (size_t i = 0 ; i < mDiscretizedAvailableControls.size() ; ++i)
			{
				mLeftWaveFunctionCacheStepper.resetState(lhs);
				for (size_t propIndex = controlIndexStart ; propIndex < controlIndexStop ; ++propIndex)
				{
					mLeftWaveFunctionCacheStepper.step( i );
					tmp.set(propIndex, mDiscretizedAvailableControls.get(i));	
				}
				
				const auto & newStateLeft = mLeftWaveFunctionCacheStepper.state();

				mRightWaveFunctionCacheStepper.resetState(rhs);
				const auto & newStateRight = mRightWaveFunctionCacheStepper.state();
				
				const double cost = mCost(newStateLeft, newStateRight, tmp);
				++mCostEvals;

				if (cost < bestCost)
				{
					changed = true;
					bestControlIndex = i;
					bestCost = cost;
					bestState = newStateLeft;
				}
			}

			if (changed)
			{
				qengine_assert(bestCost < prevBestCost, "Disallowed increase in cost.");

				for (size_t propIndex = controlIndexStart ; propIndex < controlIndexStop ; ++propIndex)
				{
					mCurrentControl.at(propIndex) = mDiscretizedAvailableControls.at(bestControlIndex);
					mDiscretizedIndexOrdering.at(propIndex) = bestControlIndex;
				}			

				// update the cache
				mLeftWaveFunctionCache.UpdateCache(controlIndexStop - 1, bestState);
				if (controlIndexStop  < mInitialControl.size() - 1)
				{
					mRightWaveFunctionCache.EraseCacheFrom( controlIndexStop );
				}
			}
			
			return changed;
		}


		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    bool SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::CheckStopConditions()
		{
			mStopRequest = mStopper(*this) || GetDefaultStopCriterion();
			return mStopRequest;
		}

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    void SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::ConstructAccessOrder()
		{
			mAccessOrderInd.reserve(mInitialControl.size());
			for (size_t i = 1 ; i < mInitialControl.size() - 1  ; ++i)
			{
				mAccessOrderInd.emplace_back( i );
			}

			switch (mAccessOrder)
			{
				case sga::AccessOrder::Forward:
				{
					break;
				}
				case sga::AccessOrder::Backward:
				{
					std::sort(mAccessOrderInd.begin(), mAccessOrderInd.end(), [] (const auto lhs, const auto rhs) { return lhs > rhs; });
					break;
				}
				case sga::AccessOrder::Random:
				{
					std::shuffle(mAccessOrderInd.begin(), mAccessOrderInd.end(), mGen);				
					break;
				}
			}
		}

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    void SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::RemakeAccessOrder()
		{
			switch (mAccessOrder)
			{
				case sga::AccessOrder::Forward:
				{
					// no changes required
					break;
				}
				case sga::AccessOrder::Backward:
				{
					// no changes required
					break;
				}
				case sga::AccessOrder::Random:
				{
					std::shuffle(mAccessOrderInd.begin(), mAccessOrderInd.end(), mGen);
					break;
				}
			}		
		}

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    void SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::CheckDiscretizedControls()
		{
			// sanity checks
			qengine_assert(mDiscretizedAvailableControls.paramCount() == mInitialControl.paramCount(), "Dimensionality of discretized and initial controls is not the same.");
			qengine_assert(mDiscretizedAvailableControls.size() > 0, "No discretized control points available.");
		}

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    WaveFunction SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::GetWaveFunctionLeft(const size_t index)
		{
			if (index == 0)
			{
				return mInitialState;
			}

			if (CheckCacheAvailableLeft(index))
			{
				return GetCachedResultLeft(index);
			}
			return PropagateLeftFrom(index, mLeftWaveFunctionCache.GetLatestWF(), mLeftWaveFunctionCache.GetLatestKey());
		}

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    WaveFunction SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::GetWaveFunctionRight(const size_t index)
		{
			if (index == mCurrentControl.size() - 1)
			{
				return mTargetState;
			}

			if (CheckCacheAvailableRight(index))
			{
				return GetCachedResultRight(index);
			}
			return PropagateRightFrom(index, mRightWaveFunctionCache.GetLatestWF(), mRightWaveFunctionCache.GetLatestKey());
		}	

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    WaveFunction SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::PropagateLeftFrom(const size_t index, const WaveFunction & ref, const size_t startIndex)
		{
			qengine_assert(startIndex < index, "Invalid starting index for PropagateLeftFrom");

			mLeftWaveFunctionCacheStepper.resetState(ref);
			for (int i = startIndex + 1; i <= index; ++i)
			{
				mLeftWaveFunctionCacheStepper.step( mDiscretizedIndexOrdering.at(i) );
				mLeftWaveFunctionCache.UpdateCache(i, mLeftWaveFunctionCacheStepper.state());
			}
			return mLeftWaveFunctionCacheStepper.state();
		}	

		template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector, typename CachingStepper>
	    WaveFunction SGA<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector, CachingStepper>::PropagateRightFrom(const size_t index, const WaveFunction & ref, const size_t startIndex)
		{
			qengine_assert(startIndex > index, "Invalid starting index for PropagateRightFrom");

			mRightWaveFunctionCacheStepper.resetState(ref);
			for (int i = startIndex - 1; i >= index; --i)
			{
				mRightWaveFunctionCacheStepper.step( mDiscretizedIndexOrdering.at( i ) );
				mRightWaveFunctionCache.UpdateCache(i, mRightWaveFunctionCacheStepper.state());
			}
			return mRightWaveFunctionCacheStepper.state();
		}
	}
