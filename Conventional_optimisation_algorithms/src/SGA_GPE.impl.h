    #pragma once

    #include <vector>
    #include <unordered_map>
    #include <chrono>
    #include <random>
    #include <type_traits>

    #include <future>
    #include <iterator>

    #include <qengine/qengine.h>
    #include <WaveFunctionCache.h>


    #include "SeedingStrategies/RandomizedSineSeedingStrategy.h"

    #include <SequenceCache.h>
    #include <CachingDirectExpTimeStepper.h>
    #include <DiscretizedControls.h>

    namespace qengine
    {

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        Control SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::GetOptimizedControl()
    	{
    		return mCurrentControl;
    	}

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        double SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::GetFidelity()
    	{
    		mLeftStepper.reset(mInitialState, mCurrentControl.get(0));
    		for (int i = 1 ; i < mCurrentControl.size() ; ++i)
    		{
    			mLeftStepper.step( mCurrentControl.get(i) );
    		}
    		return qengine::fidelity(mLeftStepper.state(), mTargetState);	
    	}

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        double SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::GetCost() 
    	{
    		mLeftStepper.reset(mInitialState, mCurrentControl.get(0));
    		for (int i = 1 ; i < mCurrentControl.size() ; ++i)
    		{
    			mLeftStepper.step( mCurrentControl.get(i) );
    		}
    		return mCost(mLeftStepper.state(), mTargetState, mCurrentControl);		
    	}

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        size_t SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::GetCostEvals() const noexcept
    	{
    		return mCostEvals;
    	}

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        double SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::GetInitialFidelity() const
    	{
    		auto solverLeft = qengine::makeFixedTimeStepper(mHamiltonian, mInitialState, mDt); 

    		for (int i = 1 ; i < mInitialControl.size() ; ++i)
    		{
    			solverLeft.step( mInitialControl.get(i) );
    		}
    		return qengine::fidelity(solverLeft.state(), mTargetState);
    	}


    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        sga::IterState SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::GetIterState() const noexcept
    	{
    		return mIterState;
    	}

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        sga::AccessOrder SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::GetAccessOrder() const
    	{
    		return mAccessOrder;
    	}

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        void SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::optimize()
    	{
    		fprintf(stderr, "Iter: %llu: F: %.10f Cost: %.10f\n", GetIter(), GetFidelity(), GetCost());
    		while(!CheckStopConditions())
    		{
    			OptimizerIter();
    			mIterState = sga::IterState::Global;
    			mCollector( *this );
    			RemakeAccessOrder();
    			++mIters;
    			fprintf(stderr, "Iter: %llu: F: %.10f Cost: %.10f\n", GetIter(), GetFidelity(), GetCost());
    			if ( GetDefaultStopCriterion() ) // stop criterion according to Dries
    			{
    				fprintf(stderr, "Stopping the SGA_GPE optimizer due to no changes in controls.\n");
    			}
    		}
    	}

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        bool SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::GetDefaultStopCriterion() const
    	{
    		return !mChangedControls;
    	}


    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        size_t SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::GetIter() const
    	{
    		return mIters;
    	}

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        std::vector<size_t> SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::GetAccessOrderInd() const
    	{
    		return mAccessOrderInd;
    	}
    	
    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        Control SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::GetCurrentControl() const
    	{
    		return mCurrentControl;
    	}

        template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        bool SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::CheckCacheAvailableLeft(const size_t index)
    	{
    		return mLeftWaveFunctionCache.HasKey(index);
    	}

        template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        bool SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::CheckCacheAvailableRight(const size_t index)
    	{
    		return mRightWaveFunctionCache.HasKey(index);
    	}

        template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        WaveFunction SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::GetCachedResultLeft(const size_t index)
    	{
    		return mLeftWaveFunctionCache.GetCachedWF(index);
    	}

        template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        WaveFunction SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::GetCachedResultRight(const size_t index)
    	{
    		return mRightWaveFunctionCache.GetCachedWF(index);
    	}    

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        bool SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>:: OptimizerIter()
    	{
    		mIterState = sga::IterState::Sub;
    		mChangedControls = false;
    		for (const auto index : mAccessOrderInd)
    		{
    			switch(mIndexType)
    			{
    				case sga::IndexType::Bucket:
    				{
    					mChangedControls |= OptimizerSubiterBucket( index );
    					break;
    				}
    				case sga::IndexType::Singular:
    				{
    					mChangedControls |= OptimizerSubiter( index );
    					break;
    				}
    			}

    			mCollector( *this );

    			if (mStopper(*this))
    			{
    				break;
    			}			
    		}
    		return mChangedControls;
    	}

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        bool SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::OptimizerSubiter(const size_t controlIndex)
    	{
    		const auto & lhs = GetWaveFunctionLeft(controlIndex - 1);

    		WaveFunction bestState = GetWaveFunctionLeft(controlIndex);

    		double bestCost = mCost(GetWaveFunctionLeft(mCurrentControl.size() - 1), mTargetState, mCurrentControl);

    		const double prevBestCost = bestCost;
    		size_t bestControlIndex = 0;
    		bool changed = false;

    		
    		Control tmp = mCurrentControl;

    		auto solverTmpLeft = qengine::makeFixedTimeStepper(mHamiltonian, lhs, mDt);
    		for (size_t i = 0 ; i < mDiscretizedAvailableControls.size() ; ++i)
    		{
    			solverTmpLeft.reset(lhs, mCurrentControl.get(controlIndex - 1));
    			solverTmpLeft.step( mDiscretizedAvailableControls.get(i) );
    			auto modState = solverTmpLeft.state();
    			for (size_t k = controlIndex + 1 ; k < mCurrentControl.size() ; ++k)
    			{
    				solverTmpLeft.step( mCurrentControl.get(k) );
    			}
    			const auto & newStateLeft = solverTmpLeft.state();

    			const auto & newStateRight = mTargetState;

    			tmp.set(controlIndex, mDiscretizedAvailableControls.get(i));
    			const double cost = mCost(newStateLeft, newStateRight, tmp);
    			if (cost < bestCost)
    			{
    				changed = true;
    				bestControlIndex = i;
    				bestCost = cost;
    				bestState = modState;
    			}

    		}

    		fprintf(stderr, "Iter: %llu: Index: %llu Cost: %.9f (from %.9f)Changed: %d \n", GetIter(), controlIndex, bestCost, prevBestCost, (int) changed);
    		if (changed)
    		{
    			qengine_assert(bestCost < prevBestCost, "Disallowed increase in cost.");

    			mCurrentControl.at(controlIndex) = mDiscretizedAvailableControls.at(bestControlIndex);
    			mDiscretizedIndexOrdering.at(controlIndex) = bestControlIndex;

    			mLeftWaveFunctionCache.UpdateCache(controlIndex, bestState);
    		}
    		
    		return changed;
    	}


    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        bool SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::OptimizerSubiterBucket(const size_t bucketIndex)
    	{
    		const auto bucketRange = mIndexBuckets.at( bucketIndex );
    		const auto controlIndexStart = bucketRange.first;
    		const auto controlIndexStop = bucketRange.second;

    		const auto & lhs = GetWaveFunctionLeft(controlIndexStart - 1);
    		double bestCost = mCost(GetWaveFunctionLeft(mCurrentControl.size() - 1), mTargetState, mCurrentControl);

    		const double prevBestCost = bestCost;
    		size_t bestControlIndex = 0;
    		bool changed = false;

    		
    		Control tmp = mCurrentControl;

    		auto solverTmpLeft = qengine::makeFixedTimeStepper(mHamiltonian, lhs, mDt);
    		for (size_t i = 0 ; i < mDiscretizedAvailableControls.size() ; ++i)
    		{
    			solverTmpLeft.reset(lhs, mCurrentControl.get(controlIndexStart - 1));			
    			for (size_t propIndex = controlIndexStart ; propIndex < controlIndexStop ; ++propIndex)
    			{
    				solverTmpLeft.step( mDiscretizedAvailableControls.get(i) );
    			}

    			for (size_t k = controlIndexStop ; k < mCurrentControl.size() ; ++k)
    			{
    				solverTmpLeft.step( mCurrentControl.get(k) );
    			}


    			const auto & newStateLeft = solverTmpLeft.state();			
    			
    			const auto & newStateRight = mTargetState;

    			const double cost = mCost(newStateLeft, newStateRight, tmp);
    			if (cost < bestCost)
    			{
    				changed = true;
    				bestControlIndex = i;
    				bestCost = cost;
    			}
    		}

    		if (changed)
    		{
    			qengine_assert(bestCost < prevBestCost, "Disallowed increase in cost.");

    			// alter the current controls with the chosen point
    			for (size_t propIndex = controlIndexStart ; propIndex < controlIndexStop ; ++propIndex)
    			{
    				mCurrentControl.at(propIndex) = mDiscretizedAvailableControls.at(bestControlIndex);
    				mDiscretizedIndexOrdering.at(propIndex) = bestControlIndex;
    			}			

    			mLeftWaveFunctionCache.UpdateCache(controlIndexStart - 1, lhs);
    		}
    		
    		return changed;
    	}

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        bool SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::CheckStopConditions()
    	{
    		mStopRequest = mStopper(*this) || GetDefaultStopCriterion();
    		return mStopRequest;
    	}

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        void SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::ConstructAccessOrder()
    	{
    		// construct the vector
    		mAccessOrderInd.reserve(mInitialControl.size());
    		for (size_t i = 1 ; i < mInitialControl.size() - 1  ; ++i) // we won't move the first and last controls (fixed!!!)
    		{
    			mAccessOrderInd.emplace_back( i );
    		}

    		switch (mAccessOrder)
    		{
    			case sga::AccessOrder::Forward:
    			{
    				// starts out sorted in ascending order (constructor)
    				break;
    			}
    			case sga::AccessOrder::Backward:
    			{
    				std::sort(mAccessOrderInd.begin(), mAccessOrderInd.end(), [] (const auto lhs, const auto rhs) { return lhs > rhs; });
    				break;
    			}
    			case sga::AccessOrder::Random:
    			{
    				std::shuffle(mAccessOrderInd.begin(), mAccessOrderInd.end(), mGen);				
    				break;
    			}
    		}
    	}

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        void SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::RemakeAccessOrder()
    	{
    		switch (mAccessOrder)
    		{
    			case sga::AccessOrder::Forward:
    			{
    				// no changes required
    				break;
    			}
    			case sga::AccessOrder::Backward:
    			{
    				// no changes required
    				break;
    			}
    			case sga::AccessOrder::Random:
    			{
    				std::shuffle(mAccessOrderInd.begin(), mAccessOrderInd.end(), mGen);
    				break;
    			}
    		}		
    	}

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        void SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::CheckDiscretizedControls()
    	{
    		// sanity checks
    		qengine_assert(mDiscretizedAvailableControls.paramCount() == mInitialControl.paramCount(), "Dimensionality of discretized and initial controls is not the same.");
    		qengine_assert(mDiscretizedAvailableControls.size() > 0, "No discretized control points available.");
    	}


    	// propagation / cache retrieval
    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        WaveFunction SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::GetWaveFunctionLeft(const size_t index)
    	{
    		if (index == 0)
    		{
    			return mInitialState;
    		}

    		if (CheckCacheAvailableLeft(index))
    		{
    			return GetCachedResultLeft(index);
    		}
    		return PropagateLeftFrom(index, mLeftWaveFunctionCache.GetLatestWF(), mLeftWaveFunctionCache.GetLatestKey());
    	}

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        WaveFunction SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::GetWaveFunctionRight(const size_t index)
    	{
    		if (index == mCurrentControl.size() - 1)
    		{
    			return mTargetState;
    		}
    		return PropagateRightFrom(index, mTargetState, mCurrentControl.size() - 1);
    	}	

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        WaveFunction SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::PropagateLeftFrom(const size_t index, const WaveFunction & ref, const size_t startIndex)
    	{
    		qengine_assert(startIndex < index, "Invalid starting index for PropagateLeftFrom");
    		auto solverLeft = qengine::makeFixedTimeStepper(mHamiltonian, ref, mDt); // todo:: instantiate only once per class (reuse with reset)

    		solverLeft.reset(ref, mCurrentControl.get(startIndex));
    		for (int i = startIndex + 1; i <= index; ++i)
    		{
    			solverLeft.step( mCurrentControl.get(i) );
    			mLeftWaveFunctionCache.UpdateCache(i, solverLeft.state());
    		}
    		return solverLeft.state();

    	}	

    	template <typename WaveFunction, typename Hamiltonian, typename CostFunction, typename Stopper, typename Collector>
        WaveFunction SGA_GPE<WaveFunction, Hamiltonian, CostFunction, Stopper, Collector>::PropagateRightFrom(const size_t index, const WaveFunction & ref, const size_t startIndex)
    	{

    		qengine_assert(startIndex > index, "Invalid starting index for PropagateRightFrom");

    		auto solverRight = qengine::makeFixedTimeStepper(mHamiltonian, ref, -mDt); // todo:: instantiate only once per class (reuse with reset)

    		solverRight.reset(ref, mCurrentControl.get(startIndex));
    		for (int i = startIndex - 1; i >= index; --i)
    		{
    			solverRight.step( mCurrentControl.get( i ) );
    		}
    		return solverRight.state();

    	}
    }