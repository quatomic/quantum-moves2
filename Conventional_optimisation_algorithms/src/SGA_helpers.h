#pragma once
#include <string>

namespace qengine
{

	namespace sga
	{
		enum class AccessOrder {Forward = 0, Backward = 1, Random = 2};

		std::string GetAccessOrderString(const sga::AccessOrder input)
		{
			switch (input)
			{
				case sga::AccessOrder::Forward:
				{
					return "Forward";
				}
				case sga::AccessOrder::Random:
				{
					return "Random";
				}
				case sga::AccessOrder::Backward:
				{
					return "Backward";
				}
				default:
				{
					throw std::runtime_error("Invalid type of access order for SGA.");
				}
			}
		}

		enum class IterState {Global, Sub};
		enum class IndexType {Singular, Bucket};
	}
}