    #include <tuple>
    #include <iostream>
    #include <chrono>
    #include <random>
    // #include "qengine/qengine.h"

    #include "qengine/OneParticle.h"
    #include "qengine/GPE.h"
    #include "qengine/OptimalControl.h"
    #include "qengine/OneParticle_OC.h"
    #include "qengine/GPE_OC.h"
    #include "src/version.h"
    #include "qengine/DataContainer.h"




    #include "Boilerplate.h"
    #include "HelperMacros.h"
    #include "OptimizerTypes.h"
    #include "Systems.h"
    #include "ControlFactories.h"
    #include "RAII.h"
    #include "armadillo"
    #include "SeedingStrategies/RandomSineSeeding.h"
    #include "SeedingStrategies/LinearSeeding.h"
    #include "DifferentialEvolution.h"
    #include "SeedingStrategies/RandomizedSineSeedingStrategy.h"
    #include "SeedingStrategies/RandomSeeding.h"

    template <typename Control>
    qengine::Control MakeSeed(	const SeedingType strat, const size_t ctrlSize, const size_t ctrlDim,
    				const qengine::RVec & leftBoundary, 	const qengine::RVec & rightBoundary, 
    				const qengine::RVec & initialControl, 	const qengine::RVec & finalControl,
    				const double dt, const Control & t)
    {
        using namespace qengine;
    	switch (strat)
    	{
    		case SeedingType::RS:
    		{
                RandomSeedingStrategy rs(leftBoundary, rightBoundary, ctrlSize, initialControl, finalControl, dt);
                return rs();			
    		}
    		case SeedingType::RSS:
    		{
                auto makeSeeding = [] (const auto & t, const auto u_reference) 
                { 
                    return RandomizedSineSeedingStrategy{t, u_reference, 0.01}; 
                };

                // ugly temporary hotfix
                auto adjustSeeding = [=] (auto seeding)
                {
                    // example use of the new seed generation mechanism
                    if (1 == ctrlDim)
                    {
    	                seeding.SetSineBasisRandomized({{1, 80}});
    	                seeding.SetMaxAbsAmplitudeFixed({ {1} });
    	                seeding.SetAmplitudeDecayDistribution(CoeffDistribution::Uniform);
    	                seeding.SetMaxAbsHarmonicOffsetFixed({{0.1}});
    	                seeding.SetFluenceFixed({{10}});
                    }
                    else if (2 == ctrlDim)
                    {
                        seeding.SetSineBasisRandomized({{1, 80}, {1, 80}});
                        seeding.SetMaxAbsAmplitudeFixed({ {1}, {1} });
                        seeding.SetAmplitudeDecayDistribution(CoeffDistribution::Uniform);
                        seeding.SetMaxAbsHarmonicOffsetFixed({{0.1}, {0.1}});
                        seeding.SetFluenceFixed({{10}, {10}});
                    }
                    else
                    {
                    	fprintf(stderr, "Invalid dimension (>3) of controls for RSS.\n");
                    }

                    return seeding;
                };

    			auto u_ref_tmp = Control::zeros(ctrlSize, ctrlDim, dt);
                u_ref_tmp.at(0) = initialControl;
                u_ref_tmp.at(ctrlSize - 1) = finalControl;
                // start off at the fixed controls, then put the sines starting from the middle of the allowed range of values
                for (size_t dim = 0 ; dim < u_ref_tmp.paramCount() ; ++dim)
                {
                    for (size_t index = 1 ; index < u_ref_tmp.size() - 1; ++index)
                    {
                        u_ref_tmp.at(index).at(dim) = (leftBoundary.at(dim) + rightBoundary.at(dim)) / 2.0;
                    }
                }
                auto seeder = makeSeeding(t, u_ref_tmp);
                seeder = adjustSeeding( seeder );
                qengine::Control u = seeder();      
                ImposeBoundaries(u, leftBoundary, rightBoundary);   
                return u;  			
    		}
    		default:
    		{
    			fprintf(stderr, "Unknown seeding strategy. Aborting...\n");
    			exit(1);
    		}
    	}
    }
