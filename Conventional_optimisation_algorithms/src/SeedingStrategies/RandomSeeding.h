#pragma once

#include <vector>
#include <random>
#include <chrono>

#include <qengine/qengine.h>

class RandomSeedingStrategy
{
public:
	RandomSeedingStrategy(	const RVec leftBoxBound,
							const RVec rightBoxBound, 
							const size_t numberPoints, 
							const RVec initialControl, 
							const RVec finalControl, 
							const double dt) : 	mLeftBoxBound(leftBoxBound),
												mRightBoxBound(rightBoxBound), 
												mNumberPoints(numberPoints),
												mInitialControl(initialControl), 
												mFinalControl(finalControl), 
												mDt(dt),
												mGen( std::chrono::system_clock::now().time_since_epoch().count() )
	{
			qengine_assert(leftBoxBound.size() == rightBoxBound.size(), "Invalid value cd bounds for Random Seeding strategy.");
	}

	qengine::Control operator()()
	{
		using namespace qengine;
		std::vector<Control> outputs;
		for (size_t dim = 0 ; dim < mLeftBoxBound.size() ; ++dim)
		{
			Control tmp = qengine::Control::zeros(mNumberPoints, (size_t) 1u, mDt);

			tmp.at(0) = mInitialControl.at(dim);
			tmp.at(mNumberPoints - 1) =  mFinalControl.at(dim);
			std::uniform_real_distribution<double> dist(mLeftBoxBound.at(dim), mRightBoxBound.at(dim));

			for (size_t i = 1 ; i < mNumberPoints - 1; ++i)
			{
				tmp.at(i) = dist(mGen);
			}
			outputs.emplace_back( tmp );
		}
		return makeControl(outputs);
	}
private:
	const RVec mFinalControl;
	const RVec mInitialControl;
	const RVec mLeftBoxBound;
	const RVec mRightBoxBound;
	const size_t mNumberPoints;
	const double mDt;

	std::mt19937 mGen;

};