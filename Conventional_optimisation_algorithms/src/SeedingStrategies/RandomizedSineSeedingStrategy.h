#pragma once
#include <qengine/qengine.h>
#include <armadillo>
#include "SeedingStrategy.h"
#include "ControlFactories.h"

#include <algorithm>


using namespace qengine;

enum class CoeffDistribution {Uniform, Normal, UserDefined};

    template <typename Out, typename In>
    Out ConvertValue(const In arg)
    {
        return static_cast<Out> (arg);
    }

    template <>
    size_t ConvertValue<size_t, double> (const double arg)
    {
        return static_cast<size_t> ( floor(arg) );
    }

class RandomizedSineSeedingStrategy
{
    template <typename Out>
    using ValueGenerator = std::function<Out(void)>;

    template <typename Out>
    using GeneratorVec = std::vector<ValueGenerator<Out>>;

    

public:
    // type available outside for convenient type checks for custom functors
    using AmplitudeTransformation = std::function<BasisControl(size_t const )>;
    
    RandomizedSineSeedingStrategy(  const Control & t, 
                                    const Control & ref,
                                    const double sigTh = 0.05,
                                    const size_t basisSize = 20,  
                                    const double maxAmp = 1.0,
                                    const double maxHarmOff = 0.5,
                                    const double fluence = 1.0,
                                    const CoeffDistribution ampDist = CoeffDistribution::Uniform) :     
                                                mTime(t), 
                                                mReference(ref), 
                                                mSigmoidThreshold(sigTh),
                                                mAmplitudeDistribution(ampDist) 
    { 
        arma::arma_rng::set_seed_random();
        mfSineBasisSizeGenerator.reserve(ref.paramCount());
        mfMaxAbsSineAmplitudeGenerator.reserve(ref.paramCount());
        mfMaxAbsHarmonicOffsetGenerator.reserve(ref.paramCount());
        InitializeDefaultFuncContainers(basisSize, maxAmp, maxHarmOff, fluence);
    }

    void SetReferenceControl(const Control & ref)
    {
        qengine_assert(ref.paramCount() == mReference.paramCount(), "New reference control has to have the same parameter count as the one used for initialization of the seeder.");
        mReference = ref;
    }

    Control operator()() const
    {
        std::vector<Control> ui;

        for(auto i = 0; i < mReference.paramCount(); ++i)
        {
            ui.emplace_back( std::move( GenerateControlDim(i) ) );
        }

        qengine_assert(ui.size()       == mReference.paramCount(), "generated control has the wrong number of controls");
        for (const auto & ctrl : ui)
        {
            qengine_assert(ui.size() == mReference.paramCount(),      "generated control has the wrong number of time steps");
        }
        return makeControl( ui );   
    }

    void SetSineBasisFixed(const std::vector<RVec> & fixedPerDim)
    {
        SetFixedVals<size_t> (mfSineBasisSizeGenerator, fixedPerDim);
    }

    void SetSineBasisRandomized(const std::vector<RVec> & rangePerDim)
    {
        SetFuncRandomizedRange<size_t> (mfSineBasisSizeGenerator, rangePerDim);   
    }

    void SetMaxAbsAmplitudeFixed(const std::vector<RVec> & fixedPerDim)
    {
        SetFixedVals<double> (mfMaxAbsSineAmplitudeGenerator, fixedPerDim);
    }

    void SetMaxAbsAmplitudeRandomized(const std::vector<RVec> & rangePerDim)
    {
        SetFuncRandomizedRange<double> (mfMaxAbsSineAmplitudeGenerator, rangePerDim);   
    }

    void SetAmplitudeDecayDistribution( const CoeffDistribution arg)
    {
        switch(arg)
        {
            case CoeffDistribution::UserDefined:
            {
                qengine_assert(mAmplitudeDecayFunctions.size(), "Amplitude decay functions were not defined by the user.");
                break;
            }
            default:
            {
                break;
            }
        }
        mAmplitudeDistribution = arg;
    }

    void SetAmplitudeDecayCustom(const std::vector<AmplitudeTransformation> & arg)
    {
        qengine_assert(arg.size() == mReference.paramCount(), "Invalid size of harmonic amplitude transformations.");
        mAmplitudeDistribution = CoeffDistribution::UserDefined;
        mAmplitudeDecayFunctions = arg;
    }


    void SetMaxAbsHarmonicOffsetFixed(const std::vector<RVec> & fixedPerDim)
    {
        SetFixedVals<double> (mfMaxAbsHarmonicOffsetGenerator, fixedPerDim);
    }

    void SetMaxAbsHarmonicOffsetRandomized(const std::vector<RVec> & rangePerDim)
    {
        SetFuncRandomizedRange<double> (mfMaxAbsHarmonicOffsetGenerator, rangePerDim);   
    }    


    void SetFluenceFixed(const std::vector<RVec> & fixedPerDim)
    {
        SetFixedVals<double> (mfFluenceGenerator, fixedPerDim);
    }

    void SetFluenceRandomized(const std::vector<RVec> & rangePerDim)
    {
        SetFuncRandomizedRange<double> (mfFluenceGenerator, rangePerDim);   
    }        

    std::string strategyName()
    {
        return "RandomizedSineSeedingStrategy";
    }

private:
    Control mTime;
    Control mReference;
    double mSigmoidThreshold;

    CoeffDistribution mAmplitudeDistribution;

    GeneratorVec<size_t> mfSineBasisSizeGenerator;
    GeneratorVec<double> mfMaxAbsSineAmplitudeGenerator;
    GeneratorVec<double> mfMaxAbsHarmonicOffsetGenerator;
    GeneratorVec<double> mfFluenceGenerator;

    std::vector<AmplitudeTransformation> mAmplitudeDecayFunctions;

    Control GenerateControlDim(const size_t dimIndex) const
    {
        const auto S = makeNewSigmoidShapeFunction(mTime, mSigmoidThreshold);

        const auto sineBasisSize = mfSineBasisSizeGenerator.at(dimIndex)();

        const auto maxRandValSinOmega = mfMaxAbsHarmonicOffsetGenerator.at(dimIndex)();

        const auto maxRandValCoefficients = mfMaxAbsSineAmplitudeGenerator.at(dimIndex)();

        const auto b = S*makeSineBasis(sineBasisSize, 1, mReference.size(), mReference.dt(), maxRandValSinOmega);

        auto u_rand = qengine::Control();
        switch (mAmplitudeDistribution)
        {
            case CoeffDistribution::Uniform:
            {
                const auto c =  makeRandomizedCoefficients(sineBasisSize, 1, maxRandValCoefficients);
                u_rand = BasisControl(c)*b;
                break;
            }
            case CoeffDistribution::Normal:
            {
                const double fwhm = static_cast<double> (sineBasisSize); 
                const auto variance = fwhm / 2.355; // approx value
                const auto c =  makeGaussRandomizedCoefficients(sineBasisSize, 1, maxRandValCoefficients, 0.0, variance);
                u_rand = BasisControl(c)*b;                    
                break;
            }
            case CoeffDistribution::UserDefined:
            {
                u_rand = mAmplitudeDecayFunctions.at(dimIndex)(sineBasisSize) * b;
                break;
            }
        }

        const double fluence = mfFluenceGenerator.at(dimIndex)();

        u_rand = u_rand / fluence;

        RMat ui_reference(1, mReference.size()); // size: 1 x n_steps
        for(auto j = 0; j < mReference.size(); ++j)
        {
            ui_reference.col(j).at(0) = mReference.get(j).at(dimIndex);
        } 

        return Control(ui_reference, mReference.dt()) + u_rand;       
    }

    void InitializeDefaultFuncContainers(const size_t basisSize, const double absSineAmp, const double absHarmOff, const double fluence)
    {
        InitializeDefaultFuncContainer(basisSize, mfSineBasisSizeGenerator);
        InitializeDefaultFuncContainer(absSineAmp, mfMaxAbsSineAmplitudeGenerator);
        InitializeDefaultFuncContainer(absHarmOff, mfMaxAbsHarmonicOffsetGenerator);
        InitializeDefaultFuncContainer(fluence, mfFluenceGenerator);
    }

    template <typename Out, typename Container>
    void InitializeDefaultFuncContainer(const Out defaultVal, Container & container)
    {
        std::vector<RVec> tmp;
        tmp.reserve(mReference.paramCount());

        for (size_t i = 0 ; i < mReference.paramCount() ; ++i)
        {
            tmp.emplace_back(RVec{static_cast<double> (defaultVal)});
        }
        SetFixedVals<Out> (container, tmp);        
    }

    template <typename ValType, typename Container>
    void SetFixedVals(Container & funcVec, const std::vector<RVec> & fixedPerDim)
    {
        FuncGeneratorDimensionSanityCheck(fixedPerDim, 1);
        GenerateFixedFuncs<ValType> (funcVec, fixedPerDim);
    }

    template <typename ValType, typename Container>
    void SetFuncRandomizedRange(Container & funcVec, const std::vector<RVec> & rangePerDim)
    {
        FuncGeneratorDimensionSanityCheck(rangePerDim, 2);
        FuncGeneratorRangeSanityCheck(rangePerDim);
        GenerateRandFunc<ValType> (funcVec, rangePerDim);
    }

    template <typename ValType, typename Container>
    void GenerateFixedFuncs(Container & funcVec, const std::vector<RVec> & fixedPerDim)
    {
        funcVec.clear();
        for (const auto & rvec : fixedPerDim )
        {
            funcVec.emplace_back( [rvec, this] () { return ConvertValue<ValType> ( rvec.at(0) ); } );
        }        
    }

    template <typename ValType, typename Container>
    void GenerateRandFunc(Container & funcVec, const std::vector<RVec> & rangePerDim)
    {
        funcVec.clear();
        for (const auto & rvec : rangePerDim )
        {
            qengine_assert(rvec.at(0) <= rvec.at(1), "Invalid RVec range for randomized value generation (SetFuncRandomizedRange)");
            funcVec.emplace_back( [rvec, this] () { return ConvertValue<ValType> (rvec.at(0) + arma::randu() * (rvec.at(1) - rvec.at(0))); } );
        }           
    }

    void FuncGeneratorDimensionSanityCheck(const std::vector<RVec> & input, const size_t expectedSize)
    {
        qengine_assert(input.size() == mReference.paramCount(), "Invalid number of ranges (must be equal to control paramCount");

        if (!std::all_of(input.cbegin(), input.cend(), [expectedSize] (const auto & arg) { return arg.size() == expectedSize; }))
        {
            const std::string message("Invalid sizes of RVecs with randomized values (must be " + std::to_string( expectedSize ) + ") (FuncGeneratorDimensionSanityCheck)\n");
            qengine_assert(false, message);
        }
    }   

    void FuncGeneratorRangeSanityCheck(const std::vector<RVec> & input)
    {
        if (!std::all_of(   input.cbegin(), 
                            input.cend(), 
                            [] (const auto & arg) 
                            { 
                                for (size_t i = 0 ; i < arg.size() ; ++i)
                                {
                                    if (arg.at(i) < 0)
                                    {
                                        return false;
                                    } 
                                }
                                return true;
                            }))
        {
            const std::string message("Invalid values within RVecs with randomized values (they must be >= to define abs ranges) (FuncGeneratorRangeSanityCheck)\n");
            qengine_assert(false, message);
        }
    }
};

