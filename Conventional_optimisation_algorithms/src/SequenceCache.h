	#pragma once

	#include <type_traits>
	#include <unordered_map>
	// #include <future>
	// #include <vector>

	namespace qengine
	{
		template <typename Sequence, typename ConversionFunc>
		class SequenceCache
		{
			using StoredType = decltype(std::declval<ConversionFunc>()(std::declval<Sequence>().get(0)));
		
		public:
			SequenceCache(	const Sequence & inputSequence, 
							const ConversionFunc foo) : 	mSequence( inputSequence ),
															mConvert( foo )
			{
				ConstructCache();
			}

			SequenceCache(const SequenceCache & other) : 	mSequence(other.mSequence), 
															mConvert(other.mConvert), 
															mCache(other.mCache) { }

			SequenceCache & operator=(const SequenceCache & other)
			{
				mSequence = other.mSequence;
				mConvert = other.mConvert;
				mCache = other.mCache;
			}

			StoredType at(const size_t key) const
			{
				return mCache.at( key );
			}

			StoredType & at(const size_t key) 
			{
				return mCache.at( key );
			}

			void Reset(const Sequence & newSeq, const ConversionFunc foo)
			{
				mSequence = newSeq;
				mConvert = foo;

				mCache.clear();
				ConstructCache();
			}

			void Reset(const Sequence & newSeq)
			{
				Reset(newSeq, mConvert);
			}

			void Reset(const ConversionFunc foo)
			{
				Reset(mSequence, foo);
			}		

			virtual ~SequenceCache() {}

		private:

			void ConstructCache()
			{
				for (size_t i = 0 ; i < mSequence.size() ; ++i)
				{
					mCache.emplace( std::make_pair(i, mConvert( mSequence.get(i))) );
				}
			}

			Sequence mSequence;
			ConversionFunc mConvert;
			std::unordered_map<size_t, StoredType> mCache;
		};
	}