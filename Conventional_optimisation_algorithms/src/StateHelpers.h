#pragma once
#include <tuple>
#include "qengine/qengine.h"

namespace qengine
{

    template<class State>
    auto projectionSum(const State& state, const State& singlet_t, const State& triplet_t)
    {
        return fidelity(state,singlet_t) + fidelity(state,triplet_t);
    }


    template<class SingleparticleWavefunction>
    auto Singlet(const SingleparticleWavefunction& a,const  SingleparticleWavefunction& b, bool NORMALIZE=true)
    {

        if(!(a._serializedHilbertSpace() == b._serializedHilbertSpace()))
        {
            throw std::runtime_error("a and b have different hilbert spaces");
        };

       auto s2 = qengine::two_particle::makeHilbertSpace( a._serializedHilbertSpace().xLower,
                                                          a._serializedHilbertSpace().xUpper,
                                                          a._serializedHilbertSpace().dim,a._serializedHilbertSpace().kinematicFactor);

       auto vec = 1/std::sqrt(2.0)*(kron(a.vec(),b.vec()) - kron(b.vec(),a.vec()));
       return makeFunctionOfX(s2._serializedHilbertSpace(), vec);
    }


    template<class SingleparticleWavefunction>
    auto Triplet(const SingleparticleWavefunction& a,const  SingleparticleWavefunction& b, bool NORMALIZE=true)
    {

        if(!(a._serializedHilbertSpace() == b._serializedHilbertSpace()))
        {
            throw std::runtime_error("a and b have different hilbert spaces");
        };

       auto s2 = qengine::two_particle::makeHilbertSpace( a._serializedHilbertSpace().xLower,
                                                          a._serializedHilbertSpace().xUpper,
                                                          a._serializedHilbertSpace().dim,a._serializedHilbertSpace().kinematicFactor);

       auto vec = 1/std::sqrt(2.0)*(kron(a.vec(),b.vec()) + kron(b.vec(),a.vec()));
       return makeFunctionOfX(s2._serializedHilbertSpace(), vec);
    }
}
