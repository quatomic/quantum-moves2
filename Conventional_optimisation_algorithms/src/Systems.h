#pragma once
#include <tuple>
#include <atomic>
#include "qengine/qengine.h"
#include "bindings/Scaling.h"



#include "HelperMacros.h"



namespace qengine
{
    inline decltype(auto) gpe_bhw_setup()
    {
        const auto xLimitsPhysical = RVec{-2, 2};
        const auto yLimitsPhysical = RVec{-150-130, 100};
        const auto initialControlPhysical   = RVec{-1.0, -130.0};
        const auto finalControlPhysical     = initialControlPhysical;

        constexpr auto width = 0.5;
        
        const auto xScalingFunc = utility::GetCoordinateReferenceScalings({xLimitsPhysical.at(0)}, {xLimitsPhysical.at(1)}, 0);
        const auto yScalingFunc = utility::GetCoordinateReferenceScalings({yLimitsPhysical.at(0)}, {yLimitsPhysical.at(1)}, 0);

        const utility::Scaling<Control, RVec> controlScaling(
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.first( ctrl.at(0) ),  yScalingFunc.first(ctrl.at(1))}; }, // SCALE FORWARD
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.second( ctrl.at(0) ), yScalingFunc.second(ctrl.at(1))}; }); // SCALE INVERSE

        const auto initialControl = controlScaling.Scale(initialControlPhysical, utility::ScalingType::FORWARD);
        const auto finalControl   = controlScaling.Scale(finalControlPhysical, utility::ScalingType::FORWARD); 

        return std::make_tuple(GPE_BHW_SETUP_RETURN_LIST);
    }

    template <class SetupTuple>
    inline decltype(auto) gpe_bhw(const SetupTuple & t, const count_t dim)
    {
        std::string physicsproblem = "bringhomewater";
        std::string systemType = "oneparticle";
        auto [GPE_BHW_SETUP_RETURN_LIST] = t;

        constexpr double kinFactor = 0.5;
        const auto s = qengine::gpe::makeHilbertSpace(-3, 3, dim, kinFactor);
        const auto x = s.x();

        const auto staticTweezerParams = RVec{1.0,-130};
        const auto V_static = staticTweezerParams.at(1) * exp(-2*pow(x - staticTweezerParams.at(0), 2) / (width*width));

        constexpr double g1D = 0.0;
        const auto leftBoundaryPhysical = RVec{xLimitsPhysical.at(0), -150.0};
        const auto rightBoundaryPhysical = RVec{xLimitsPhysical.at(1), 0.0};
        const auto leftBoundary = controlScaling.Scale({leftBoundaryPhysical},   utility::ScalingType::FORWARD);
        const auto rightBoundary = controlScaling.Scale({rightBoundaryPhysical}, utility::ScalingType::FORWARD);                
        constexpr auto boundaryStrength = 2e3;

        const auto V_dynamic = makePotentialFunction([x, width, controlScaling](const RVec& pIn)
        {
            const RVec p = controlScaling.Scale(pIn, utility::ScalingType::INVERSE);
            const auto x0 = p.at(0);
            const auto amplitude = p.at(1);

            const auto x_disp = x - x0;
            return amplitude * exp(-2*x_disp*x_disp / (width*width));
        }, initialControl);        

        const auto psi_0 = makeWavefunction((s.T() + V_static)[0]);    // ground state in static tweezer at t=0
        const auto psi_t = makeWavefunction((s.T() + V_dynamic(finalControl))[0]); // groundstate in dynamic tweezer at t=T

        const auto V = makePotentialFunction(wrap([x, width,V_static,V_dynamic](const RVec& p)
        {
            return V_dynamic(p) + V_static;
        }), initialControl);

        const auto H = s.T() + V + makeGpeTerm(g1D);
        return std::make_tuple(GPE_BHW_RETURN_LIST);
    }

    inline decltype(auto) bhw_setup()
    {
        const auto xLimitsPhysical = RVec{-2, 2};
        const auto yLimitsPhysical = RVec{-150-130, 100};
        const auto initialControlPhysical   = RVec{-1.0, -130.0};
        const auto finalControlPhysical     = initialControlPhysical;

        constexpr auto width = 0.5;
        
        const auto xScalingFunc = utility::GetCoordinateReferenceScalings({xLimitsPhysical.at(0)}, {xLimitsPhysical.at(1)}, 0);
        const auto yScalingFunc = utility::GetCoordinateReferenceScalings({yLimitsPhysical.at(0)}, {yLimitsPhysical.at(1)}, 0);

        const utility::Scaling<Control, RVec> controlScaling(
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.first( ctrl.at(0) ),  yScalingFunc.first(ctrl.at(1))}; }, // SCALE FORWARD
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.second( ctrl.at(0) ), yScalingFunc.second(ctrl.at(1))}; }); // SCALE INVERSE

        const auto initialControl = controlScaling.Scale(initialControlPhysical, utility::ScalingType::FORWARD);
        const auto finalControl   = controlScaling.Scale(finalControlPhysical, utility::ScalingType::FORWARD); 

        return std::make_tuple(BHW_SETUP_RETURN_LIST);
    }

    template <class SetupTuple>
    inline decltype(auto) bhw(const SetupTuple & t, const count_t dim)
    {
        std::string physicsproblem = "bringhomewater";
        std::string systemType = "oneparticle";
        auto [GPE_BHW_SETUP_RETURN_LIST] = t;

        constexpr double kinFactor = 0.5;
        const auto s = qengine::gpe::makeHilbertSpace(-3, 3, dim, kinFactor);
        const auto x = s.x();

        const auto staticTweezerParams = RVec{1.0,-130};
        const auto V_static = staticTweezerParams.at(1) * exp(-2*pow(x - staticTweezerParams.at(0), 2) / (width*width));

        const auto leftBoundaryPhysical = RVec{xLimitsPhysical.at(0), -150.0};
        const auto rightBoundaryPhysical = RVec{xLimitsPhysical.at(1), 0.0};
        const auto leftBoundary = controlScaling.Scale({leftBoundaryPhysical},   utility::ScalingType::FORWARD);
        const auto rightBoundary = controlScaling.Scale({rightBoundaryPhysical}, utility::ScalingType::FORWARD);                
        constexpr auto boundaryStrength = 2e3;

        const auto V_dynamic = makePotentialFunction([x, width, controlScaling](const RVec& pIn)
        {
            const RVec p = controlScaling.Scale(pIn, utility::ScalingType::INVERSE);
            const auto x0 = p.at(0);
            const auto amplitude = p.at(1);

            const auto x_disp = x - x0;
            return amplitude * exp(-2*x_disp*x_disp / (width*width));
        }, initialControl);        

        const auto psi_0 = makeWavefunction((s.T() + V_static)[0]);    // ground state in static tweezer at t=0
        const auto psi_t = makeWavefunction((s.T() + V_dynamic(finalControl))[0]); // groundstate in dynamic tweezer at t=T

        const auto V = makePotentialFunction(wrap([x, width,V_static,V_dynamic](const RVec& p)
        {
            return V_dynamic(p) + V_static;
        }), initialControl);

        const auto H = s.T() + V;
        return std::make_tuple(BHW_RETURN_LIST);
    }

    inline decltype(auto) gpe_shakeup_setup()
    {
        using namespace qengine;
        std::string physicsproblem = "shakeup";
        const auto xLimitsPhysical = RVec{-1.25, 1.25};
        // const auto yLimitsPhysical = RVec{-400, 100};
        const auto Voffset = -130.0;
        const auto initialControlPhysical = RVec{0.0};
        const auto finalControlPhysical = RVec{0.0};

        const auto xScalingFunc = utility::GetCoordinateReferenceScalings( {xLimitsPhysical.at(0)}, {xLimitsPhysical.at(1)}, 0);

        const utility::Scaling<Control, RVec> controlScaling(
            [xScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.first(  ctrl.at(0) ) }; }, // SCALE FORWARD
            [xScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.second( ctrl.at(0) ) }; }); // SCALE INVERSE

        const auto initialControl = controlScaling.Scale(initialControlPhysical, utility::ScalingType::FORWARD);
        const auto finalControl   = controlScaling.Scale(finalControlPhysical, utility::ScalingType::FORWARD); 

        const auto Vext_maker = [p2{65.8392},p4{97.6349},p6{-15.3850},Voffset, controlScaling] (const auto & x)
        {
            const auto Vext = [=](const RVec & vec2)
            {
                const RVec p = controlScaling.Scale(vec2, utility::ScalingType::INVERSE);
                const auto u = p.at(0);
                const auto x_u = x - u;
                const auto x_uPow2 = x_u * x_u;
                const auto x_uPow4 = x_uPow2 * x_uPow2;
                const auto x_uPow6 = x_uPow2 * x_uPow4;

                return  p2*x_uPow2 + p4*x_uPow4 + p6*x_uPow6 + Voffset;
            };
            return Vext;
        };

        return std::make_tuple(GPE_SHAKEUP_SETUP_RETURN_LIST); 
    }

    template<class SetupTuple>
    inline decltype(auto) gpe_shakeup(const SetupTuple & t, const count_t dim)
    {
        std::string physicsproblem = "shakeup";
        std::string systemType = "gpe";
        auto [GPE_SHAKEUP_SETUP_RETURN_LIST] = t;

        constexpr auto kinFactor = 0.36537;
        const auto s = qengine::gpe::makeHilbertSpace(-2, +2, dim, kinFactor);
        const auto x = s.x();
        const auto V = makePotentialFunction(Vext_maker(x), initialControl);
        constexpr auto g1D = 1.8299;


        constexpr auto leftBoundaryPhysical = -1.0;
        constexpr auto rightBoundaryPhysical = 1.0;
        const auto leftBoundary = controlScaling.Scale({leftBoundaryPhysical},   utility::ScalingType::FORWARD);
        const auto rightBoundary = controlScaling.Scale({rightBoundaryPhysical}, utility::ScalingType::FORWARD);
        constexpr auto boundaryStrength = 2e3;

        const auto H = s.T() + V +  makeGpeTerm(g1D);
        const auto psi_0 = makeWavefunction(H(initialControl)[0]);
        const auto psi_t = makeWavefunction(H(finalControl  )[1]);
        return std::make_tuple(GPE_SHAKEUP_RETURN_LIST);
    }


    inline decltype(auto) gpe_split_setup()
    {
        const auto yLimitsPhysical = RVec{0, 1};

        const auto initialControlPhysical = RVec{ 0.0 };
        const auto finalControlPhysical =   RVec{ 1.0 };

        const auto yScalingFunc = utility::GetCoordinateReferenceScalings({yLimitsPhysical.at(0)}, {yLimitsPhysical.at(1)}, 0);

        const utility::Scaling<Control, RVec> controlScaling(
            [yScalingFunc] (const RVec & ctrl) { return RVec{ yScalingFunc.first(  ctrl.at(0) ) }; }, // SCALE FORWARD
            [yScalingFunc] (const RVec & ctrl) { return RVec{ yScalingFunc.second( ctrl.at(0) ) }; }); // SCALE INVERSE

        const auto initialControl = controlScaling.Scale(initialControlPhysical,    utility::ScalingType::FORWARD);
        const auto finalControl   = controlScaling.Scale(finalControlPhysical,      utility::ScalingType::FORWARD); 

        const auto V_offset = 0.001;

        const real Gr = 0.2;
        const real BI = 1.0;
        
        const real Bdet = 0.90024;
        const real pV = 8794.1;

        const auto Vext_maker = [controlScaling, V_offset, Bdet,pV,BI, Gr] (const auto & x)
        {
            const auto Vext = [=](const RVec & pIn)
            {
                const RVec p = controlScaling.Scale(pIn, utility::ScalingType::INVERSE);
                const auto uy = p.at( 0 );

                const auto x_disp = x;

                const auto Bs = sqrt(Gr*Gr*x_disp*x_disp + BI*BI);            
                const auto Brf  = 0.5 + 0.3 * uy;
                auto pot = sqrt((Bs - Bdet) * (Bs - Bdet) + ((0.5 * Brf * BI) / Bs) * ((0.5 * Brf * BI) / Bs));
                pot = pot - min(pot) + V_offset;
                return pV*pot;
            };       
            return Vext;
        };
        return std::make_tuple(GPE_SPLIT_SETUP_RETURN_LIST);    
    }

    template <class SetupTuple>
    inline decltype(auto) gpe_split(const SetupTuple & t, const count_t dim)
    {
        std::string physicsproblem = "splitting";
        std::string systemType = "gpe";
        auto & [GPE_SPLIT_SETUP_RETURN_LIST] = t;
        constexpr auto kinFactor = 0.36537;
        const auto s = qengine::gpe::makeHilbertSpace(-3.5, +3.5, 256, kinFactor);
        const auto x = s.x();
        constexpr auto g1D = 1.8299;

        constexpr auto leftBoundaryPhysical = 0.0;
        constexpr auto rightBoundaryPhysical = 1.0;
        const auto leftBoundary = controlScaling.Scale({leftBoundaryPhysical},   utility::ScalingType::FORWARD);
        const auto rightBoundary = controlScaling.Scale({rightBoundaryPhysical}, utility::ScalingType::FORWARD);        
        constexpr auto boundaryStrength = 2e3;

        const auto V = makePotentialFunction(Vext_maker(x), initialControl);

        const auto H = s.T() + V +  makeGpeTerm(g1D);
        const auto psi_0 = makeWavefunction(H(initialControl)[0]);

        const auto spectrum = H(finalControl).makeSpectrum(1, GROUNDSTATE_ALGORITHM::MIXING, 1e-8, 10000);
        const auto L = spectrum.eigenFunction(0) + spectrum.eigenFunction(1);
        const auto R = spectrum.eigenFunction(0) - spectrum.eigenFunction(1);
        const auto PL = 0.5;
        const auto PR = 1-PL;
        const auto cL = std::sqrt(PL);
        const auto cR = std::sqrt(PR);
        const auto psi_t = normalize(cL*L + cR*R);

        return std::make_tuple(GPE_SPLIT_RETURN_LIST);
    }
}
