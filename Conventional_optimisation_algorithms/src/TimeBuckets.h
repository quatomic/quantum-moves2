#include <algorithm>
#include <vector>
#include <iterator>
#include <cassert>

#include <qengine/qengine.h>

struct BucketsInfo
{
	const double mDT;
	const double mLeftLimit;
	const double mRightLimit;
	const double mReferenceT;
};

inline void PrintBucketsInfo(const BucketsInfo info, FILE * file = stdout)
{
	fprintf(file, "BucketInfo:\n\tmDT: %f\n\tLimits: [%f, %f]\n\tReferenceT: %f\n", info.mDT, info.mLeftLimit, info.mRightLimit, info.mReferenceT);
}

struct BucketsResult
{
	const std::vector<double> mBucketsTs;
};

inline void AssertVerifyBucket(const BucketsInfo info);

inline std::vector<double> ConstructBucketsMultipliers(const BucketsInfo info);

inline BucketsResult ConstructBuckets(const BucketsInfo info);

inline void AssertVerifyBucket(const BucketsInfo info)
{
	qengine_assert(info.mDT > 0, "Invalid bucket dt");
	qengine_assert(info.mLeftLimit < info.mRightLimit, "Invalid bucket limits.");
	qengine_assert(info.mReferenceT > 0 , "Invalid bucket reference T");
	qengine_assert((info.mRightLimit - info.mLeftLimit)/info.mDT >= 1, "Number of multipliers for buckets = 0. Check bucket limits and dt.");
}

std::vector<double> ConstructBucketsMultipliers(const BucketsInfo info)
{
	std::vector<double> multipliers;
	const int multiplierNumber = floor((info.mRightLimit - info.mLeftLimit)/info.mDT) + 1;
	assert(multiplierNumber >= 1);
	multipliers.reserve( multiplierNumber );
	for (int i = 0 ; i < multiplierNumber ; ++i) // skip 0.0
	{
		multipliers.emplace_back(info.mLeftLimit + i * info.mDT);
	}	
	qengine_assert( std::is_sorted(multipliers.cbegin(), multipliers.cend()) , "Invalid multipliers");
	return multipliers;
}

inline BucketsResult ConstructBuckets(const BucketsInfo info)
{
	AssertVerifyBucket(info);
	std::vector<double> multipliers = ConstructBucketsMultipliers(info);
	std::vector<double> outputTs;
	outputTs.reserve( multipliers.size() );
	std::transform(	multipliers.cbegin(), 
					multipliers.cend(), 
					std::back_inserter(outputTs), 
					[info] (const auto val) { return val * info.mReferenceT; });
	qengine_assert( std::is_sorted(outputTs.cbegin(), outputTs.cend()) , "Invalid bucket of T");

	outputTs.emplace_back(info.mReferenceT);
	std::sort(outputTs.begin(), outputTs.end());

	return {std::move( outputTs )};
}

inline Control ConstructTBucketControl(const BucketsInfo info)
{
	BucketsResult bucketInformation = ConstructBuckets( info );
	return qengine::makeControl( { qengine::RVec{ bucketInformation.mBucketsTs } }, info.mDT );
}