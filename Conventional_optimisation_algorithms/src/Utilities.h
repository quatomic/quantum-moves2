#pragma once
#include <chrono>
#include <iostream>
#include <tuple>
#include "qengine/qengine.h"

enum class OptimizerType {GRAPE, SGA, SGA_BUCKET};

// RS - Random Seeding
// RSS - Random Sine Seeding
enum class SeedingType {RS, RSS};

template <typename HistoryType, typename DC>
void SaveHistoryVectorized(const HistoryType & history, DC & dc, const std::string & name) 
{
	using namespace qengine;
	RMat historyDataMat(history.size(), 1 + history.at(0).second.size(), 0.0);
	size_t i = 0;
	for(auto & pair : history)
	{ 	
		// first element is the 'iteration', rest of the elements represent the costs associated with population members
	    historyDataMat.at(i,0) = pair.first;
	    auto & vec = pair.second;
	    for (size_t costInd = 0 ; costInd < vec.size() ; ++costInd)
	    {
	    	historyDataMat.at(i, costInd + 1) = vec.at( costInd );	
	    }
	    i++;
	}
	dc[name] = historyDataMat;
}

std::string TranslateOptimizer(const OptimizerType opt)
{
	switch (opt)
	{
		case OptimizerType::GRAPE:
		{
			return "GRAPE";
		}
		case OptimizerType::SGA:
		{
			return "SGA";
		}
		case OptimizerType::SGA_BUCKET:
		{
			return "SGA_BUCKET";
		}
		default:
		{
			throw std::runtime_error("Unknown optimizer type");
			return ""; // compiler silence
		}
	}

}


std::string TranslateSeedingType(const SeedingType st)
{
	switch (st)
	{
		case SeedingType::RS:
		{
			return "RS";
		}
		default:
		{
			throw std::runtime_error("Unknown seeding type");
			return ""; // compiler silence
		}
	}
}

/// TIMER 1
class Timer
{
public:	
	using Clock = std::chrono::high_resolution_clock;
	using TimePoint = std::chrono::time_point<Clock>;
	Timer(const std::string & msg) : mMessage( msg ), mStart( TimePoint{} ), mStop( TimePoint{} ) 
	{ 

	}

	void SetMessage(const std::string & msg)
	{
		mMessage = msg;
	}

	void SetStart(const std::string & msg)
	{
		SetMessage( msg );
		Start();
	}

	void Start()
	{
		mStart = GetTime();
	} 

	void Stop()
	{
		mStop = GetTime();
	}

	void StopPrint()
	{
		Stop();
		const auto durationUS = std::chrono::duration_cast<std::chrono::microseconds> (mStop - mStart);
		const auto durationMS = std::chrono::duration_cast<std::chrono::milliseconds> (mStop - mStart);
		std::cout << "Time elapsed for \"" << mMessage << "\": " << durationMS.count() << " ms (" << durationUS.count() << " us)" << std::endl;
	}

	void Reset()
	{
		mStart = TimePoint{};
		mStop = TimePoint{};
	}

	TimePoint GetTime()
	{
		return std::chrono::high_resolution_clock::now();
	}

private:
	std::string mMessage;
	TimePoint mStart;
	TimePoint mStop;
};

/// TIMER 2
inline auto makeClockNow(){return std::chrono::high_resolution_clock::now();};
struct EventTimer
{
    EventTimer(): canToc(false), event(""), tic_t(makeClockNow()){};

    void tic(std::string theEvent){
        tic_t = makeClockNow();
        event = theEvent;
        canToc = true;
    };

    double toc()
    {
        if(!canToc)
        {
            std::cout << "Warning: toc() called without an corresponding tic() !" << std::endl;
        }
        auto toc_t = makeClockNow();
        std::chrono::duration<double> elapsed = toc_t - tic_t;
        canToc = false;
        return double(elapsed.count());
    };

    bool canToc;
    std::string event;
    decltype(makeClockNow()) tic_t;
};



template<class T,class Name,class... TaskArgs>
void doTask(T task,Name name, TaskArgs&&... args)
{
    EventTimer t;

    std::cout << name << std::endl << "---------------" << std::endl;
    t.tic(name);
    task(args...);
    std::cout << "---------------" << std::endl;
    std::cout << name << " execution time: " << t.toc() << "s" << std::endl;
    std::cout << "######################################" << std::endl;
}


struct CostFidelity
{
	double mCost;
	double mFidelity;
	CostFidelity(const double cost, const double fid) : mCost(cost), mFidelity(fid) {}
};
