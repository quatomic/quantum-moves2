		#pragma once

		#include <algorithm>
		#include <vector>

		namespace qengine
		{

			namespace wfcache
			{
				enum class Ordering {Forward, Backward};
			}

			template<typename Key, typename WaveFunction>
			class WaveFunctionCache
			{
				enum class CacheState {Init, Ready};
				
				using KWFPair = std::pair<Key, WaveFunction>;
			public:
				WaveFunctionCache(const wfcache::Ordering order) : mOrder(order) 
				{
					ConstructComparator();
				}

				Key GetLatestKey() 
				{ 
					return mLatestKey; 
				}

				WaveFunction GetLatestWF() 
				{ 
					return GetCachedWF( mLatestKey ); 
				}

				void UpdateCache(const Key & key, const WaveFunction & wf)
				{
					// empty cache
					if (Empty())
					{
						mLatestKey = key;
						mCacheVec.emplace_back(KWFPair{key, wf});
						return;
					}

					// we invalidate further cache entries
					if ( mKeyComparator(key, mLatestKey) || (key == mLatestKey))
					{
						EraseCacheFrom(key);
					}

					mCacheVec.emplace_back( KWFPair{key, wf} );

					mLatestKey = key;
				}


				const WaveFunction &  GetCachedWF(const Key & key)
				{
					const auto iter = std::find_if(mCacheVec.cbegin(), mCacheVec.cend(),  MakeLookup(key) );
					qengine_assert(mCacheVec.cend() != iter, "WaveFunction Cache doesn't have key: " + std::to_string(key));
					return iter->second;
				}

				bool HasKey(const Key & key)
				{
					const auto iter = std::find_if(mCacheVec.cbegin(), mCacheVec.cend(),  MakeLookup(key) );
					return (mCacheVec.cend() != iter);
				}

				void EraseCacheFrom(const Key & key)
				{
					auto iter = std::find_if(mCacheVec.begin(), mCacheVec.end(),  MakeLookup(key) );
					if (mCacheVec.cend() == iter)
					{
						return;
					}
					mLatestKey = ( iter - 1)->first;
					mCacheVec.erase(iter, mCacheVec.end());
				}

				bool Empty()
				{
					return mCacheVec.empty();
				}

				void PrintCacheState(const std::string & cacheName)
				{
					fprintf(stderr, "Size: %llu\n", mCacheVec.size());
					fprintf(stderr, "%s CacheState: mLatestKey: %llu \n", cacheName.c_str(), mLatestKey);
					fprintf(stderr, "CacheState: mLatestKey: %llu, [ ", mLatestKey);
					for (const auto & pair : mCacheVec)
					{
						fprintf(stderr, "%llu ", pair.first);
					}
					fprintf(stderr, "]\n");
				}

				virtual ~WaveFunctionCache() {}
			private:

				auto MakeLookup(const Key & key)
				{
					return [key] (const KWFPair & pair) { return pair.first == key; };
				}

				void ConstructComparator()
				{
					switch (mOrder)
					{
						case wfcache::Ordering::Forward:
						{
							mComparator = [] (const KWFPair & lhs, const KWFPair & rhs) { return lhs.first < rhs.first; };
							mKeyComparator = [] (const auto & lhs, const auto & rhs) { return lhs < rhs; };
							break;
						}
						case wfcache::Ordering::Backward:
						{
							mComparator = [] (const KWFPair & lhs, const KWFPair & rhs) { return lhs.first > rhs.first; };
							mKeyComparator = [] (const auto & lhs, const auto & rhs) { return lhs > rhs; };
							break;
						}
					}
				}

				bool CheckCacheOrdering()
				{
					return std::is_sorted(mCacheVec.cbegin(), mCacheVec.cend(), mComparator);
				}

				void ReorderCache()
				{
					std::sort(mCacheVec.begin(), mCacheVec.end(), mComparator);
				}


			private:
				std::unordered_map<Key, WaveFunction> mCache;
				std::vector<KWFPair> mCacheVec;
				Key mLatestKey;
				CacheState mState = CacheState::Init;
				wfcache::Ordering mOrder;
				std::function<bool(KWFPair const &, KWFPair const &)> mComparator;
				std::function<bool(size_t, size_t)> mKeyComparator;
			};	
		}