#pragma once
#include <functional>
#include <utility>
#include "bindings/Utilities.h"

namespace qengine
{
namespace utility
{


using CoordinateScalingFunc = typename std::function<double(double)>;
using CoordinateScalingFuncPair = std::pair<CoordinateScalingFunc, CoordinateScalingFunc>;

enum class ScalingType {FORWARD, INVERSE, IDENTITY};

template <typename Control, typename ControlPoint>
class Scaling
{
public:
	using FuncType = typename std::function<ControlPoint(ControlPoint)>;

	Scaling(const FuncType scale, const FuncType invScale);

	Scaling();

	Scaling(const Scaling & other);

	ControlPoint Scale(const ControlPoint &, const ScalingType) const;

	void ScaleInPlace(ControlPoint &, const ScalingType) const;

	Control MapScale(const Control &, const ScalingType) const;

	void MapScaleInPlace(Control &, const ScalingType) const;

	void SetScaling(const FuncType) noexcept;

	void SetInvScaling(const FuncType) noexcept;

	FuncType GetScalingFunc() const { return mfScale; }

	FuncType GetInvScalingFunc() const { return mfInvScale; }

	virtual ~Scaling() = default;

private:
	
	const FuncType & GetScalingFunc(const ScalingType scalingType) const
	{
		switch (scalingType)
		{
			case ScalingType::FORWARD:
			{
				return mfScale;
			}
			case ScalingType::INVERSE:
			{
				return mfInvScale;
			}
			case ScalingType::IDENTITY:
			{
				return mfIdentity;
			}
			default:
			{
				// silence the compiler
				qengine_assert(false, "Scaling of this type is undefined.");
				return mfIdentity;
			}
		}
	}

	FuncType mfScale;
	FuncType mfInvScale;
	FuncType mfIdentity;
};

inline double GetRangeScaling(const RVec & rangeVec)
{
	return rangeVec.at(1) - rangeVec.at(0);
}

template <typename HS>
double GetHilbertSpaceScaling(const HS & hs)
{
	const auto hsRange = hs.xUpper() - hs.xLower();
	qengine_assert(hsRange >= 0 , "Invalid (negative) scaling set by Hilbert Space");
	return hsRange;
}

template <class T>
/* constexpr (C++17) */ static std::function<T(T)> IdentityScaling = [] (const auto & v) { return v; };

template <class Control, class ControlPoint>
Scaling<Control, ControlPoint>::Scaling(const FuncType scale, const FuncType invScale) : mfScale(scale), mfInvScale(invScale), mfIdentity(IdentityScaling<ControlPoint>) { }

template <class Control, class ControlPoint>
Scaling<Control, ControlPoint>::Scaling(const Scaling & other) : mfScale(other.mfScale), mfInvScale(other.mfInvScale), mfIdentity(IdentityScaling<ControlPoint>) { }

template <class Control, class ControlPoint>
Scaling<Control, ControlPoint>::Scaling() : mfScale(IdentityScaling<ControlPoint>), mfInvScale(IdentityScaling<ControlPoint>), mfIdentity(IdentityScaling<ControlPoint>) { }

template <class Control, class ControlPoint>
ControlPoint Scaling<Control, ControlPoint>::Scale(const ControlPoint & point, const ScalingType scalingType) const
{
	const auto & foo = GetScalingFunc(scalingType);
	return foo( point );
}

template <class Control, class ControlPoint>
void Scaling<Control, ControlPoint>::ScaleInPlace(ControlPoint & point, const ScalingType scalingType) const
{
	const auto & foo = GetScalingFunc(scalingType);
	point = foo( point );
}


template <class Control, class ControlPoint>
Control Scaling<Control, ControlPoint>::MapScale(const Control & ctrl, const ScalingType scalingType) const
{
   Control scaledOutput(ctrl.dt());
   const auto & foo = GetScalingFunc(scalingType);
   for(auto i = 0u; i < ctrl.size(); ++i)
    {
        scaledOutput.append( Control(foo( ctrl.at( i )), ctrl.paramCount(), ctrl.dt() ) );
    }

    qengine_assert(scaledOutput.paramCount() == ctrl.paramCount(), "Rescaled controls' parameter counts must match.");
    qengine_assert(scaledOutput.size() == ctrl.size(), "Rescaled controls' sizes must match.");
    return scaledOutput;
}

template <class Control, class ControlPoint>
void Scaling<Control, ControlPoint>::MapScaleInPlace(Control & ctrl, const ScalingType scalingType) const
{
   const auto & foo = GetScalingFunc(scalingType);
   for(auto i = 0u; i < ctrl.size(); ++i)
    {
        ctrl.set(i, foo( ctrl.at( i )) );
    }
}

template <class Control, class ControlPoint>
void Scaling<Control, ControlPoint>::SetScaling(const FuncType scale) noexcept
{
	mfScale = scale;
}

template <class Control, class ControlPoint>
void Scaling<Control, ControlPoint>::SetInvScaling(const FuncType invScale) noexcept
{
	mfInvScale = invScale;
}

CoordinateScalingFuncPair GetRangeScalings(const RVec & rangeVec)
{
	// Produce a pair of scaling functions that normalize input vectors according to the extent of the input range
	qengine_assert(rangeVec.size() == 2, "Invalid range vector, unable to define a range scaling.");
	qengine_assert(rangeVec.at(1) >= rangeVec.at(0), "Range can't be an empty set.");
	const auto range = rangeVec.at(1) - rangeVec.at(0);
	return std::make_pair( 	[range] (const auto val) { return val / range; },
							[range] (const auto val) { return val * range; });
}

CoordinateScalingFuncPair GetCoordinateReferenceScalings(const RVec & p0, const RVec & p1, const size_t coordinate)
{
	// Produce a pair (forward, inverse) of linear scaling functions that take reference coordinates x0, x1 from input points p0, p1
	// such that: f(x0) = 0 and f(x1) - f(x0) = 1
	qengine_assert((p0.size() >= coordinate + 1) & (p1.size() >= coordinate + 1) , "Invalid RVec size for determining control scaling.");
	const double x0 = p0.at(coordinate);
	const double x1 = p1.at(coordinate);
	qengine_assert(x0 != x1, "Axis reference scaling has to refer to 2 different values.");
	const double a = 1.0 / (x1 - x0);
	const double b = x0 / (x0 - x1);
	return std::make_pair([a, b] (const double x) { return a * x + b; },
						  [a, b] (const double x) { return (x - b) / a; });

}	


}
}