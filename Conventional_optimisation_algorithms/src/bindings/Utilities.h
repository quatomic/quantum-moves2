#pragma once
#include <mutex>
#include "qengine/qengine.h"

namespace qengine
{


enum class WavePart {NORM_SQUARED, RE, IM, NONE};
enum class WavefuncType {TARGET, INIT, CURRENT};

using RVec = qengine::Vector<double>;

namespace utility
{
template <typename Func>
RVec MapRVec(const Func & foo, const RVec & input)
{
	RVec output(input.size());
	for (size_t i = 0 ; i < output.size() ; ++i)
	{
		output.at(i) = foo( input.at(i) );
	}
	return output;
}

using MutexType = std::recursive_mutex;

}

}