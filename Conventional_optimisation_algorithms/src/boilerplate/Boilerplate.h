# pragma once


#define BOILERPLATE1 \
DataContainer dc; \
 \
const real duration = Ts.get(i).at(0); \
const auto u_reference = constructLinearControl(duration,dt,initialControl,finalControl); \
const auto n_steps = u_reference.size(); \
const auto t = makeTimeControl(n_steps,u_reference.dt()); \
 \
const auto seeder = seedingMechanism; \
 \
auto makeProblem = [H{H},psi_0{psi_0},psi_t{psi_t},regularization,lowerBoundary,upperBoundary,boundaryStrength](const auto& u_in) \
{ \
    return makeStateTransferProblem(H,psi_0,psi_t,u_in) + regularization*Regularization(u_in) + boundaryStrength*Boundaries(u_in,lowerBoundary,upperBoundary); \
}; \
/*pre-selection of seeds */ \
auto u_best_candidate = seeder(); \
auto problemTemp  = makeProblem(u_best_candidate); \
real J_best = std::numeric_limits<double>::max(); \
for(int i = 0; i < nPreselectionSeeds; i++) \
{ \
     auto u_candidate = seeder(); \
     problemTemp.update(u_candidate); \
     if(problemTemp.cost() < J_best) \
     { \
        u_best_candidate = u_candidate; \
        J_best = problemTemp.cost(); \
     } \
} \
auto u = u_best_candidate; \
 \
std::string stopMsgLegend = "0 = iteration, 1 = fpp, 2 = fidelity, 3 = stepsize, fpp-fidelity checkpoint"; \
int stopCode = -1; \
const auto stopper = makeStopper([&stopCode,n_steps,fidelityThreshold,stepsizeThreshold,fppThreshold,iterationThreshold,fppCheckpoint,walltimeThreshold,elapsedHoursTotal,&stepSizeFinder](auto& opt) -> bool \
{ \
        bool stop = false; \
        if (opt.iteration() == iterationThreshold)                              {stop = true; stopCode = 0 ; std::cout << "Iteration threshold exceeded. Stopping." << std::endl; } \
        if (round(opt.problem().nPropagationSteps()/n_steps)  > fppThreshold)   {stop = true; stopCode = 1 ; std::cout << "fpp threshold exceeded. Stopping."       << std::endl; } \
        if (opt.problem().fidelity() > fidelityThreshold)                       {stop = true; stopCode = 2 ; std::cout << "fidelity threshold exceeded. Stopping."  << std::endl; } \
        if (opt.stepSize() < stepsizeThreshold && !stepSizeFinder.wrapped().didRollback()) {stop = true; stopCode = 3 ; std::cout << "stepsize threshold exceeded. Stopping."  << std::endl; } \
        if(fppCheckpoint.first < round(opt.problem().nPropagationSteps()/n_steps) && fppCheckpoint.second > opt.problem().fidelity()) \
                                                                                {stop = true; stopCode = 4 ; std::cout << "fidelity too low at fpp checkpoint. Stopping."  << std::endl; } \
        if(elapsedHoursTotal().count() > std::chrono::duration_cast<hours>(walltimeThreshold).count())  {stop = true; stopCode = 5 ; std::cout << "walltime threshold exceeded. Stopping."  << std::endl; } \
 \
        if(stop) std::cout << std::endl; \
        return stop; \
}); \
const auto collector = makeCollector([&printInterval,elapsedHoursTotal,&dc](auto& optimizer) \
{ \
    if(optimizer.iteration()%printInterval == 0) \
    { \
            auto n_steps = optimizer.problem().control().size(); \
            std::cout << \
                 "ITER "       << optimizer.iteration() << " | " << \
                 "fidelity : " << optimizer.problem().fidelity() << "\t " << \
                 "stepSize : " << optimizer.stepSize()   << "\t " << \
                 "fpp : "      << round(optimizer.problem().nPropagationSteps()/n_steps)   << "\t " << \
                 "walltime(hrs) : "   <<  elapsedHoursTotal().count() << "\t" << \
            std::endl; \
    } \
    try \
    { \
        dc["Ff"] = optimizer.problem().fidelity(); \
    } \
    catch(...) \
    { \
        dc["exception_thrown_from_fidelity"] = std::string("exception_thrown_from_fidelity"); \
    } \
}); \
 \



#define BOILERPLATE2 \
EventTimer timer;\
dc["Fi"] = opt.problem().fidelity();      \
dc["ui"] = opt.problem().control().mat(); \
dc["optimizertype"] = optimizerType;\
dc["dim"] = dim; \
dc["dt"]  = dt; \
\
auto launchRAII = [&]()\
{\
    collector(opt);\
    timer.tic("opt. time");\
    opt.optimize();\
};\
\
auto cleanRAII = [&]()\
{\
    printInterval = 1;\
    collector(opt);\
    dc["optimization_walltime"] = timer.toc();\
    dc["stopMsgLegend"] = stopMsgLegend;\
    dc["stopCode"] = stopCode;\
\
    dc["uf"] = opt.problem().control().mat();\
    dc["iter"] = opt.iteration();\
    dc["fpp"] = round(opt.problem().nPropagationSteps()/n_steps);\
    dc["nUnrollsPerformed"]   = stepSizeFinder.wrapped().nUnrollsPerformed();\
    dc["nRollbacksPerformed"] = stepSizeFinder.wrapped().nRollbacksPerformed();\
    dc["T"] = duration;\
\
    dc.save("seed_T" + std::to_string(T_index) + "_" + std::to_string(runNumber) + ".mat");\
};\
\
auto raii = makeRAII(launchRAII,cleanRAII);\
\
try\
{\
    raii.launch();\
}\
catch (const std::exception& e)\
{\
    std::cout << "exception caught: " << e.what() << std::endl;\
    dc["exception_thrown"] = std::string(e.what());\
}\
catch(...)\
{\
    std::cout << "fallthrough" << std::endl;\
}\

