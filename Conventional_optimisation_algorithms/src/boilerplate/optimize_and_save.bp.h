#include <qengine/qengine.h>
using namespace qengine;

EventTimer timer;
dc["Fi"] = opt.problem().fidelity();
dc["ui"] = opt.problem().control().mat();
dc["optimizertype"] = TranslateOptimizer( optimizerType );
dc["dim"] = dim;
dc["dt"]  = dt;

auto launchRAII = [&]()
{
    collector(opt);
    timer.tic("opt. time");
    opt.optimize();
};

auto cleanRAII = [&]()
{
    collector(opt);
    dc["optimization_walltime"] = timer.toc();
    dc["stopMsgLegend"] = stopMsgLegend;
    dc["stopCode"] = stopCode;

    RMat fidelityHistoryMat(fidelityHistory.size(),2,0.0);
    {
    count_t i=0;
    for(auto e:fidelityHistory)
    {
        fidelityHistoryMat.at(i,0) = e.first;
        fidelityHistoryMat.at(i,1) = e.second;
        i++;
    }
    dc["fidelityHistory"] = fidelityHistoryMat;
    }

    dc["nBFGSRestarts"] = nBFGSRestarts;
    dc["uf"] = opt.problem().control().mat();
    dc["iter"] = opt.iteration();
    dc["fpp"] = round(opt.problem().nPropagationSteps()/n_steps);
    dc["nUnrollsPerformed"]   = stepSizeFinder.wrapped().nUnrollsPerformed();
    dc["nRollbacksPerformed"] = stepSizeFinder.wrapped().nRollbacksPerformed();
    dc["T"] = duration;

    dc.save(saveName);
    std::cout << std::endl;
};

auto raii = makeRAII(launchRAII,cleanRAII);

try
{
    raii.launch();
}
catch (const std::exception& e)
{
    std::cout << "exception caught: " << e.what() << std::endl;
    dc["exception_thrown"] = std::string(e.what());
}
catch(...)
{
    std::cout << "fallthrough" << std::endl;
    dc["exception_thrown"] = std::string("fallthrough");
}

