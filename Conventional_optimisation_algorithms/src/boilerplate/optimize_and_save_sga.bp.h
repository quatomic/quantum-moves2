#include <qengine/qengine.h>
using namespace qengine;

EventTimer timer;
dc["Fi"] = opt.GetFidelity();
dc["ui"] = opt.GetCurrentControl().mat();
dc["optimizertype"] = TranslateOptimizer( optimizerType );
dc["dim"] = dim;
dc["dt"]  = dt;

auto start = std::chrono::high_resolution_clock::now();
evalsHistory.emplace_back(opt.GetCostEvals());
fidelityHistory.emplace_back(opt.GetFidelity());
costHistory.emplace_back(opt.GetCost());
regHistory.emplace_back(  regularization * Regularization( opt.GetCurrentControl() ).cost() );
timeMsHistory.emplace_back( (size_t) elapsedMsTotal().count() );

auto launchRAII = [&]()
{
    timer.tic("opt. time");
    opt.optimize();
};

auto cleanRAII = [&]()
{
    dc["optimization_walltime"] = timer.toc();
    dc["stopMsgLegend"] = stopMsgLegend;
    dc["stopCode"] = stopCode;

    dc["iter"] = opt.iteration();
    dc["T"] = duration;

    dc["ui"] = u.mat();
    dc["ui_optimizer"] = opt.GetInitialControl().mat();
    dc["uf"] = opt.GetOptimizedControl().mat();
    dc["uf_sga"] = opt.GetOptimizedControl().mat();
    dc["dt"] = dt;
    dc["Ff"] = opt.GetFidelity();
    dc["Ff_sga"] = opt.GetFidelity();
    dc["Cf"] = opt.GetCost();
    dc["Cf_sga"] = opt.GetCost();
    dc["Fi"] = opt.GetInitialFidelity();
    dc["iters"] = opt.GetIter();
    dc["access_order_sga"] = static_cast<int> (opt.GetAccessOrder());

    RMat fidelityHistoryMat(fidelityHistory.size(),2,0.0);
    {
    for (size_t i = 0 ; i < fidelityHistory.size() ; ++i)
    {
        fidelityHistoryMat.at(i,0) = i;
        fidelityHistoryMat.at(i,1) = fidelityHistory.at(i);
    }
    dc["fidelityHistory"] = fidelityHistoryMat;
    }

    RVec costHistoryRVec(costHistory.size());
    for (size_t i = 0 ; i < costHistoryRVec.size() ; ++i)
    {
        costHistoryRVec.at(i) = costHistory.at(i);
    }
    dc["cost_history"] = costHistoryRVec;  

    RVec regHistoryRVec(regHistory.size());
    for (size_t i = 0 ; i < regHistoryRVec.size() ; ++i)
    {
        regHistoryRVec.at(i) = regHistory.at(i);
    }
    dc["reg_history"] = regHistoryRVec; 

    RVec evalsHistoryRVec(evalsHistory.size());
    for (size_t i = 0 ; i < evalsHistory.size() ; ++i)
    {
        evalsHistoryRVec.at(i) = evalsHistory.at(i);
    }
    dc["evals_history"] = evalsHistoryRVec; 

    RVec timeMsHistoryRVec(timeMsHistory.size());
    for (size_t i = 0  ; i < timeMsHistory.size() ; ++i)
    {
        timeMsHistoryRVec.at( i ) = timeMsHistory.at( i );
    }                       
    dc["time_ms_history"] = timeMsHistoryRVec;

    RVec timeMsSGA_GlobalIterHistoryRVec(timeMsSGA_GlobalIterHistory.size());
    for (size_t i = 0  ; i < timeMsSGA_GlobalIterHistory.size() ; ++i)
    {
        timeMsSGA_GlobalIterHistoryRVec.at( i ) = timeMsSGA_GlobalIterHistory.at( i );
    }                       
    dc["time_ms_history_global_iter"] = timeMsSGA_GlobalIterHistoryRVec;   


    dc.save(saveName);
    std::cout << std::endl;
};

auto raii = makeRAII(launchRAII,cleanRAII);

try
{
    raii.launch();
}
catch (const std::exception& e)
{
    std::cout << "exception caught: " << e.what() << std::endl;
    dc["exception_thrown"] = std::string(e.what());
}
catch(...)
{
    std::cout << "fallthrough" << std::endl;
    dc["exception_thrown"] = std::string("fallthrough");
}

