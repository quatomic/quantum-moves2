            switch (optimizerType)
            {
                case OptimizerType::GRAPE:
                {
                        auto stepSizeFinder = makeInterpolatingStepSizeFinder(stepsizemax,stepsizeinitial,linesearchEpsilon);

                        #include "boilerplate/optimizer_settings.bp.h"
                        auto problem = makeProblem(u);
                        auto opt = makeGrape_bfgs_L2(problem,stopper,collector,stepSizeFinder,bfgsRestarter);
                        #include "boilerplate/optimize_and_save.bp.h"
                        break;
                }
                case OptimizerType::SGA:
                {
                    const sga::AccessOrder order = sga::AccessOrder::Random;

                    #include "boilerplate/optimizer_settings_sga.bp.h"

                    auto dcPack = sga::makeDiscretizedControlPack(u, cacheDiscretized);
                    u = dcPack.mDiscretizedReference;

                    SGA <decltype(psi_0), decltype(H), decltype(costFun), decltype(stopper), decltype(collectorSGA), decltype(leftCachingStepper)> opt(H, dcPack, psi_0, psi_t, stopper, collectorSGA, order, costFun, leftCachingStepper, rightCachingStepper);
                    #include "boilerplate/optimize_and_save_sga.bp.h"
                    break;
                }
                case OptimizerType::SGA_BUCKET:
                {
                    const sga::AccessOrder order = sga::AccessOrder::Random;

                    #include "boilerplate/optimizer_settings_sga.bp.h"

                    auto dcPack = sga::makeDiscretizedControlPack(u, cacheDiscretized);
                    u = dcPack.mDiscretizedReference;

                    SGA <decltype(psi_0), decltype(H), decltype(costFun), decltype(stopper), decltype(collectorSGA), decltype(leftCachingStepper)> opt(H, dcPack, psi_0, psi_t, stopper, collectorSGA, order, costFun, leftCachingStepper, rightCachingStepper,
                        sgaBuckets);
                    #include "boilerplate/optimize_and_save_sga.bp.h"
                    break;
                }                 
                default :
                {
                    throw std::runtime_error("Unknown optimizer type.");
                }
            }