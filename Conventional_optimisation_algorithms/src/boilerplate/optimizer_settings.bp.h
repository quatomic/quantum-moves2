#include <qengine/qengine.h>
using namespace qengine;

std::string stopMsgLegend = "-1 = exception, 0 = iteration, 1 = fpp, 2 = fidelity, 3 = stepsize, 4 = fpp-fidelity checkpoint, 5 = walltime";
int stopCode = -1;
const auto stopper = makeStopper([&stopCode, optimizerType, n_steps,fidelityThreshold,stepsizeThreshold,fppThreshold,iterationThreshold,fppCheckpoint,walltimeThreshold,walltimeThresholdAll,elapsedHoursTotal,elapsedHours,nTs,&stepSizeFinder](auto& opt) -> bool
{
        bool stop = false;
        if (opt.iteration() == iterationThreshold)                              {stop = true; stopCode = 0 ; std::cout << "Iteration threshold exceeded. Stopping." << std::endl; }
        if (round(opt.problem().nPropagationSteps()/n_steps)  > fppThreshold)   {stop = true; stopCode = 1 ; std::cout << "fpp threshold exceeded. Stopping."       << std::endl; }
        if (opt.problem().fidelity() > fidelityThreshold)                       {stop = true; stopCode = 2 ; std::cout << "fidelity threshold exceeded. Stopping."  << std::endl; }
        if (opt.stepSize() < stepsizeThreshold ) {stop = true; stopCode = 3 ; std::cout << "stepsize threshold exceeded. Stopping."  << std::endl; }        
        if(fppCheckpoint.first < round(opt.problem().nPropagationSteps()/n_steps) && fppCheckpoint.second > opt.problem().fidelity())  {stop = true; stopCode = 4 ; std::cout << "fidelity too low at fpp checkpoint. Stopping."  << std::endl; }
        if(elapsedHoursTotal().count() > std::chrono::duration_cast<hours>(walltimeThresholdAll).count())  {stop = true; stopCode = 5 ; std::cout << "walltime threshold exceeded. Stopping."  << std::endl; }
        if(elapsedHours().count()      > std::chrono::duration_cast<hours>(walltimeThreshold).count())  {stop = true; stopCode = 5 ; std::cout << "walltime threshold exceeded. Stopping."  << std::endl; }
        return stop;
});
                
std::cout.precision(6);
count_t nBFGSRestarts = 0;
std::vector<std::pair<count_t,real>> fidelityHistory;
const auto collector = makeCollector([printInterval,elapsedHoursTotal,&dc, &nBFGSRestarts,&fidelityHistory](auto& optimizer)
{
    if(optimizer.iteration()%printInterval == 0)
    {
            auto n_steps = optimizer.problem().control().size();
            std::cout <<
                 "I "       << optimizer.iteration() << " | " <<
                 "F : " << optimizer.problem().fidelity() << "\t " <<
                 "Cost: " << optimizer.problem().cost() << "\t " <<
                 "step : " << optimizer.stepSize()   << "\t " <<
                 "fpp : "      << round(optimizer.problem().nPropagationSteps()/n_steps)   << "\t " <<
                 "bfgsRestart : " << optimizer.didRestart() << "\t " <<
                 "|g| : " << optimizer.problem().gradient().norm() << "\t " <<
                 "hrs : "   <<  elapsedHoursTotal().count() << "\t" <<
            std::endl;

    }

    if(optimizer.didRestart()) nBFGSRestarts++;

    try
    {
        fidelityHistory.emplace_back(optimizer.iteration(),optimizer.problem().fidelity());
        dc["Ff"] = optimizer.problem().fidelity();
    }
    catch(...)
    {
        dc["exception_thrown_from_fidelity"] = std::string("exception_thrown_from_fidelity");
    }
});

