#include <qengine/qengine.h>
using namespace qengine;

auto costFun = 
[regularization] (const auto & lhs, const auto & rhs, const auto & u_in) 
{ 
    const double fidelity = qengine::fidelity(lhs, rhs);
    return 1.0 - fidelity;// + regularization * Regularization(u_in).cost(); 
};


std::string stopMsgLegend = "-1 = exception, 0 = iteration, 1 = fpp, 2 = fidelity, 3 = stepsize, 4 = fpp-fidelity checkpoint, 5 = walltime, 6 = no changes in controls";
int stopCode = -1;
const auto stopper = makeStopper([&stopCode, optimizerType, n_steps,fidelityThreshold,stepsizeThreshold,fppThreshold,iterationThreshold,fppCheckpoint,walltimeThreshold,walltimeThresholdAll,elapsedHoursTotal,elapsedHours,nTs](auto& opt) -> bool
{
        bool stop = false;
        if (opt.iteration() == iterationThreshold)                              {stop = true; stopCode = 0 ; std::cout << "Iteration threshold exceeded. Stopping." << std::endl; }
        if (opt.GetFidelity() > fidelityThreshold)                       {stop = true; stopCode = 2 ; std::cout << "fidelity threshold exceeded. Stopping."  << std::endl; }
        
        if (opt.GetDefaultStopCriterion()) { stop = false; stopCode = 6; }
         
        if(elapsedHoursTotal().count() > std::chrono::duration_cast<hours>(walltimeThresholdAll).count())  {stop = true; stopCode = 5 ; std::cout << "walltime threshold exceeded. Stopping."  << std::endl; }
        if(elapsedHours().count()      > std::chrono::duration_cast<hours>(walltimeThreshold).count())  {stop = true; stopCode = 5 ; std::cout << "walltime threshold exceeded. Stopping."  << std::endl; }
        return stop;
});
                
std::cout.precision(6);
std::vector<double> fidelityHistory;
std::vector<double> costHistory;
std::vector<double> regHistory;
std::vector<size_t> evalsHistory;
std::vector<size_t> timeMsHistory;
std::vector<size_t> timeMsSGA_GlobalIterHistory;

auto collectorSGA = [&fidelityHistory, &costHistory, regularization, &regHistory, &evalsHistory, &timeMsHistory, &timeMsSGA_GlobalIterHistory, elapsedMsTotal] 
    (auto & alg) mutable
    {
        switch(alg.GetIterState())
        {
            case sga::IterState::Global:
            {
                timeMsSGA_GlobalIterHistory.emplace_back( (size_t) elapsedMsTotal().count() );
                break;
            }
            case sga::IterState::Sub:
            {
                evalsHistory.emplace_back(alg.GetCostEvals());
                fidelityHistory.emplace_back(alg.GetFidelity());
                costHistory.emplace_back(alg.GetCost());
                regHistory.emplace_back(  regularization * Regularization( alg.GetCurrentControl() ).cost() );
                timeMsHistory.emplace_back( (size_t) elapsedMsTotal().count() );
                break;
            }
            default:
            {
                throw std::runtime_error("Invalid interation state of SGA.");
            }
        }
    };
