    // rng for names
    unsigned seed1 = std::chrono::system_clock::now().time_since_epoch().count();
    std::mt19937 g1 (seed1); 
    std::uniform_int_distribution<size_t> distribution(0, 1e9);
    auto makeRandName = [g1, distribution] ()  mutable
    {
        return std::to_string( distribution( g1 ) );
    };


    #define saveNameMacro \
    "seed_T" + std::to_string(T_index) + "_" + std::to_string(T_subindex) + "_" + std::to_string(runNumber) + "_" + std::to_string(walltimeThreshold.count()) + makeRandName() +  "_" + TranslateOptimizer(optimizerType) + "_" + TranslateSeedingType(seedType) + ".mat"

    #define saveNameMacroJSON \
    "seed_T" + std::to_string(T_index) + "_" + std::to_string(T_subindex) + "_" + std::to_string(runNumber) + "_" + std::to_string(walltimeThreshold.count()) + makeRandName() + "_" + TranslateOptimizer(optimizerType) + ".json"
