#include <qengine/qengine.h>
using namespace qengine;

auto u_best_candidate = seeder();
count_t harmonic_best_candidate = 0;
real randomNormalizerBestCandidate = 1.0;
if (!u_ref_Rescaled)
{
	u_best_candidate = controlScaling.MapScale(u_best_candidate, utility::ScalingType::FORWARD);	
}

ImposeBoundaries(u_best_candidate, leftBoundary, rightBoundary);

auto problemTemp  = makeProblem(u_best_candidate);
real J_best = std::numeric_limits<double>::max();

for (int refInd = 0 ; refInd < nReferenceCandidates ; ++refInd)
{
	auto newRef = constructRefControl();
	seeder.SetReferenceControl(newRef);
	for(int seedIndex = 0; seedIndex < nPreselectionSeeds; seedIndex++)
	{

		auto u_candidate = seeder();
		
		if (!u_ref_Rescaled)
		{
			u_candidate = controlScaling.MapScale(u_candidate, utility::ScalingType::FORWARD);
		}

		ImposeBoundaries(u_candidate, leftBoundary, rightBoundary);	

		problemTemp.update(u_candidate);
		if(problemTemp.cost() < J_best)
		{
			u_best_candidate = u_candidate;
			J_best = problemTemp.cost();
		}
		
	}
}


seeder.SetReferenceControl( u_best_candidate );
for (int reimpr = 0 ; reimpr < nRandomizedReimprovements ; ++reimpr)
{
		auto u_candidate = seeder();
		
		if (!u_ref_Rescaled)
		{
			u_candidate = controlScaling.MapScale(u_candidate, utility::ScalingType::FORWARD);
		}

		ImposeBoundaries(u_candidate, leftBoundary, rightBoundary);	

		problemTemp.update(u_candidate);
		if(problemTemp.cost() < J_best)
		{

			u_best_candidate = u_candidate;

			J_best = problemTemp.cost();

		}
}


u = u_best_candidate;
