# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from scipy.interpolate import interp1d
import json
import matplotlib.pyplot as plt
import os
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import pickle

from sklearn.cluster import KMeans, DBSCAN, OPTICS
from matplotlib.backends.backend_pdf import PdfPages

from importlib import reload  # Python 3.4+ only.
import CVUtilities as cval

curves_all = np.genfromtxt('../FT-curves/Challenge_curves_QM2.csv', delimiter=';')

# file with general player seeds
#filename_player = '/QM2_Serialized_Datasets_Algorithm_Seeding/playerData/upso_serialized.csv'
filename_player = '/QM2_Serialized_Datasets_Algorithm_Seeding/playerData/upso_serialized.csv'

df = pd.read_csv(filename_player, sep = '|')
print(df.columns)

#########################
# chose level here

load_paths = True  # load paths from pickled binary

n_poi = 1000

x_ar = np.linspace(0,1,n_poi)

symetrize_controls = False

cmap_str = 'PuOr'

poi_str = ['s','.','v','^','>','<','1','2','3','4','*','x']
mark_sizes = [3, 4,  5,8,8,8, 10,10,10,10, 10]


##################
cval = reload(cval)
pdc = cval.PhysicsDataCollector()

jsonListFile = "./sim_input_file_list.json"
programPath = "/PhysicsVisualization/psiTest.exe"

# Where output .json data is saved
# Output .json data is the 'standard' physics visualization outputs
# composed of the wavefunction, level data, fidelity, duration etc.
outputDataDirectory = "./output_paths"

# Name of the output .json file name
outputCSVFile = "./sim_output_fidelity_"

with open('path_template_new.json', 'r') as fid:
    path_temp = json.load(fid)      # path template file

simulate = False
run_simulation = False

run_mean_subtraction = False

for level_order in [0]: # 0,1,2 : BWH, Splitting, Shake-Up
    
    level_name = ['Bring-Home-Water', 'Splitting', 'Shake-Up'][level_order]
    
    ctrl_tag = ['physics_x', 'physics_y', 'physics_x'][level_order]
    
    time_lim = [(0.24, 0.32), (0.9, 1.0), (0.8, 0.9)][level_order]
    
    x_ev_ran = [[-3,3], [-2,2], [-2, 2]][level_order]  # spatial evolution range
    T_QSL = [0.37, 0.92, 0.89][level_order]
    dt = [0.00035, 0.001, 0.001][level_order]
    u_bound = [(-2, 2), (0, 1), (-1,1)][level_order]
    
    level_id = [16, 11, 7][level_order] # 7: Shake-Up; 11: Splitting; 16: BWH
    
    challenge_fun = interp1d(curves_all[:,0], curves_all[:,level_order + 1])
        
    df_l = df[(df['levelId'] == level_id) & (df['Duration']*T_QSL <= time_lim[1]) & (df['Duration']*T_QSL > time_lim[0])].copy()
    
    print('Number of paths in the time bin: %d' % (len(df_l),))
    
    df_l['challenge_fidelity'] = df_l['Duration'].apply(challenge_fun)
    df_l = df_l[(df_l['FinalFidelity'] > 0.95) & (df_l['FinalFidelity'] < 0.999)]  # only paths that did not hit the hard optimizer boundary 0.999
    
    print('out of those, number of paths within 2 percent from the challenge curve: %d' % (len(df_l),))
    
#    df_l = df_l.iloc[:10]  # resctrict the number of paths
        
    with open('basic_path.pckl', 'wb') as fid:
        pickle.dump(df_l, fid)
    
    plt.figure()
    plt.title('sampling density')
    plt.hist(df[df['levelId'] == level_id]['Duration'], 51)
    plt.xlabel('Duration [T_SL]')
    plt.ylabel('number of paths')
    
    plt.figure()
    plt.plot(df_l['Duration'],df_l['FinalFidelity'],'.')
    plt.plot(curves_all[:,0], curves_all[:,level_order + 1],'.-')
    plt.xlabel('duration [T_SQL]')
    plt.ylabel('fidelity')
    plt.legend(['data', 'challenge curve'])
    
    plt.figure()
    plt.semilogy(df_l['Duration']*T_QSL, 1 - df_l['FinalFidelity'],'.')
    plt.xlabel('duration [sim. units]')
    plt.ylabel('infidelity')
        
    pckl_name = 'df_path_%d_more_pop.pckl' % (level_id,)
    
    if load_paths:
        with open(pckl_name, 'rb') as fid:
            df_p, n_pop_list = pickle.load(fid)
    
    else:  # load and interpolate paths form the file
        df_p = pd.DataFrame(columns = range(n_poi))
        
        n_pop_list = []
        for i, i_ in enumerate(df_l.index):
            path = df_l.loc[i_]
            
            filename = path['filename'].strip("'") 
            
            if np.mod(i, 100) == 1:
                print('Processed ', i, 'paths out of', len(df_l), 'for level', level_name)
            
            with open(filename, 'r') as f:
                data = json.load(f)
         
                n_pop = 0
                y_i = list(data[ctrl_tag])
                for y_k in data[ctrl_tag]:
                    if y_k < 0:
                        y_i.pop(0)
                        n_pop += 1
                    else:  # stop poping once the tweezer goes over the middle
                        break                 
                
                n_pop_list.append(n_pop)
                
                x_i = np.linspace(0,1,len(y_i))
                    
                f_u = interp1d(x_i, y_i)  # the control function 
                y = f_u(x_ar)
                
                df_p.loc[path['path_id']] = y
        
        with open(pckl_name, 'wb') as fid:
            pickle.dump((df_p, n_pop_list),fid)
            
    ############################################        
    # limit the number of paths
    
    if symetrize_controls and (level_id == 7):  # add inverse of all controls
        df_p = pd.concat([df_p, -df_p])
        
    
    dur_pop =  - np.array(n_pop_list)*dt
        
    i_sel = (df_l['Duration']*T_QSL > 0.24) & (df_l['FinalFidelity'] > 0.95)
    
    n_pop_ar = np.array(n_pop_list)[i_sel]
    
    fidelity = df_l['FinalFidelity'].loc[i_sel].values
    duration = df_l['Duration'].loc[i_sel].values * T_QSL
    
    dur_pop = duration - n_pop_ar*dt
    
    clust = DBSCAN(eps = 3) #, min_samples = 10)
#    clust = KMeans(n_clusters = 7)
    
    x = df_p.values[i_sel,:]
    #x = StandardScaler().fit_transform(x)  
    
    clust.fit(x)
    
    un_clust = np.unique(clust.labels_)
    
    plt.figure()
    plt.semilogy(duration, 1 - fidelity,'.')
    
    new_fig = plt.gcf().number + 1
    
    T_ref = 0.24
    x_fit = np.linspace(-0.02,0.08, 300)
    
    clust_d = {'un_clust': un_clust.tolist()}
    
    clust_leg = []
    mean_dist = {}
    i_best_ar = []
    fit_pars = []
    for i_c, p_, m_s in zip(un_clust,poi_str, mark_sizes):
#        if i_c != -1:
        is_clust = clust.labels_ == i_c
        x_cl = x[is_clust,:]
        u_mean = np.mean(x_cl, axis = 0)
        u_std = np.std(x_cl, axis = 0)
        
        mean_ar = np.dot(np.ones((x.shape[0],1)), u_mean.reshape((1, n_poi)))
        
        mean_dist[i_c] = np.sqrt(np.mean((x - mean_ar)**2, axis = 1))  # euclidean metric
#        mean_dist[i_c] = np.mean((x - mean_ar), axis = 1)   # additive metric
        
        plt.figure(new_fig)
        plt.plot(x_ar, u_mean)
        plt.fill_between(x_ar, u_mean + u_std, u_mean - u_std, alpha=0.5)
        
        lab_ = str(i_c) + '; n=' + str(sum(is_clust))
        clust_leg += [lab_]
        
        plt.figure(new_fig + 1)
        plt.semilogy(dur_pop[is_clust], 1 - fidelity[is_clust], p_ , markersize = m_s, label = lab_)   
        
        clust_d[str(i_c)] = {'u_mean':u_mean.tolist(), 'u_std': u_std.tolist(), 'x_ar': x_ar.tolist(),
                            'lab':lab_, 'markersize': m_s, 'poi_str': p_, 'dur': dur_pop[is_clust].tolist(),
                            'fid': fidelity[is_clust].tolist()}
        
        if i_c != -1:
            i_best = np.argmax(fidelity*is_clust)
            i_best_ar.append(i_best)  # append intex of the highest fidelity solution from the cluster (has duration close to the longest)
            
            print("Best fidelity in cluster %d is %0.5f and it's duartion is %0.3f" %(i_c, fidelity[i_best], dur_pop[i_best]))
            
            y_ = np.log10(1 - fidelity[is_clust])
            x_ = dur_pop[is_clust] - T_ref
            
            par = np.polyfit(x_, y_, 1)
            fit_pars.append(par)
            lab_fit = 'y = 10^(%0.2f %0.1f*(T - %0.2f))' %(par[1], par[0], T_ref)
            print(lab_fit)
            y_fit = 10**np.polyval(par, x_fit)
        
            plt.semilogy(x_fit + T_ref, y_fit, '--k', label = str(i_c) + '; ' + lab_fit)
            
            clust_d[str(i_c)]['x_fit'] = (x_fit + T_ref).tolist()
            clust_d[str(i_c)]['y_fit'] = y_fit.tolist()
            clust_d[str(i_c)]['fit_lab'] = lab_fit
            
        
        plt.figure(new_fig + 2)
        plt.plot(duration[is_clust], n_pop_ar[is_clust]*dt, '.')        
        
        plt.figure(new_fig + 4 + i_c)
        plt.plot(x_cl[:30,:].T)
        plt.title(str(i_c) + ' cluster examples')
        
#            
#            plt.plot(x_ar, u_mean + u_std,'--')
#            plt.plot(x_ar, u_mean - u_std, '--')

    clust_d['clust_leg'] = clust_leg
    
    with open('bhw_clustering_data.json', 'w') as file_id:
        json.dump(clust_d, file_id)

    plt.figure(new_fig + 1)
#    plt.legend(clust_leg)
    plt.legend()
    plt.xlabel('Duration [sim. units]')
    plt.ylabel('Infidelity')
    plt.ylim((0.001, 0.05))
    plt.xlim((0.23, 0.32))
    
    plt.figure(new_fig)
    plt.xlabel('Duration [rescaled to 1]')
    plt.ylabel('tweezer position')
    plt.legend(clust_leg)
    
    
    
    plt.figure()
    plt.hist(clust.labels_)
        
    plt.figure()
    for i_c in un_clust:
        is_clust = clust.labels_ == i_c
        plt.plot(mean_dist[0][is_clust],mean_dist[1][is_clust],'.')
    
    plt.xlabel('Distance from 0 cluster mean')
    plt.ylabel('Distance from 1 cluster mean')
    plt.legend(clust_leg)
    plt.plot([0,1],[0,1])
    
    ############
    dist_sum = mean_dist[1] + mean_dist[0]  # x_2 + x_1 : approximate mesure of spikiness
    dist_dif = mean_dist[1] - mean_dist[0]  # x_2 - x_1 : simplified clustering parameter
    cl_simp_idx = 1*(dist_dif < 0)  # clustering index based on nearest cluster
    
    plt.figure()
    for i_c in [0,1]:
        is_clust = cl_simp_idx == i_c        
        infid_ref = np.polyval(fit_pars[i_c], dur_pop[is_clust] - T_ref)
        infid_ratio = 10**(np.log10(1 - fidelity[is_clust]) - infid_ref)  # ratio of infidelity to the cluster infidelity
        
        plt.semilogy(dist_sum[is_clust], infid_ratio, '.') #'10**infid_diff,'.')
    
    plt.xlabel('Distance sum from cluster mean 0 and 1')
    plt.ylabel('infidelity ref. ratio')
    plt.legend([0, 1])
    
    ##############################################################
    # scan the principal component
    if simulate:

        path = path_temp.copy()
          # set horizontal control to 0
        
        x_recon = x
        
        outputCSVFile_lev = outputCSVFile + level_name + '_no_filter.csv'        
        
        if run_simulation:
            inp_list = []
            for i_ in range(int(x_recon.shape[0])):
                T_path = df_l['Duration'].iloc[i_]
                n_poi_path = int(np.round(T_path*T_QSL/dt))
                
                x_path = np.linspace(0, 1, n_poi_path)
                
                path_fun = interp1d(x_ar, x_recon[i_,:])
                u_path = path_fun(x_path)
            
                path['level_id'] = level_id
            
                path['physics_x'] = list(u_path)
                path['physics_y'] = [-150 for x_ in x_path]
                
                inp_name = 'input_paths/input_path_%d_no_filter.json' %(i_,) 
                inp_list.append(inp_name)
            
                with open(inp_name, 'w') as fid:
                    json.dump(path, fid)
                        
            ########################################
            # save the input files    
            out_list = [n_.replace('input_path','output_path') for n_ in inp_list]
            inp_dict = {"seed_filenames" : inp_list, "output_filenames": out_list}
            
            with open(jsonListFile, 'w') as fid:
                json.dump(inp_dict, fid)
            
            # Launch the program
            pdc.ProcessControlsJSON(jsonListFile = jsonListFile, 
                                    programPath = programPath, 
                                    outputDataDirectory = outputDataDirectory,
                                   outputCSVFile = outputCSVFile_lev)
        
        # Take a peek at the output csv file
    
        data = pd.read_csv(outputCSVFile_lev, delimiter = '|')

        print(data)
        n_path_sim = len(data['fidelity'])
        
        data['duration'] = list(df_l['Duration'].iloc[:n_path_sim])
        data['fidelity_raw'] = list(df_l['FinalFidelity'].iloc[:n_path_sim])
        
        plt.figure()
        plt.plot(data['duration'], data['fidelity'],'.')
        plt.plot(data['duration'], data['fidelity_raw'],'.')
        plt.xlabel('duration')
        plt.ylabel('fidelity')
        
        plt.legend(['after interpolation', 'before interpolation'])
        
        plt.figure()
        fidelity_degradation = data['fidelity']/data['fidelity_raw'] - 1
        plt.plot(data['duration'], fidelity_degradation,'.')
        plt.xlabel('duration')
        plt.ylabel('fidelity degradation')
        plt.title('Mean fidelity degradation: %0.3f' % np.mean(fidelity_degradation ))
#        i_max = np.argmax(data['fidelity'].values)
        mean_sub_file = 'control_mean_subtracted_' + level_name + '_no_filter.pckl'
        
        u_sub_ar = [] 
#        for i_max in range(n_path_sim):
        for i_c, i_max in zip([0,1],i_best_ar):
            ##################################################
            # examine the chosen wavefunction evolution 
            
            with open('output_paths/output_path_%d_no_filter.json' %(i_max,), 'r') as fid:
                test_path = json.load(fid)
                
            psi_re = np.array(test_path['psi_re'])
            psi_im = np.array(test_path['psi_im'])
            pot = np.array(test_path['potential'])
            
            psi_dens = psi_re**2 + psi_im**2
            
            n_x = psi_dens.shape[1]
            n_t = psi_dens.shape[0]
            
            t_vec = np.linspace(0,1,n_t)
            x_vec = np.linspace(x_ev_ran[0], x_ev_ran[1],n_x)  # the wavefunction is evolved in the range [-3, 3]
            
            psi_x_ar, t_ar = np.meshgrid(x_vec, t_vec)
            
            psi_x = np.sum(psi_dens * psi_x_ar, axis = 1)/psi_dens.sum(axis = 1)
            
            aux, psi_x_mean = np.meshgrid(x_vec, psi_x)
            
            psi_std = np.sqrt(np.sum(psi_dens * (psi_x_ar - psi_x_mean)**2, axis = 1)/psi_dens.sum(axis = 1))
    
            pos_fun = interp1d(t_vec, psi_x)
            pos_int = pos_fun(x_ar)
            
            u_sub = x[i_max,:] - pos_int
            
#            u_sub_ar.append(u_sub * np.sign(u_sub[-2]))  #correct with sign with previous to last control value
            u_sub_ar.append(u_sub)
            
            print('Analysing %d-th path.' %(i_max,))
            
            if (i_max < 10) or True:
                ###########################
                # calculate classical trajectory

                mass = 1.0 
                dt = 0.00035  #t_vec[1] - t_vec[0]
                pot_scale = 380 # scaling of the potential to make it -130 deep; 380 = 130/0.34210526315789475; potential depth is 0.34210526315789475 in sim and 130 in the game

                x1 = 1 # initial position = 1
                x2 = 0 # initial velocity = 0                

                # verify scaling
                scale_fac = 1.0
                
                x_vec *= scale_fac  
                x1 *= scale_fac
                dt *= scale_fac  # time scales linearly with distance

                #####################
                dx = x_vec[1] - x_vec[0]
                
                def f1(x1, x2):
                    return x2
                
                x_s = np.zeros((n_t,))  
                v_s = np.zeros((n_t,))  
                for i_s in range(1, n_t): # 4th order Runge-Kutta
                    # dx1/dt = f1(x1, x2)
                    # dx2/dt = f2(x1, x2)
                    
                    force_fun = interp1d((x_vec[1:] + x_vec[:-1])/2, - pot_scale*(pot[i_s,1:] + pot[i_s - 1,1:] - pot[i_s,:-1] - pot[i_s - 1,:-1])/dx/2)
                    
                    def f2(x1, x2):
                        return force_fun(x1)/mass
                    
                    k11 = dt*f1(x1, x2)
                    k21 = dt*f2(x1, x2)
                    k12 = dt*f1(x1 + 0.5*k11, x2 + 0.5*k21)
                    k22 = dt*f2(x1 + 0.5*k11, x2 + 0.5*k21)
                    k13 = dt*f1(x1 + 0.5*k12, x2 + 0.5*k22)
                    k23 = dt*f2(x1 + 0.5*k12, x2 + 0.5*k22)
                    k14 = dt*f1(x1 + k13, x2 + k23)
                    k24 = dt*f2(x1 + k13, x2 + k23)
                    x1 += (k11 + 2*k12 + 2*k13 + k14)/6
                    x2 += (k21 + 2*k22 + 2*k23 + k24)/6
                    
                    v_s[i_s] = x2
                    x_s[i_s] = x1
                
                ##############################
                n_sub_plt_y = 3
                n_sub_plt_x = 2
        
                plt.figure()
                plt.subplot(n_sub_plt_y,n_sub_plt_x ,1)
                plt.plot(x_ar, x[i_max,:])
    #            plt.plot(x_ar, x_recon[i_max,:],'--')
                plt.xlim([0,1])
        
                plt.plot(t_vec, psi_x,'-')
                plt.plot(t_vec, psi_x + psi_std,'--')
                plt.plot(t_vec, psi_x - psi_std,'--')
                
                # plot classical trajectory
                plt.plot(t_vec, x_s)
                plt.legend(['u','x qu.', 'std.','std.', 'x cl.'])
                
                plt.subplot(n_sub_plt_y,n_sub_plt_x ,6)
                plt.plot(t_vec, v_s)
                plt.title('particle classical velocity')
        
                plt.subplot(n_sub_plt_y,n_sub_plt_x,2)
                plt.imshow(np.flipud(psi_dens.T))
                plt.title('density evolution')
                
                plt.subplot(n_sub_plt_y,n_sub_plt_x,4)
                plt.imshow(np.flipud(pot.T))
                plt.title('potential evolution')
            
                plt.subplot(n_sub_plt_y,n_sub_plt_x,3)
                # subtract the mean position of the wavepacket from the control
                
                plt.plot(x_ar, x[i_max,:] - pos_int)
                plt.plot(x_ar, 0*pos_int, 'k')
                plt.plot(t_vec, 5*psi_std,'--')
                plt.ylim([-2,2])
                
                plt.xlim([0,1])
                
                plt.subplot(n_sub_plt_y,n_sub_plt_x,5)
                plt.plot(x_vec, pot[30,:])
                plt.title('potential at t = ' + str(x_ar[30]))
                
                ####################################
                plt.subplot(n_sub_plt_y, n_sub_plt_x, 5)
                plt.plot(x_vec, pot[0,:])
                
                def gauss(x, sig):
                    return np.exp(-x**2/(2*sig**2))
                
                pot_A = -130.0/380.0
                
                plt.plot(x_vec, pot[0,:].max() + pot_A*gauss(x_vec - 1, 0.25) + pot_A*gauss(x_vec + 1, 0.25), '--')

#                plt.title('initial potential')
                
    #            fig = plt.gcf()
    #            fig.set_size_inches(6.5, 8.5)
                
                mngr = plt.get_current_fig_manager()
                # to put it into the upper left corner for example:
                mngr.window.setGeometry(50,100,640, 845)
                
                #####################
                F = fidelity[i_max]
                T = dur_pop[i_max]
                T_max = 0.32
                
                extent = [-3, 3, 0, T]
                
                plt.figure()
                plt.imshow(np.flipud(psi_dens), extent=extent, aspect = np.abs((extent[1]-extent[0])/(extent[3] - extent[2])))
                plt.plot(x_s, t_vec*T, 'k--')
                plt.plot(x[i_max,:], x_ar*T, color="orange")
                plt.ylabel('time [sim. un.]')
                plt.xlabel('position [sim. un.]')
                plt.xlim([-1.5, 1.5])
                plt.ylim([0, 0.32])
                plt.title('cluster %d; F = %0.4f; T = %0.3f' %(i_c, F, T) )
                
            elif not run_mean_subtraction:
                break
                
        if run_mean_subtraction:
            with open(mean_sub_file, 'wb') as fid:
                pickle.dump(u_sub_ar, fid)
             
        with open(mean_sub_file, 'rb') as fid:
            u_sub_ar = pickle.load(fid)
            
        ##############################################    
        u_sub_ar = np.array(u_sub_ar)
        
        # correct the sign again
#        for i_u in range(n_path_sim):
#            u_sub_ar[i_u, :] *= np.sign(u_sub_ar[i_u,-100:].mean())
        
        plt.figure()
        plt.plot(u_sub_ar[:30,:].T)
        
        perform_dist_scan = False
        if perform_dist_scan:
            eps_ar = np.linspace(1,20,30)
            n_clust_ar = np.zeros((10, len(eps_ar)))
            for i_, e_ in enumerate(eps_ar):
                clust = DBSCAN(eps = e_)
                clust.fit(u_sub_ar)
            
                un_clust = np.unique(clust.labels_)
                for i_c in un_clust:
                    n_clust_ar[i_c + 1, i_] = np.sum(clust.labels_ == i_c)
                    
            plt.figure()
            plt.plot(eps_ar, n_clust_ar.T)
            plt.xlabel('epsilon')
            plt.ylabel('cluster population')
            plt.legend([-1, 0, 1, 2, 3, 4])
            
        #perform clustering
#        clust = KMeans(n_clusters = 2)
        clust = DBSCAN(eps = 5.4)
#        clust = OPTICS(min_samples = 20)
        
        clust.fit(u_sub_ar)
        
        un_clust = np.unique(clust.labels_)
        
        for i_c in un_clust:
            n_lab = np.sum(clust.labels_ == i_c)
            max_n_examp = min(50, n_lab)
            
            u_clust = u_sub_ar[clust.labels_ == i_c, :] 
            
            plt.figure()
            plt.plot(u_clust[:max_n_examp, :].T)
            plt.ylim([-2,2])
            plt.title('There is %d paths in cluster %d-th. Showing %d examples.' % (n_lab, i_c, max_n_examp))
         
        #####################################
        pca = PCA()
        
        u_tr = pca.fit_transform(u_sub_ar)
        
        x_lim = 30
        
        plt.figure()
        plt.subplot(1,3,1)
        plt.bar(range(1,len(pca.explained_variance_)+1),pca.explained_variance_)
        plt.title('Variance')
        plt.xlim([0, x_lim])
        
        plt.subplot(1,3,2)
        plt.bar(range(1,len(pca.explained_variance_)+1),pca.explained_variance_ratio_)
        plt.title('propotional variance')
        plt.xlim([0, x_lim])
        
        plt.subplot(1,3,3)
        plt.bar(range(1,len(pca.explained_variance_)+1),np.cumsum(pca.explained_variance_ratio_))
        plt.title('cummulative variance')
        plt.xlim([0, x_lim])
        
        plt.figure()
        for i_c in un_clust:
            is_clust = clust.labels_ == i_c
            plt.plot(u_tr[is_clust,0], u_tr[is_clust,1],'.')
        
        plt.xlabel('first component')
        plt.ylabel('second component')
        plt.legend(un_clust)
            
        plt.figure()
        for i_c in un_clust:
            u_mean = np.mean(u_sub_ar[clust.labels_ == i_c, :], axis = 0)
            u_std = np.std(u_sub_ar[clust.labels_ == i_c, :], axis = 0)
            
            plt.plot(x_ar, u_mean)
            plt.fill_between(x_ar, u_mean + u_std, u_mean - u_std, alpha=0.5)
#            
#            plt.plot(x_ar, u_mean + u_std,'--')
#            plt.plot(x_ar, u_mean - u_std, '--')
        
        plt.legend(un_clust)
        
        plt.figure()
        plt.hist(clust.labels_)
        
        plt.figure()
        plt.plot(pca.components_[:2,].T)
        u_mean_m1 = np.mean(u_sub_ar[clust.labels_ == 1, :], axis = 0)
        u_mean_m0 = np.mean(u_sub_ar[clust.labels_ == 0, :], axis = 0)
        
        plt.plot((u_mean_m1 - u_mean_m0)/10)
        
        plt.legend(['1st component', '2nd component', 'mean cluster difference'])
        
###################################
plt.figure()

x_d2 = x[:,:-2] -2*x[:,1:-1] + x[:,2:]

x_d2_vec = np.reshape(x_d2, (x_d2.shape[0]*x_d2.shape[1],))

plt.hist(x_d2_vec, 500, log = True, density = True)
plt.xlabel('second differential of the control')
plt.ylabel('coutns') 
plt.xlim([-3,3])
        
###################################
# save all figure to pdf
        
def multipage(filename, figs=None, dpi=200):
    pp = PdfPages(filename)
    
    try:
        os.mkdir('Figures')
    except:
        pass
    
    if figs is None:
        figs = [plt.figure(n) for n in plt.get_fignums()]
    for fig in figs:
        fig.savefig(pp, format='pdf')
        
        fig.savefig('Figures/figure_%d.png' % fig.number)
    pp.close()

###############################
multipage(__file__ + '.pdf')

   
    
    

        
