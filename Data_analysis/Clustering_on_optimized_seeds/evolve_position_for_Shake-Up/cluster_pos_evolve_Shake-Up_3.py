# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pickle, json

from sklearn.cluster import DBSCAN

process_data = False

reconstruction_test = False 

method_ar = ['players', 'RS']
method_lab = ['PS', 'RS']

info_cols = ['dur','fid', 'n_poi']

method_tag = 'i_method'

data_cols = ['FinalFidelity', 'Duration']

i_off = len(info_cols)

n_comp = 20  # number of cosine components

new_cols = info_cols + list(range(n_comp))   

n_dat = {}
for i_m, method in enumerate(method_ar):  
    #####################
    save_name = 'components_' + method + '.pckl'
    print(save_name)
    
    if process_data:
        filename = 'all_paths_' + method + '_with_position.pckl'     
        
        with open(filename, 'rb') as fid:
            data_l = pickle.load(fid)
            
        df = pd.DataFrame(columns = new_cols)
        for i_, path in enumerate(data_l):
            u_x = np.array(path['physics_x'])    # control
            psi_x = np.array(path['psi_x'])      # mean position of the atom
            
            u_rel = u_x - psi_x   # control relative to the atom position
            
            n_u = len(u_x)
            
            df.loc[i_] = [path['path_info'][c_] for c_ in ['Duration', 'FinalFidelity']] + [n_u] + list(np.zeros(n_comp))
            
            u_fft = np.fft.fft(np.concatenate((u_rel, np.flip(u_rel))))/n_u 
            
            u_cos = np.real(u_fft)  # the cosinus component
            u_cos[0] /=2     # reduce the DC component by 2
            
            n_val = int(min(n_comp, n_u/2))  # number of valid components
            
            if reconstruction_test:
                u_sin = np.imag(u_fft)  # the sinus component
                
                t_aux = np.linspace(0,1,n_u)
                u_rec = 0*t_aux
                for i_, (c_, s_) in enumerate(zip(u_cos[:100], u_sin[:100])):
                    u_rec += c_*np.cos(np.pi*t_aux*i_) + s_*np.sin(np.pi*t_aux*i_)
                
                plt.figure()
                plt.plot(u_rel)
                plt.plot(u_rec)
            
            df.loc[i_,i_off:(n_val + i_off)] = u_cos[:n_val] 
            
            if np.mod(i_,100) == 1:
                print('processed ', i_)
        
        with open(save_name, 'wb') as fid:
            pickle.dump(df, fid)
    else:
        with open(save_name, 'rb') as fid:
            df = pickle.load(fid)

    df[method_tag] = i_m

    n_dat[method] = len(df)
    ##################
    if i_m == 0:
        df_a = df.copy()
    else:
        df_a = pd.concat([df_a, df])
    
#################################################
        
info_cols += method_tag    

n_comp = 6   # 0 is DC, 1 is half, 2 is one full oscillation and so fort; n_comp = 6 mean that 2.5 oscillation is the highest

comp_cols = [c_ for c_ in df_a.columns if c_ not in info_cols]

comp_cols = np.array(comp_cols[:n_comp])/2

i_fig = plt.figure().number
for i_ in df_a.index[:10]:
    sol = df_a.iloc[i_]
    
    plt.figure(i_fig)
    plt.plot(sol.values[i_off:])
    
    plt.figure(i_fig + 1)
    plt.plot(sol['dur'], sol['fid'], '.')
    
################################
# select on time and fidelity
T_min = 0.3
T_max = 1.2
F_min = 0.6

n_all = len(df_a)

print('Number of all solutions: ', n_all)

met_count = []
for i_m in np.unique(df_a['i_method']):
    met_count.append(sum(df_a['i_method'] == i_m))

print('Methode count:', met_count)

df_a = df_a[(df_a['dur'] >= T_min) & (df_a['dur'] < T_max) & (df_a['fid'] > F_min)]
    
n_sol = len(df_a)# 9000   # finite number of solutions to cluster

print('Number of solution to cluster:', n_sol)

met_count = []
for i_m in np.unique(df_a['i_method']):
    met_count.append(sum(df_a['i_method'] == i_m))

print('Methode count:', met_count)

dur = df_a['dur'].values[:n_sol]
fid = df_a['fid'].values[:n_sol]
methode = df_a[method_tag].values[:n_sol]
n_poi = df_a['n_poi'].values[:n_sol]

x = df_a.values[:n_sol, i_off:(i_off + n_comp)]

x_sign = 2*(x.max(axis = 1) == np.max(np.abs(x), axis = 1)) - 1  # sign of the biggest component
    
x[:] /= x_sign.reshape((n_sol,1))  # correct the sign

i_max = np.argmax(x, axis = 1)

plt.figure()
plt.plot(dur, i_max,'.')

clust = DBSCAN(eps = 0.10, min_samples = 250)   #eps = 0.3)

clust.fit(x)

un_clust = np.unique(clust.labels_)
un_methode = np.unique(methode)

clust_d = {}   # dictionary for plotting data 

i_fig = plt.figure().number
clust_leg = []
clust_leg_2 = []
dur_mean = []
for i_c in un_clust:
    is_clust = clust.labels_ == i_c
    
    clust_leg_aux = 'cl. ' + str(i_c)
    # count method occurances
    for i_m, m_ in enumerate(un_methode):
        met_frac = sum(methode[is_clust] == m_) / met_count[i_m]
        clust_leg_aux += '; %s: %0.0f%%' % (method_lab[i_m], met_frac*100)
    
    dur_mean.append(dur[is_clust].mean())
    
    clust_leg_2.append(clust_leg_aux)
    
    n_member = sum(is_clust)
    u_mean = np.mean(x[is_clust, :], axis = 0)
    u_std = np.std(x[is_clust, :], axis = 0)

    if i_c != -1:
        plt.figure(i_fig)
        plt.plot(comp_cols, u_mean)
        plt.fill_between(comp_cols, u_mean + u_std, u_mean - u_std, alpha=0.5)
        
        clust_leg.append('cl. ' + str(i_c))
        
        plt.figure(i_fig + 1)
        plt.plot(dur[is_clust], fid[is_clust],'.')
        
        plt.figure(i_fig + 2)
        plt.semilogy(dur[is_clust], 1- fid[is_clust],'.')
        
    
    else:
        plt.figure(i_fig + 2)
        plt.semilogy(dur[is_clust], 1- fid[is_clust],'.k')
#        plt.plot(dur[is_clust], fid[is_clust],'.k')
#        plt.title('noise cluster')
        
        print('Noise cluster size:', n_member)       
    
    clust_d[int(i_c)] = {'u_mean': u_mean.tolist(), 'u_std': u_std.tolist(), 'dur': dur[is_clust].tolist(),
                    'fid': fid[is_clust].tolist(), 'comp_cols': comp_cols.tolist(), 'seed_type': methode[is_clust].tolist()}
    
clust_d['clust_leg'] = clust_leg
clust_d['clust_leg_2'] = clust_leg_2
    
with open('clust_shakeup_data.json','w') as file_id:
    json.dump(clust_d, file_id)
    
    
dur_mean = np.array(dur_mean)    

plt.figure(i_fig)
plt.legend(clust_leg)
plt.xlabel('N oscillations')
plt.ylabel('component weight')
plt.title('Cluster interpretation: cosine components')

plt.figure(i_fig + 2)
plt.xlabel('Duration [T_QSL]')
plt.ylabel('Infidelity')
plt.title('Clustering in I-T space')
plt.legend(clust_leg_2)

plt.figure()
plt.hist(clust.labels_)

######################################################
i_sort = np.argsort(dur_mean)

n_rec = 300

t_aux = np.linspace(0,1, n_rec)

n_sel = 30

n_found_cls = len(un_clust) - 1

mean_rec = np.empty((n_found_cls, n_rec))

############################
for i_c, i_so, d_m in zip(un_clust[i_sort], i_sort, dur_mean[i_sort]):
    if i_c != -1:
        is_clust = clust.labels_ == i_c
    
        x_sel = x[is_clust, :][:n_sel,:]
        
        x_mean = np.mean(x[is_clust, :], axis = 0)  # average the cluster cosine components
        x_std = np.std(x[is_clust, :], axis = 0)  # std the cluster cosine components
        
        u_rec = 0*t_aux
        for i_, (c_, s_) in enumerate(zip(x_mean, x_std)):
            u_rec += c_*np.cos(np.pi*t_aux*i_)   # reconstruct the cluster mean
            
        mean_rec[i_c,:] = u_rec.copy()
        
        plt.figure()
        for i_s in range(n_sel):
            u_rec = 0*t_aux
            for i_, c_ in enumerate(x_sel[i_s,:]):
                u_rec += c_*np.cos(np.pi*t_aux*i_)
            
            plt.plot(u_rec)
    
        plt.title('Examples from cluster %d, dur: %0.3f; fid: %0.3f' % (i_c, d_m,fid[is_clust].mean()))
            

int_d = {'t_aux': t_aux.tolist(), 'mean_rec': mean_rec.tolist(), 'clust_leg': clust_leg }

with open('interpretation_data.json', 'w') as fid:
    json.dump(int_d, fid)


t_aux = np.array(int_d['t_aux'])
mean_rec = np.array(int_d['mean_rec'])

plt.figure()
plt.plot(t_aux, mean_rec.T)
plt.plot([0,1],[0,0],'--k')
plt.legend(int_d['clust_leg'])
plt.xlabel('Rescaled duration')
plt.ylabel('Mean control dispalacement')
plt.title('Cluster interpretation: half-integer oscillations')

