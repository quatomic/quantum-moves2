# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from scipy.interpolate import interp1d
import json
import matplotlib.pyplot as plt
import os
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import pickle

from sklearn.cluster import KMeans, DBSCAN, OPTICS
from matplotlib.backends.backend_pdf import PdfPages

from importlib import reload  # Python 3.4+ only.
import CVUtilities as cval
import h5py

 ##################
cval = reload(cval)
pdc = cval.PhysicsDataCollector()

jsonListFile = "./sim_input_file_list.json"
programPath = "../../PhysicsVisualization/psiTest.exe"

# Where output .json data is saved
# Output .json data is the 'standard' physics visualization outputs
# composed of the wavefunction, level data, fidelity, duration etc.
outputDataDirectory = "./output_paths"

# Name of the output .json file name
outputCSVFile = "./sim_output_fidelity_"

with open('path_template_new.json', 'r') as fid:
    path_temp = json.load(fid)      # path template file

curves_all = np.genfromtxt('../FT-curves/Challenge_curves_QM2.csv', delimiter=';')

# =============================================================================
# Specify the data file
# =============================================================================

data_file = 'all_paths_%s_with_position_aux.pckl'


for i_met in [0]:  #range(2):
    # file with general player seeds
    filename_player = ['/QM2_Serialized_Datasets_Algorithm_Seeding/playerData/upso_serialized.csv',
                       '/QM2_Serialized_Datasets_Algorithm_Seeding/computerData/gpe_shakeup/serialized_ShakeUp_7_GRAPE_RS.csv'][i_met]
                       
    is_json_path = [True, False][i_met]
    
    method = ['players', 'RS'][i_met]
    
    head_len = [0,1][i_met]
    df = pd.read_csv(filename_player, sep = '|', header = head_len)
    print(df.columns)
    
    load_json_paths = is_json_path  # is true, load paths from text based .json files, otherwise load from binary .mat files
    
    for level_order in [2]: # 0,1,2 : BWH, Splitting, Shake-Up
        
        level_name = ['Bring Home Water', 'Splitting', 'Shake-Up'][level_order]
        
        ctrl_tag = ['physics_x', 'physics_y', 'physics_x'][level_order]
        
        time_lim = [(0.65, 0.75), (0.9, 1.0), (0.9, 1.0)][level_order]  ## watch out!!!!!!!!
        T_QSL = [0.37, 0.92, 0.89][level_order]
        dt = [0.00035, 0.001, 0.001][level_order]
        u_bound = [(-2, 2), (0, 1), (-1,1)][level_order]
        
        level_id = [16, 11, 7][level_order] # 7: Shake-Up; 11: Splitting; 16: BWH
        
        if not load_json_paths:  # it's the computer data, and the durations are saved in the simulation units
            df['Duration'] /= T_QSL
        
        if method == 'players':
            df_l = df[(df['levelId'] == level_id)]
        else:
            df_l = df.copy()
        
        print('Number of paths in the time bin: %d' % (len(df_l),))
        
        plt.figure()
        plt.plot(df_l['Duration'], df_l['FinalFidelity'], '.')
        plt.plot(curves_all[:,0], curves_all[:,level_order + 1],'.-')
        
        outputCSVFile_lev = outputCSVFile + level_name + '.csv'        
        
        n_files = 100
        n_paths = len(df_l)
        n_runs = int(n_paths/n_files)
        if n_runs * n_files < n_paths:
            n_runs += 1
            
        if False: # load existing data file
            try:
                with open(data_file % (method,), 'rb') as fid:
                    data_l = pickle.load(fid)
            except:
                raise(Exception('Coud not load the previous paths for: ' + method))
        else:
            data = []
            
        for i_r in range(n_runs):
            n_files_min = min(n_files, n_paths - i_r*n_files)
            
            inp_list = []
            for i_ in range(n_files_min):                                
                path_info = df_l.iloc[i_r*n_files + i_]
                filename = path_info['filename'].strip("'") 
                
                data_l.append({})  # append an empty dictionary
                data_l[-1]['path_info'] = path_info    # save fidelity and other stuff
                
                if i_ == 0:
                    print('Processed ', i_r*n_files + i_, 'paths out of', len(df_l), 'for level', level_name, 'for methode', method)
            
                # Load th control
                if load_json_paths:
                    with open(filename, 'r') as f:
                        data = json.load(f)
                        y_i = data[ctrl_tag]
                else:
                    with h5py.File(filename,'r') as fid:
                        data_keys = list(fid.keys())
                        y_i = (np.array(fid.get('uf'))[:,0] - 0.5)/0.4  # make the control go in the range [-1, 1]
                    
                #####################################
                path = path_temp.copy()
                    
                path['physics_x'] = list(y_i)
                path['physics_y'] = [0 for x_ in y_i]
                
                data_l[-1]['physics_x'] = path['physics_x']    # save the control
                
                inp_name = 'input_paths/input_path_%d.json' %(i_,) 
                inp_list.append(inp_name)
            
                with open(inp_name, 'w') as fid:
                    json.dump(path, fid)
                        
            ########################################
            # save the input files    
            out_list = [n_.replace('input_path','output_path') for n_ in inp_list]
            inp_dict = {"seed_filenames" : inp_list, "output_filenames": out_list}
            
            with open(jsonListFile, 'w') as fid:
                json.dump(inp_dict, fid)
            
            # Launch the program
            pdc.ProcessControlsJSON(jsonListFile = jsonListFile, 
                                    programPath = programPath, 
                                    outputDataDirectory = outputDataDirectory,
                                   outputCSVFile = outputCSVFile_lev)
            
            for i_ in range(n_files_min):   # evaluate the atom position and std                 
                with open('output_paths/output_path_%d.json' %(i_,), 'r') as fid:
                    test_path = json.load(fid)
                    
                psi_re = np.array(test_path['psi_re'])
                psi_im = np.array(test_path['psi_im'])
                
                psi_dens = psi_re**2 + psi_im**2
                
                n_x = psi_dens.shape[1]
                n_t = psi_dens.shape[0]
                
                t_vec = np.linspace(0,1,n_t)
                x_vec = np.linspace(-2,2,n_x)  # the wavefunction is evolved in the range [-2, 2]
                
                psi_x_ar, t_ar = np.meshgrid(x_vec, t_vec)
                
                psi_x = np.sum(psi_dens * psi_x_ar, axis = 1)/psi_dens.sum(axis = 1)
                
                aux, psi_x_mean = np.meshgrid(x_vec, psi_x)
                
                psi_std = np.sqrt(np.sum(psi_dens * (psi_x_ar - psi_x_mean)**2, axis = 1)/psi_dens.sum(axis = 1))
        
                # save the position and std of the atom
                data_l[i_r*n_files + i_]['psi_x'] = psi_x
                
                data_l[i_r*n_files + i_]['psi_std'] = psi_std
                
            if True:# save the data as it comes
                with open(data_file % (method,), 'wb') as fid:
                    pickle.dump(data_l, fid)

#            if i_r == 2:
#                break
#            
