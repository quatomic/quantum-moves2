# -*- coding: utf-8 -*-


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.neighbors.kde import KernelDensity
import pickle, json

import scipy.interpolate as si

def interp2d_pairs(*args,**kwargs):
    """ Same interface as interp2d but the returned interpolant will evaluate its inputs as pairs of values.
    """
    fun = si.interp2d(*args,**kwargs)
    # Internal function, that evaluates pairs of values, output has the same shape as input
    def interpolant(x,y,f):
        x,y = np.asarray(x), np.asarray(y)
        return (si.dfitpack.bispeu(f.tck[0], f.tck[1], f.tck[2], f.tck[3], f.tck[4], x.ravel(), y.ravel())[0]).reshape(x.shape)
    # Wrapping the scipy interp2 function to call out interpolant instead
    return lambda x,y: interpolant(x,y,fun)

def monotonous_curve(x, y, sign = -1):
    # if sign -1, it's decreasing curve, if sign +1 it's increasing curve
    i_s = np.argsort(x)
    x_l = [x[i_s[0]]]
    y_l = [y[i_s[0]]]
    
    for x_, y_ in zip(x[i_s], y[i_s]):
        if y_*sign > y_l[-1]*sign:
            if x_ > x_l[-1]:
                x_l.append(x_) 
                y_l.append(y_)
            else:
                y_l[-1] = y_
    
    return np.array(x_l), np.array(y_l)

# =============================================================================
# Common definitions
# =============================================================================

lev_idx = 1

lev_sel = [16, 11, 7][lev_idx]
lev_fol = ['bhw', 'gpe_split', 'gpe_shakeup'][lev_idx]
lev_name = ['BringHomeWater', 'Splitting', 'ShakeUp'][lev_idx]

T_QSL = [0.37, 0.92, 0.89][lev_idx]
x_lim_ar = np.array([0,1.2])


methods_l = ['GRAPE_RS', 'SGA_BUCKET_RS', 'SGA_RS'] 

comp_file = 'computerData/' + lev_fol + '/serialized_' + lev_name + '_' + str(lev_sel) + '_'

df_temp = pd.DataFrame(columns = ['dur', 'infid'])
df_dict = {}
data_path = '/QM2_Serialized_Datasets_Algorithm_Seeding/'

# =============================================================================
# Load Unoptimized player seeds computer optimized
# =============================================================================
filename = data_path + 'playerData/upso_serialized.csv'

df = pd.read_csv(filename, sep = '|')

df_Upso = df[df['levelId'] == lev_sel]

df_aux = df_temp.copy()
df_aux['dur'] = df_Upso['Duration']
df_aux['infid'] = 1 - df_Upso['FinalFidelity']

df_dict['Upso'] = df_aux

# =============================================================================
# Load player seeds unoptimized and optimized
# =============================================================================

file2 = data_path + 'playerData/ps_db.csv'

df2 = pd.read_csv(file2, sep = ',')

df_dbU = df2[(df2['level_id'] == lev_sel) & (df2['optimization_iterations'] == 0)]
df_dbO = df2[(df2['level_id'] == lev_sel) & (df2['optimization_iterations'] != 0)]

df_aux = df_temp.copy()
df_aux['dur'] = df_dbU['obtained_duration']
df_aux['infid'] = 1 - df_dbU['obtained_fidelity']

df_dict['Ups'] = df_aux

df_aux = df_temp.copy()
df_aux['dur'] = df_dbO['obtained_duration']
df_aux['infid'] = 1 - df_dbO['obtained_fidelity']

df_dict['Ops'] = df_aux

# =============================================================================
# Load computer data
# =============================================================================
for m_ in methods_l:
    file3 = data_path + comp_file + m_ + '.csv'

    df3 = pd.read_csv(file3, sep = '|', header = 1)
    
    df_aux = df_temp.copy()
    df_aux['dur'] = df3['Duration'] / T_QSL
    df_aux['infid'] = 1 - df3['FinalFidelity']
    
    df_dict[m_] = df_aux


# =============================================================================
# Plot the results
# =============================================================================

plt.figure()
leg_l = []
mon_dict = {}
for k_ in df_dict:
    df_aux = df_dict[k_]    
    plt.semilogy(df_aux['dur']*T_QSL,df_aux['infid'],'.', markersize = 2)
    
    x_l, y_l = monotonous_curve(df_aux['dur'].values, df_aux['infid'].values)
    
    mon_dict[k_] = [x_l, y_l]
    
#    plt.semilogy(x_l, y_l)
    
#    leg_l += [k_, k_ + '_mon']
    leg_l += [k_ + '; ' + str(len(df_aux)) + ' seeds']
   
plt.plot([0,1.2],[0.01,0.01],'k--')
    
plt.xlim(x_lim_ar*T_QSL)
plt.xlabel('duration [sim. un.]')
plt.ylabel('infidelity')
plt.legend(leg_l)


plt.figure()
for k_ in mon_dict:
    x_l, y_l = mon_dict[k_]
    plt.semilogy(np.array(x_l)*T_QSL, y_l,'.-', markersize = 4)
    
plt.title('monotonous F-T trade-off: ' + lev_name)
plt.plot([0,1.2],[0.01,0.01],'k--')
plt.xlim(x_lim_ar*T_QSL)
plt.xlabel('duration [sim. un.]')
plt.ylabel('infidelity')
plt.legend(leg_l)

##########################################
plt.figure()
for k_ in df_dict:
    df_aux = df_dict[k_]    
    plt.hist(df_aux['dur']*T_QSL, bins = 30, range = (0,0.4), histtype  = 'step' , linewidth = 3)


plt.xlabel('duration [sim. un.]')
plt.ylabel('number of seeds')
plt.legend(leg_l)
plt.xlim([0,0.4])

################################################
# define bins
bin_cen = np.unique(df_dict['GRAPE_RS']['dur']*T_QSL)
bin_bound = [bin_cen[0] - (bin_cen[1] - bin_cen[0])/2]  # start point
bin_bound += list((bin_cen[:-1] + bin_cen[1:])/2)  # middle point
bin_bound = np.array(bin_bound + [bin_cen[-1] + (bin_cen[-1] - bin_cen[-2])/2]) # end point

bin_sig = (bin_bound[1:] - bin_bound[:-1])/2  # width of the gaussian

def is_bin(dur):
    return np.sum(dur*T_QSL > bin_bound) /10

def ap_log(inp):
    return np.log10(inp)/10

plt.figure()
for k_ in df_dict: 
    df_dict[k_]['bin'] = df_dict[k_]['dur'].apply(is_bin)
    df_dict[k_]['infid_log'] = df_dict[k_]['infid'].apply(ap_log)
    plt.hist(df_dict[k_]['dur']*T_QSL, bins = bin_bound, range = (0,0.4), histtype  = 'step' , linewidth = 3)

plt.xlabel('duration [sim. un.]')
plt.ylabel('number of seeds')
plt.legend(leg_l, loc='upper left')
plt.xlim([0,0.4])

###############################################
# estimate density
if True:
    kde = KernelDensity(kernel='epanechnikov', bandwidth=0.008)  # parabolic
    #kde = KernelDensity(kernel='gaussian', bandwidth=0.004)
    il_mesh = np.linspace(0, -3.0, 50)
    dur_mesh = np.linspace(0.8,1.0, 40)
    extent = [dur_mesh.min(), dur_mesh.max(), il_mesh.min(), il_mesh.max()]
    
    ###########################
    # define logarithmic tics manually
    dec_ar = np.arange(1,0.1,-0.1)
    
    x_tic_pos = np.array(list(dec_ar) + list(dec_ar/10) + list(dec_ar/100) + [0.001])
    x_tic_pos = np.log10(x_tic_pos)
    x_tic_lab = ['' for x_t in x_tic_pos]
    x_tic_lab[0] = 1
    x_tic_lab[9] = 0.1
    x_tic_lab[18] = 0.01
    x_tic_lab[27] = 0.001
    ###############################
    
    X, Y = np.meshgrid(il_mesh/10, dur_mesh/T_QSL)
    xy = np.vstack([Y.ravel(), X.ravel()]).T
    
    #cmap_str = 'PuOr'
    #cmap_str = 'terrain'
    cmap_str = 'gist_stern'
    
    prop_cycle = plt.rcParams['axes.prop_cycle']
    colors = prop_cycle.by_key()['color']
    
    def find_bin(dur):
        return np.sum(dur > dur_mesh/T_QSL)
    
    #plt.figure()
    leg_dist = []
    im_ar = []
    for k_, c_ in zip(df_dict, colors): 
        if k_ not in ['Ups', 'Ops']:
            X_dat = df_dict[k_][['dur', 'infid_log']].copy()
            
            kde.fit(X_dat)    
            
            Z = np.exp(kde.score_samples(xy))
            Z = Z.reshape(X.shape).T
            
            X_dat = X_dat[(X_dat['dur'] > extent[0]/T_QSL) & (X_dat['dur'] < extent[1]/T_QSL)]  # select only the ploted durations
            X_dat['bin'] =  X_dat['dur'].apply(find_bin)
            
#           
            x_low_P = []
            y_low_P = []
            for i_ in range(Z.shape[1]):
                Z[:,i_] /= Z[:,i_].sum()
                
                dat_aux = X_dat[X_dat['bin'] == i_].copy()  # pick datapoints from the bin
                
                Z_fun = si.interp1d(il_mesh, Z[:,i_])
                
                dat_aux['score'] = Z_fun(dat_aux['infid_log']*10)
                
                dat_aux = dat_aux[dat_aux['score'] < 0.002]
                
                x_low_P += list(dat_aux['dur']*T_QSL)
                y_low_P += list(dat_aux['infid_log']*10)           
            
            im_ar.append(Z)
            
            ######
            
            x_l, y_l = mon_dict['Upso'].copy()
            
            plt_data = {'solution_density': Z.tolist(), 'y_tic_pos': x_tic_pos.tolist(), 'y_tic_lab': x_tic_lab, 'x_label': 'Duration [sim. u.]', 'y_label': 'Infidelity',
                        'extent': extent, 'c_map': cmap_str, 'title': (k_ + '; solution density'), 'best_player_outline_x': list(x_l*T_QSL), 
                        'best_player_outline_y': np.log10(y_l).tolist(), 'low_chance_x': x_low_P, 'low_chance_y': y_low_P, 'vmax': 0.16}

            with open('FT_density/dens_plot_splitting_' + k_ +'.json', 'w') as fid:
                json.dump(plt_data, fid)
            
            ###########
            plt.figure()
            aspect = np.abs((plt_data['extent'][1] - plt_data['extent'][0])/(plt_data['extent'][3] - plt_data['extent'][2]))
    
            plt.imshow(plt_data['solution_density'], cmap = plt.get_cmap(plt_data['c_map']), vmin = 0.0, vmax = plt_data['vmax'], interpolation='none', extent=plt_data['extent'], aspect = aspect)
            
            plt.yticks(plt_data['y_tic_pos'], plt_data['y_tic_lab'])
            
            plt.plot(plt_data['best_player_outline_x'], plt_data['best_player_outline_y'], color="orange")
            
            plt.plot(plt_data['low_chance_x'], plt_data['low_chance_y'], 'xw', markersize = 4)
            
            plt.xlim(plt_data['extent'][:2])
            plt.ylim(plt_data['extent'][2:])
            
            plt.title(plt_data['title'])
            
            plt.xlabel(plt_data['x_label'])
            plt.ylabel(plt_data['y_label'])
            
            plt.colorbar()               
            
            
        
    plt.figure()
    plt.imshow(im_ar[0] - im_ar[2])
