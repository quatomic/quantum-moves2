import mysql
from  mysql.connector import connection
import json
import subprocess
import os
import sys
import statistics
import logging
import re
import csv
import ntpath
import datetime
import copy
import numpy as np



# In[ ]:


def MakeLogFileName(workDir, name):
    return  os.path.join(workDir, name + str(datetime.datetime.now()))


# In[3]:


def ConfigureLoggingDefault():
    logging.basicConfig(
        format='%(asctime)s %(levelname)-8s %(message)s',
        level=logging.INFO,
        stream = sys.stdout,
        datefmt='%Y-%m-%d %H:%M:%S')
    
def ConfigureLoggingFile(fname):
    logging.basicConfig(
        filename = fname,
        filemode = 'a',
        format='%(asctime)s %(levelname)-8s %(message)s',
        level=logging.INFO,
        datefmt='%Y-%m-%d %H:%M:%S')


# In[4]:


# Get credentials
def AcquireCredentials(fileName):
    with open(fileName, 'r') as fh:
        strCred = json.load(fh)   
    return strCred


# In[5]:


# Miscellaneous
def MakeDir(path):
    try:
        os.mkdir(path)
    except FileExistsError:
        pass
    return path


# In[6]:


# Query construction
def ExecuteQuery(conn, query):
    logging.info("Executing query: \"{}\"".format(query))
    cursor = conn.cursor()
    cursor.execute( query )
    return cursor

def ConstructQuery(query, **kwargs):
    if kwargs:
            logging.debug("Preparing query: \"{}\"".format(query.format(**kwargs)))
            return query.format(**kwargs)
    return query


# In[7]:

# Data acquisition
def GetCursorData(cursor):
    data = cursor.fetchall()
    cursor.close()
    return data

def GetControls(conn, path_id):
    query = ConstructQuery( 'SELECT p.physics_x, p.physics_y '
                            'FROM path AS p WHERE p.path_id = \'{path_id}\'  '
                            'ORDER BY p.id;', 
                           **{'path_id' : path_id})
    cursor = ExecuteQuery(conn, query)
    return GetCursorData( cursor )
   
def GetControlsInfoWithPathId(conn, pidList):
    if len(pidList) == 0:
        logging.error("Invalid path_id list (empty) to construct query.")
        exit(1)
    pidString = "("
    for pid in pidList[:-1]:
        pidString += "\'{}\',".format(pid)
    pidString += "\'{}\')".format(pidList[-1])
    query = ConstructQuery('SELECT t.level_id, t.path_id, t.final_fidelity, t.duration, t.optimization_iteration '
                           'FROM basic_path_info AS t WHERE t.path_id IN {path_ids};',
                           **{'path_ids' : pidString})
    cursor = ExecuteQuery(conn, query)
    return GetCursorData( cursor )
    
    
def GetControlsInfo(conn):
    # query = ConstructQuery('SELECT t.level_id, t.path_id, t.final_fidelity, t.duration, t.optimization_iteration '
    #                        'FROM basic_path_info AS t WHERE t.created_at BETWEEN DATE(\'2018-12-01\') AND DATE(\'2018-12-09\') AND level_id IN (7,11,16);')
    query = ConstructQuery('SELECT t.level_id, t.path_id, t.final_fidelity, t.duration, t.optimization_iteration '
                           'FROM basic_path_info AS t WHERE level_id IN (7,11,16);')    
    # query = ConstructQuery('SELECT t.level_id, t.path_id, t.final_fidelity, t.duration '
    #                        'FROM basic_path_info AS t WHERE t.id > 5580 AND t.id < 5700;')    
    cursor = ExecuteQuery(conn, query)
    return GetCursorData( cursor )

# based on the query right above
def GetControlInfoParser():
    return lambda x: {  "level_id" : x[0], 
                        "path_id": x[1], 
                        "final_fidelity": x[2], 
                        "duration": x[3],
                        "optimization_iteration" : x[4]}

def GetSingleControlsInfo(conn, path_id):
    logging.info("Launching query for path_id: {}...".format(path_id))
    query = ConstructQuery('SELECT t.level_id, t.path_id, t.final_fidelity, t.duration, t.optimization_iteration '
                           'FROM basic_path_info AS t WHERE t.path_id = \'{path_id}\' AND level_id != 0;', 
                           **{'path_id' : path_id})
    cursor = ExecuteQuery(conn, query)
    logging.info("Collecting query results...")
    return GetCursorData( cursor )

def GetLevelsInfo(conn):
    logging.info("Launching query for level info")
    query = ConstructQuery( 'SELECT l.level_id, l.order_in_game, l.level_name, l.T_QSL, l.dt_qengine, l.physics_engine, l.physics_model, l.has_optimization, l.special_scietific_interest, l.created_at, l.updated_at '
                            ' FROM level AS l;')
    cursor = ExecuteQuery(conn, query)
    logging.info("Collecting query results...")
    return GetCursorData( cursor )

class LevelInfo:
    # POD that contains level information
    def __init__(self, tup):
        self._mId = tup[0]
        self._mOrderInGame = tup[1]
        self._mLevelName = tup[2]
        self._mTQSL = tup[3]
        self._mDt = tup[4]
        self._mPhysicsEngine = tup[5]
        self._mPhysicsModel = tup[6]
        self._mHasOptimization = tup[7]
        self._mSpecialScientificInterest = tup[8]
        self._mCreatedAt = tup[9]
        self._mUpdatedAt = tup[10]
        self._mConvolutionKernel = None

    def SetConvolutionKernel(self, inp):
        self._mConvolutionKernel = inp

    def GetConvolutionKernel(self):
        return self._mConvolutionKernel

    def GetDt(self):
        return self._mDt

    def GetId(self):
        return self._mId

def LoadKernelDataCSV(csvFileName, delimiter, refT):
    with open(csvFileName, 'r') as fh:
        data = csv.reader(fh, delimiter = delimiter)
        X = []
        Y = []
        for line in data:
            X += [ float(line[0]) ]
            Y += [ float(line[1]) ]
        return { "x": X, "y": Y,  "ReferenceT": refT}

def ConstructLevelKernel(kernelData, dt):
    xMin = 0
    xMax = max(kernelData['x'])
    assert(xMax > 0)
    xSteps = int( (xMax - xMin) / dt )
    assert(xSteps > 0)
    X = [i * dt for i in range(xSteps)]
    kernelYSum = sum(kernelData['y'])
    kernelYNormalized = list(map(lambda x: x / kernelYSum, kernelData['y']))
    Y = list(np.interp(x = X, xp = kernelData['x'], fp = kernelYNormalized))
    newYSum = sum(Y)
    Y = list(map(lambda x: x / newYSum, Y))
    return {'x' : X, 'y' : Y}

def ConstructLevelInfoKernels(kernelData, levelInfos):
    # Modify the dictionary with level info by adding the convolution kernel data
    for (lvlId, lvlInfo) in levelInfos.items():
        logging.info("Constructing convolution kernel for level {id} with dt = {dt}.".format(id = lvlId, dt = lvlInfo.GetDt()))
        lvlInfo.SetConvolutionKernel( ConstructLevelKernel(kernelData, lvlInfo.GetDt()) )
        levelInfos[ lvlId ] = lvlInfo
    return levelInfos

def ConstructLevelsInfo(conn):
    # Construct a dictionary with key = level_id, value = full level info
    data = GetLevelsInfo(conn)
    output = {}
    for datum in data:
        lvlInfo = LevelInfo(datum)
        output[ lvlInfo.GetId() ] = lvlInfo
    return output 

# def LoadConvolutionKernel(jsonDict, fileName, fieldName = "convKernel"):
#     with open(fileName, 'r') as fh:
#         data = json.load(fh)
#         convData = data[fieldName]
#         jsonDict[fieldName] = convData
#         jsonDict["performConvolution"] = 1
#         return jsonDict

def EnableConvolutionKernel(jsonDict, csvFileName):
    assert(csvFileName.endswith(".csv"))
    jsonDict["convolutionFileName"] = csvFileName
    jsonDict["performConvolution"] = 1
    return jsonDict        

def DisableConvolutionKernel(jsonDict):
    jsonDict["performConvolution"] = 0
    return jsonDict

def ExtractData(pattern, data, refFileName, expectedMatches = 1):
    matchesList = re.findall(pattern, data)
    if len(matchesList) == 0:
        logging.error("Could not read pattern \'{}\' from file {}.".format(pattern, refFileName))
        sys.exit(1)
    elif (len(matchesList) != expectedMatches):
        logging.error("Found different number of matches than expected (pattern: \'{}\', file: {})".format(len(matchesList), 
                                                                                                           expectedMatches, 
                                                                                                           pattern, 
                                                                                                           refFileName))
        sys.exit(1)
    return matchesList

# In[8]:
class AdditionalJSONData:
    def __init__(self, addData):
        assert(type(addData) == dict)
        self._mData = copy.deepcopy(addData)

    def ModifyDataJSONFile(self, fileName):
        with open(fileName, 'r') as fhread:
            logging.info("Attempting to append modifications (additional data) to {}".format(fileName))
            jsonData = json.load(fhread)
            (outputJSONData, wasModified) = self.ModifyMerge(ref = jsonData, mod = self._mData)

        if (wasModified):
            with open(fileName, 'w') as fhwrite:
                json.dump(outputJSONData, fhwrite)
                if wasModified:
                    logging.info("{} has been modified".format(fileName))
        return wasModified

    def ModifyMerge(self, ref, mod):
        output = {}
        # modified = False
        # for (k, v) in ref.items():
        #     if k not in mod:
        #         output[k] = v
        #     else:
        #         if (ref[k] != mod[k]):
        #             modified = True
        #         output[k] = mod[k]
        # for (k, v) in mod.items():
        #     if k in output:
        #         continue
        #     output[k] = v

        for (k, v) in ref.items():
            output[k] = v
        for (k, v) in mod.items():
            output[k] = v

        return (output, True)

    def ModifyDataDict(self, mod):
        (self._mData, __) = self.ModifyMerge(ref = self._mData, mod = mod)

    def GetDataDict(self):
        return self._mData

class ValidationDatum:
    def __init__(self, path_id, completedProcess, expectedF, 
                 obtainedF, retcode, expectedDuration, obtainedDuration, 
                 numberOfPoints, levelId):
        self.mPathId = str(path_id)
        self.mCompletedProcess = completedProcess
        self.mExpectedFidelity = float(expectedF)
        self.mObtainedFidelity = float(obtainedF)
        self.mRetcode = int(retcode)
        self.mExpectedDuration = float(expectedDuration)
        self.mObtainedDuration = float(obtainedDuration)
        self.mNumPoints = int(numberOfPoints)
        self.mLevelId = int(levelId)
        self.mOptimizerIteration = None
        
    def GetExpectedFidelity(self):
        return self.mExpectedFidelity
    
    def GetObtainedFidelity(self):
        return self.mObtainedFidelity
    
    def GetRetcode(self):
        return self.mRetcode
    
    def GetPathId(self):
        return self.mPathId
    
    def GetExpectedDuration(self):
        return self.mExpectedDuration
    
    def GetObtainedDuration(self):
        return self.mObtainedDuration
    
    def GetNumberOfPoints(self):
        return self.mNumPoints
    
    def GetLevelId(self):
        return self.mLevelId
    
    def DumpCSVString(self, delimiter):
        return "{PAI}{d}{RET}{d}{EF}{d}{OF}{d}{ED}{d}{OD}{d}{NUM}{d}{LID}\n".format(PAI = self.GetPathId(), 
                                                      RET = self.GetRetcode(), 
                                                      EF = self.GetExpectedFidelity(), 
                                                      OF = self.GetObtainedFidelity(), 
                                                      ED = self.GetExpectedDuration(),
                                                      OD = self.GetObtainedDuration(),
                                                      NUM = self.GetNumberOfPoints(),
                                                      LID = self.GetLevelId(),
                                                      d = delimiter)
    @staticmethod
    def DumpCSVHeader(delimiter):
        return "path_id,return_code,expected_fidelity,obtained_fidelity,expected_duration,obtained_duration,number_of_points,level_id\n"
        
    def RequestOptimizationIteration(self, creds):
        query = ConstructQuery('SELECT t.optimization_iteration FROM basic_path_info AS t WHERE path_id = \'{}\';'.format(self.GetPathId()))
        try:
            logging.info("Attempting to connect to the database...")
            conn = connection.MySQLConnection(**creds)
            logging.info("Connection made")
            data = GetCursorData(ExecuteQuery(conn, query))        
        except mysql.connector.Error as err:
            logging.critical("Error: {}".format(err.msg))
            return
        else:
            conn.close()        
        if data == []:
            return False
        else:
            self.mOptimizerIteration = data[0]
            return True

    def HasOptimizationIteration(self):
        return self.mOptimizerIteration is not None


    def GetOptimizationIteration(self):
        return self.mOptimizerIteration




class ValidationDatumTBuckets:
    def __init__(self, validationDatumCollection):
        self.mValidationSeedDatumCollection = validationDatumCollection
        self.__CollectPutSeedsToBuckets(validationDatumCollection)
        self.SortTBuckets()

    def __SingleArraysToList(self, inp):
        return list(map(lambda x: x[0], inp))

    def __CollectPutSeedsToBuckets(self, validationDatumCollection):
        self.mTSeedDict = {}
        self.buckets = [[2 * i * 0.05, (2 * i + 2) * 0.05] for i in range(12)]
        self.bucketNames = [x for x in range(12)]
        self.bucketNameDict = {x : self.buckets[x] for x in self.bucketNames}
        self.mTSeedDict = {bucketId: [] for bucketId in self.bucketNames}
        checkInBucket = lambda x, y: x.GetObtainedDuration() >= y[0] and x.GetObtainedDuration() < y[1]
        for seedDatum in validationDatumCollection:
            for bucketId in self.bucketNames:
                if (checkInBucket(seedDatum, self.bucketNameDict[bucketId])):
                    midPoint = round((self.buckets[bucketId][1] + self.buckets[bucketId][0])/2, 2)
                    self.mTSeedDict[bucketId] += [seedDatum]

    def SortTBuckets(self):
        self.mTSeedDictSorted = {}
        for (k, v) in self.mTSeedDict.items():
            sortT = sorted(v, key = lambda x: x.GetObtainedFidelity(), reverse = True)
            self.mTSeedDictSorted[k] = sortT
        return self.mTSeedDictSorted
    
    def GetSortedBucketList(self):
        if self.mTSeedDictSorted is None:
            self.SortTBuckets()
        output = []
        for (k, v) in self.mTSeedDictSorted.items():
            output += [(k,v)]
        output = sorted(output, key = lambda x: x[0])
        return output
    
    def GetFTCurve(self):
        bucketList = self.GetSortedBucketList()
        times = list(map(lambda x: float(x[1][0].GetObtainedDuration()), bucketList ))
        topF = list(map(lambda x: float(x[1][0].GetObtainedFidelity()), bucketList))
        return (times, topF)
    
    # def SelectTopUniqueSeedsPortionPerBucket(self, part):
    #     if part > 1 or part < 0:
    #         assert("Please select portion of the dataset in range of values [0, 1]".format(part))
        
    #     if self.mTSeedDictSorted is None:
    #         self.SortTBuckets()        

    #     outputs = {}
    #     for (bucketId, seedList) in self.mTSeedDictSorted.items():
    #         seedsInBucket = len(seedList)
    #         if seedsInBucket:
    #             portion = int( part * seedsInBucket )
    #             subOutput = [seed for seed in seedList[:portion]]
    #             outputs[bucketId] = subOutput
    #     return outputs

    def SelectTopUniqueSeedsNumberPerBucket(self, pathId_seedPathIdMapping, numberOfSeeds):
        if numberOfSeeds < 0:
            assert("Please select a positive number of seeds (not {})".format(numberOfSeeds))
        
        if self.mTSeedDictSorted is None:
            self.SortTBuckets()        

        outputs = {}
        pathIdSet = set()
        for (bucketId, seedList) in self.mTSeedDictSorted.items():
            seedsInBucket = len(seedList)
            if seedsInBucket > numberOfSeeds:
                subOutput = []
                for seed in seedList:
                    if (len(subOutput) >= numberOfSeeds):
                        break

                    if pathId_seedPathIdMapping[seed.GetPathId()] in pathIdSet:
                        continue
                    else:
                        # print("Skipping {}".format(pathId_seedPathIdMapping[seed.GetPathId()]))
                        pathIdSet.add( pathId_seedPathIdMapping[seed.GetPathId()] )
                        subOutput += [seed]
            else:
                subOutput = seedList

            outputs[bucketId] = subOutput
        return outputs


    def GetIFTCurve(self):
        bucketList = self.GetSortedBucketList()
        times = list(map(lambda x: float(x[1][0].GetObtainedDuration()), bucketList))
        topIF = list(map(lambda x: 1 - float(x[1][0].GetObtainedFidelity()), bucketList))
        return (times, topIF)
    
    def GetFTCurveDurationNotNorm(self, QSL):
        ftCurve = self.GetFTCurve()
        durations = ftCurve[0]
        durations = list(map(lambda x: x * QSL, durations))
        return (durations, ftCurve[1])

    def GetIFTCurveDurationNotNorm(self, QSL):
        iftCurve = self.GetIFTCurve()
        durations = iftCurve[0]
        durations = list(map(lambda x: x * QSL, durations))
        return (durations, iftCurve[1])

    def GetQSLSeed(self, F_QSL):
        bestSeed = None
        for seed in self.mValidationSeedDatumCollection:
            if seed.GetObtainedFidelity() >= F_QSL:
                if not bestSeed:
                    # print("Found new best QSL Seed at : ({}, {})".format(seed.GetObtainedDuration(), seed.GetObtainedFidelity()))
                    bestSeed = seed
                    continue
                if bestSeed and bestSeed.GetObtainedDuration() > seed.GetObtainedDuration():
                    # print("Changing best QSL seed ({}, {}) -> ({}, {})".format(bestSeed.GetObtainedDuration(), bestSeed.GetObtainedFidelity(), seed.GetObtainedDuration(), seed.GetObtainedFidelity()))
                    bestSeed = seed
        if not bestSeed:
            logging.error("No seed compliant to F_QSL = {fqsl} was found".format(fqsl = F_QSL))
        return bestSeed

    
    def GetQSLSeedReport(self, F_QSL, T_QSL, datasetLabel, additionalDurationScaling = lambda x: x):
        qslSeed = self.GetQSLSeed(F_QSL)
        if not qslSeed:
            logging.warning("No QSL found for ({})".format(datasetLabel))
            print("No QSL found")
            return None

        qslSeedReport = "T: {T} | F: {F} | Optimizer: {opt} ({doneBy}) | Seeding: {seeding} | path_id: {fname} | Seeds: {seedNum}".format(T = round(additionalDurationScaling(qslSeed.GetObtainedDuration() * T_QSL), 4),
                                                                                                             F = round(qslSeed.GetObtainedFidelity(), 3),
                                                                                                             opt = "GRAPE",
                                                                                                             seeding = "Player",
                                                                                                             fname = qslSeed.GetPathId(),
                                                                                                             doneBy = datasetLabel,
                                                                                                             seedNum = len(self.mValidationSeedDatumCollection) )
        print("{T} & {F} &  {opt} & {seeding} | Seeds: {seedNum}\\\\".format(T = round(additionalDurationScaling(qslSeed.GetObtainedDuration() * T_QSL), 4),
                                                                                                             F = round(qslSeed.GetObtainedFidelity(), 3),
                                                                                                             opt = "GRAPE",
                                                                                                             seeding = "Player",
                                                                                                             seedNum = len(self.mValidationSeedDatumCollection) ))
        return (qslSeed, qslSeedReport, qslSeedReport, additionalDurationScaling(qslSeed.GetObtainedDuration() * T_QSL) , qslSeed.GetObtainedFidelity())

# In[9]:


class CompletedProcess:
    def __init__(self, dataFile, retcode):
        self.mDataFile = dataFile
        self.returncode = retcode


# In[10]:


# Data verification
class ControlsVerifier:
    
    def __init__(self):
        pass
    
    def VerifyControlsListPathIds(self, dbCredentials, workDirectory, programPath, pathIds, outputLogsPath = None, additionalJSONData = None):
        if not (self.VerifyPaths([workDirectory, programPath])):
            logging.critical("Aborting controls verification")
            return
       # Create the connection
        try:
            logging.info("Attempting to connect to the database...")
            conn = connection.MySQLConnection(**dbCredentials)
            logging.info("Connection made")
            self._mLevelsInfo = GetLevelsInfo(conn)
            controlCollection = self.__ObtainControlsProcedurePathIdList(conn, pathIds, workDirectory)
        except mysql.connector.Error as err:
            logging.critical("Error: {}".format(err.msg))
            return
        else:
            conn.close()
        outputs = self.__VerifyControlSet(controlCollection, workDirectory, programPath, outputLogsPath, additionalJSONData)
        self.__DumpReturnInfoToCSV(outputs, csvOutputPath)
        return outputs
    
    def VerifyAllControls(self, dbCredentials, workDirectory, programPath, csvOutputPath, outputLogsPath = None, additionalJSONData = None, controlInfoDumpFile = None):
        # if not (self.VerifyPaths([workDirectory, programPath])):
        #     logging.critical("Aborting controls verification")
        #     return
       # Create the connection
        try:
            logging.info("Attempting to connect to the database...")
            conn = connection.MySQLConnection(**dbCredentials)
            logging.info("Connection made")
            self._mLevelsInfo = ConstructLevelsInfo(conn)
            controlCollection = self.__ObtainControlsProcedure(conn, workDirectory)
            if controlInfoDumpFile:
                self.__DumpControlInfo(controlCollection[0], controlInfoDumpFile)

        except mysql.connector.Error as err:
            logging.critical("Error: {}".format(err.msg))
            return
        else:
            conn.close()
        outputs = self.__VerifyControlSet(controlCollection, workDirectory, programPath, outputLogsPath, additionalJSONData)
        self.__DumpReturnInfoToCSV(outputs, csvOutputPath)
        logging.info("Verification complete.")
        return outputs
        
    # def VerifySingleControl(self, dbCredentials, workDirectory, programPath, path_id):
    #     # Verify a single control path_id
    #     if not (self.VerifyPaths([workDirectory, programPath])):
    #         logging.critical("Aborting controls verification")
    #         return
    #    # Create the connection
    #     try:
    #         logging.info("Attempting to connect to the database...")
    #         conn = connection.MySQLConnection(**dbCredentials)
    #         logging.info("Connection made")
    #         controlCollection = self.__ObtainSingleControlProcedure(conn, path_id)
    #     except mysql.connector.Error as err:
    #         logging.critical("Error: {}".format(err.msg))
    #         return
    #     else:
    #         conn.close()
    #     if (len(controlCollection[0]) == 0):
    #         logging.error("Controls with path_id \'{}\' not found.".format(path_id))
    #         return
    #     outputs = self.__VerifyControlSet(controlCollection, workDirectory, programPath)
    #     self.__DumpReturnInfoToCSV(outputs, csvOutputPath)
    #     logging.info("Verification complete.")
    #     return outputs
        
    def __VerifyControlSet(self, controlCollection, workDirectory, programPath, outputLogsPath = None, additionalJSONData = None):
        # General routine for verifying a set of controls
        logging.info("Serializing data to JSON")
        
        # Check if convolutions should be performed
        if "performConvolution" in additionalJSONData.GetDataDict() and additionalJSONData.GetDataDict()["performConvolution"]:
            assert("convolutionFileName" in additionalJSONData.GetDataDict())
            kernelData = LoadKernelDataCSV(additionalJSONData.GetDataDict()["convolutionFileName"], delimiter = ',', refT = "1ms")
            self._mLevelsInfo = ConstructLevelInfoKernels(kernelData, self._mLevelsInfo)

        # Serialize obtained data to JSON
        jsonFiles = self.__DumpControlsJson(workDirectory, controlCollection, additionalJSONData)

        # Create directory for outputs
        if outputLogsPath is None:
            logsPath = MakeDir(os.path.join(workDirectory, "logs"))
        else:
            logsPath = MakeDir(outputLogsPath)

        # Launch the verification tasks
#         returnInfo = self.__LaunchVerification(programPath, jsonFiles, logsPath)
        returnInfo = self.__LaunchVerificationAsync(programPath, jsonFiles, logsPath)
        
        logging.info("Verification complete")
        outputs = { "controls" : controlCollection, 
                    "jsonFiles" : jsonFiles,
                    "returnInfo" : returnInfo}
        self.__ComputeSuccessRate(outputs)
        self.__ComputeFidelityStdevMean(outputs)
        return outputs
    
    def __ObtainControlsProcedurePathIdList(self, conn, pathIds, directory):
            # Query for the general information about controls
            logging.info("Requesting controls from the database...")
            # obtain the controls info first (level id, path_id, final_fidelity,  etc.)
            controlsInfo = GetControlsInfoWithPathId( conn, pathIds )
            controls = []
            # check if the data files are available within the directory
            controlsInfo = self.__CheckIfDataFilesAvailable(controlsInfo, directory)
            for infoTuple in controlsInfo:
                 # if the data files weren't available, pull them from the database
                if not (infoTuple[4]):
                    # use the 'path_id' to obtain an ordered list of (x,y) control tuples
                    controls += [GetControls(conn, infoTuple[1])]
                else:
                    logging.info("Data file found for path_id:'{}/{}.json' Skipping query...".format(directory, infoTuple[1]))     
                    controls += [None]
                    
            logging.info("Obtained a total of {} controls".format(len(controlsInfo)))
            if len(controls) == 0:
                logging.error("No controls to be verified.")
                sys.exit(1)
            return (controlsInfo, controls)
    

    def __DumpControlInfo(self, controlsInfo, outputFile):
        parsedDicts = {}
        controlInfoParser = GetControlInfoParser()
        for infoTuple in controlsInfo:
            parsedDicts[infoTuple[1]] = controlInfoParser( infoTuple )
        with open(outputFile, 'w') as fh:
            json.dump(parsedDicts, fh)
    

    def __ObtainControlsProcedure(self, conn, directory):
            # Query for the general information about controls
            logging.info("Requesting controls from the database...")
            # obtain the controls info first (level id, path_id, final_fidelity,  etc.)
            controlsInfo = GetControlsInfo( conn )
            controls = []
            # check if the data files are available within the directory
            controlsInfo = self.__CheckIfDataFilesAvailable(controlsInfo, directory, '.json')            
            for infoTuple in controlsInfo:
                 # if the data files weren't available, pull them from the database
                if not (infoTuple[-1]):
                    # use the 'path_id' to obtain an ordered list of (x,y) control tuples
                    controls += [GetControls(conn, infoTuple[1])]
                else:
                    logging.info("Data file found for path_id:'{}/{}.json' Skipping query...".format(directory, infoTuple[1]))                    
                    controls += [ None ]                
                
            logging.info("Obtained a total of {} controls".format(len(controlsInfo)))
            if len(controls) == 0:
                logging.error("No controls to be verified.")
                sys.exit(1)            
            return (controlsInfo, controls)
    
    def __ObtainSingleControlProcedure(self, conn, path_id):
            # Query for the general information about controls
            logging.info("Requesting control from the database...")
            # obtain the controls info first (level id, path_id, final_fidelity,  etc.)
            controlsInfo = GetSingleControlsInfo( conn, path_id )
            controls = []
            for infoTuple in controlsInfo:
                # use the 'path_id' to obtain an ordered list of (x,y) control tuples
                controls += [GetControls(conn, infoTuple[1])] 
            return (controlsInfo, controls)
    
    def __CheckIfDataFilesAvailable(self, controlsInfo, directory, suffix = None):
        # Check if .json files are available for some of the input path_ids
        # Output a modified list of tuples (with a boolean on the last place)
        # that tells if data is already in the data folder or not
        fileList = os.listdir(directory)
        if suffix is not None:
            fileList = list(filter(lambda x: x.endswith('.json'), fileList))
            fileList = list(map(lambda x: x.replace('.json', ''), fileList))
        fileDict = set()
        for item in fileList:
            fileDict.add(item)
        controlsInfo = [(*x, x[1] in fileDict) for x in controlsInfo]
        return controlsInfo
    
    def __CheckIfOutputFilesAvailable(self, jsonFiles, logsPath, suffix = None):
        # DISABLED for now (convolution refactor)

        # Check the output (logs) file directory for paths maching input json data files
        # Output a set of .json files that correspond to 
        # paths that have already been processed and outputs are available
        # fileList = os.listdir(logsPath)
        # fileList = list(map(lambda x: ntpath.basename(x.replace(suffix, '')), fileList ))
        # jsonFilesCopy = jsonFiles
        # jsonFilesCopy = list(map(lambda x: (ntpath.basename(x.replace('.json', '')), x), jsonFilesCopy ))
        # jsonFilesDict = {}
        # for item in jsonFilesCopy:
        #     jsonFilesDict[item[0]] = item[1]
        # doneFiles = set()
        # for item in fileList:
        #     if item in jsonFilesDict:
        #         doneFiles.add(jsonFilesDict[item])
        # # return doneFiles
        return set()
        
    def __DumpControlsJson(self, workDirectory, controlCollection, additionalJSONData = None):
        # Serialize ll the obtained control information into .json files
        # Skip serialization if the .json file is already there (has been processed in a previous launch)
        filenameTemplate = os.path.join("{workDirectory}", "{path_id}.json")
        controlsInfo = controlCollection[0]
        controlsData = controlCollection[1]
        fileNames = []
        for index in range(len(controlsInfo)):
            path_id = controlsInfo[index][1]
            fileName = filenameTemplate.format(workDirectory = workDirectory,
                                                               path_id = path_id)
            fileNames += [ fileName ]
            
            # construct the filename but don't serialize the data to files (the file already exists)
            if not (controlsInfo[index][-1]):
                self.__DumpSingleControlJson(fileName, controlCollection, index)

            # append/modify the existing json file with the additional arbitrary data (currently not very efficient) 
            if additionalJSONData:
                localJsonData = AdditionalJSONData( additionalJSONData.GetDataDict() )
                if "performConvolution" in additionalJSONData.GetDataDict() and additionalJSONData.GetDataDict()["performConvolution"]:
                    levelKernel = self._mLevelsInfo[controlsInfo[index][0]].GetConvolutionKernel()
                    localJsonData.GetDataDict()["convKernel"] = levelKernel['y']                    
                localJsonData.ModifyDataJSONFile(fileName)            
        return fileNames
    
    def __DumpSingleControlJson(self, fileName, controlCollection, controlIndex):
        # Serialize control information to a .json file
        singleControlInfo = controlCollection[0][ controlIndex ]
        singleControlData = controlCollection[1][ controlIndex ]
        jsonData = self.__ConstructJsonData(singleControlInfo, singleControlData)
        logging.debug("Saving to {}".format(fileName))
        with open(fileName, 'w') as fh:
            json.dump(obj = jsonData, fp = fh)

    def __DumpReturnInfoToCSV(self, outputs, csvOutputPath):
        # Dump the whole validation return info to a CSV file
        returnInfo = outputs['returnInfo']
        logging.info("Serializing validation output ({} results) info in {}".format(len(returnInfo), csvOutputPath))
        delimiter = ','
        with open(csvOutputPath, 'w') as fh:
            fh.write( ValidationDatum.DumpCSVHeader(delimiter = delimiter) )
            for item in returnInfo:
                fh.write(item.DumpCSVString(delimiter = delimiter))
            
#     def __DumpSingleJoinControlJson(self, fileName, controlCollection, controlInfoRecord):
#         # Serialize control information to a .json file
#         singleControlInfo = controlInfoRecord
#         singleControlData = controlCollection[controlInfoRecord]
#         jsonData = self.__ConstructJsonData(singleControlInfo, singleControlData)
#         logging.debug("Saving to {}".format(fileName))
#         with open(fileName, 'w') as fh:
#             json.dump(obj = jsonData, fp = fh)            
            
    def __ConstructJsonData(self, info, data):
        # Construct a .json (verification input) file name
        jsonData = {}
        # Get general information
        jsonData['level_id'] = float( info[0] ) #  required hack to have the qengine DataContainer load it...
        jsonData['path_id'] = info[1]
        jsonData['final_fidelity'] = info[2]
        jsonData['duration'] = info[3]
        jsonData["performConvolution"] = 0 # disable convolutions by default
        
        # Construct controls data
        dataX = []
        dataY = []
        for point in data:
            dataX += [point[0]]
            dataY += [point[1]]
        jsonData['physics_x'] = dataX
        jsonData['physics_y'] = dataY
        return jsonData
        
    def __ConstructLogFilename(self, file, logsPath):
        # Construct a 'log' (verification output) file name
        fileName = os.path.split(file)[1]
        fileName = fileName[:fileName.rfind('.')] + ".txt"
        outputFile = os.path.join(logsPath, fileName)
        return outputFile
        
    def __LaunchVerificationAsync(self, programPath, jsonFiles, logsPath):
        # This routine takes care of launching the verification programs in async mode 
        # polling for their retcodes and postprocessing verification ouputs
        procs = []
        outputFiles = []
        logging.info("Launching the verification programs")
        
        # Check if the outputs directory already contains results of previous run -> we can reuse the results
        doneDataFiles = self.__CheckIfOutputFilesAvailable(jsonFiles, logsPath, '.txt')
        
        batchSize = 50
        # Iterate throught the data files (.json containing controls, durations etc.) and launch verification on them
        for file in jsonFiles:
            outputFile = self.__ConstructLogFilename(file, logsPath)
            outputFiles += [ outputFile ]
            if file in doneDataFiles:
                logging.info("Skipping path_id: \'{}\' (detected a log for this path in the output logs folder).".format(outputFile))
                continue
            procs += [ self.__LaunchSingleVerificationAsync(programPath = programPath, dataFile = file, output = outputFile)]

            while(sum(list(map(lambda x: 1 if x.poll() is None else 0, procs))) > batchSize):
                time.sleep(1)
                logging.info("Waiting to start up more processed")
        finished = False
        
        # Wait for all of the programs to return
        while(not finished):
            retcodes = list(map(lambda x: x.wait(), procs))
            finished = all(map(lambda x: x is not None, retcodes))
            
        # Launch parsing of the output data
        logging.info("Parsing the verification outputs...")
        outputs = []
        for outputFile in outputFiles:
            logging.info("Parsing {}".format(outputFile))
            # TODO:: refactor this monstrosity
            path_id =  ntpath.basename(outputFile).replace('.txt', '')
            (expectedF, obtainedF, 
             retcode, expectedDuration, 
             obtainedDuration, numberOfPoints, levelId) = self.__ParseVerificationOutput( outputFile )
            outputs += [ ValidationDatum(path_id, CompletedProcess(outputFile, retcode), 
                                              expectedF, obtainedF, 
                                              retcode, expectedDuration, 
                                              obtainedDuration, numberOfPoints, 
                                              levelId)]
        return outputs
        
    def __LaunchSingleVerificationAsync(self, programPath, dataFile, output):
        # Launch a process with the verification program (asynchronously)
        logging.info("Launching verification program \"{}\" with json: \"{}\"".format(programPath, dataFile))
        with open(output, 'w') as outputFH:
            proc = subprocess.Popen([ programPath, dataFile], stdout = outputFH, stderr = outputFH)
        return proc
            
#     def __LaunchVerification(self, programPath, jsonFiles, logsPath):
#         outputs = []
#         logging.info("Launching the verification programs...")
#         for file in jsonFiles:
#             outputFile = self.__ConstructLogFilename(file, logsPath)
#             outputs += [ self.__LaunchSingleVerification(programPath = programPath, dataFile = file, output = outputFile) ]
#         return outputs
        
#     def __LaunchSingleVerification(self, programPath, dataFile, output):   
#         logging.debug("Launching verification program \"{}\" with json: \"{}\"".format(programPath, dataFile))
#         with open(output, 'w') as outputFH:
#             retCode = subprocess.run([programPath, dataFile], stdout = outputFH, stderr = outputFH)
#             (expectedF, obtainedF) = self.__ParseVerificationOutput(output)
#         taskOutput = ValidationDatum( retCode, expectedF, obtainedF )
#         return taskOutput
    
    def __ParseVerificationOutput(self, fileName):
        # Extract expected fidelity, obtained fidelity and program returncode (simply cached for later) 
        # from the verification program output
        patternExpectedF = "Expected fidelity: (\d+\.\d+)"
        patternObtainedF = "Obtained fidelity: (\d+\.\d+)"
        patternRetcode = "Retcode: (\d)"
        patternExpectedD = "Expected duration: (\d+\.\d+)"
        patternObtainedD = "Obtained duration: (\d+\.\d+)"
        patternNumPoints = "Number of points: (\d+)"
        patternLevelId = "Level id: (\d+)"
        with open(fileName, 'r') as fh:
            data = fh.read()
            expectedF = float( ExtractData( pattern = patternExpectedF, 
                                            data = data, 
                                            refFileName = fileName, 
                                            expectedMatches = 1)[0])
              
            obtainedF = float( ExtractData( pattern = patternObtainedF, 
                                            data = data, 
                                            refFileName = fileName, 
                                            expectedMatches = 1)[0])            
            retcode = int( ExtractData( pattern = patternRetcode, 
                                            data = data, 
                                            refFileName = fileName, 
                                            expectedMatches = 1)[0])
            expectedDuration = float( ExtractData( pattern = patternExpectedD, 
                                            data = data, 
                                            refFileName = fileName, 
                                            expectedMatches = 1)[0]) 
            obtainedDuration = float( ExtractData( pattern = patternObtainedD, 
                                            data = data, 
                                            refFileName = fileName, 
                                            expectedMatches = 1)[0]) 
            numberOfPoints = int( ExtractData( pattern = patternNumPoints, 
                                            data = data, 
                                            refFileName = fileName, 
                                            expectedMatches = 1)[0]) 
            levelId = int( ExtractData( pattern = patternLevelId, 
                                            data = data, 
                                            refFileName = fileName, 
                                            expectedMatches = 1)[0]) 
        return (expectedF, obtainedF, retcode, expectedDuration, obtainedDuration, numberOfPoints, levelId)
        
    def VerifyPaths(self, paths):
        out = True
        for path in paths:
            if not os.path.exists(path):
                logging.error("\"{}\" does not exist.".format(path))
                out = False
        return out
    
    def __ComputeSuccessRate(self, retcodes):
        # Take all the return codes of the verified paths
        # retcodes = 0 mean successful verification, the rest failed for some reason
        if len(retcodes) == 0:
            logging.warning("No controls were verified by the program.")
            return
        counter = {}
        for item in retcodes['returnInfo']:
            if item.GetRetcode() not in counter:
                counter[item.GetRetcode()] = 1
            else:
                counter[item.GetRetcode()] += 1
        allControls = 0
        for item in counter.values():
            allControls += item
        if 0 not in counter:
            counter[0] = 0
        logging.info("Successful verification: {}% of the controls.".format(100 * float(counter[0]) / (float(allControls))))
    
    def __ComputeFidelityStdevMean(self, outputs):
        # Compute stdev and mean of fidelity differences obtained in verification
        retcodes = outputs['returnInfo']
        if len(retcodes) == 1:
            return
        for item in retcodes:
            logging.info("E: {} O: {} Return code: {} Data file: {} path_id: {}".format(item.GetExpectedFidelity(), item.GetObtainedFidelity(), item.GetRetcode(), item.mCompletedProcess.mDataFile, item.GetPathId()))
        diffList = list(map(lambda x: x.GetExpectedFidelity() - x.GetObtainedFidelity(), retcodes))
        stdevOut = statistics.stdev(diffList)
        meanOut = statistics.mean(map(lambda x: abs(x), diffList))
        logging.info("STDEV of expected and obtained fidelities: {}".format(stdevOut))
        logging.info("MEAN of expected - obtained fidelities: {}".format(meanOut))


### Similar to the Controls Verifier, it allows for acquisition and data visualization for controls, e.g. wave function evolution over time etc.
class PhysicsDataCollector(ControlsVerifier):
    def __init__(self):
        pass

    def ProcessControlsJSON(self, jsonListFile, outputCSVFile, programPath, outputDataDirectory, suffix = "_physics_data"):
        logging.info("Initializing the controls processing.")

        # Read the input json file
        logging.info("Opening the input JSON file {}".format(jsonListFile))
        with open(jsonListFile, 'r') as fh:
            jsonData = json.load(fh)
        
        logging.info("Querying for \"seed_filenames\" and \"output_filenames\"")
        inputFileList = jsonData["seed_filenames"]
        inLen = len(inputFileList)
        logging.info("Total of {} input seed files.".format(inLen))

        outputFileList = jsonData["output_filenames"]
        outLen = len(outputFileList)
        logging.info("Total of {} output files.".format(outLen))                

        if (outLen != inLen):
            mess = "Mismatch of the number of input and output file names. ({} input, {} outputs)".format(inLen, outLen)
            logging.error( mess )
            raise ValueError(mess)

        logging.debug("Listing seed file names for computing F/Psi.")
        for item in inputFileList:
            logging.debug(item)
        logging.debug("Done")

        logging.info("Attempting to create the directory: {}".format(outputDataDirectory))
        try:
            os.mkdir(outputDataDirectory)
        except FileExistsError as err:
            logging.warning("Warning: Directory already exists. {}".format(err))
        
        # create the directory for the CSV file
        targetCSVDir = os.path.dirname(outputCSVFile)
        logging.info("Attempting to create the directory: {}".format(targetCSVDir))      
        try:
            os.mkdir( targetCSVDir )
        except FileExistsError as err:
            logging.warning("Warning: Directory already exists. {}".format(err))
        
        logging.info("Launching the controls processing...")
        index = 0
        for (inp, output) in zip(inputFileList, outputFileList):
            logging.debug("Launching: {} > {} > {}".format(inp, programPath, output))            
            out = subprocess.run([programPath, inp, output], capture_output = True)

            if (out.returncode != 0):
                logging.warning("Error (retcode: {rc}) for seed: {fname}.\nFull report: {full}".format(rc = out.returncode, 
                                                                                                       fname = inp, 
                                                                                                       full = out))
            if (index % 10 == 0):
                logging.info("Processing seed {} out of {}.".format(index + 1, inLen))
                index += 1

        logging.info("Postprocessing the output files to create the CSV: {}".format(outputCSVFile))
        with open(outputCSVFile, 'w') as csvFH:
            csvFH.write("seed_filename|output_filename|fidelity\n")
            for (inp, output) in zip(inputFileList, outputFileList):
                with open(output, 'r') as fh:
                    outputDatum = json.load(fh)
                    fidelity = outputDatum["real_fidelity"]
                    csvFH.write("{}|{}|{}\n".format(inp, output, fidelity))


    def ProcessPathIds(self, dbCredentials, workDirectory, programPath, outputDirectory, pathIds, suffix = "_physics_data"):
        self.VerifyPaths(paths = [programPath, workDirectory])

       # Create the connection
        try:
            logging.info("Attempting to connect to the database...")
            conn = connection.MySQLConnection(**dbCredentials)
            logging.info("Connection made")
            self._mLevelsInfo = GetLevelsInfo(conn)
            controlCollection = self._ControlsVerifier__ObtainControlsProcedurePathIdList(conn, pathIds, workDirectory)
        except mysql.connector.Error as err:
            logging.critical("Error: {}".format(err.msg))
            return
        else:
            conn.close()

        jsonFiles = self._ControlsVerifier__DumpControlsJson(workDirectory, controlCollection)
        MakeDir(outputDirectory)
        logsPath = os.path.join(outputDirectory, "logs")
        MakeDir(logsPath)
        return self.__LaunchCollectionAsync(   programPath = programPath, 
                                        jsonFiles = jsonFiles, 
                                        outputDirectory = outputDirectory, 
                                        logsPath = logsPath,
                                        suffix = suffix)

    def __LaunchCollectionAsync(self, programPath, jsonFiles, outputDirectory, logsPath, suffix):
        # This routine takes care of launching the collector programs in async mode 
        # polling for their retcodes and postprocessing verification ouputs
        procs = []
        logs = []
        outputFiles = []
        logging.info("Launching the collector programs")

        # Iterate throught the data files (.json containing controls, durations etc.) and launch verification on them
        for file in jsonFiles:
            log = self._ControlsVerifier__ConstructLogFilename(file, logsPath)
            out = self.__ConstructOutputFileName(file, outputDirectory, suffix)
            outputFiles += [out]
            procs += [ self.__LaunchSingleCollectionAsync(programPath = programPath, dataFile = file, output = out, log = log)]
        finished = False
        
        # Wait for all of the programs to return
        while(not finished):
            retcodes = list(map(lambda x: x.wait(), procs))
            finished = all(map(lambda x: x is not None, retcodes))
            
        logging.info("Done")
        return outputFiles
        
    def __LaunchSingleCollectionAsync(self, programPath, dataFile, output, log):
        # Launch a process with the verification program (asynchronously)
        logging.info("Launching collector program \"{}\" with json: \"{}\", output: \"{}\", log file: \"{}\"".format(programPath, dataFile, output, log))
        with open(log, 'w') as outputFH:
            proc = subprocess.Popen([ programPath, dataFile, output], stdout = outputFH, stderr = outputFH)
        return proc

    def __ConstructOutputFileName(self, file, outputDirectory, suffix):
        # Construct a 'log' (verification output) file name
        fileName = os.path.split(file)[1]
        fileName = fileName[:fileName.rfind('.')] + suffix + ".json"
        outputFile = os.path.join(outputDirectory, fileName)
        return outputFile


def LoadValidationCSV(filename):
    logging.info("Opening {}".format(filename))
    with open(filename, 'r') as fh:
        logging.info("Loading file {}".format(filename))
        dataCSV = csv.reader(fh, delimiter = ',')
        data = []
        for line in dataCSV:
            data += [ line ]
        logging.info("Skipping CSV Header line")
        data = data[1:] # skup the CSV header  
    return data
    
def ParseValidationData(dataList):
    output = []
    for datum in dataList:
        output += [ValidationDatum(path_id = datum[0], 
                                   retcode = datum[1], 
                                   expectedF = datum[2], 
                                   obtainedF = datum[3],
                                   expectedDuration = datum[4],
                                   obtainedDuration = datum[5],
                                   numberOfPoints = datum[6],
                                   levelId = datum[7], 
                                   completedProcess = None)]
    return output

def GetUnoptmizedPathIds(dbCredentials):
    conn = connection.MySQLConnection(**dbCredentials)   
    query = ConstructQuery('SELECT t.path_id FROM basic_path_info AS t WHERE t.created_at >= DATE(\'2018-12-01\') AND level_id IN (7,11,16) AND optimization_iteration = 0;')
    data = GetCursorData(ExecuteQuery(conn, query))
    data = list(map(lambda x: x[0], data))
    return data

def GetUnoptmizedPathIds(dbCredentials):
    conn = connection.MySQLConnection(**dbCredentials)   
    query = ConstructQuery('SELECT t.path_id FROM basic_path_info AS t WHERE t.created_at >= DATE(\'2018-12-01\') AND level_id IN (7,11,16) AND optimization_iteration = 0;')
    data = GetCursorData(ExecuteQuery(conn, query))
    data = list(map(lambda x: x[0], data))
    return data