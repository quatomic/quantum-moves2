# -*- coding: utf-8 -*-


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.neighbors.kde import KernelDensity
import pickle
import json, os

import scipy.interpolate as si
import h5py

# =============================================================================
# Common definitions
# =============================================================================

lev_idx = 0

lev_sel = [16, 11, 7][lev_idx]
lev_fol = ['bhw', 'gpe_split', 'gpe_shakeup'][lev_idx]
lev_name = ['BringHomeWater', 'Splitting', 'ShakeUp'][lev_idx]

T_QSL = [0.37, 0.92, 0.89][lev_idx]
x_lim_ar = np.array([0,1.2])

methods_l = ['GRAPE_RS', 'SGA_BUCKET_RS', 'SGA_RS']

comp_file = 'computerData/' + lev_fol + '/serialized_' + lev_name + '_' + str(lev_sel) + '_'

df_temp = pd.DataFrame(columns = ['dur', 'infid'])
df_dict = {}

data_path = '/seeding_at_T=0p26985/Grendel_run/output_BHW_MG_14_02_20/Work/quantum-moves2-conventional-algorithms/programs/jobs/bhw/output_BHW/'

data_key_list = ['F_ref', 'Ff',  'Fi',  'T',  'dim',  'dt', 'fidelityHistory', 'fpp', 'iter', 'nBFGSRestarts', 
 'nRollbacksPerformed', 'nUnrollsPerformed', 'optimization_walltime','optimizertype', 
 'piecewisePieces', 'stopCode', 'stopMsgLegend', 'u_ref', 'uf', 'ui']

df_save = 'fidelity_and_pieces_2_3.pckl'  # should be all the new 2000 seeds per type

data_list = os.listdir(data_path)

load_raw_data = True

df = pd.DataFrame(columns = ['fid', 'dur', 'n_piec', 'wall_time', 'stop_code', 'n_iter'])

if load_raw_data:
    i_ = 0
    for f_ in data_list:
        if '_GRAPE_RS.mat' in f_:
            full_name = data_path + f_
            
            with h5py.File(full_name,'r') as fid:
                data_keys = list(fid.keys())
                n_piec = int(np.array(fid.get('piecewisePieces'))[0,0])
                fidelity = float(np.array(fid.get('Ff'))[0,0])
                dur = float(np.array(fid.get('T'))[0,0])
                opt_walltime = float(np.array(fid.get('optimization_walltime'))[0,0])
                stop_code = int(np.array(fid.get('stopCode'))[0,0])
                n_iter = int(np.array(fid.get('iter'))[0,0])
            
            
            df.loc[i_] = [fidelity, dur, n_piec, opt_walltime, stop_code, n_iter]
            i_ += 1       
            
            if np.mod(i_, 100) == 1:
                print('Evaluated ', i_)
                    
    with open(df_save, 'wb') as fid:
        pickle.dump(df, fid)
    
else:
    with open(df_save, 'rb') as fid:
        df = pickle.load(fid)
#####################################
        
df['n_piec'] = df['n_piec'].apply(int)
df['infid'] = 1 - df['fid']
df['infid_log'] = df['infid'].apply(np.log10)
        
fid = df['fid'].values
infid = 1 - fid
infid_log = np.log10(infid)
n_piec = df['n_piec'].values

infid_mean = df.groupby('n_piec').mean()['infid_log']
infid_std = df.groupby('n_piec').std()['infid_log']

n_piec_ar = np.array(np.log2(infid_mean.index))
        
plt.figure()
plt.loglog(n_piec, infid,'.')

plt.figure()
plt.plot(n_piec_ar, infid_mean )
plt.fill_between(n_piec_ar, infid_mean - infid_std, infid_mean + infid_std, alpha = 0.5)
plt.ylabel('log10(infidelity)')
plt.xlabel('log2(n_pieces)')

##########################
# estimate density
n_bin = 50

il_mesh = np.linspace(-3.0, 0, n_bin + 1)
piec_mesh = np.unique(df['n_piec'])
n_val = len(piec_mesh)
x_grid = np.arange(n_val)

extent = [x_grid.min(), x_grid.max() + 1, il_mesh.min(), il_mesh.max()]

def x_grid_bin(piec):
    return sum(piec > piec_mesh)

df['x_grid_bin'] = df['n_piec'].apply(x_grid_bin)

###########################
# define logarithmic tics manually
dec_ar = np.arange(1,0.1,-0.1)

y_tic_pos = np.array(list(dec_ar) + list(dec_ar/10) + list(dec_ar/100) + [0.001])
y_tic_pos = np.log10(y_tic_pos)
y_tic_lab = ['' for x_t in y_tic_pos]
y_tic_lab[0] = 1
y_tic_lab[9] = 0.1
y_tic_lab[18] = 0.01
y_tic_lab[27] = 0.001

norm_ar = np.empty(n_val)
dens_ar = np.empty((n_bin, n_val))
for i_, p_ in enumerate(piec_mesh):
    is_piec = n_piec == p_
    
    counts, edges = np.histogram(infid_log[is_piec], il_mesh)

    norm_ar[i_] = np.sum(counts)

    dens_ar[:,i_] = np.flip(counts)/ norm_ar[i_]
######################

infid_family = np.log10([0.008597, 0.00124])    # tunneling, shoveling 
color = ['orange', 'green']

n_seeds = norm_ar.mean()

plt.figure()
aspect = np.abs(extent[1] - extent[0])/(extent[3] - extent[2])
        
plt.imshow(dens_ar, cmap = 'gist_stern', vmax = 0.1, interpolation='none', extent=extent, aspect = aspect)

plt.colorbar()

plt.yticks(y_tic_pos, y_tic_lab)
plt.xticks(x_grid + 0.5, piec_mesh)
plt.xlabel('number of seed control segments')
plt.ylabel('Infidelity')
plt.title('RS GRAPE in BHW at T = 0.27; %d seeds per type' % (n_seeds,))

for inf_, c_ in zip(infid_family, color):
    plt.plot([extent[0], extent[1]], inf_*np.ones(2), '--',color = c_)

is_shovel = infid_log < infid_family[0]

plt.plot(df['x_grid_bin'].values[is_shovel] + 0.5, infid_log[is_shovel], 'wx')


plt.figure()
plt.subplot(2,1,1)
counts, bins = np.histogram(df['x_grid_bin'].values[is_shovel], np.arange(n_val + 1) + 0.5)
plt.bar(x_grid + 1, counts)
plt.xticks(x_grid, piec_mesh)
plt.xlim([x_grid.min() - 0.5, x_grid.max() + 0.5])
plt.xlabel('number of seed control segments')
plt.ylabel('N solutions below front-swing QSL')
plt.title('RS GRAPE in BHW at T = 0.27; %d seeds per type' % (n_seeds,))

plt.figure()
plt.plot(piec_mesh, norm_ar)

print('fraction of optimization time outs:', sum(df['stop_code'] == 5)/len(df))

hist_cols = ['wall_time', 'stop_code', 'n_iter']

for h_c in hist_cols:
    plt.figure()
    plt.hist(df[h_c], 50)
    plt.xlabel(h_c)