import mysql
from  mysql.connector import connection
import json
import subprocess
import os
import sys
import statistics
import logging
import re
import csv
import ntpath
import datetime
import copy
import numpy as np



# In[ ]:


def MakeLogFileName(workDir, name):
    return  os.path.join(workDir, name + str(datetime.datetime.now()))


# In[3]:


def ConfigureLoggingDefault():
    logging.basicConfig(
        format='%(asctime)s %(levelname)-8s %(message)s',
        level=logging.INFO,
        stream = sys.stdout,
        datefmt='%Y-%m-%d %H:%M:%S')
    
def ConfigureLoggingFile(fname):
    logging.basicConfig(
        filename = fname,
        filemode = 'a',
        format='%(asctime)s %(levelname)-8s %(message)s',
        level=logging.INFO,
        datefmt='%Y-%m-%d %H:%M:%S')


# Miscellaneous
def MakeDir(path):
    try:
        os.mkdir(path)
    except FileExistsError:
        pass
    return path


# In[6]:


# Query construction
def ExecuteQuery(conn, query):
    logging.info("Executing query: \"{}\"".format(query))
    cursor = conn.cursor()
    cursor.execute( query )
    return cursor

def ConstructQuery(query, **kwargs):
    if kwargs:
            logging.debug("Preparing query: \"{}\"".format(query.format(**kwargs)))
            return query.format(**kwargs)
    return query


# In[7]:

# Data acquisition
def GetCursorData(cursor):
    data = cursor.fetchall()
    cursor.close()
    return data

def GetControls(conn, path_id):
    # TODO:: remove the SQL query
    # Construct query which pulls the (x,y) data for a specific control
    query = ConstructQuery( 'placeholder',
                           **{'path_id' : path_id})
    cursor = ExecuteQuery(conn, query)
    return GetCursorData( cursor )
   
def GetControlsInfoWithPathId(conn, pidList):
    if len(pidList) == 0:
        logging.error("Invalid path_id list (empty) to construct query.")
        exit(1)
    pidString = "("
    for pid in pidList[:-1]:
        pidString += "\'{}\',".format(pid)
    pidString += "\'{}\')".format(pidList[-1])
    # Construct query which pulls the (x,y) data for specific controls
    query = ConstructQuery( 'placeholder',
                           **{'path_ids' : pidString})  
    cursor = ExecuteQuery(conn, query)
    return GetCursorData( cursor )
    

def GetControlsInfo(conn): 
    # Construct query which pulls the meta data for specific controls
    query = ConstructQuery( 'placeholder')                              
    cursor = ExecuteQuery(conn, query)
    return GetCursorData( cursor )

# based on the query right above
def GetControlInfoParser():
    return lambda x: {  "level_id" : x[0], 
                        "path_id": x[1], 
                        "final_fidelity": x[2], 
                        "duration": x[3],
                        "optimization_iteration" : x[4]}

def GetSingleControlsInfo(conn, path_id):
    logging.info("Launching query for path_id: {}...".format(path_id))
    # Construct query which pulls the meta data for a specific control
    query = ConstructQuery( 'placeholder', **{'path_id' : path_id})     
    cursor = ExecuteQuery(conn, query)
    logging.info("Collecting query results...")
    return GetCursorData( cursor )

def GetLevelsInfo(conn):
    logging.info("Launching query for level info")
    # Construct query which pulls the meta data for game specific levels
    query = ConstructQuery( 'placeholder')    
    cursor = ExecuteQuery(conn, query)
    logging.info("Collecting query results...")
    return GetCursorData( cursor )

class LevelInfo:
    # POD that contains level information
    def __init__(self, tup):
        self._mId = tup[0]
        self._mOrderInGame = tup[1]
        self._mLevelName = tup[2]
        self._mTQSL = tup[3]
        self._mDt = tup[4]
        self._mPhysicsEngine = tup[5]
        self._mPhysicsModel = tup[6]
        self._mHasOptimization = tup[7]
        self._mSpecialScientificInterest = tup[8]
        self._mCreatedAt = tup[9]
        self._mUpdatedAt = tup[10]

    def GetDt(self):
        return self._mDt

    def GetId(self):
        return self._mId

def ConstructLevelsInfo(conn):
    # Construct a dictionary with key = level_id, value = full level info
    data = GetLevelsInfo(conn)
    output = {}
    for datum in data:
        lvlInfo = LevelInfo(datum)
        output[ lvlInfo.GetId() ] = lvlInfo
    return output 

# In[8]:
class AdditionalJSONData:
    def __init__(self, addData):
        assert(type(addData) == dict)
        self._mData = copy.deepcopy(addData)

    def ModifyDataJSONFile(self, fileName):
        with open(fileName, 'r') as fhread:
            logging.info("Attempting to append modifications (additional data) to {}".format(fileName))
            jsonData = json.load(fhread)
            (outputJSONData, wasModified) = self.ModifyMerge(ref = jsonData, mod = self._mData)

        if (wasModified):
            with open(fileName, 'w') as fhwrite:
                json.dump(outputJSONData, fhwrite)
                if wasModified:
                    logging.info("{} has been modified".format(fileName))
        return wasModified

    def ModifyMerge(self, ref, mod):
        output = {}
        for (k, v) in ref.items():
            output[k] = v
        for (k, v) in mod.items():
            output[k] = v

        return (output, True)

    def ModifyDataDict(self, mod):
        (self._mData, __) = self.ModifyMerge(ref = self._mData, mod = mod)

    def GetDataDict(self):
        return self._mData

class ValidationDatum:
    def __init__(self, path_id, completedProcess, expectedF, 
                 obtainedF, retcode, expectedDuration, obtainedDuration, 
                 numberOfPoints, levelId):
        self.mPathId = str(path_id)
        self.mCompletedProcess = completedProcess
        self.mExpectedFidelity = float(expectedF)
        self.mObtainedFidelity = float(obtainedF)
        self.mRetcode = int(retcode)
        self.mExpectedDuration = float(expectedDuration)
        self.mObtainedDuration = float(obtainedDuration)
        self.mNumPoints = int(numberOfPoints)
        self.mLevelId = int(levelId)
        self.mOptimizerIteration = None
        
    def GetExpectedFidelity(self):
        return self.mExpectedFidelity
    
    def GetObtainedFidelity(self):
        return self.mObtainedFidelity
    
    def GetRetcode(self):
        return self.mRetcode
    
    def GetPathId(self):
        return self.mPathId
    
    def GetExpectedDuration(self):
        return self.mExpectedDuration
    
    def GetObtainedDuration(self):
        return self.mObtainedDuration
    
    def GetNumberOfPoints(self):
        return self.mNumPoints
    
    def GetLevelId(self):
        return self.mLevelId
    
    def DumpCSVString(self, delimiter):
        return "{PAI}{d}{RET}{d}{EF}{d}{OF}{d}{ED}{d}{OD}{d}{NUM}{d}{LID}\n".format(PAI = self.GetPathId(), 
                                                      RET = self.GetRetcode(), 
                                                      EF = self.GetExpectedFidelity(), 
                                                      OF = self.GetObtainedFidelity(), 
                                                      ED = self.GetExpectedDuration(),
                                                      OD = self.GetObtainedDuration(),
                                                      NUM = self.GetNumberOfPoints(),
                                                      LID = self.GetLevelId(),
                                                      d = delimiter)
    @staticmethod
    def DumpCSVHeader(delimiter):
        return "path_id,return_code,expected_fidelity,obtained_fidelity,expected_duration,obtained_duration,number_of_points,level_id\n"
        
    def RequestOptimizationIteration(self, creds):
        # Construct query which pulls the maximum optimization iteration for a specific control
        query = ConstructQuery( 'placeholder', **{'path_id' : self.GetPathId()})    
        try:
            logging.info("Attempting to connect to the database...")
            conn = connection.MySQLConnection(**creds)
            logging.info("Connection made")
            data = GetCursorData(ExecuteQuery(conn, query))        
        except mysql.connector.Error as err:
            logging.critical("Error: {}".format(err.msg))
            return
        else:
            conn.close()        
        if data == []:
            return False
        else:
            self.mOptimizerIteration = data[0]
            return True

    def HasOptimizationIteration(self):
        return self.mOptimizerIteration is not None


    def GetOptimizationIteration(self):
        return self.mOptimizerIteration

class ValidationDatumTBuckets:
    def __init__(self, validationDatumCollection):
        self.mValidationSeedDatumCollection = validationDatumCollection
        self.__CollectPutSeedsToBuckets(validationDatumCollection)
        self.SortTBuckets()

    def __SingleArraysToList(self, input):
        return list(map(lambda x: x[0], input))

    def __CollectPutSeedsToBuckets(self, validationDatumCollection):
        self.mTSeedDict = {}
        self.buckets = [[2 * i * 0.05, (2 * i + 2) * 0.05] for i in range(12)]
        self.bucketNames = [x for x in range(12)]
        self.bucketNameDict = {x : self.buckets[x] for x in self.bucketNames}
        self.mTSeedDict = {bucketId: [] for bucketId in self.bucketNames}
        checkInBucket = lambda x, y: x.GetObtainedDuration() >= y[0] and x.GetObtainedDuration() < y[1]
        for seedDatum in validationDatumCollection:
            for bucketId in self.bucketNames:
                if (checkInBucket(seedDatum, self.bucketNameDict[bucketId])):
                    midPoint = round((self.buckets[bucketId][1] + self.buckets[bucketId][0])/2, 2)
                    self.mTSeedDict[bucketId] += [seedDatum]

    def SortTBuckets(self):
        self.mTSeedDictSorted = {}
        for (k, v) in self.mTSeedDict.items():
            sortT = sorted(v, key = lambda x: x.GetObtainedFidelity(), reverse = True)
            self.mTSeedDictSorted[k] = sortT
        return self.mTSeedDictSorted
    
    def GetSortedBucketList(self):
        if self.mTSeedDictSorted is None:
            self.SortTBuckets()
        output = []
        for (k, v) in self.mTSeedDictSorted.items():
            output += [(k,v)]
        output = sorted(output, key = lambda x: x[0])
        return output
    
    def GetFTCurve(self):
        bucketList = self.GetSortedBucketList()
        times = list(map(lambda x: float(x[1][0].GetObtainedDuration()), bucketList ))
        topF = list(map(lambda x: float(x[1][0].GetObtainedFidelity()), bucketList))
        return (times, topF)

    def SelectTopUniqueSeedsNumberPerBucket(self, pathId_seedPathIdMapping, numberOfSeeds):
        if numberOfSeeds < 0:
            assert("Please select a positive number of seeds (not {})".format(numberOfSeeds))
        
        if self.mTSeedDictSorted is None:
            self.SortTBuckets()        

        outputs = {}
        pathIdSet = set()
        for (bucketId, seedList) in self.mTSeedDictSorted.items():
            seedsInBucket = len(seedList)
            if seedsInBucket > numberOfSeeds:
                subOutput = []
                for seed in seedList:
                    if (len(subOutput) >= numberOfSeeds):
                        break

                    if pathId_seedPathIdMapping[seed.GetPathId()] in pathIdSet:
                        continue
                    else:
                        pathIdSet.add( pathId_seedPathIdMapping[seed.GetPathId()] )
                        subOutput += [seed]
            else:
                subOutput = seedList

            outputs[bucketId] = subOutput
        return outputs


    def GetIFTCurve(self):
        bucketList = self.GetSortedBucketList()
        times = list(map(lambda x: float(x[1][0].GetObtainedDuration()), bucketList))
        topIF = list(map(lambda x: 1 - float(x[1][0].GetObtainedFidelity()), bucketList))
        return (times, topIF)
    
    def GetFTCurveDurationNotNorm(self, QSL):
        ftCurve = self.GetFTCurve()
        durations = ftCurve[0]
        durations = list(map(lambda x: x * QSL, durations))
        return (durations, ftCurve[1])

    def GetIFTCurveDurationNotNorm(self, QSL):
        iftCurve = self.GetIFTCurve()
        durations = iftCurve[0]
        durations = list(map(lambda x: x * QSL, durations))
        return (durations, iftCurve[1])

    def GetQSLSeed(self, F_QSL):
        bestSeed = None
        for seed in self.mValidationSeedDatumCollection:
            if seed.GetObtainedFidelity() >= F_QSL:
                if not bestSeed:
                    bestSeed = seed
                    continue
                if bestSeed and bestSeed.GetObtainedDuration() > seed.GetObtainedDuration():
                    bestSeed = seed
        if not bestSeed:
            logging.error("No seed compliant to F_QSL = {fqsl} was found".format(fqsl = F_QSL))
        return bestSeed

class CompletedProcess:
    def __init__(self, dataFile, retcode):
        self.mDataFile = dataFile
        self.returncode = retcode
