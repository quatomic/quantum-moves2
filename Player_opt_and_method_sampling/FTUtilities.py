import json
import logging
import os.path
import sys
import h5py
import numpy as np
import concurrent
import multiprocessing
import pickle
import matplotlib.pyplot as plt
import shutil
from importlib import reload 
import CVUtilities as cval
cval = reload(cval)

exceptionType = BaseException

# Takes the directory name, produces the file names of all the seeds and metadata file contained within the directory
class FTFileNames:
    def __init__(self, directoryName):
        #####################################################
        # Mirek: correct the filename to load from the network drive
        directoryName = directoryName.replace('/path/to/data/challengeCurves', 
                                    '/path/to/data/QuantumMoves2_SeedAnalysis')
        
        ##############################################
        
        self.__GetFileNames(directoryName)
        
    def __GetFileNames(self, directoryName):
        # Check if the diretory exists
        if not os.path.exists(directoryName):
            raise exceptionType("Invalid directory name " + directoryName)
        # List all the files within the directory and filter out everything except .mat files
        fileNameList = os.listdir(directoryName)

        if fileNameList is []:
            raise exceptionType("File list is empty")
        fileNameList = list(filter(lambda x: x.endswith(".mat"), fileNameList))
        # Find the metadata file
        metadataFile = list(filter(lambda x: x.startswith("metadata"), fileNameList))
        if (metadataFile is []):
            raise exceptionType("Could not find metadata.mat")
        # Remove the metadata from the seed file list
        seedFileList = fileNameList.remove(metadataFile[0])
        # Prepend the filenames with the path
        fileNameList = list(map(lambda fname: os.path.join(directoryName, fname), fileNameList))
        metadataFile = list(map(lambda fname: os.path.join(directoryName, fname), metadataFile))
        self.mMetadataFile = metadataFile[0]
        self.mSeedFileNames = fileNameList 
        
    def GetMetadataFile(self):
        return self.mMetadataFile
    
    def GetSeedFileNames(self):
        return self.mSeedFileNames

# Helper function
def FileRemover(ftFileNames, targetDir):
    seedFileNames = ftFileNames.GetSeedFileNames()
    for fileName in seedFileNames:
        targetFilePart = os.path.split(fileName)[-1]
        targetFileName = os.path.join(targetDir, targetFilePart)
        if os.path.exists(targetFileName):
            os.remove(targetFileName)

# Helper function
def FileMover(ftFileNames, filterFunction, targetDir):
    fileNamesList = ftFileNames.GetSeedFileNames()
    futs = []
    with concurrent.futures.ProcessPoolExecutor(max_workers = 6) as executor:
        i = 0
        batch = 0
        dataLen = len(ftFileNames.GetSeedFileNames())
        for fileName in fileNamesList:
            if not (filterFunction(fileName)):
                continue
            targetFilePart = os.path.split(fileName)[-1]
            targetFileName = os.path.join(targetDir, targetFilePart)

            futs += [executor.submit(shutil.move, fileName, targetFileName)]
            i += 1
            batch +=1
            if (batch > 500):
                while not all(list(map(lambda x: x.done(), futs))):
                    time.sleep(2)
                    logging.info("Processing {} out of {}.".format(i, dataLen))
                batch = 0
        while not all(list(map(lambda x: x.done(), futs))):
            time.sleep(2)
            logging.info("Waiting for all to finish")

# Helper function
def _count_file(fn):
    with open(fn, 'rb') as f:
        return _count_file_object(f)


# Helper function
def _count_file_object(f):
    total = 0
    for line in f:
        total += len(line)
    return total

# Helper function
def unzip_member_f3(zip_filepath, filename, dest):
    with open(zip_filepath, 'rb') as f:
        zf = zipfile.ZipFile(f)
        zf.extract(filename, dest)
    fn = os.path.join(dest, filename)
    return _count_file(fn)

# Helper function
def ParallelExtractor(zipFile, targetDirectory):
    with open(zipFile, 'rb') as f:
        zf = zipfile.ZipFile( f )
        futures = []
        with concurrent.futures.ProcessPoolExecutor() as executor:
            for member in zf.infolist():
                futures.append(executor.submit(unzip_member_f3, zf, member.filename, targetDirectory))
            total = 0
            for future in concurrent.futures.as_completed(futures):
                total += future.result()
        return total

# Helper function
def Extractor(zipFile, targetDirectory):
    with open(zipFile, 'rb') as f:
        zipfile.ZipFile(f).extractall(targetDirectory)

def SingleFileAppendSuffix(fileName, suffix):
    if suffix in fileName:
        return
    fnameSplit = fileName.split(".mat")
    newFileName = "{old}_{suffix}.mat".format(old = fnameSplit[0], suffix = suffix)
    os.rename(fileName, newFileName)

# Helper function
def FilesAppendSuffix(ftFileNames, suffix):
    futs = []
    with concurrent.futures.ProcessPoolExecutor(max_workers = 6) as executor:
        i = 0
        batch = 0
        dataLen = len(ftFileNames.GetSeedFileNames())
        for fileName in ftFileNames.GetSeedFileNames():
            futs += [executor.submit(SingleFileAppendSuffix, fileName, suffix)]
            i += 1
            batch +=1
            if (batch > 500):
                while not all(list(map(lambda x: x.done(), futs))):
                    time.sleep(2)
                    logging.info("Processing {} out of {}.".format(i, dataLen))
                batch = 0
        while not all(list(map(lambda x: x.done(), futs))):
            time.sleep(2)
            logging.info("Waiting for all to finish")

        logging.info("Done".format(i, dataLen))

# Encapsulates the 'seed' produced using conventional methods (.mat files)
class SeedDatum:
    def __init__(self, fileName):
        self.mKeys = []
        self.mStorage = {}
        #####################################################
        # Mirek: correct the filename to load from the network drive
        fileName = fileName.replace('/path/to/data/challengeCurves', 
                                    '/path/to/data/QuantumMoves2_SeedAnalysis')
        
        ##############################################
        with h5py.File(fileName, 'r') as f: 
            try:
                for (k, v) in f.items():
                    self.mStorage[k] = v[:]
                    self.mKeys += [k]
            except:
                for (k, v) in f.items():
                    self.mStorage[k] = v[()]
                    self.mKeys += [k]    
          
        self.mStorage["filename"] = fileName

    def SaveSeed(self, filename):
        with h5py.File(filename, 'w') as f:
            for (k, v) in self.mStorage.items():
                f[k] = v

    def GetFilename(self):
        return self.mStorage["filename"]

    def GetKeys(self):
        return self.mKeys
    
    def GetStorage(self):
        return self.mStorage
    
    def GetFinalFidelity(self):
        return self.mStorage["Ff"][0]

    def GetObtainedFidelity(self):
        return self.GetFinalFidelity()

    def GetObtainedDuration(self):
        return (len(self.GetFinalControl()[0]) - 1) * self.Getdt()
    
    def GetT(self):
        return self.mStorage['T'][0][0]

    def GetFidelityHistory(self):
        return self.GetStorage()['fidelityHistory']
    
    def GetInfidelityHistory(self):
        fh = self.GetFidelityHistory()
        return [fh[0], [1 - x for x in fh[1]]]

    def GetFinalControl(self):
        params = len(self.GetStorage()['uf'][0]) + 1
        dt = self.Getdt()
        self.mFinalControlParams = [ [] for x in range(params)]
        i = 0
        for item in self.GetStorage()['uf']:
            self.mFinalControlParams[0] += [dt * i]
            i += 1
            for k in range(1, params):
                self.mFinalControlParams[k] += [item[k - 1]]
        return self.mFinalControlParams
    
    def GetInitialControl(self):
        params = len(self.GetStorage()['ui'][0]) + 1
        dt = self.Getdt()
        self.mFinalControlParams = [ [] for x in range(params)]
        i = 0
        for item in self.GetStorage()['ui']:
            self.mFinalControlParams[0] += [dt * i]
            i += 1
            for k in range(1, params):
                self.mFinalControlParams[k] += [item[k - 1]]
        return self.mFinalControlParams
    
    def GetSineRandomizedControl(self):
        params = len(self.GetStorage()['u_best_candidate'][0]) + 1
        dt = self.Getdt()
        self.mBCControlParams = [ [] for x in range(params)]
        i = 0
        for item in self.GetStorage()['u_best_candidate']:
            self.mBCControlParams[0] += [dt * i]
            i += 1
            for k in range(1, params):
                self.mBCControlParams[k] += [item[k - 1]]
        return self.mBCControlParams         
    
    def GetKeyControl(self, key):
        params = len(self.GetStorage()[key][0]) + 1
        dt = self.Getdt()
        output = [ [] for x in range(params)]
        i = 0
        for item in self.GetStorage()[key]:
            output[0] += [dt * i]
            i += 1
            for k in range(1, params):
                output[k] += [item[k - 1]]
        return output      
    
    def Getdt(self):
        return self.GetStorage()['dt'][0][0]
    
    def GetKey(self, key):
        return self.GetStorage()[key][0][0]

    def GetStopCode(self):
        return self.GetStorage()['stopCode'][0][0]

    def GetOptimizerType(self):
        return self.GetStorage()['optimizertype'][0][0]



# In[5]:
# Helper function (for passing to the concurrency module)
def LoadSeedDatum(fileName):
    return SeedDatum(fileName)

# Obsolete, Encapsulates a batch of the seed metacollection
class SeedMetaCollectionBatch:

    def __init__(self, metaDatum, metaFileName, seedDatums, seedFileList):
        self.mMetaData = metaDatum
        self.mMetadataFilename = metaFileName
        self.mSeedDatumlist = seedDatums
        self.mFilenameList = seedFileList

    def GetSeedDatums(self):
        return self.mSeedDatumlist
    
    def GetMetaData(self):
        return self.mMetaData    

    def GetMetaDataFilename(self):
        return self.mMetadataFilename

    def GetFilenameList(self):
        return self.mFilenameList        

# Encapsulates the collection of seeds produced using the conventional methods
class SeedMetaCollection:

    def __init__(self, fileNamesList, metadataFile, workerProcesses):
        self.mSeedDatumlist = []
        self.__WorkerProcesses = workerProcesses
        self.__InitializeMetadata(metadataFile)
        self.__InitializeSeedDatumListParallel(fileNamesList)
        self.mMetadataFilename = metadataFile
        self.mFilenameList = fileNamesList

    def GetMetaDataFilename(self):
        return self.mMetadataFilename

    def GetFilenameList(self):
        return self.mFilenameList
        
    def __InitializeSeedDatumList(self, fileNameList):
        self.mSeedDatumlist = []
        i = 1
        howMany = len(fileNameList)
        for fileName in fileNameList:
            logging.info("Processing file " + fileName + "({} out of {})".format(i, howMany))
            self.mSeedDatumlist += [SeedDatum(fileName)]
            i += 1

    def __InitializeSeedDatumListParallel(self, fileNameList):
        maxItems = len(fileNameList)
        futures = []
        batchSize = 1000
        i = 0
        while (i < maxItems):
            if (i + batchSize >= maxItems):
                newBatch = fileNameList[i:]
            else:
                newBatch = fileNameList[i:i+batchSize]
            i += batchSize
            self.__InitializeSeedDatumBatch(newBatch)
            logging.info("Processed {} out of {} files.".format(min(i, maxItems), maxItems))

    def __InitializeSeedDatumBatch(self, fileNameBatch):
        futures = []
        with concurrent.futures.ProcessPoolExecutor(max_workers = self.__WorkerProcesses) as executor:
            for fname in fileNameBatch:
                # logging.info("Processing file " + fname) # + "({} out of {})".format(i, len(fileNameList)))
                futures += [executor.submit(LoadSeedDatum, fname)]

            for fut in futures:
                self.mSeedDatumlist += [fut.result()]
                # logging.info("Finished processing {}".format(self.mSeedDatumlist[-1].mStorage["filename"]))

    def __InitializeMetadata(self, metadataFile):
        logging.info("Processing file " + metadataFile)
        self.mMetaData = SeedDatum(metadataFile)
        
    def GetSeedDatums(self):
        return self.mSeedDatumlist
    
    def GetMetaData(self):
        return self.mMetaData

    def GetDataBatch(self, batchSize):
        if (batchSize > len(self.mSeedDatumlist)):
            raise ValueError("Batch size larger than the seed dataset")
        data = random.sample(list(zip(self.mSeedDatumlist, self.mFilenameList)), batchSize)
        return SeedMetaCollectionBatch( self.mMetaData, 
                                        self.mMetadataFilename, 
                                        list(map(lambda x: x[0], data)), 
                                        list(map(lambda x: x[1], data)))

# In[ ]:

# Encapsulates a collection of seeds with ordering by best-per-duration with functionality 
# that allows to extract a QSL seed, FT curve etc.
class SeedTBuckets:
    def __init__(self, seedMetaCollection):
        self._mSeedMetaCollection = seedMetaCollection
        self.__CollectPutSeedsToBuckets(seedMetaCollection)
        self.SortTBuckets()
        
    def GetSeedDatums(self):
        return self._mSeedMetaCollection.GetSeedDatums()

    def __SingleArraysToList(self, input):
        return list(map(lambda x: x[0], input))

    def __CollectPutSeedsToBuckets(self, seedMetaCollection):
        self.mTSeedDict = {}
        for seedDatum in seedMetaCollection.mSeedDatumlist:
            storage = seedDatum.GetStorage()
            durationKey = str( round(seedDatum.GetObtainedDuration(), 5) )
            if durationKey not in self.mTSeedDict:
                self.mTSeedDict[durationKey] = []
            self.mTSeedDict[durationKey] += [seedDatum]
            
    
    def SortTBuckets(self):
        self.mTSeedDictSorted = {}
        for (k, v) in self.mTSeedDict.items():
            sortT = sorted(v, key = lambda x: x.GetObtainedFidelity(), reverse = True)
            self.mTSeedDictSorted[k] = sortT
        return self.mTSeedDictSorted
    
    def GetSortedBucketList(self):
        if self.mTSeedDictSorted is None:
            self.SortTBuckets()
        output = []
        for (k, v) in self.mTSeedDictSorted.items():
            output += [(k,v)]
        output = sorted(output, key = lambda x: x[0])
        return output
    
    def GetFTCurve(self):
        bucketList = self.GetSortedBucketList()
        times = list(map(lambda x: float(x[0]), bucketList))
        topF = list(map(lambda x: float(x[1][0].GetObtainedFidelity()), bucketList))
        return (times, topF)
    
    def GetIFTCurve(self):
        bucketList = self.GetSortedBucketList()
        times = list(map(lambda x: float(x[0]), bucketList))
        topIF = list(map(lambda x: 1 - float(x[1][0].GetObtainedFidelity()), bucketList))
        return (times, topIF)
    
    def GetQSLSeed(self, F_QSL):
        self.mTQSLSeed = None
        candidateSeed = None
        bestDuration = 1e32
        for (k,v) in self.GetSortedBucketList():
            if (v[0].GetFinalFidelity() >= F_QSL):
                if candidateSeed is None:
                    candidateSeed = v[0]
                    continue
                if (v[0].GetObtainedDuration() <= candidateSeed.GetObtainedDuration()):
                    candidateSeed = v[0]
                    continue    
        if candidateSeed is None:
            raise exceptionType("No seed compliant to F_QSL = {fqsl} was found".format(fqsl = F_QSL))
        self.mTQSLSeed = candidateSeed
        return self.mTQSLSeed
        
        
    def GetStopCodes(self):
        self.mStopCodes = [0 for x in range(7)]
        for (k,v) in self.mTSeedDict.items():
            for seed in v:
                stopCode = seed.GetStopCode()
                self.mStopCodes[int(stopCode)] += 1
        self.mStopCodes = [[x for x in range(0, 7)], self.mStopCodes]
        return self.mStopCodes
    
# Helper function
def AcquireAlgorithmName(fileName):
        fileNameLower = fileName.lower()
        if 'grape' in fileNameLower :
            return "GRAPE"
        elif 'dgroup' in fileNameLower:
            return "SGA_BUCKET"
        elif 'sga' in fileNameLower:
            return "SGA"
        else:
            raise ValueError("Unknown optimizer for file {}".format(fileName))        

# Helper function
def AcquireSeedingType(fileName):
    seedTypeList = []
    if 'RS' in fileName:
        return 'RS'
    else:
        raise ValueError("Unknown seeding type for {}".format(fileName))

# Helper function
def AcquireProblemName(fileName):
    if "bhw" in fileName:
        return  ("BringHomeWater", 16)
    elif "gpe_shakeup" in fileName:
        return ("ShakeUp", 7)
    elif "gpe_split" in fileName:
        return ("Splitting", 11)
    else:
        raise ValueError("Invalid level name for directory {}".format(directory))    

# Saves the summary of the seed meta collection to a file
def DumpSeedMetaCollection(outputFile, metaCollection):
    outputDict = {}
    # things to be output
    seedTBuckets = SeedTBuckets(metaCollection)   

    # obtain the optimizer name
    algNames = list(map(AcquireAlgorithmName, metaCollection.GetFilenameList()))
    allSame = list(map(lambda x: x == algNames[0], algNames))
    outputFileOptimizer = outputFile.format(optimizerType = algNames[0])
    outputDict["optimizerType"] = algNames[0]
    outputDict["FT_Curve_plot"] = seedTBuckets.GetFTCurve()
    outputDict["IFT_Curve_plot"] = seedTBuckets.GetIFTCurve()
    outputDict["FT_Curve_filename"] = [x[1][0].GetFilename() for x in seedTBuckets.GetSortedBucketList()]
    outputDict["metadata_filename"] = metaCollection.GetMetaDataFilename()
    outputDict["seedFiles_filename"] = metaCollection.GetFilenameList()

    logging.info("Saving summary to {}".format(outputFileOptimizer))
    with open(outputFileOptimizer, 'w') as fh:
        json.dump(outputDict, fh)
    return outputFileOptimizer

# Encapsulates a 'collection' of data summaries. Used primarily for making the dataset plots
class SummaryCollection:
    def __init__(self, fileName):
        self.mSummaryFilename = fileName
        self.SetLevelId(fileName)
        self.SetOptimizerName(fileName)
        # self.SetOptimizerName(fileName)
        with open(fileName, 'r') as fh:
            self.mSummaryFileData = json.load(fh)
        self.mMetadataFilename = self.mSummaryFileData['metadata_filename']
        self.mMetadataFileLoaded = False
        self.mMetadata = []

        self.mSeedFilenames = self.mSummaryFileData['seedFiles_filename']
        self.mSeedFilesLoaded = False
        self.mSeedData = []

        self.mFTCurveFilenames = self.mSummaryFileData['FT_Curve_filename']
        self.mFTFilesLoaded = False
        self.mFTCurveSeedData = []

        # FT Curve
        self.mFTCurveData = self.mSummaryFileData['FT_Curve_plot']
        self.mIFTCurveData = self.mSummaryFileData['IFT_Curve_plot']

        self.RenormalizeFTCurves()
        self.SetSeedingType()

    def GetNumOfSeeds(self):
        return len(self.mSeedFilenames)

    def SetLevelId(self, fileName):
        fileNameLower = fileName.lower()
        if 'bhw' in fileNameLower :
            self.mLevelId = 16
            self.mQSL = 0.37
        elif 'shake' in fileNameLower:
            self.mLevelId = 7
            self.mQSL = 0.89
        elif 'split' in fileNameLower:
            self.mLevelId = 11
            self.mQSL = 0.92
        else:
            raise "Unknown level id for file {}".format(fileName)

    def SetOptimizerName(self, fileName):
        fileNameLower = fileName.lower()
        if 'grape' in fileNameLower:
            self.mOptimizerType = "GRAPE"
        elif 'sga_bucket' in fileNameLower:
            self.mOptimizerType = "SGA_BUCKET"
        elif 'sga' in fileNameLower:
            self.mOptimizerType = "SGA"
        else:
            raise ValueError("Unknown optimizer for file {}".format(fileName))     

    def SetSeedingType(self):
        seedTypeList = []
        for fname in self.mSeedFilenames:
            if 'RS' in fname:
                seedTypeList += ['RS']
            else:
                raise ValueError("Unknown seeding type for {}".format(fname))

        firstType = seedTypeList[0]
        allSame = list(map(lambda x: x == firstType, seedTypeList))
        if not (all(allSame)):
            raise ValueError("Some files had different seeding type than first type: {}".format(firstType))

        self.mSeedingType = firstType

    def RenormalizeFTCurves(self):
        ftDurations = list(map(lambda x: x / self.mQSL, self.mFTCurveData[0]))
        self.mFTNormCurveData = [ftDurations, self.mFTCurveData[1]]
        self.mIFTNormCurveData = [ftDurations, self.mIFTCurveData[1]]

    def GetQSLSeed(self, fidelityThreshold = 0.99, additionalDurationScaling = lambda x: x):
        logging.info("Extracting QSL Seed from summary {}".format(self.mSummaryFilename))
        qslSeed = None
        for item in zip(self.mFTCurveData[0], self.mFTCurveData[1]):
            if (item[1] >= fidelityThreshold):
                qslSeed = item
                break
        if not qslSeed:
            logging.error("QSL Seed was not found in summary {}".format(self.mSummaryFilename))
            return None
        qslSeedReportShort = "T: {T} | F: {F} | Optimizer: {opt} | Seeding: {seeding} | Seeds: {seedNum}".format(T = round(additionalDurationScaling(qslSeed[0]), 4),
                                                                                                             F = qslSeed[1],
                                                                                                             opt = self.mOptimizerType,
                                                                                                             seeding = self.mSeedingType,
                                                                                                             seedNum = self.GetNumOfSeeds())        
        print("{T} & {F} &  {opt} & {seeding} & {seedNum} seeds\\\\".format(T = round(additionalDurationScaling(qslSeed[0]), 4),
                                                                                                             F = round(qslSeed[1], 3),
                                                                                                             opt = self.mOptimizerType,
                                                                                                             seeding = self.mSeedingType,
                                                                                                             seedNum = self.GetNumOfSeeds()))              
        return (qslSeed, None, qslSeedReportShort, qslSeed[0], qslSeed[1])    

# Dumps data describing the QSL seeds found in the dataset, according to the list of fidelity values 'qslFidList'
def DumpQSLSeedData(buckets, outputFile, qslFidList):
    for F_QSL in qslFidList:
        try:
            qslSeed = buckets.GetQSLSeed(F_QSL)
        except:
            logging.error("Trouble with QSL: {}".format(F_QSL))
            continue
        seedName = qslSeed.GetStorage()
        outputFile = outputFile.format(optimizerType = AcquireAlgorithmName(qslSeed.GetStorage()['filename']))
        with open(outputFile + "_qsl_{}_seed_info.txt".format(F_QSL), 'w') as fh:
            for (k,v) in qslSeed.GetStorage().items():
                fh.write("{} {}\n".format(k, v))
            fh.write("optimizerType {optimizerType}".format(optimizerType = AcquireAlgorithmName(qslSeed.GetStorage()['filename'])))