import json
import csv
import logging
import os
import json
import sys
import matplotlib.pyplot as plt
import numpy as np
from importlib import reload  # Python 3.4+ only.

import CVUtilities as cval
from  mysql.connector import connection
import FTUtilities as ft
import gc
import concurrent
import saturationCfg as scfg
scfg = reload(scfg)
import concurrent.futures
import math

class ValidationDatumPSO:
    def __init__(self, fileName):
        with open(fileName, 'r') as fh:
            self.mData = json.load(fh)
            self.mFileName = fileName
        self._DetermineDtQSL()
        self._ComputeDuration()
        
    def GetObtainedFidelity(self):
        return self.mData['final_fidelity']
    
    def GetObtainedDuration(self):
        return self.mDuration
        
    def GetPhysicalDuration(self):
        return self.mDuration * self.mData['T_QSL']
    
    def GetLevelId(self):
        return self.mData['level_id']

    def GetOptimizationIteration(self):
        return self.mData['optimization_iteration']
    
    def GetPathId(self):
        return self.mData['path_id']
    
    def _DetermineDtQSL(self):
        if self.GetLevelId() in (7, 11):
            self.mData['dt'] = 1e-3
        elif self.GetLevelId() == 16:
            self.mData['dt'] = 3.5e-4
        else:
            raise ValueError("Invalid level id")
            
        if self.GetLevelId() == 7:
            self.mData['T_QSL'] = 0.89
        elif self.GetLevelId() == 11:
            self.mData['T_QSL'] = 0.92
        elif self.GetLevelId() == 16:
            self.mData['T_QSL'] = 0.37
        else:
            raise ValueError("Invalid Level Id")
            
    def DidConvolution(self):
        return self.mData['performConvolution'] == 1
    
    def _ComputeDuration(self):
        self.mDuration = (len(self.mData['physics_x']) - 1) * self.mData['dt']  / self.mData['T_QSL']
        
    def GetDt(self):
        return self.mData['dt']
    
    def GetStopCondition(self):
        return self.mData['termination_condition']

    def GetFilename(self):
        return self.mFileName

class ValidationCSVDatumPSO:
    def __init__(self, csvDict):
        self.mData = {}
        for (k, v) in csvDict.items():
            self.mData[k] = v 
        self._DetermineDtQSL()
        
    def CheckInitializeSeed(self):
        if self.mSeed is None:
            seedFileName = self.mData['filename']
            seedFileName = [x if x != '\'' else '' for x in seedFileName]
            seedFileName = "".join(seedFileName)
            self.mSeed = ValidationDatumPSO( seedFileName )        

    def GetObtainedFidelity(self):
        return float(self.mData['FinalFidelity'])
    
    def GetObtainedDuration(self):
        return float(self.mData['Duration'])
        
    def GetPhysicalDuration(self):
        return self.GetObtainedDuration() * self.mData['T_QSL']
    
    def GetLevelId(self):
        return math.floor(float(self.mData['levelId']))

    def GetOptimizationIteration(self):
        return math.floor(float(self.mData['optimization_iteration']))
    
    def GetPathId(self):
        return self.mData['path_id']
    
    def _DetermineDtQSL(self):
        if self.GetLevelId() in (7, 11):
            self.mData['dt'] = 1e-3
        elif self.GetLevelId() == 16:
            self.mData['dt'] = 3.5e-4
        else:
            raise ValueError("Invalid level id")
            
        if self.GetLevelId() == 7:
            self.mData['T_QSL'] = 0.89
        elif self.GetLevelId() == 11:
            self.mData['T_QSL'] = 0.92
        elif self.GetLevelId() == 16:
            self.mData['T_QSL'] = 0.37
        else:
            raise ValueError("Invalid Level Id")
            
    def DidConvolution(self):
        return self.mData['performConvolution'] == 1

    def GetDt(self):
        return self.mData['dt']
    
    def GetStopCondition(self):
        return float(self.mData['termination_condition'])

    def GetFilename(self):
        return self.mFileName


def ExtractPSODatasetFileNames(directoryName):
    fileNames = os.listdir(directoryName)
    fileNames = list(filter(lambda x: 'optimized' in x, fileNames))
    return list(map(lambda x: os.path.join(directoryName, x), fileNames))    

def LoadPSODataset(directoryName):
    fileNames = ExtractPSODatasetFileNames(directoryName)
    outputs = []
    for item in fileNames:
        outputs += [ValidationDatumPSO(item)]
    return outputs

def DumpPSODatasetInfo(psoData, psoDataDumpFile):
    controlInfoParser = cval.GetControlInfoParser()
    dataDump = {}
    for datum in psoData:
        dump = (int(datum.GetLevelId()), 
                    datum.GetPathId(), 
                    datum.GetObtainedFidelity(), 
                    datum.GetObtainedDuration(), 
                    datum.GetOptimizationIteration())
        dataDump[datum.GetPathId()] = controlInfoParser(dump)
    with open(psoDataDumpFile, 'w') as fh:
        json.dump(dataDump, fh)

def SerializeSinglePSODatumCSV(seedDatum):
    return "{D}|{finalF}|{dt}|'{filename}'|{levelId}|'{algorithm}'|'{seeding}'|{termination_condition}|{optIter}|{path_id}\n".format(  D = seedDatum.GetObtainedDuration(),
                                                                                finalF = seedDatum.GetObtainedFidelity(),
                                                                                dt = seedDatum.GetDt(),
                                                                                filename = seedDatum.GetFilename(),
                                                                                levelId = seedDatum.GetLevelId(),
                                                                                algorithm = "GRAPE",
                                                                                seeding = "Player",
                                                                                termination_condition = seedDatum.GetStopCondition(),
                                                                                optIter = seedDatum.GetOptimizationIteration(),
                                                                                path_id = seedDatum.GetPathId()
                                                                                )

def MakeSerializedPSOCSVHeader():
    return "Duration|FinalFidelity|dt|filename|levelId|optimizerType|seedingType|termination_condition|optimization_iteration|path_id\n"

def SerializePSODataset(directoryName, outputFile):
    fileNames = ExtractPSODatasetFileNames(directoryName)
    psoDatums = []
    for fileName in fileNames:
        psoDatums += [ValidationDatumPSO(fileName)]
    with open(outputFile, 'w') as fh:
        fh.write(MakeSerializedPSOCSVHeader())
        for seed in psoDatums:
            line = SerializeSinglePSODatumCSV(seed)
            fh.write( line )
    return outputFile

def LoadSerializedPSOCSVDatums(csvFileName, delimiter = '|'):
    with open(csvFileName, 'r') as fh:
        data = csv.DictReader(fh, delimiter = delimiter)
        seedDatums = []
        for line in data: 
            seedDatums += [ ValidationCSVDatumPSO(line) ]
        return seedDatums
