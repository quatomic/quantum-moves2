import os
import os.path
import shutil
import json
import logging
from optparse import OptionParser

def ProduceFileNameBatches(fileNames, jobsNum):
	# Divide the list of files into #jobsNum batches, output a list of lists of filenames
	batches = [[] for __ in range(jobsNum)]
	currentJobIndex = 0
	for filename in fileNames:
		if currentJobIndex < jobsNum:
			batches[currentJobIndex] += [filename]
			currentJobIndex += 1
		else:
			currentJobIndex = 0
			batches[currentJobIndex] += [filename]
			currentJobIndex += 1
	return batches

def ProduceFolderBatches(executorFile, pythonExecutor, fileNameBatches, jobsNum, directory, timeLimit):
	# produce actual folders that contain the described files
	# include the executor program
	# include the executor python script

	# iterate through all the jobs
	for i in range(1, jobsNum + 1):
		logging.info("Making batch {} out of {}".format(i, jobsNum))
		print("Making batch {} out of {}".format(i, jobsNum))
		ProduceFolderBatch(executorFile, pythonExecutor, fileNameBatches[i -1], i, directory, timeLimit)


def ProduceFolderBatch(executorFile, pythonExecutor,  fileNameBatch, index, directory, timeLimit):
	try:
		os.mkdir(directory)
	except FileExistsError as err:
		# shutil.rmtree(batchDirName)
		# logging.warning("Directory {} already exists. Removing...".format(batchDirName))
		# os.mkdir(batchDirName)
		pass	
	batchDirName = os.path.join(directory, "playerSeedBatch_{}".format(index))
	try:
		os.mkdir(batchDirName)
	except FileExistsError as err:
		# shutil.rmtree(batchDirName)
		# logging.warning("Directory {} already exists. Removing...".format(batchDirName))
		# os.mkdir(batchDirName)
		pass

	# create the directory for optimization outputs
	optOutputDir = os.path.join(batchDirName, "output") 
	try:
		os.mkdir( optOutputDir )
	except FileExistsError as err:
		# logging.warning("Directory {} already exists.".format(batchDirName))
		pass 

	if os.path.isfile(executorFile):
		execNewName = os.path.join(batchDirName, os.path.split(executorFile)[-1])
		shutil.copy(executorFile, execNewName)
	else:
		raise ValueError("No executor file {}.".format(executorFile))

	if os.path.isfile(pythonExecutor):
		pythonExecutorNewName = os.path.join(batchDirName, os.path.basename(pythonExecutor))
		shutil.copy(pythonExecutor, pythonExecutorNewName)
	else:
		raise ValueError("No Python executor script {}".format(pythonExecutor))

	# copy the files over
	fileNameBatchReady = []
	for file in fileNameBatch:
		fileNameOut = os.path.split(file)[-1]
		fileNameOut = os.path.join(batchDirName, fileNameOut)
		shutil.copy(file, fileNameOut)
		fileNameBatchReady += [fileNameOut]

	config = MakeBatchConfigDict(fileNameBatchReady, execNewName, optOutputDir, timeLimit)
	configFileName = os.path.join(batchDirName, "config.json")
	with open(configFileName, 'w') as cfgFH:
		json.dump(config, cfgFH)

	grendelConfig = MakeBatchGrendelConfigDict(fileNameBatchReady, execNewName, optOutputDir, timeLimit)
	grendelCongifFileName = os.path.join(batchDirName, "grendelConfig.json")
	with open(grendelCongifFileName, 'w') as cfgFH:
		json.dump(grendelConfig, cfgFH)	

def MakeBatchConfigDict(fileNameBatch, executor, outputOptDir, timeAllocated):
	if (len(fileNameBatch)) == 0:
		timePerFile = 0
	else:
		# in seconds
		timePerFile = timeAllocated / float( len(fileNameBatch) ) - 0.017 # include 1 additional minute for copying?
	configDictionary = {"filenames" : fileNameBatch, "timePerFile" : timePerFile, "outputDirectory" : outputOptDir, "executor" : executor}
	return configDictionary

def MakeBatchGrendelConfigDict(fileNameBatch, executor, outputOptDir, timeAllocated):
	if (len(fileNameBatch)) == 0:
		timePerFile = 0
	else:
		# in seconds
		timePerFile = timeAllocated / float( len(fileNameBatch) ) - 0.017 # include 1 additional minute for copying?
	localizedFileNames = []
	for fileName in fileNameBatch:
		localFileName = os.path.basename(fileName)
		localizedFileNames += [localFileName]

	localizedExecutor = os.path.basename(executor)

	localizedOutputDir = os.path.basename(outputOptDir)

	configDictionary = {"filenames" : localizedFileNames, 
						"timePerFile" : timePerFile, 
						"outputDirectory" : localizedOutputDir, 
						"executor" : localizedExecutor}
	return configDictionary

def FilterLevels(fileNames):

	filteredFiles = []
	for fileName in fileNames:
		# print("Checking {}".format(fileName))
		try:
			with open(fileName, 'r') as fh:
				data = json.load(fh)
				if (data['level_id']) in (7, 11, 16):
					filteredFiles += [ fileName ]
		except KeyError as ex:
			logging.error("Key error for file {}".format(fileName))
	print("Input files: {} output files: {}".format(len(fileNames), len(filteredFiles)))
	return filteredFiles

def BuildFileDict(fileNames):
	fileDict = {}
	fullyOptimized = {}
	for fileName in fileNames:
		try:
			with open(fileName, 'r') as fh:
				data = json.load(fh)
				data['fileName'] = fileName
				if (data['level_id']) not in (7, 11, 16):
					continue

				pathId = data['path_id']

				if pathId in fullyOptimized:
					print("fullyOptimized skip")
					continue

				if 'termination_condition' in data:
					print("termination_condition skip")
					if data['termination_condition'] in (1, 2, 3):
						fullyOptimized[data['path_id']] = data
						continue
				
				if pathId in fileDict:
					otherDatum = fileDict[pathId]
					if 'optimization_iteration' not in data:
						print("weaker file skip")
						continue
					if 'optimization_iteration' not in otherDatum:
						fileDict[pathId] = data
					if data['optimization_iteration'] > otherDatum['optimization_iteration'] :
						fileDict[pathId] = data
				else:
					fileDict[pathId] = data
		except KeyError as ex:
			logging.error("Key error for file {}".format(fileName))	

	print("Input files: {} output files: {} fullyOptimized: {}".format(len(fileNames), len(fileDict), len(fullyOptimized)))
	return ([x['fileName'] for x in fileDict.values()], [x['fileName'] for x in fullyOptimized.values()])

def CopyFullyOptimizedOut(fullyOptimizedFiles, outputDir):
	outputFileNames = list(map(lambda x: os.path.join(outputDir, os.path.basename(x)), fullyOptimizedFiles ))
	for (fin, fout) in zip(fullyOptimizedFiles, outputFileNames):
		shutil.copy(fin, fout)

def main(configFile):
	with open(configFile, 'r') as fh:
		configuration = json.load(fh)	
	seedsDir = configuration['seedsDir']
	fileNames = os.listdir(seedsDir)
	fileNames = list(map(lambda x: os.path.join(seedsDir, x), fileNames))
	(fileNames, fullyOptimized) = BuildFileDict(fileNames)
	CopyFullyOptimizedOut(fullyOptimized, configuration['optimizedOutput'])
	jobsNum = configuration['jobs']
	executor = configuration['executor']
	pythonExecutor = configuration['pythonExecutor']
	outputDir = configuration['outputDirectory']
	timeLimit = configuration['timeLimit']

	fileNameBatches = ProduceFileNameBatches(fileNames, jobsNum)
	ProduceFolderBatches(executor, pythonExecutor, fileNameBatches, jobsNum, outputDir, timeLimit)

if __name__== "__main__":
	parser = OptionParser()
	parser.add_option("-c", "--cfg", dest = "cfg", action = "store", default = False)
	(options, args) = parser.parse_args()
	if not options.cfg:
		parser.error("Lack of cfg option")
	main( options.cfg )