from optparse import OptionParser

import os
import json
import logging
import subprocess as sp

def LaunchExecution(executor, fileName, timeLimit, outputFile):
	rep = "{} {} {} {}".format(executor, fileName, timeLimit, outputFile)
	if (executor == os.path.basename(executor)):
		executor = os.path.join(os.getcwd(), executor)
	arr = [executor, fileName, str(timeLimit), outputFile]
	print(arr)
	v = sp.run(arr)
	print(v)

def main(configFile):
	print(configFile)
	with open(configFile, 'r') as fh:
		configuration = json.load(fh)

	outputDir = configuration['outputDirectory']
	executor = configuration["executor"]
	timeLimit = configuration["timePerFile"] # seconds
	if len(configuration['filenames']):
		for fileName in configuration["filenames"]:
			outputFile = os.path.basename(fileName).split('.')[0]
			outputFile = os.path.join(outputDir, outputFile + "_optimized.json")
			LaunchExecution(executor, fileName, timeLimit, outputFile)
	else:
		logging.error("No seeds to be optimized (empty file name list)")
		return

if __name__== "__main__":
	parser = OptionParser()
	parser.add_option("-c", "--cfg", dest = "cfg", action = "store", default = False)
	(options, args) = parser.parse_args()
	if not options.cfg:
		parser.error("Lack of cfg option")
	main( options.cfg )