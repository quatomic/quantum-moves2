import csv
import logging
import os
import json
import sys
import matplotlib.pyplot as plt
import numpy as np
from importlib import reload  # Python 3.4+ only.
import gc
import shutil
import subprocess
import shutil

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    stream = sys.stdout,
    datefmt='%Y-%m-%d %H:%M:%S')

def LevelIdToName(nameId):
    return {7: "gpe_shakeup", 11: "gpe_split", 16: "bhw"}[nameId]

from optparse import OptionParser
parser = OptionParser()
parser.add_option("-c", "--config", dest = "configFile")

(options, args) = parser.parse_args()

configFile = options.configFile

with open(configFile, 'r') as fh:
    config = json.load(fh)
   
batchNum = config["batchNum"]
levelId = config["levelId"]
batchDir = config["batchDir"]

# directories, execs etc.
execFileOptimizer = "./fileSeedOptimizer".format(batchDir = batchDir)
metaFileTmp = "./metadata.mat"
inputDirTemp = "./seeds"
outNameTemplate = "{outDir}/{path_id}"

metaFile = metaFileTmp
inputDir = inputDirTemp 

files = list(map(lambda x: os.path.join(inputDir, x), os.listdir(inputDir)))

localOutputDirTmp = "./output_RPSO/{physics}"
outputDir = localOutputDirTmp.format(physics = LevelIdToName(levelId))

try:
    shutil.rmtree("./output_RPSO")
except FileNotFoundError:
    pass

os.mkdir("./output_RPSO")
os.mkdir(outputDir)

logging.info("Starting {} batch {}".format(LevelIdToName(levelId), batchNum))
# Launch the optimization
logging.info("Using optimizer: {}".format(execFileOptimizer))
for fname in files:
    outNametmp = os.path.basename(fname).split("_")[0]
    outName = outNameTemplate.format(outDir = outputDir, path_id = outNametmp)
    logging.info("\nRunning optimization: \n\tseed: {} \n\toutput directory: {}".format(fname, outputDir))
    subprocess.run([execFileOptimizer, fname, outName])
logging.info("\nMoving metadata file: {} to  {}".format(metaFile, outputDir))    
# Copy the metadata file
shutil.move(metaFile, os.path.join(outputDir, "metadata.mat"))
logging.info("OK")