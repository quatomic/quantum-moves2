import csv
import logging
import os
import json
import sys
import matplotlib.pyplot as plt
import numpy as np
from importlib import reload  # Python 3.4+ only.
import gc
import shutil

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    stream = sys.stdout,
    datefmt='%Y-%m-%d %H:%M:%S')

def ConstructLevelInfo(levelId, name, qsl, outputDirectory):
    return { "levelId" : levelId, "levelName": name, "T_QSL": qsl, "outputDirectory": outputDirectory}

def MapSeedingStrategyToEnum(strategy):
    return {"RS" : 0, "RSS" : 1}[strategy]

def MapEnumToSeedingStrategy(strategy):
    return {0 : "RS", 1 : "RSS"}[strategy]

def LevelIdToName(nameId):
    return {7: "gpe_shakeup", 11: "gpe_split", 16: "bhw"}[nameId]

def ConstructPreselectionSeedDatum(seed, strat, levelInfo, minutesPerSeed, onlyCheckFidelity):
    return {"path_id": seed.GetPathId(), 
            "duration": seed.GetObtainedDuration() * levelInfo['T_QSL'], # transform to simulation units 
            "levelId": seed.GetLevelId(),
            "seedingStrategy": MapSeedingStrategyToEnum(strat),
            "minutesPerSeed": minutesPerSeed,
            "onlyCheckFidelity": onlyCheckFidelity
           }

from optparse import OptionParser
parser = OptionParser()
parser.add_option("-c", "--config", dest = "configFile")

(options, args) = parser.parse_args()

configFile = options.configFile

with open(configFile, 'r') as fh:
    config = json.load(fh)

execDir = config["execDir"]    
taskDir = config["taskDir"]
execFileTemplate = config["execFileTemplate"]
localScriptLocation = config["localScriptLocation"]
jobs = config["jobs"]

# Distribute the job batches
execFileOptimizer = {levelId: execFileTemplate.format(execDir = execDir, physics = LevelIdToName(levelId)) for levelId in [7, 11, 16]}

inputDirTemp = "{taskDir}/SeedsBatches_stage4/{physics}/batch_{jobNum}"

initialSeedInfoDir = os.path.join(taskDir, "InitialSeedInfo_stage0")

levelInfoList =  [ConstructLevelInfo(7, "ShakeUp", 0.89, os.path.join(initialSeedInfoDir, "gpe_shakeup")), 
                  ConstructLevelInfo(11, "Splitting", 0.92, os.path.join(initialSeedInfoDir, "gpe_split")), 
                  ConstructLevelInfo(16, "BringHomeWater", 0.37, os.path.join(initialSeedInfoDir, "bhw"))]

outDirTemp = "{taskDir}/SeedsBatches_stage4/{physics}"
for levelInfo in levelInfoList:
    levelId = levelInfo['levelId']
    dirBatchesName = "{taskDir}/SeedsPreselected_stage3/{physics}".format(	taskDir = taskDir, 
    																		physics = LevelIdToName(levelId))
    logging.info("Processing {}".format(dirBatchesName))
    execFile = execFileOptimizer[levelId]
    fileNames = list(map(lambda x: os.path.join(dirBatchesName, x), os.listdir(dirBatchesName)))
    outputDir = outDirTemp.format(	taskDir = taskDir, 
    								physics = LevelIdToName(levelId))
    bucketBatches = [[] for __ in range(jobs)]
    bucketIndex = 0
    for fname in fileNames:
        bucketBatches[bucketIndex] += [fname]
        bucketIndex = (bucketIndex + 1 ) % jobs

    try:
        shutil.rmtree(outputDir)
    except FileNotFoundError:
        pass

    os.mkdir(outputDir)
    
    # build the directory for optimizations
    for bucketBatchId in range(len(bucketBatches)):
        bucketDirIndex = bucketBatchId + 1
    	# Build the batch directories
        try:
            dirName = "{outDir}/batch_{ID}".format(outDir = outputDir, ID = bucketDirIndex)
            shutil.rmtree(dirName)
        except FileNotFoundError:
            pass

        os.mkdir(dirName)
        logging.info("Copying files to {}".format(dirName))

        try:
            dirName = "{outDir}/batch_{ID}/seeds".format(outDir = outputDir, ID = bucketDirIndex)
            os.mkdir(dirName)
        except FileExistsError:
            pass
            
        # Modify and Distribute the seeds
        inputSeedFileNames = bucketBatches[bucketBatchId]
        for fileName in inputSeedFileNames:
            with open(fileName, 'r') as fh:
                data = json.load(fh)
                data['minutesPerSeed'] = config['minutesPerSeed']
            outputFile = os.path.join(dirName, os.path.basename(fileName))
            with open(outputFile, 'w') as fout:
                json.dump(data, fout)

        # Distribute the config files
        levelId = levelInfo["levelId"]
        cfgFileDir = inputDirTemp.format(taskDir = taskDir, physics = LevelIdToName(levelId), jobNum = bucketDirIndex)
        rpsoConfig = {	"batchNum" : bucketBatchId, 
        				"levelId": levelId,
        				"batchDir": cfgFileDir
        				}

        
        with open(os.path.join(cfgFileDir, "config.json"), 'w') as fh:
            json.dump(rpsoConfig, fh)

        # Distribute the local scripts
        shutil.copy(localScriptLocation, cfgFileDir)

        # Distribute the exec files
        shutil.copy(execFile, cfgFileDir)
            
    logging.info("Total seeds in level {}: {}".format(levelId, sum(list(map(len, bucketBatches)))))
  