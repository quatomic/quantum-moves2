import subprocess
import logging
import sys
import time
import numpy as np
import os

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    stream = sys.stdout,
    datefmt='%Y-%m-%d %H:%M:%S')

seeds = 100
minutesPerSeed = 13
batchSize = 7


doPairs = [
("/path/to/data/quantum-moves2-conventional-algorithms/programs/jobs/bhw/Release/trial_sga_bhw.exe", "/path/to/data/sga_vs_grape/output_bhw_fast_sga_bucket"),
# ("/path/to/data/quantum-moves2-conventional-algorithms/programs/jobs/gpe_shakeup/Release/trial_sga_shakeup.exe", "/path/to/data/sga_vs_grape/output_shakeup"),
# ("/path/to/data/quantum-moves2-conventional-algorithms/programs/jobs/gpe_split/Release/trial_sga_split.exe", "/path/to/data/sga_vs_grape/output_split")
]

if __name__ == '__main__':
    __spec__ = "ModuleSpec(name='builtins', loader=<class '_frozen_importlib.BuiltinImporter'>)"
    completeTotal = 0
    while (True):
        for (optimizer, outputDirectory) in doPairs:
            logging.info("Running SGA vs. GRAPE for:\noptimizer: {}\noutput directory: {}".format(optimizer, outputDirectory))
            seedNameTemplate = "seed_sga_vs_grape_{seedId}_{randId}.mat"

            procs = []
            for seedNum in range(batchSize):
                seedFileName = seedNameTemplate.format(seedId = seedNum, randId = np.random.randint(0, 100000))
                seedFileName = os.path.join(outputDirectory, seedFileName)
                procs.append( subprocess.Popen([optimizer, seedFileName, str(minutesPerSeed)]) )

            while True:
                for item in procs:
                    print(list(map(lambda x: x.poll(), procs)))
                    complete = sum(map(lambda x: x.poll() is not None, procs))
                logging.info("{} out of {} sga vs. grape optimizations complete.".format(completeTotal, seeds))
                if complete == len(procs):
                    break
                time.sleep(10)
            completeTotal += complete