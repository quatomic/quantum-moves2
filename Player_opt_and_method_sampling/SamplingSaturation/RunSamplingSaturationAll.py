import subprocess
import logging
import sys
import time
logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    stream = sys.stdout,
    datefmt='%Y-%m-%d %H:%M:%S')

if __name__ == '__main__':
    __spec__ = "ModuleSpec(name='builtins', loader=<class '_frozen_importlib.BuiltinImporter'>)"
    procs = []
    procs.append( subprocess.Popen(['python', 'computerBHWSaturation.py']) )
    procs.append( subprocess.Popen(['python', 'computerShakeUpSaturation.py']))
    procs.append( subprocess.Popen(['python', 'computerSplitSaturation.py']))
    procs.append( subprocess.Popen(['python', 'psoSaturation.py']))
    procs.append( subprocess.Popen(['python', 'optPlayerSaturation.py']))

    while True:
        for item in procs:
            complete = sum(map(lambda x: x.poll() == 0, procs))
        logging.info("{} out of {} sampling saturation scripts complete.".format(complete, len(procs)))
        if complete == len(procs):
            sys.exit(0)
        time.sleep(5)