import random
import warnings
import json
import numpy as np
import os
import logging
import random
import matplotlib.pyplot as plt

def MakeRescaleDurations(levelId, isPlayerData):
    # Normalize durations according to the game
    if levelId == 16:
        regameT_QSL = 0.37
        T_QSL = 0.255
    elif levelId == 7:
        regameT_QSL = 0.89
        T_QSL = 0.89
    elif levelId == 11:
        regameT_QSL = 0.92
        T_QSL = 0.92
    else:
        raise ValueError("Invalid problem")
    if isPlayerData:
        logging.debug("Player data rescaler: x * {} / {}".format(regameT_QSL, T_QSL))
        return lambda x: x * regameT_QSL / T_QSL
    logging.debug("Computer data rescaler: x / {}".format(T_QSL))
    return lambda x: x / T_QSL    
    
def MakeDurationBins(limits, binsNumber):
    return np.linspace(limits[0], limits[1], binsNumber + 1)

def AssignDatasetToBins(dataset, durationBins, durationRescaling):
    dataBins = [[] for __ in range(len(durationBins))]
    for item in dataset:
        assigned = False
        for i in range(len(durationBins) - 1):
            binLeftLimit = durationBins[i]
            binRightLimit = durationBins[i + 1]
            
            itemRescaledDuration = durationRescaling( item.GetObtainedDuration() )
            # (left, right] as the 'left' can be well outside the samplig range (e.g. 0.78), but 'right' is fixed at 1.2            
            if (itemRescaledDuration > binLeftLimit and itemRescaledDuration <= binRightLimit):
                dataBins[i] += [item]
                assigned = True
                break
        if not assigned:
            logging.warning("Not assigned seed: {}->{} bin: [{}, {}]".format(item.GetObtainedDuration(), itemRescaledDuration, binLeftLimit, binRightLimit))

    return dataBins[:-1]

def SortBins(dataBins):
    return list(map(lambda bin: sorted(bin, key = lambda x: x.GetObtainedFidelity(), reverse = True), dataBins))
    
def GetFTCurveSeedsFromBins(dataBins):
    ftCurveSeeds = list(map(lambda dataBin: max(dataBin, key = lambda x: x.GetObtainedFidelity()), dataBins))
    return ftCurveSeeds
    
def GetFTCurveFromSeeds(ftCurveSeeds, durationRescaling):
    durations = list(map(lambda x: durationRescaling( x.GetObtainedDuration() ), ftCurveSeeds))
    fidelities =  list(map(lambda x: x.GetObtainedFidelity(), ftCurveSeeds))
    return (durations, fidelities)
    
def MakePolyfitPlot(ftCurve, polyFitResult, degree, sampleSize, datasetLength):
    file = "/path/to/data/fitPlots/degree_{}_sampleSize_{}.png".format(degree, sampleSize)
    ifCurve = list(map(lambda x: 1 - x, ftCurve[1]))
    logScaled = list(map(np.log10, ifCurve))
    plt.rcParams.update({'font.size': 16})
    plt.figure(figsize = (15, 10))
    # plot the sampled data
    plt.scatter(ftCurve[0], logScaled, label = "Sample size: {}".format(sampleSize), color = 'blue')

    # plot the polyfit
    plotFitDurations = np.linspace(min(ftCurve[0]), max(ftCurve[0]), num = 150)
    plt.plot(plotFitDurations, [np.polyval(polyFitResult, x) for x in plotFitDurations], color = 'black', label = "Polynomial (degree: {}) fit".format(degree))
    plt.axhline(y = -2, label = "Infidelity threshold", color = 'red')
    plt.xlabel("Normalized Duration [-]")
    plt.ylabel(r"$\log_{10}(IF)$")
    plt.title("Total available population in sampling range: {}".format(datasetLength))
    plt.legend()
    plt.savefig(file, dpi = 300)
    plt.close()

def MakePolyfit(ftCurve, degree):
    # batch-local T_QSL extraction routine
    with warnings.catch_warnings():
        warnings.filterwarnings('error')
        try:
            ifCurve = list(map(lambda x: 1 - x, ftCurve[1]))
            logScaled = list(map(np.log10, ifCurve))
            polyFitResult = np.polyfit(ftCurve[0], logScaled, deg = degree)
            return polyFitResult
        except np.RankWarning:
            logging.error("Polyfit failure")
            return None 


def ComputePolyfitTQSL(ftCurve, polyfit, dT = 1e-3):
    # batch-local T_QSL extraction routine
    currentDurationAttempt = min(ftCurve[0])
    while (currentDurationAttempt < max(ftCurve[0])):
        rescaledIF = np.polyval(polyfit, currentDurationAttempt)
        if (rescaledIF <= -2): # 1e-2
            return currentDurationAttempt
        else:
            currentDurationAttempt += dT
    return None

def ComputeFidelityForT_Ref(ftCurve,  polyfit, refT):
    if (refT < min(ftCurve[0]) or refT > max(ftCurve[0])):
        return None
    logIfvalue = np.polyval(polyfit, refT)
    ifValue = np.power(10, logIfvalue)
    fValue = 1 - ifValue
    return fValue


def ComputeTQSL_Fidelity_Batches(subDataset, bins, sampleSize, levelId,  durationRescaling, trialsPerSampleSize, polyDegree):
    if levelId == 16:
        refTQSL = 0.255
    elif levelId == 11:
        refTQSL = 0.92
    elif levelId == 7:
        refTQSL = 0.89
    else:
        raise ValueError("Unknown level id : {}".format(levelId))    

    t_qsl_values = []
    fidelityAtRefQSL_values = []

    fidelityAtTimes = {}

    polyCoeffs = []

    failedAttemptsTQSL = 0
    failedAttemptsF = 0
    if (sampleSize > len(subDataset)):
        logging.error("Sample size larger than dataset: {} > {} ".format(sampleSize, len(subDataset)))
        return None
  
    numGatheredTQSL_Values = 0
    numOfFidelityValues = 0

    plotMade = False

    trials = 0
    while trials < trialsPerSampleSize:
        # Take a sample from the dataset (with returns)
        sample = random.sample(population = subDataset, k = sampleSize)

        # Assign the sample to bins, filter out the empty bins
        sampledBinsData = AssignDatasetToBins(sample, bins, durationRescaling)

        # extract the best-per-bin seed and collect the fidelity
        zipBinsData = zip(bins, sampledBinsData)
        zipBinsData = list(filter(lambda x: len(x[1]) > 0, zipBinsData))
        ftCurveSeeds = GetFTCurveSeedsFromBins([x[1] for x in zipBinsData])
        for (time, seed) in zip([x[0] for x in zipBinsData], ftCurveSeeds):
            fidelity = seed.GetObtainedFidelity()
            if time not in fidelityAtTimes:
                fidelityAtTimes[time] = [fidelity]
            else:
                fidelityAtTimes[time] += [fidelity]

        # Select the F(T) curve from the sample
        ftCurve = GetFTCurveFromSeeds(ftCurveSeeds, durationRescaling)

        # Fit a polynomial of 'polyDegree' to (durations, log10(1 - fidelity))
        polyFit = MakePolyfit(ftCurve, degree = polyDegree)

        # in case of a total total failure, as the fit was impossible to do
        if polyFit is None:
            return None 

        polyCoeffs += [polyFit]

        # Compute the first value within the durations of the obtained F(T) curve that crosses the infidelity of 0.01 ( log10(0.01) = -2 )
        t_qsl = ComputePolyfitTQSL(ftCurve, polyFit)

        # Compute the fidelity obtainable using this sample at the reference T_QSL, refQSL here is rescaled and equal to 1
        fidelityAtRefQSL = ComputeFidelityForT_Ref(ftCurve, polyFit, refT = 1)

        # Append the obtained values
        t_qsl_values += [t_qsl]
        fidelityAtRefQSL_values += [ fidelityAtRefQSL ]

        # Compute the number of failures, the number of times we still need to sample etc., capture all this data
        numGatheredTQSL_Values = sum( list(map(lambda x: 1 if x is not None else 0, t_qsl_values)) )
        numOfFidelityValues = sum( list(map(lambda x: 1 if x is not None else 0, fidelityAtRefQSL_values)) )
        failedAttemptsTQSL = len(t_qsl_values) - numGatheredTQSL_Values 
        failedAttemptsF = len(fidelityAtRefQSL_values) -  numOfFidelityValues
        trials += 1

    # Filter out all the failures, but track their number
    t_qsl_values = list(filter(lambda x: x is not None, t_qsl_values))
    fidelityAtRefQSL_values = list(filter(lambda x: x is not None, fidelityAtRefQSL_values))

    # Compute mean T_QSL, its stdev, mean fidelity at T_QSL_REF and its stdev
    if t_qsl_values == []:
        logging.warning("Empty t_qsl_values list for {} sample size.".format(sampleSize))
        meanTQSL = np.nan
        stdTQSL = np.nan
    else:
        meanTQSL = np.mean(t_qsl_values)
        stdTQSL = np.std(t_qsl_values)
    
    if fidelityAtRefQSL_values == []:
        logging.warning("Empty fidelityAtRefQSL_values list for {} sample size.".format(sampleSize))    
        meanFidelityAtRefTQSL = np.nan
        stdFidelityAtRefTQSL = np.nan
    else:
        meanFidelityAtRefTQSL = np.mean(fidelityAtRefQSL_values)
        stdFidelityAtRefTQSL = np.std(fidelityAtRefQSL_values)
    
    meanFidelityAtTimes = []
    stdFidelityAtTimes = []
    for (k, v) in fidelityAtTimes.items():
        if v == []:
            logging.warning("Empty bin data for duration {}.".format(k))
            continue
        meanV = np.mean(v)
        stdV = np.std(v)
        meanFidelityAtTimes += [(k, meanV)]
        stdFidelityAtTimes  += [(k, stdV)]

    meanFidelityAtTimes = sorted(meanFidelityAtTimes, key = lambda x: x[0])
    stdFidelityAtTimes = sorted(stdFidelityAtTimes, key = lambda x: x[0])

    meanPolyfitCoefficients = list( np.mean(polyCoeffs, axis = 0) )

    reportString = "Size of sample: {} | Num of T_QSL: {} | Norm T_QSL = {} p/m {} | T_QSL [ms]: {} | Fidelity:{} at {} | Fidelity stdev: {} | failed T_QSL scan attempts: {} | failed fidelity attempts: {} | mean fit coeffs: {}".format(
                            len(sample), 
                            len(t_qsl_values),
                            round(meanTQSL, 4),  round(stdTQSL,4),
                            round(meanTQSL * refTQSL, 4), 
                            meanFidelityAtRefTQSL,
                            refTQSL,
                            stdFidelityAtRefTQSL,
                            failedAttemptsTQSL,
                            failedAttemptsF,
                            meanPolyfitCoefficients)
    logging.debug(reportString)
    return (sampleSize, meanTQSL, stdTQSL, meanFidelityAtRefTQSL, stdFidelityAtRefTQSL, failedAttemptsTQSL, failedAttemptsF, meanFidelityAtTimes, stdFidelityAtTimes, meanPolyfitCoefficients)
  
def SelectDatasetDurationSubset(dataset, durationRescaling, durationRange):
    outputDataset = []
    for item in dataset:
        itemRescaledDuration = durationRescaling( item.GetObtainedDuration())
        if (itemRescaledDuration >= durationRange[0] and itemRescaledDuration < durationRange[1]):
            outputDataset += [item]
    return outputDataset

def ConstructDurationMeanFidelityDict(outputSampleSizes, meanFidelityAtTimes):
    durationMeanFidelityDict = {}
    for (sampleSize, data) in zip(outputSampleSizes, meanFidelityAtTimes):
        durations = [round(x[0], 5) for x in data]
        fidelities = [x[1] for x in data]
        for (duration, fidelity) in zip(durations, fidelities):
            if duration not in durationMeanFidelityDict:
                durationMeanFidelityDict[duration] = [(sampleSize, fidelity)]
            else:
                durationMeanFidelityDict[duration] += [(sampleSize, fidelity)]
    return durationMeanFidelityDict


def Make_TQSL_Report_Polyfits(data, cutOffAt):
    if data['dataLabel'] == "PSO":
        data['dataLabel'] = "Players (Computer Optimized)"
    elif data['dataLabel'] == "OptimizedByPlayerData":
        data['dataLabel'] = 'Players (Player Optimized)'   
            
    if data['levelId'] == 16:
        refTQSL = 0.255 * 0.38731
        rounding = 4
    elif data['levelId'] == 11:
        refTQSL = 0.92
        rounding = 3
    elif data['levelId'] == 7:
        refTQSL = 0.89
        rounding = 3
    else:
        raise ValueError("Unknown level id : {}".format(data['levelId']))          
        
    reportMeanT_QSL = None
    reportSampleSize = None
    for (sampleSize, mean_TQSL) in zip(data['sampleSizes'], data['mean_TQSL']):
        if sampleSize == cutOffAt:
            reportMeanT_QSL = mean_TQSL
            reportSampleSize = sampleSize
            break

    if reportMeanT_QSL is None:
        reportMeanT_QSL = data['mean_TQSL'][-1]
        reportSampleSize = data['sampleSizes'][-1]        

    report = "Sample Size: {ss} | Normalized T_QSL: {nqsl} | T_QSL: {qsl} |  Optimizer: {opt}".format(
                            nqsl = round(mean_TQSL, rounding), 
                            qsl = round(mean_TQSL * refTQSL, rounding),
                            ss = sampleSize, 
                            opt = data['dataLabel'])  
    return (report, reportMeanT_QSL)
    
def Make_F_at_TQSL_Report_Polyfits(data, cutOffAt):
    if data['dataLabel'] == "PSO":
        data['dataLabel'] = "Players (Computer Optimized)"
    elif data['dataLabel'] == "OptimizedByPlayerData":
        data['dataLabel'] = 'Players (Player Optimized)'
        
    if data['levelId'] == 16:
        refTQSL = 0.255 * 0.38731
    elif data['levelId'] == 11:
        refTQSL = 0.92
    elif data['levelId'] == 7:
        refTQSL = 0.89
    else:
        raise ValueError("Unknown level id : {}".format(data['levelId']))                

    reportMeanF = None
    reportSampleSize = None
    for (sampleSize, meanF) in zip(data['sampleSizes'], data['meanF']):
        if sampleSize == cutOffAt:
            reportMeanF = meanF
            reportSampleSize = sampleSize
            break

    if reportMeanF is None:
        reportMeanF = data['meanF'][-1]
        reportSampleSize = data['sampleSizes'][-1]        

    report = "Sample Size: {ss} | Reference T_QSL: {ref_TQSL} | F: {F} |  Optimizer: {opt}".format(
                            ref_TQSL = round(refTQSL, 4), 
                            F = round(reportMeanF, 4),
                            ss = sampleSize, 
                            opt = data['dataLabel'])        

    return (report, reportMeanF)

def ComputeBinPopulations(dataset, bins, durationRescaling):
    # assign the dataset to bins to figure out how many seeds are there per bin, capture this data
    seedBinsTmp = AssignDatasetToBins(dataset, bins, durationRescaling)
    binLeftsRounded = [round(x, 5) for x in bins]
    binPopulationData = [(x[0], len(x[1])) for x in zip(binLeftsRounded, seedBinsTmp)]
    for item in binPopulationData:
        print("Bin: {} Population: {}".format(item[0], item[1]))
    print("Total population: {}".format(sum([len(x) for x in seedBinsTmp])))  
    return { binLeft : pop for (binLeft, pop) in binPopulationData}

def SerializeSaturationPlayerData(  outputSampleSizes, meanTQSL, stdTQSL, meanF, stdF, failuresQSL, 
                                    failuresF, meanFidelitiesAtTimes, stdFidelitiesAtTimes, meanPolyfitCoefficients, durationMeanFidelityDict, binPopulationData,
                                    levelId, dataLabel, polyDegree, outputFile):
    logging.info("Saving sampling saturation data to {}.".format(outputFile))
    with open(outputFile, 'w') as fh:
        outputDict = {}
        outputDict['sampleSizes'] = outputSampleSizes
        outputDict['mean_TQSL'] = meanTQSL
        outputDict['std_TQSL'] = stdTQSL
        outputDict['meanF'] = meanF
        outputDict['stdF'] = stdF
        outputDict['failuresQSL'] = failuresQSL
        outputDict['failuresF'] = failuresF
        outputDict['levelId'] = levelId
        outputDict['dataLabel'] = dataLabel
        outputDict['meanFidelitiesAtTimes'] = meanFidelitiesAtTimes
        outputDict['stdFidelitiesAtTimes'] = stdFidelitiesAtTimes
        try:
            meanPolyfitCoefficients = [[y[0] for y in x] for x in meanPolyfitCoefficients]
            outputDict['meanPolyfitCoefficients'] = meanPolyfitCoefficients
        except IndexError:
            logging.error("Trouble with meanPolyfitCoefficients in saving to {}".format(outputFile))
        
        outputDict['durationMeanFidelityDict'] = durationMeanFidelityDict
        outputDict['binPopulationData'] = binPopulationData
        outputDict['fitDegree'] = polyDegree
        json.dump(outputDict, fh)

def ComputeSamplingSaturation(dataset, dataLabel, levelId, isPlayerData, config, outputDirectory):
    # Construct the name of the output file
    fileName = "samplingSaturation_level_{level_id}_{data_label}.json".format(level_id = levelId, 
                                                                   data_label = dataLabel)  
    outputFile = os.path.join(outputDirectory, fileName)
    logging.info("Computing sampling saturation for: {}".format(fileName))

    # prepare the duration renormalizing function 
    durationRescaling = MakeRescaleDurations(levelId, isPlayerData)

    # filter out the seed durations outside our sampling bounds (to only investigate the neighborhood of T_QSL)
    subDataset = SelectDatasetDurationSubset(dataset, durationRescaling, config.SamplingDurationRange)

    # compute the seeds per bin data
    binPopulationData = ComputeBinPopulations(subDataset, config.bins, durationRescaling)

    logging.info("Length of the dataset (within allowed sampling bounds): {}".format(len(subDataset)))

    # bin/seed sanity check, check if the subset of the data has all the seeds in the sampling duration range
    for item in subDataset:
        d = item.GetObtainedDuration()
        dn = durationRescaling( d )
        if (dn < config.SamplingDurationRange[0] or dn > config.SamplingDurationRange[1]):
            raise ValueError("Invalid seed duration")    

    # Compute the data for each of the specified sample sizes
    outputs = []
    for sampleSize in config.trialSampleSizes:
        logging.debug("Sample size: {}".format(sampleSize))
        if (sampleSize > len(subDataset)):
            continue            
        outputs += [ComputeTQSL_Fidelity_Batches(subDataset, config.bins, sampleSize, 
                                                   levelId, durationRescaling, config.trialsPerSampleSize,
                                                  config.polynomialDegree)]

    outputs = list(filter(lambda x: x is not None, outputs))

    # gather up all the data from the batches
    outputSampleSizes = []
    t_qsl_for_sampleSize = []
    t_qsl_stdev_for_sampleSize = []
    fidelitiesAtRefQSL = []
    fidelitiesStdevRefQSL = []
    failuresTQSL = []
    failuresF = []
    meanFidelityAtTimes = []
    stdFidelityAtTimes = []
    meanPolyfitCoefficients = []
    for output in outputs:
        outputSampleSizes += [output[0]]
        t_qsl_for_sampleSize+= [output[1]]
        t_qsl_stdev_for_sampleSize += [output[2]]
        fidelitiesAtRefQSL += [output[3]]
        fidelitiesStdevRefQSL += [output[4]]
        failuresTQSL += [output[5]]
        failuresF += [output[6]]
        meanFidelityAtTimes += [output[7]]
        stdFidelityAtTimes += [output[8]]                
        meanPolyfitCoefficients += [output[9]]

    # Construct a convenient data structure for the (meanFidelity per sample size per bin) data
    durationMeanFidelityDict = ConstructDurationMeanFidelityDict(outputSampleSizes, meanFidelityAtTimes)

    # Save the data to the output file
    SerializeSaturationPlayerData(  outputSampleSizes, 
                                    t_qsl_for_sampleSize,
                                    t_qsl_stdev_for_sampleSize,
                                    fidelitiesAtRefQSL,
                                    fidelitiesStdevRefQSL,
                                    failuresTQSL,
                                    failuresF,
                                    meanFidelityAtTimes,
                                    stdFidelityAtTimes,
                                    meanPolyfitCoefficients,
                                    durationMeanFidelityDict,
                                    binPopulationData,                                        
                                    levelId,
                                    dataLabel,
                                    config.polynomialDegree,
                                    outputFile)      