# This program performs the sampling of the Bring Home Water problem seeds (produced by the conventional methods), 
# selects the F(T) curves from the samples, performs the fits and evaluates the T_QSL of the sample along with their fidelities at T_QSL.
# The code executing the 'sampling-fits' procedure is in the file SamplingSaturation.py.

import logging
import sys

from importlib import reload  # Python 3.4+ only.

import saturationCfg as scfg

import FTUtilities as ft
import ConventionalDataSerialization as cds # my module
import SamplingSaturation


maxSample = scfg.maxSample
binsNumber = scfg.binsNumber
bins = scfg.bins
trialsPerSampleSize = scfg.trialsPerSampleSize
trialSampleSizes =scfg.trialSampleSizes
outputDirectory = scfg.outputDir

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s BHW: %(message)s',
    level=logging.INFO,
    stream = sys.stdout,
    datefmt='%Y-%m-%d %H:%M:%S')

if __name__ == '__main__':
    __spec__ = "ModuleSpec(name='builtins', loader=<class '_frozen_importlib.BuiltinImporter'>)"
    dirSums = [
    #  (  "/path/to/data/bhw/25_05_19_SGA_BUCKET/output_BHW",             "/path/to/data/bhw/25_05_19_SGA_BUCKET/summary_25_05_19_SGA_BUCKET_output_BHW_SGA_BUCKET.json"),
    # (  "/path/to/data/bhw/12_05_19/output_BHW"          ,             "/path/to/data/bhw/12_05_19/summary_12_05_19_output_BHW_GRAPE.json"),
    # (  "/path/to/data/bhw/24_05_19_RS/output_BHW"     ,               "/path/to/data/bhw/24_05_19_RS/summary_24_05_19_RS_output_BHW_GRAPE.json"),
    #     ("/path/to/data/bhw/17_05_19_SGA/output_BHW",                 "/path/to/data/bhw/17_05_19_SGA/summary_17_05_19_SGA_output_BHW_SGA.json"),
    # ("/path/to/data/bhw/19_05_19/output_BHW",                         "/path/to/data/bhw/19_05_19/summary_19_05_19_output_BHW_SGA_GRAPE.json"),
      (  "/path/to/data/bhw/11_11_19_SGA_RS/output_BHW",                       "/path/to/data/bhw/11_11_19_SGA_RS/summary_11_11_19_SGA_RS_output_BHW_SGA.json"),  
    ]
    
    
    for (dirName, summaryFile) in dirSums:
        fileNamesData = ft.FTFileNames(dirName)
        summary = ft.SummaryCollection(summaryFile)
        dataLabel = "{opt} ({seeding})".format(  opt = summary.mOptimizerType, 
                                                 seeding = summary.mSeedingType)

        if scfg.doLoadRawSeeds:
            ### Load the raw .mat files
            seedCollection = ft.SeedMetaCollection(fileNamesData.GetSeedFileNames(), 
                                                    fileNamesData.GetMetadataFile(), 
                                                    scfg.workerProcesses)    
        else:
            ### Load serialized versions of the datasets (requires previous dataset serialization)
            serializedCSVName = cds.MakeSerializationCSVFileName(dirName)        
            seedCollection = cds.LoadSerializedCSVDatums(serializedCSVName)

        dataset = seedCollection.mSeedDatumlist
        isPlayerData = False
        levelId = summary.mLevelId

        SamplingSaturation.ComputeSamplingSaturation(dataset, dataLabel, levelId, isPlayerData, scfg, outputDirectory)        
