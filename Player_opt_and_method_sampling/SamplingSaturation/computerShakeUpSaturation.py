# This program performs the sampling of the GPE ShakeUp problem seeds (produced by the conventional methods), 
# selects the F(T) curves from the samples, performs the fits and evaluates the T_QSL of the sample along with their fidelities at T_QSL.
# The code executing the 'sampling-fits' procedure is in the file SamplingSaturation.py.

import csv
import logging
import os
import json
import sys
import matplotlib.pyplot as plt
import numpy as np
from importlib import reload  # Python 3.4+ only.

import CVUtilities as cval
import FTUtilities as ft
import ConventionalDataSerialization as cds # my module
import SamplingSaturation

from  mysql.connector import connection
import gc
import concurrent
import saturationCfg as scfg
import concurrent.futures

maxSample = scfg.maxSample
binsNumber = scfg.binsNumber
bins = scfg.bins
trialsPerSampleSize = scfg.trialsPerSampleSize
trialSampleSizes =scfg.trialSampleSizes
outputDirectory = scfg.outputDir

doLoadRawSeeds = scfg.doLoadRawSeeds


logging.basicConfig(
    format='%(asctime)s %(levelname)-8s ShakeUp: %(message)s',
    level=logging.INFO,
    stream = sys.stdout,
    datefmt='%Y-%m-%d %H:%M:%S')




if __name__ == '__main__':
    __spec__ = "ModuleSpec(name='builtins', loader=<class '_frozen_importlib.BuiltinImporter'>)"

    dirSums = [
    # ("/path/to/data/gpe_shakeup/21_05_19/output_GPE_SHAKE_UP", "/path/to/data/gpe_shakeup/21_05_19/summary_21_05_19_output_GPE_SHAKE_UP_SGA.json"),
    # ("/path/to/data/gpe_shakeup/26_05_19_SGA/output_GPE_SHAKE_UP", "/path/to/data/gpe_shakeup/26_05_19_SGA/summary_26_05_19_SGA_output_GPE_SHAKE_UP_SGA.json"),
    # (  "/path/to/data/gpe_shakeup/15_05_19/output_GPE_SHAKE_UP"          , "/path/to/data/gpe_shakeup/15_05_19/summary_15_05_19_output_GPE_SHAKE_UP_GRAPE.json"),
    # (  "/path/to/data/gpe_shakeup/24_05_19_RS/output_GPE_SHAKE_UP"       , "/path/to/data/gpe_shakeup/24_05_19_RS/summary_24_05_19_RS_output_GPE_SHAKE_UP_GRAPE.json"),
    # (  "/path/to/data/gpe_shakeup/24_05_19_RSS/output_GPE_SHAKE_UP"      , "/path/to/data/gpe_shakeup/24_05_19_RSS/summary_24_05_19_RSS_output_GPE_SHAKE_UP_GRAPE.json"),
    ( "/path/to/data/gpe_shakeup/27_08_19_SGA_BUCKET_RS/output_GPE_SHAKE_UP", "/path/to/data/gpe_shakeup/27_08_19_SGA_BUCKET_RS/summary_27_08_19_SGA_BUCKET_RS_output_GPE_SHAKE_UP_SGA_BUCKET.json")
    ]
    
    for (dirName, summaryFile) in dirSums:
        fileNamesData = ft.FTFileNames(dirName)
        summary = ft.SummaryCollection(summaryFile)
        dataLabel = "{opt} ({seeding})".format(   opt = summary.mOptimizerType, 
                                                seeding = summary.mSeedingType)


        if scfg.doLoadRawSeeds:
            ### Load the raw .mat files
            seedCollection = ft.SeedMetaCollection(fileNamesData.GetSeedFileNames(), 
                                                    fileNamesData.GetMetadataFile(), 
                                                    scfg.workerProcesses)    
        else:
            ### Load serialized versions of the datasets (requires previous dataset serialization)
            serializedCSVName = cds.MakeSerializationCSVFileName(dirName, fileNamesData)        
            seedCollection = cds.LoadSerializedCSVDatums(serializedCSVName)


        dataset = seedCollection.mSeedDatumlist
        isPlayerData = False
        levelId = summary.mLevelId

        SamplingSaturation.ComputeSamplingSaturation(dataset, dataLabel, levelId, isPlayerData, scfg, outputDirectory)        
