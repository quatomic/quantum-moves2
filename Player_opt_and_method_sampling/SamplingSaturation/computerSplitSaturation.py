# This program performs the sampling of the GPE Splitting problem seeds (produced by the conventional methods), 
# selects the F(T) curves from the samples, performs the fits and evaluates the T_QSL of the sample along with their fidelities at T_QSL.
# The code executing the 'sampling-fits' procedure is in the file SamplingSaturation.py.

import csv
import logging
import os
import json
import sys
import matplotlib.pyplot as plt
import numpy as np
from importlib import reload  # Python 3.4+ only.

import CVUtilities as cval
import FTUtilities as ft
import ConventionalDataSerialization as cds # my module
import SamplingSaturation

from  mysql.connector import connection
import gc
import concurrent
import saturationCfg as scfg
import concurrent.futures

maxSample = scfg.maxSample
binsNumber = scfg.binsNumber
bins = scfg.bins
trialsPerSampleSize = scfg.trialsPerSampleSize
trialSampleSizes =scfg.trialSampleSizes

outputDirectory = scfg.outputDir

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s Splitting: %(message)s',
    level=logging.INFO,
    stream = sys.stdout,
    datefmt='%Y-%m-%d %H:%M:%S')


if __name__ == '__main__':
    __spec__ = "ModuleSpec(name='builtins', loader=<class '_frozen_importlib.BuiltinImporter'>)"
    dirSums = [
    # (    "/path/to/data/gpe_split/12_05_19/output_GPE_SPLIT",            "/path/to/data/gpe_split/12_05_19/summary_12_05_19_output_GPE_SPLIT_GRAPE.json"),
    # ("/path/to/data/gpe_split/26_05_19_SGA/output_GPE_SPLIT",            "/path/to/data/gpe_split/26_05_19_SGA/summary_26_05_19_SGA_output_GPE_SPLIT_SGA.json"),
    # ("/path/to/data/gpe_split/21_05_19/output_GPE_SPLIT",                "/path/to/data/gpe_split/21_05_19/summary_21_05_19_output_GPE_SPLIT_SGA.json"),
    # (    "/path/to/data/gpe_split/24_05_19_RS/output_GPE_SPLIT",         "/path/to/data/gpe_split/24_05_19_RS/summary_24_05_19_RS_output_GPE_SPLIT_GRAPE.json"),
    # ( "/path/to/data/gpe_split/27_08_19_SGA_BUCKET_RS/output_GPE_SPLIT", "/path/to/data/gpe_split/27_08_19_SGA_BUCKET_RS/summary_27_08_19_SGA_BUCKET_RS_output_GPE_SPLIT_SGA_BUCKET.json"),
    ( "/path/to/data/gpe_split/05_09_19_SGA_RS/output_GPE_SPLIT", "/path/to/data/gpe_split/05_09_19_SGA_RS/summary_05_09_19_SGA_RS_output_GPE_SPLIT_SGA.json")
    
    ]

    for (dirName, summaryFile) in dirSums:
        fileNamesData = ft.FTFileNames(dirName)
        summary = ft.SummaryCollection(summaryFile)
        dataLabel = "{opt} ({seeding})".format(   opt = summary.mOptimizerType, 
                                                seeding = summary.mSeedingType)
        fileName = "samplingSaturation_level_{level_id}_{data_label}.json".format(level_id = summary.mLevelId, 
                                                                       data_label = dataLabel)

        if scfg.doLoadRawSeeds:
            ### Load the raw .mat files
            seedCollection = ft.SeedMetaCollection(fileNamesData.GetSeedFileNames(), 
                                                    fileNamesData.GetMetadataFile(), 
                                                    scfg.workerProcesses)    
        else:
            ### Load serialized versions of the datasets (requires previous dataset serialization)
            serializedCSVName = cds.MakeSerializationCSVFileName(dirName, fileNamesData)        
            seedCollection = cds.LoadSerializedCSVDatums(serializedCSVName)

        dataset = seedCollection.mSeedDatumlist
        isPlayerData = False
        levelId = summary.mLevelId

        SamplingSaturation.ComputeSamplingSaturation(dataset, dataLabel, levelId, isPlayerData, scfg, outputDirectory)        

