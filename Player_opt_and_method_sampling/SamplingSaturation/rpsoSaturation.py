import csv
import logging
import os
import json
import sys
import matplotlib.pyplot as plt
import numpy as np
from importlib import reload  # Python 3.4+ only.

import CVUtilities as cval
import FTUtilities as ft
import ConventionalDataSerialization as cds # my module
import SamplingSaturation

from  mysql.connector import connection
import gc
import concurrent
import saturationCfg as scfg
import concurrent.futures

maxSample = scfg.maxSample
binsNumber = scfg.binsNumber
bins = scfg.bins
trialsPerSampleSize = scfg.trialsPerSampleSize
trialSampleSizes =scfg.trialSampleSizes

outputDirectory = scfg.outputDir

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s RPSO: %(message)s',
    level=logging.INFO,
    stream = sys.stdout,
    datefmt='%Y-%m-%d %H:%M:%S')


commonDir = "/path/to/data/playerSeeds/RandomSeedsPreselection/SeedsOptimized_stage5"
if __name__ == '__main__':
    __spec__ = "ModuleSpec(name='builtins', loader=<class '_frozen_importlib.BuiltinImporter'>)"
    dirSums = [
                # ("{commonDir}/optimized_2/output_RPSO/bhw_RS/output_BHW", "{commonDir}/optimized_2/output_RPSO/bhw_RS/summary_bhw_RS_output_BHW_GRAPE.json"),
                ("{commonDir}/optimized_1/output_RPSO/bhw_RSS/output_BHW", "{commonDir}/optimized_1/output_RPSO/bhw_RSS/summary_bhw_RSS_output_BHW_GRAPE.json"),
                # ("{commonDir}/optimized_2/output_RPSO/gpe_shakeup_RS/output_GPE_SHAKE_UP", "{commonDir}/optimized_2/output_RPSO/gpe_shakeup_RS/summary_gpe_shakeup_RS_output_GPE_SHAKE_UP_GRAPE.json"),
                # ("{commonDir}/optimized_2/output_RPSO/gpe_shakeup_RSS/output_GPE_SHAKE_UP", "{commonDir}/optimized_2/output_RPSO/gpe_shakeup_RSS/summary_gpe_shakeup_RSS_output_GPE_SHAKE_UP_GRAPE.json"),
                # ("{commonDir}/optimized_2/output_RPSO/gpe_split_RS/output_GPE_SPLIT", "{commonDir}/optimized_2/output_RPSO/gpe_split_RS/summary_gpe_split_RS_output_GPE_SPLIT_GRAPE.json"),
                # ("{commonDir}/optimized_2/output_RPSO/gpe_split_RSS/output_GPE_SPLIT", "{commonDir}/optimized_2/output_RPSO/gpe_split_RSS/summary_gpe_split_RSS_output_GPE_SPLIT_GRAPE.json")
    ]

    for (dirName, summaryFile) in dirSums:
        dirName = dirName.format(commonDir = commonDir)
        summaryFile = summaryFile.format(commonDir = commonDir)
        fileNamesData = ft.FTFileNames(dirName)
        summary = ft.SummaryCollection(summaryFile)
        dataLabel = "RPSO {opt} ({seeding}) (1 cycle)".format(   opt = summary.mOptimizerType, 
                                                seeding = summary.mSeedingType)
        fileName = "samplingSaturation_level_{level_id}_{data_label}.json".format(level_id = summary.mLevelId, 
                                                                       data_label = dataLabel)

        scfg.doLoadRawSeeds = True
        if scfg.doLoadRawSeeds:
            ### Load the raw .mat files
            seedCollection = ft.SeedMetaCollection(fileNamesData.GetSeedFileNames(), 
                                                    fileNamesData.GetMetadataFile(), 
                                                    scfg.workerProcesses)    
        else:
            ### Load serialized versions of the datasets (requires previous dataset serialization)
            serializedCSVName = cds.MakeSerializationCSVFileName(dirName, fileNamesData)        
            seedCollection = cds.LoadSerializedCSVDatums(serializedCSVName)

        dataset = seedCollection.mSeedDatumlist
        isPlayerData = False
        levelId = summary.mLevelId

        SamplingSaturation.ComputeSamplingSaturation(dataset, dataLabel, levelId, isPlayerData, scfg, outputDirectory)        

