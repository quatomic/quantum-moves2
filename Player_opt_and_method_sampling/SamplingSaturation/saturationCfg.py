import CVUtilities as cval
from  mysql.connector import connection
import FTUtilities as ft
import gc
import numpy as np
def MakeDurationBins(limits, binsNumber):
    return np.linspace(limits[0], limits[1], binsNumber + 1)

SamplingDurationRange = [0.8, 1.2]
maxSample = 10000
binsNumber = 40
polynomialDegree = 1
bins = MakeDurationBins(limits = [0.0, 1.2], binsNumber = binsNumber)
acceptedBinIndices = []
for index in range(len(bins)):
	binVal = bins[index]
	if (binVal >= SamplingDurationRange[0] and binVal <= SamplingDurationRange[1]):
		acceptedBinIndices += [index]
acceptedBinIndices = sorted(acceptedBinIndices)

# don't leave out the seeds outside first bin, as they may still be within the sampling duration range
if ( bins[ acceptedBinIndices[0] ] > SamplingDurationRange[0]  and acceptedBinIndices[0] > 0):
	acceptedBinIndices = [acceptedBinIndices[0] - 1] + acceptedBinIndices

bins = list(map(lambda x: bins[x], acceptedBinIndices))

# print("Bins after introducing control duration constraint: {}".format(len(bins)))
trialsPerSampleSize = 1000
trialSampleSizes = list(range(len(bins) * 2, 100, 1)) + list(range(100, 1000, 10)) + list(range(1000, maxSample, 25)) 
outputDir = "/path/to/data/SamplingSaturation"
doLoadRawSeeds = False
workerProcesses = 6