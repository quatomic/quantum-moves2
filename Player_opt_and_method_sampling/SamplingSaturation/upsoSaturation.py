import csv
import logging
import os
import json
import sys
import gc
import numpy as np
from importlib import reload  # Python 3.4+ only.
from  mysql.connector import connection

import CVUtilities as cval
import FTUtilities as ft
import SamplingSaturation
import PSOUtilities as pso

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s Upso: %(message)s',
    level=logging.INFO,
    stream = sys.stdout,
    datefmt='%Y-%m-%d %H:%M:%S')

# Get the db credentials
passFileName = '/path/to/key/key'
creds = cval.AcquireCredentials(passFileName)

# get list of path_ids from the db that weren't optimized
unoptimizedPathIds = cval.GetUnoptmizedPathIds(creds)
unoptimizedPathIdsSet = set()
for item in unoptimizedPathIds:
    unoptimizedPathIdsSet.add(item)

passFileName2 = '/path/to/key/key'
creds2 = cval.AcquireCredentials(passFileName2)
unoptimizedPathIds2 = cval.GetUnoptmizedPathIds(creds2)
for item in unoptimizedPathIds2:
    unoptimizedPathIdsSet.add(item)
print(len(unoptimizedPathIdsSet))

# load the player-seed optimization dataset
dirName = "/path/to/data/SamplingSaturation/03_06_19/output_PSO"
doLoadRawSeeds = False
if doLoadRawSeeds:
# Load the raw version of the PSO data set
    upsoDataset = pso.LoadupsoDataset(dirName) 
else:
# Load the serialized version of the PSO data set
    upsoDataset = pso.LoadSerializedPSOCSVDatums(dirName + "/../upso_serialized.csv")


# Filter out the player-optimized datums
upsoDataset = list(filter(lambda x: x.GetPathId() in unoptimizedPathIdsSet, upsoDataset))
print(len(upsoDataset))    

# Filter out durations > 1.2 (errors in database)
upsoDataset = list(filter(lambda x: x.GetObtainedDuration() <= 1.2, upsoDataset))

import saturationCfg as scfg
if __name__ == '__main__':
    __spec__ = "ModuleSpec(name='builtins', loader=<class '_frozen_importlib.BuiltinImporter'>)"
    maxSample = scfg.maxSample
    binsNumber = scfg.binsNumber
    bins = scfg.bins
    trialsPerSampleSize = scfg.trialsPerSampleSize
    trialSampleSizes = scfg.trialSampleSizes 
    outputDir = scfg.outputDir
    dataLabel = "Upso"
    isPlayerData = True
    outputDirectory = scfg.outputDir

    for levelId in (7, 11, 16):
        dataset = list(filter(lambda x: x.GetLevelId() == levelId, upsoDataset))
        SamplingSaturation.ComputeSamplingSaturation(dataset, dataLabel, levelId, isPlayerData, scfg, outputDirectory)