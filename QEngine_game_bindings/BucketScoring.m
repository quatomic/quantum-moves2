function BucketScoring
clc
clear all
close all

%Fs = 0.999;
%Fs = linspace(0,1,10000);
Fs = 0:1e-4:1.0;
%Fs = [0.2,0.25,0.45,0.5,0.7,0.75,0.90,0.95,0.97,0.99];


Fthreshold = 0.999;

Fj = [0.25; 0.50; 0.75; Fthreshold];
q  = [2.5;    2.5;    4.0;    1.0];
g  = [1/5;    1/5;    1/2;    0].*q;


[Fj_padded, g_padded, q_padded, Q, K] = makeParameters((Fj), q, g);


score = @(F) scoreFull(F,Fj_padded, g_padded, q_padded, Q, K)


scores = zeros(size(Fs));
for l=1:length(Fs)
    scores(l) = score((Fs(l)));
end
scores
plot(Fs,scores,'k.');
xlim([0,1.1])
ylim([0,26])


hold on
for j=1:length(Q)
    % Qline
    plot([0 Fj_padded(j)], [Q(j) Q(j)],'k--','linewidth',1) 
    plot([0 0], [0 Q(j)],'k--') 
        
   if j>1
       offset = 0.02;
       
       % q
        plot([Fj_padded(j) Fj_padded(j)], [Q(j-1) Q(j-1)+q_padded(j)],'b--','linewidth',2) % q line
        text(Fj_padded(j)+offset,q_padded(j)/2 + Q(j-1),['$q_' num2str(j-1) '$'],'interpreter','latex','fontsize',25)
        
        % g
        plot([Fj_padded(j) Fj_padded(j)], [Q(j-1) + q_padded(j), Q(j)],'r--','linewidth',2) % q line
        text(Fj_padded(j)+offset, Q(j)-g(j-1)/2,['$g_' num2str(j-1) '$'],'interpreter','latex','fontsize',25)
      
        % sigma 
        offsetx = 0.0;
        offsety = -0.5;
        text(Fj_padded(j-1)+(Fj_padded(j)-Fj_padded(j-1))/2+offsetx,q_padded(j)/2 + Q(j-1) + offsety,['$\sigma_' num2str(j-1) '$'],'interpreter','latex','fontsize',25)


   end
   %annotation('textarrow',[Fj_padded(j) Fj_padded(j)], [0 Q(j)],'String','y = x ')
end
Fj_padded
xticks(Fj_padded)
%xticklabels({'$F_0=0$','$F_1$','$F_2$','$F_3$','$F_4=F_{threshold}$'})

yticks(Q)
yticklabels({'$Q_0=0$','$Q_1$','$Q_2$','$Q_3$','$Q_4$'})

set(gca,'TickLabelInterpreter', 'latex','fontsize',25);

end

function [Fj, g, q, Q, K] = makeParameters(Fj, q, g)

Fj = [0; Fj;]
K = length(Fj)-1;
g = [0; g;0]; % padding to not go out of bounds
q = [0; q;0]; % padding to not go out of bounds

Q = zeros(length(Fj),1);
for j = 1:K
    Q(j+1) = q(j+1) + g(j+1) + Q(j);
end

end


function s=scoreFull(F, Fj, g, q, Q, K)

for j = 1:K
    if (Fj(j) <= F) && (F < Fj(j+1))  
       
       x1 = Fj(j);   y1 = Q(j);
       x2 = Fj(j+1); y2 = Q(j) + q(j+1);
       
       a = (y2 - y1)/(x2 - x1);
       b = y1 - a*x1;
              
       s = a*F + b;
       return
    end
    
    if j==K
       s=Q(j+1);
    end
end


end









