cmake_minimum_required(VERSION 3.5)

project(qengine_bindings)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

if(NOT CMAKE_BUILD_TYPE)
	set(CMAKE_BUILD_TYPE "Release")
endif()

if(${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
	set(GCC true)
else()
	set(GCC false)
endif()

if(${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")
	set(CLANG true)
else()
	set(CLANG false)
endif()

if(MSVC)
	add_definitions("/MP")
	add_definitions("/Za")
	add_definitions("/W4")
	add_definitions("/Ox")
	# add_definitions("/bigobj")
	set_property(GLOBAL PROPERTY USE_FOLDERS ON)
elseif(GCC)
	add_definitions("-Wall -Wno-long-long -fpermissive")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --std=gnu++1y -Wall -Wextra -Wno-unused-parameter")
	if(${CMAKE_BUILD_TYPE} STREQUAL "Release")
		add_definitions("-O3")
	else()
		add_definitions("-Og -g")
	endif()
elseif(CLANG)
	add_definitions("-Weverything -O3")
endif()


# load the configuration
include(settings.txt)
set(QENGINE_INC_DIR ${QENGINE_DIR}/include)

# includes
include_directories("${CMAKE_SOURCE_DIR}/inc")
include_directories("${QENGINE_INC_DIR}")
include_directories("${QENGINE_INC_DIR}/qengine")
include_directories("${QENGINE_DIR}/dependencies/armadillo/include")
include_directories("${QENGINE_DIR}/dependencies/variant")
include_directories("${QENGINE_DIR}/dependencies/googletest/include")


# libraries
link_directories(${QENGINE_LIB_DIR})
link_directories(${MKL_LIB_DIR})

# create directories for swig stuff
file(MAKE_DIRECTORY ${PROJECT_SOURCE_DIR}/swig_wrappers)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin)

install(DIRECTORY DESTINATION ${PROJECT_SOURCE_DIR}/swig_wrappers)
install(DIRECTORY DESTINATION ${PROJECT_SOURCE_DIR}/swig_wrappers/csharp)
install(DIRECTORY DESTINATION ${PROJECT_SOURCE_DIR}/bin)

# set SWIG files output directory
set(CMAKE_SWIG_OUTDIR ${PROJECT_SOURCE_DIR}/swig_wrappers/csharp)

# set C# wrapper output directory
set(CSHARP_OUTDIR ${PROJECT_SOURCE_DIR}/qengine_wrapper)
file(MAKE_DIRECTORY ${CSHARP_OUTDIR})
install(DIRECTORY DESTINATION ${CSHARP_OUTDIR})

if ("${CSHARP_OUTDIR}" STREQUAL "")
	MESSAGE(FATAL_ERRORO " CSHARP_OUTDIR is an empty string! Risk of fatal removals of important files due to other scripts.")
endif()

# create unity project script directory
set(UNITY_CSHARP_OUTDIR ${PROJECT_SOURCE_DIR}/unity_qengine_wrapper)
file(MAKE_DIRECTORY ${UNITY_CSHARP_OUTDIR})
install(DIRECTORY DESTINATION ${UNITY_CSHARP_OUTDIR})

if ("${UNITY_CSHARP_OUTDIR}" STREQUAL "")
	MESSAGE(FATAL_ERROR " UNITY_CSHARP_OUTDIR is an empty string! Risk of fatal removals of important files due to other scripts.")
endif()

# recurse into subdirs
add_subdirectory(src)
add_subdirectory(swig)

## create custom targets
# in this case we will compose the directories qengine_wrapper and unity_qengine_wrapper
# qengine_wrapper will contain .cs files, shared library and a testing executable "SwigTester.exe". This executable can be ran in order to check if the bindings work correctly.
# unity_qengine_wrapper will contain .cs files and a shared library, which can be copied to a UNITY project and used within it.

set(WRAPPER_TEST_SYMBOL WRAPPER_TESTS)

if(MSVC)
	## target to gather .cs and .so files and create an executable 

	# define commands
	set(CLEAN_CSHARP_DIR del ${CSHARP_OUTDIR}\\*.cs ${CSHARP_OUTDIR}\\*.so)
	string(REPLACE "/" "\\" CLEAN_CSHARP_DIR "${CLEAN_CSHARP_DIR}")

	set(CSHARP_MAIN_SRC_COPY_CMD COPY ${PROJECT_SOURCE_DIR}\\src\\*.cs  ${CSHARP_OUTDIR})
	string(REPLACE "/" "\\" CSHARP_MAIN_SRC_COPY_CMD "${CSHARP_MAIN_SRC_COPY_CMD}")

	set(CSHARP_SWIG_SRC_COPY_CMD COPY  ${CMAKE_SWIG_OUTDIR}\\*.cs  ${CSHARP_OUTDIR})
	string(REPLACE "/" "\\" CSHARP_SWIG_SRC_COPY_CMD "${CSHARP_SWIG_SRC_COPY_CMD}")

	set(CSHARP_LIB_COPY_CMD COPY ${PROJECT_SOURCE_DIR}\\bin\\swig\\csharp\\Release\\libcsqenginewrapper.dll ${CSHARP_OUTDIR})
	string(REPLACE "/" "\\" CSHARP_LIB_COPY_CMD "${CSHARP_LIB_COPY_CMD}")

	set(CSHARP_COMPILE_WRAPPER ${CSHARP_COMPILER} -define:${WRAPPER_TEST_SYMBOL} /out:${CSHARP_OUTDIR}\\SwigTester.exe ${CSHARP_OUTDIR}\\*.cs)

	# compose the target for the qengine_wrapper directory
	add_custom_target(csharp_exec ${CLEAN_CSHARP_DIR} COMMAND ${CSHARP_MAIN_SRC_COPY_CMD} COMMAND ${CSHARP_SWIG_SRC_COPY_CMD} COMMAND ${CSHARP_LIB_COPY_CMD} COMMAND ${CSHARP_COMPILE_WRAPPER})

	# target to run the actual SWIG wrapper tests
	add_custom_target(tests COMMAND ${CSHARP_OUTDIR}\\SwigTester.exe DEPENDS csharp_exec)

	## target to gather .cs and .so files for the UNITY wrapper directory 
	set(CLEAN_UNITY_CSHARP_DIR del ${UNITY_CSHARP_OUTDIR}\\*.cs ${UNITY_CSHARP_OUTDIR}\\*.so)
	string(REPLACE "/" "\\" CLEAN_UNITY_CSHARP_DIR "${CLEAN_UNITY_CSHARP_DIR}")

	set(UNITY_CSHARP_MAIN_SRC_COPY_CMD COPY ${PROJECT_SOURCE_DIR}\\src\\*.cs  ${UNITY_CSHARP_OUTDIR})
	string(REPLACE "/" "\\" UNITY_CSHARP_MAIN_SRC_COPY_CMD "${UNITY_CSHARP_MAIN_SRC_COPY_CMD}")

	set(UNITY_CSHARP_CLEAN_SRC_COPY_CMD del ${UNITY_CSHARP_OUTDIR}\\WrapperTests.cs)
	string(REPLACE "/" "\\" UNITY_CSHARP_CLEAN_SRC_COPY_CMD "${UNITY_CSHARP_CLEAN_SRC_COPY_CMD}")

	set(UNITY_CSHARP_SWIG_SRC_COPY_CMD COPY  ${CMAKE_SWIG_OUTDIR}\\*.cs  ${UNITY_CSHARP_OUTDIR})
	string(REPLACE "/" "\\" UNITY_CSHARP_SWIG_SRC_COPY_CMD "${UNITY_CSHARP_SWIG_SRC_COPY_CMD}")

	set(UNITY_CSHARP_LIB_COPY_CMD COPY ${PROJECT_SOURCE_DIR}\\bin\\swig\\csharp\\Release\\libcsqenginewrapper.dll ${UNITY_CSHARP_OUTDIR})
	string(REPLACE "/" "\\" UNITY_CSHARP_LIB_COPY_CMD "${UNITY_CSHARP_LIB_COPY_CMD}")

	# compose the target for the unity qengine_wrapper directory
	add_custom_target(unity_scripts ${CLEAN_UNITY_CSHARP_DIR} 
						COMMAND ${UNITY_CSHARP_MAIN_SRC_COPY_CMD} 
						COMMAND ${UNITY_CSHARP_SWIG_SRC_COPY_CMD} 
						COMMAND ${UNITY_CSHARP_LIB_COPY_CMD}
						COMMAND ${UNITY_CSHARP_CLEAN_SRC_COPY_CMD})

elseif(CMAKE_COMPILER_IS_GNUCXX )
	## target to gather .cs and .so files and create an executable 
	
	# define nunit variables/paths
	 # set(NUNIT_FRAMEWORK_LIB ${CSHARP_OUTDIR}/NUnit.3.10.1/lib/net45/nunit.framework.dll)
	# set(NUNIT_CONSOLE_RUNNER ${CSHARP_OUTDIR}/NUnit.ConsoleRunner.3.9.0/tools/nunit3-console.exe)

	 set(NUNIT_FRAMEWORK_LIB ${CSHARP_OUTDIR}/NUnit.2.6.1/lib/nunit.framework.dll)
	 set(NUNIT_CONSOLE_RUNNER ${CSHARP_OUTDIR}/NUnit.Runners.2.6.4/tools/nunit-console.exe)

	# define commands
	set(CLEAN_CSHARP_DIR rm -f ${CSHARP_OUTDIR}/*.cs ${CSHARP_OUTDIR}/libcsqenginewrapper.so ${CSHARP_OUTDIR}/*.dll)
	set(CSHARP_MAIN_SRC_COPY_CMD cp ${PROJECT_SOURCE_DIR}/src/*.cs ${CSHARP_OUTDIR})
	set(CSHARP_SWIG_SRC_COPY_CMD cp ${CMAKE_SWIG_OUTDIR}/*.cs ${CSHARP_OUTDIR})
	set(CSHARP_LIB_COPY_CMD cp ${PROJECT_SOURCE_DIR}/bin/libcsqenginewrapper.so ${CSHARP_OUTDIR})
	set(CSHARP_TEST_LIB_COPY cp ${NUNIT_FRAMEWORK_LIB} ${CSHARP_OUTDIR})
	set(CSHARP_COMPILE_WRAPPER mcs ${CSHARP_OUTDIR}/*.cs -define:NUNIT_TESTS -define:${WRAPPER_TEST_SYMBOL} -target:library -r:${NUNIT_FRAMEWORK_LIB}  -out:${CSHARP_OUTDIR}/SwigTester.dll)

	# compose the target
	add_custom_target(csharp_exec ${CLEAN_CSHARP_DIR} COMMAND ${CSHARP_MAIN_SRC_COPY_CMD} COMMAND ${CSHARP_SWIG_SRC_COPY_CMD} COMMAND ${CSHARP_LIB_COPY_CMD} COMMAND ${CSHARP_TEST_LIB_COPY} COMMAND ${CSHARP_COMPILE_WRAPPER})

	# target to run the actual SWIG wrapper tests
	add_custom_target(tests COMMAND mono ${NUNIT_CONSOLE_RUNNER} ${CSHARP_OUTDIR}/SwigTester.dll DEPENDS csharp_exec)

	## target to gather .cs and .so files for the UNITY wrapper directory 
	# define commands
	set(UNITY_CLEAN_CSHARP_DIR rm -rf ${UNITY_CSHARP_OUTDIR}/*)
	set(UNITY_CSHARP_MAIN_SRC_COPY_CMD cp ${PROJECT_SOURCE_DIR}/src/*.cs ${UNITY_CSHARP_OUTDIR})
	set(UNITY_CSHARP_CLEAN_SRC_COPY_CMD rm -f ${UNITY_CSHARP_OUTDIR}/WrapperTests.cs)
	set(UNITY_CSHARP_SWIG_SRC_COPY_CMD cp ${CMAKE_SWIG_OUTDIR}/*.cs ${UNITY_CSHARP_OUTDIR})
	set(UNITY_CSHARP_LIB_COPY_CMD cp ${PROJECT_SOURCE_DIR}/bin/libcsqenginewrapper.so ${UNITY_CSHARP_OUTDIR})

	# compose the target
	add_custom_target(unity_scripts ${UNITY_CLEAN_CSHARP_DIR} 
									COMMAND ${UNITY_CSHARP_MAIN_SRC_COPY_CMD} 
									COMMAND ${UNITY_CSHARP_SWIG_SRC_COPY_CMD} 
									COMMAND ${UNITY_CSHARP_LIB_COPY_CMD}
									COMMAND ${UNITY_CSHARP_CLEAN_SRC_COPY_CMD})
else()
    ## target to gather .cs and .so files and create an executable
    # define commands

    # define nunit variables/paths
    set(NUNIT_FRAMEWORK_LIB  /Users/au446513/.nuget/packages/nunit/3.10.1/lib/net45/nunit.framework.dll)
    set(NUNIT_CONSOLE_RUNNER /Users/au446513/.nuget/packages/nunit.consolerunner/3.8.0/tools/nunit3-console.exe)


    set(CLEAN_CSHARP_DIR rm -f ${CSHARP_OUTDIR}/*)
    set(CSHARP_MAIN_SRC_COPY_CMD cp ${PROJECT_SOURCE_DIR}/src/*.cs ${CSHARP_OUTDIR})
    set(CSHARP_SWIG_SRC_COPY_CMD cp ${CMAKE_SWIG_OUTDIR}/*.cs ${CSHARP_OUTDIR})
    set(CSHARP_LIB_COPY_CMD cp ${PROJECT_SOURCE_DIR}/bin/libcsqenginewrapper.dylib ${CSHARP_OUTDIR})
    set(CSHARP_TEST_LIB_COPY cp ${NUNIT_FRAMEWORK_LIB} ${CSHARP_OUTDIR})


    #set(CSHARP_COMPILE_WRAPPER ${MONO_BIN_DIR}/mcs -define:${WRAPPER_TEST_SYMBOL} -out:${CSHARP_OUTDIR}/SwigTester.exe ${CSHARP_OUTDIR}/*.cs)
    set(CSHARP_COMPILE_WRAPPER ${MONO_BIN_DIR}/mcs ${CSHARP_OUTDIR}/*.cs -define:NUNIT_TESTS -define:${WRAPPER_TEST_SYMBOL} -target:library -r:${NUNIT_FRAMEWORK_LIB}  -out:${CSHARP_OUTDIR}/SwigTester.dll)

    # compose the target
    add_custom_target(csharp_exec ${CLEAN_CSHARP_DIR} COMMAND ${CSHARP_MAIN_SRC_COPY_CMD} COMMAND ${CSHARP_SWIG_SRC_COPY_CMD} COMMAND ${CSHARP_LIB_COPY_CMD} COMMAND ${CSHARP_TEST_LIB_COPY} COMMAND ${CSHARP_COMPILE_WRAPPER})

    # target to run the actual SWIG wrapper tests
    #add_custom_target(tests COMMAND mono ${CSHARP_OUTDIR}/SwigTester.exe DEPENDS csharp_exec)
    add_custom_target(tests COMMAND mono ${NUNIT_CONSOLE_RUNNER} ${CSHARP_OUTDIR}/SwigTester.dll DEPENDS csharp_exec)



    ## target to gather .cs and .so files for the UNITY wrapper directory
    # define commands
    set(UNITY_CLEAN_CSHARP_DIR rm -f ${UNITY_CSHARP_OUTDIR}/*)
    set(UNITY_CSHARP_MAIN_SRC_COPY_CMD cp ${PROJECT_SOURCE_DIR}/src/*.cs ${UNITY_CSHARP_OUTDIR})
    set(UNITY_CSHARP_CLEAN_SRC_COPY_CMD rm -f ${UNITY_CSHARP_OUTDIR}/WrapperTests.cs)
    set(UNITY_CSHARP_SWIG_SRC_COPY_CMD cp ${CMAKE_SWIG_OUTDIR}/*.cs ${UNITY_CSHARP_OUTDIR})
    set(UNITY_CSHARP_LIB_COPY_CMD cp ${PROJECT_SOURCE_DIR}/bin/libcsqenginewrapper.dylib ${UNITY_CSHARP_OUTDIR}/libcsqenginewrapper.bundle)
    set(UNITY_DEPENDENCY_COPY_CMD

        cp ${QENGINE_LIB_DIR}/libqengine.dylib      ${UNITY_CSHARP_OUTDIR}/libqengine.dylib &&

        cp ${MKL_LIB_DIR}/libmkl_avx2.dylib         ${UNITY_CSHARP_OUTDIR}/libmkl_avx2.dylib &&
        cp ${MKL_LIB_DIR}/libmkl_intel.dylib        ${UNITY_CSHARP_OUTDIR}/libmkl_intel.dylib &&
        cp ${MKL_LIB_DIR}/libmkl_intel_lp64.dylib   ${UNITY_CSHARP_OUTDIR}/libmkl_intel_lp64.dylib &&
        cp ${MKL_LIB_DIR}/libmkl_sequential.dylib   ${UNITY_CSHARP_OUTDIR}/libmkl_sequential.dylib &&
        cp ${MKL_LIB_DIR}/libmkl_core.dylib         ${UNITY_CSHARP_OUTDIR}/libmkl_core.dylib
        )


    set(UNITY_LIBRARY_RPATH_CMD
        install_name_tool -add_rpath @loader_path/ ${UNITY_CSHARP_OUTDIR}/libcsqenginewrapper.bundle &&
        install_name_tool -delete_rpath  ${QENGINE_LIB_DIR} ${UNITY_CSHARP_OUTDIR}/libcsqenginewrapper.bundle &&
        install_name_tool -delete_rpath  ${MKL_LIB_DIR}     ${UNITY_CSHARP_OUTDIR}/libcsqenginewrapper.bundle &&

        install_name_tool -add_rpath @loader_path/ ${UNITY_CSHARP_OUTDIR}/libqengine.dylib &&
        install_name_tool -delete_rpath  ${MKL_LIB_DIR}     ${UNITY_CSHARP_OUTDIR}/libqengine.dylib
        )

    # compose the target
    add_custom_target(unity_scripts ${UNITY_CLEAN_CSHARP_DIR}
            COMMAND ${UNITY_CSHARP_MAIN_SRC_COPY_CMD}
            COMMAND ${UNITY_CSHARP_SWIG_SRC_COPY_CMD}
            COMMAND ${UNITY_CSHARP_LIB_COPY_CMD}
            COMMAND ${UNITY_DEPENDENCY_COPY_CMD}
            COMMAND ${UNITY_LIBRARY_RPATH_CMD}
            COMMAND ${UNITY_CSHARP_CLEAN_SRC_COPY_CMD}
            )

endif()
