function Fs_scaled = FidelityScaling(Fs)

nElements = length(Fs);
Fs_scaled = zeros(nElements,1);


for i=1:nElements
    F = Fs(i);
    
    Flog_t = 0.999;
    Flin_t = 0.3;

    F1 = linspace(0,Flin_t,1000);
    F2 = linspace(Flin_t,Flog_t,1000);

    N = (Flog_t - Flin_t)/(log(1-Flin_t) - log(1-Flog_t));
    M = Flin_t + N*log(1-Flin_t);

    Fvis1 = @(F) F;
    Fvis2 = @(F) -N*log(1-F) + M;

    if F < Flin_t 
        Fs_scaled(i) = Fvis1(F);
    else
        Fs_scaled(i) = Fvis2(F);
    end
end

    
  
end

