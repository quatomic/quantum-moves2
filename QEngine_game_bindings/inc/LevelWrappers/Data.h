#pragma once
#include <qengine/qengine.h>
#include "LevelWrappers/LevelWrapperTypes.h"

namespace qengine
{
    namespace levelwrapper
    {

        // Helper function to tie the optimizer vars created by the InitializeOptimizer to the Data member vars
        template<class DataStruct,class VarsTuple>
        void tieOptVars(DataStruct& d, VarsTuple optVars)
        {
            d->mpOptimizerStopSwitch = std::get<0>(optVars);
            d->mpControlObserver     = std::get<3>(optVars);
            d->mpOptimizedControlHistory = std::get<6>(optVars);
            d->mpOptimizerIsRunning = std::get<1>(optVars);
            d->mpOptimizerMutex     = std::get<2>(optVars);
            d->mpOptimizerIteration = std::get<4>(optVars);
            d->mpOptimizedFidelity  = std::get<5>(optVars);
            d->mpOptimizerStopMessage = std::get<7>(optVars);
            d->mpOptimizerIterationFidelityHistory = std::get<8>(optVars);
            d->mpOptimizerFinalWaveFunctionAbsSquare = std::get<9>(optVars);
            d->mpOptimizerStopCondition = std::get<10> (optVars);
            d->mpBFGSRestarts = std::get<11> (optVars);
        }


        template <class TupleType>
        struct Data
        {
            using MutexType = utility::MutexType;
            Data(TupleType&  tup): mDataTuple(tup) {}
            Data(TupleType&& tup): mDataTuple(tup) {}

            TupleType mDataTuple;

            // Const access
            const auto & mX()                        { return std::get<0>(mDataTuple);}
            const auto & mpInitialWaveFunction()     { return std::get<1>(mDataTuple);}
            const auto & mpTargetWaveFunction()      { return std::get<2>(mDataTuple);}
            const auto & mpPropagator()              { return std::get<3>(mDataTuple);}
            const auto & mpHamiltonian()             { return std::get<4>(mDataTuple);}
            const auto & mXLimits()                  { return std::get<5>(mDataTuple);}
            const auto & mYLimits()                  { return std::get<6>(mDataTuple);}
            const auto & mInitialControl()           { return std::get<7>(mDataTuple);}
            const auto & mFinalControl()             { return std::get<8>(mDataTuple);}
 
            const auto & mpOptimizerMaker()          { return std::get<10>(mDataTuple);}
            const auto & mTQSLApprox()               { return std::get<11>(mDataTuple);}
            const auto & mDt()                       { return std::get<12>(mDataTuple);}
            const auto & mDim()                      { return std::get<13>(mDataTuple);}

            const auto & mControlScaling()           { return std::get<14>(mDataTuple);}
            const auto & mPotentialScaling()         { return std::get<15>(mDataTuple);}
            const auto & mWaveFunctionScaling()      { return std::get<16>(mDataTuple);}

            const auto mGetOptimizationBoundaries()  { return std::get<17> (mDataTuple); }
            const ControlAxis mControlAxis()         { return std::get<18> (mDataTuple); }


            // Mutable access
            auto& mpOptimizer()                      { return std::get<9>(mDataTuple);}

            RVec mCurrentControl;
            qengine::Control mControlHistory;


            std::shared_ptr<qengine::Control> mpOptimizedControlHistory;
            std::shared_ptr<qengine::real> mpOptimizedFidelity;

            std::shared_ptr<TSBool> mpOptimizerStopSwitch;
            std::shared_ptr<TSBool> mpOptimizerIsRunning;
            std::shared_ptr<MutexType> mpOptimizerMutex;
            std::shared_ptr<utility::observer::DataObserver> mpControlObserver;
            std::shared_ptr<TSInt> mpOptimizerIteration;
            std::shared_ptr<std::string> mpOptimizerStopMessage;
            std::shared_ptr<std::vector<RVec>> mpOptimizerIterationFidelityHistory;
            std::shared_ptr<RVec> mpOptimizerFinalWaveFunctionAbsSquare;
            std::shared_ptr< OptimizerStopCondition > mpOptimizerStopCondition;
            std::shared_ptr<TSInt> mpBFGSRestarts;

            void ResetOptimizerState()
            {
                if (mpOptimizerIteration)
                    *mpOptimizerIteration = 0;

                if (mpOptimizedFidelity)
                    *mpOptimizedFidelity  = 0.0;

                if (mpOptimizedControlHistory)
                    *mpOptimizedControlHistory = qengine::Control::zeros(10,2,0.1);

                if (mpOptimizerStopMessage)
                    *mpOptimizerStopMessage = std::string("");

                if (mpOptimizerStopCondition)
                    *mpOptimizerStopCondition = OptimizerStopCondition::NONE;
                
                if (mpOptimizerIterationFidelityHistory)
                    *mpOptimizerIterationFidelityHistory = {};

                if (mpBFGSRestarts)
                    *mpBFGSRestarts = 0;
            }
        };


        template struct Data<gpe::GPETupleTypes>;
    }
}
