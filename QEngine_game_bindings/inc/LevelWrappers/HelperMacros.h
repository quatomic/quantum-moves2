#pragma once

#define BOILERPLATE_INIT  \
auto solver = makeFixedTimeStepper(H,psi_0,dt);\
const auto optimizerMaker = InitializeOptimizer(H,psi_0,psi_t);\
auto fakeGRAPETuple = optimizerMaker(makeControl({makeTimeControl(10,1.0),makeTimeControl(10,1.0)}),10, OptimizationBoundaries{{}, {}, 0.0});\
auto fakeGRAPEPtr = std::get<0>(fakeGRAPETuple);\
auto fakeGRAPE = *fakeGRAPEPtr;\
return std::make_tuple( xVecRescaled,\
                        std::make_shared<decltype(psi_0)>((psi_0)),\
                        std::make_shared<decltype(psi_t)>((psi_t)),\
                        std::make_shared<decltype(solver)>(solver),\
                        std::make_shared<decltype(H)>((H)),\
                        xLimits,\
                        yLimits,\
                        initialControl,\
                        finalControl,\
                        std::make_shared<decltype(fakeGRAPE)>(fakeGRAPE),\
                        std::make_shared<decltype(optimizerMaker)>((optimizerMaker)),\
                        TQSLApprox,\
                        dt,\
                        dim,\
                        controlScaling, \
                        potentialScaling,\
                        wavefunctionScaling,\
                        optimizerBounds, \
                        controlAxis);
