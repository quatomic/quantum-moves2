#pragma once
#include "Utilities/Utilities.h"

namespace qengine
{
namespace levelwrapper
{
namespace utility
{
	using LevelParameter = mpark::variant<double, int, std::string>;
	using ParameterId = std::string;
	using LevelConfig = std::unordered_map<ParameterId, LevelParameter>;

	LevelConfig ConstructShakeUpConfig(const double );

	LevelConfig ConstructSplittingConfig(const double fractionLeftWell, const double displacement);

	LevelConfig ConstructBHWConfig(const double , const int targetState);

}
}
}