#pragma once
#include "ObjectWrappers/RVecWrapper.h"
#include "ObjectWrappers/ControlWrapper.h"
#include "LevelWrappers/LevelWrapperTypes.h"
#include "LevelWrappers/Data.h"
#include "LevelWrappers/LevelConfigs.h"

#include <armadillo>
namespace qengine
{
namespace levelwrapper
{

template<class Type>
class LevelWrapperSpatial
{

public:
    using JsonStruct = std::string;
    using Control = qengine::Control;
    using MutexType = utility::MutexType;
    using ScalingType = utility::ScalingType;

    LevelWrapperSpatial();

    void MarkErrorHandled();
    void SetErrorBuffer(const std::string & error);
    std::string GetErrorBuffer();
    int GetErrorState();
    void StartRecordingSteps();
    void StopRecordingSteps();

    void DoStep(const RVecWrapper & ctrl);
    void DoResetLevel();
    void DoOptimize(const int iterations = 10);
    void DoOptimizeStopRequest();

    bool GetOptimizerIsRunning();

    bool DidOptimizerRestart();
    int GetBFGSRestarts();

    int GetOptimizerStopCondition();
    int GetControlAxis();

    RVecWrapper GetXLimits();
    RVecWrapper GetYLimits();

    RVecWrapper GetLeftLowerOptimizationBound();
    RVecWrapper GetRightUpperOptimizationBound();

    RVecWrapper GetLeftLowerOptimizationBoundPhysical();
    RVecWrapper GetRightUpperOptimizationBoundPhysical();


    RVecWrapper GetInitialControl();
    RVecWrapper GetCurrentControl();
    RVecWrapper GetFinalControl();

    RVecWrapper GetPotential();

    double GetFidelity();
    double GetOptimizedFidelity();
    double GetDt();
    double GetT();
    double GetNormalizedT();
    int GetOptimizerIteration();
    int GetFPP();


    std::string GetOptimizerStopMessage();

    size_t GetDim();

    RVecWrapper GetCurrentWavefunctionRE(const double scaling = 1.0);
    RVecWrapper GetCurrentWavefunctionIM(const double scaling = 1.0);
    RVecWrapper GetCurrentWavefunctionNORM(const double scaling = 1.0);
    RVecWrapper GetCurrentWavefunctionPhase(const double scaling = 1.0);

    RVecWrapper GetInitialWavefunctionRE(const double scaling = 1.0);
    RVecWrapper GetInitialWavefunctionIM(const double scaling = 1.0);
    RVecWrapper GetInitialWavefunctionNORM(const double scaling = 1.0);
    RVecWrapper GetInitialWavefunctionPhase(const double scaling = 1.0);

    RVecWrapper GetTargetWavefunctionRE(const double scaling = 1.0);
    RVecWrapper GetTargetWavefunctionIM(const double scaling = 1.0);
    RVecWrapper GetTargetWavefunctionNORM(const double scaling = 1.0);
    RVecWrapper GetTargetWavefunctionPhase(const double scaling = 1.0);

    RVecWrapper GetX();


    ControlWrapper GetControlHistory();
    ControlWrapper GetControlHistoryPhysical();
    ControlWrapper GetOptimizedControlHistory();
    ControlWrapper GetOptimizedControlHistoryPhysical();
    std::vector<RVecWrapper> GetOptimizerIterationFidelityHistory();


    RVecWrapper GetOptimizedFinalWavefunction(const double scaling = 1.0);

    void SetControlHistory(const ControlWrapper & controlHistory);

    bool IsNewControlDataAvailable();

    virtual ~LevelWrapperSpatial()
    {
        DoOptimizeStopRequest();
        while ( GetOptimizerIsRunning() )
        {
            // ... block until optimization is dead
        }
    };


    private:
        void DoLoadLevel_Common();
    public:
        void DoLoadLevel();


        void DoLoadLevel1();    // Transport
        void DoLoadLevel2();    // Shake-X
        void DoLoadLevel3();    // Deep well transport
        void DoLoadLevel4();    // Shallow well transport
        void DoLoadLevel5();    // Shake-Y
        void DoLoadLevel6();    // Tunneling

        void DoLoadLevel7();    // Transport w. optimize

        void DoLoadLevel_BringHomeWater_0_0();
        void DoLoadLevel_BringHomeWater_1_0();
        void DoLoadLevel_BringHomeWater_0_1();
        void DoLoadLevel_BringHomeWater_1_1();

        void DoLoadLevel_ShakeUp_0();
        void DoLoadLevel_ShakeUp_1();
        void DoLoadLevel_ShakeUp_2();
        void DoLoadLevel_ShakeUp_3();

        void DoLoadLevel_Splitting_50();
        void DoLoadLevel_Splitting_50_d();
        void DoLoadLevel_Splitting_25_d();
        void DoLoadLevel_Splitting_10_d();
        void DoLoadLevel_Splitting_0_d();



#ifdef TEST_BUILD // The variable TEST_BUILD is set in the test target, so we may easily access variables without going through the interface
public:
#else
private:
#endif
        void DoLoadLevel_BringHomeWater(const utility::LevelConfig &);
        void DoLoadLevel_ShakeUp(const utility::LevelConfig &);
        void DoLoadLevel_Splitting(const utility::LevelConfig &);


    enum class ControlState {INIT, RECORDING, COMPLETE};
    enum class ErrorCode  { CONTROL_INCOMPLETE = -10,
                            CONTROL_STILL_RECORDING = -11,
                            CONTROL_RECORDING_NOT_STARTED = -12,
                            CONTROL_TOO_SHORT = -13,
                            QENGINE_EXCEPTION = -14,
                            NONE = 0 };

    void SetControlState(const ControlState mState );
    void SetErrorState(const ErrorCode err);
    void SetError(const ErrorCode err, const std::string & message);

    RVec TransformWavefunction(const RVec& wavefunc, const RVec& potential, const double scaling = 1.0);
    void ResetControlHistory();

    void PropagateAlongControl(const Control & ctrl)
    {
            d->mpPropagator()->reset(*d->mpInitialWaveFunction(), d->mInitialControl());
            for (size_t index = 0 ; index < ctrl.size() - 1; ++index)
            {
                d->mpPropagator()->step( ctrl.get( index + 1) );
            }
    }

    RVec GetRescaledPotential(const RVec & ctrl);

    ErrorCode mError = ErrorCode::NONE;
    std::string mErrorBuffer;

    ControlState mControlState = ControlState::INIT;

    std::shared_ptr<Data<Type>> d;

};
}
}

#include <thread>

/// In this file we instantiate & define member functions of the LevelWrapperSpatial template class

/// COMMON TEMPLATE FUNCTIONS
namespace qengine
{
namespace levelwrapper
{

    template <typename Type>
    bool LevelWrapperSpatial<Type>::DidOptimizerRestart()
    {
        if (d)
        {
            return d->mpOptimizer()->didRestart();
        }
        else
        {
            return false; // no optimizer => no restart
        }
    }

    template <typename Type>
    int LevelWrapperSpatial<Type>::GetControlAxis()
    {
        return static_cast<int> ((d->mControlAxis()));
    }

    template <typename Type>
    int LevelWrapperSpatial<Type>::GetBFGSRestarts()
    {
        return static_cast<int> (*(d->mpBFGSRestarts));
    }    

    template <typename Type>
    int LevelWrapperSpatial<Type>::GetFPP()
    {
        std::lock_guard<MutexType> lock(*d->mpOptimizerMutex);
        const size_t n_steps = d->mControlHistory.size();
        return static_cast<int> (round(d->mpOptimizer()->problem().nPropagationSteps() / n_steps ));
    }

    template <typename Type>
    int LevelWrapperSpatial<Type>::GetOptimizerStopCondition()
    {
        std::lock_guard<MutexType> lock(*d->mpOptimizerMutex);
        return static_cast<int> (*(d->mpOptimizerStopCondition));
    }

    template<typename Type>
    int LevelWrapperSpatial<Type>::GetErrorState()
    {
        std::lock_guard<MutexType> lock(*d->mpOptimizerMutex);
        return static_cast<int> (mError);
    }

    template <typename Type>
    std::string LevelWrapperSpatial<Type>::GetErrorBuffer()
    {
        std::lock_guard<MutexType> lock(*d->mpOptimizerMutex);
        return mErrorBuffer;
    }

    template <typename Type>
    void LevelWrapperSpatial<Type>::SetErrorBuffer(const std::string & error)
    {
        mErrorBuffer = error;
    }

    template <typename Type>
    void LevelWrapperSpatial<Type>::SetErrorState(const ErrorCode err)
    {
        mError = err;
    }

    template <typename Type>
    void LevelWrapperSpatial<Type>::SetError(const ErrorCode err, const std::string & message)
    {
        std::lock_guard<MutexType> lock(*d->mpOptimizerMutex);
        SetErrorState(err);
        SetErrorBuffer(message);
    }

    template <typename Type>
    void LevelWrapperSpatial<Type>::MarkErrorHandled()
    {
        std::lock_guard<MutexType> lock(*d->mpOptimizerMutex);
        switch(mError)
        {
            case ErrorCode::NONE:
            {
                break;
            }
            default:
            {;
                SetError(ErrorCode::NONE, "");
                break;
            }
        }

    }

    template<typename Type>
    void LevelWrapperSpatial<Type>::StartRecordingSteps()
    {
        switch (mControlState)
        {
            case ControlState::COMPLETE: // intentional fall-through
            case ControlState::INIT:
            {
                SetControlState(ControlState::RECORDING);
                break;
            }
            case ControlState::RECORDING:
            {
                SetError(ErrorCode::CONTROL_STILL_RECORDING, "");
                break;
            }
        }
    }

    template<typename Type>
    void LevelWrapperSpatial<Type>::StopRecordingSteps()
    {
        switch(mControlState)
        {
            case ControlState::RECORDING:
            {
                // reset the internal state of the optimizer
                d->ResetOptimizerState();
                // append the final control to the recorded controls
                d->mCurrentControl = d->mFinalControl();
                d->mpPropagator()->step( d->mCurrentControl );
                d->mControlHistory.append(Control(d->mCurrentControl,2,d->mDt()));
                SetControlState(ControlState::COMPLETE);
                break;
            }
            case ControlState::INIT:
            {
                SetError(ErrorCode::CONTROL_RECORDING_NOT_STARTED, "");
                break;
            }
            case ControlState::COMPLETE:
            {
                break;
            }
        }
    }

    template<typename Type>
    void LevelWrapperSpatial<Type>::SetControlState(const ControlState state)
    {
        mControlState = state;
    }

    template<class Type>
    LevelWrapperSpatial<Type>::LevelWrapperSpatial()
    {
        try
        {
            DoLoadLevel();
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetXLimits()
    {
        try
        {
            return d->mXLimits();
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetYLimits()
    {
        try
        {
            return d->mYLimits();
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetLeftLowerOptimizationBound()
    {
        try
        {
            return d->mGetOptimizationBoundaries().mLeftBoundary;
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }
    
    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetRightUpperOptimizationBound()
    {
        try
        {
            return d->mGetOptimizationBoundaries().mRightBoundary;
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetLeftLowerOptimizationBoundPhysical()
    {
        try
        {
            return d->mControlScaling().Scale( d->mGetOptimizationBoundaries().mLeftBoundary, ScalingType::INVERSE );
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }
    
    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetRightUpperOptimizationBoundPhysical()
    {
        try
        {
            return d->mControlScaling().Scale( d->mGetOptimizationBoundaries().mRightBoundary, ScalingType::INVERSE );

        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetInitialControl()
    {
        try
        {

            return d->mControlScaling().Scale( d->mInitialControl(), ScalingType::IDENTITY );
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }

    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetCurrentControl()
    {
        try
        {
            return d->mControlScaling().Scale( d->mCurrentControl, ScalingType::IDENTITY );
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetFinalControl()
    {
        try
        {
            return d->mControlScaling().Scale( d->mFinalControl(), ScalingType::IDENTITY );
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }

    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetPotential()
    {
        try
        {
            return GetRescaledPotential(d->mCurrentControl);
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVec LevelWrapperSpatial<Type>::
    GetRescaledPotential(const RVec & ctrl)
    {
        try
        {
            return d->mPotentialScaling().Scale(d->mpHamiltonian()->operator()( ctrl )._potential().vec(), utility::ScalingType::FORWARD);
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    double LevelWrapperSpatial<Type>::
    GetDt()
    {
        return d->mDt();
    }

    template <class Type>
    double LevelWrapperSpatial<Type>::
    GetT()
    {
        try
        {
            if (d->mControlHistory.size() == 0)
            {
                throw std::runtime_error("Invalid control history length (=0). Unable to compute process duration.");
            }
            // we don't count the first point in the controls as a 'step'
            return ((int) d->mControlHistory.size() - 1) * d->mDt();
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return 0.0;
        }        
    }

    template <class Type>
    double LevelWrapperSpatial<Type>::
    GetNormalizedT()
    {
        try
        {
            return GetT()/d->mTQSLApprox();
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return 0.0;
        }
    }

    template <class Type>
    size_t LevelWrapperSpatial<Type>::
    GetDim()
    {
        return d->mDim();
    }

    template <class Type>
    double LevelWrapperSpatial<Type>::
    GetFidelity()
    {
        try
        {
            return qengine::fidelity(d->mpPropagator()->state(), *d->mpTargetWaveFunction());
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return 0.0;
        }
    }

    template <class Type>
    double LevelWrapperSpatial<Type>::
    GetOptimizedFidelity()
    {
        if(d && *d->mpOptimizerIteration > 0)
        {
            return (*d->mpOptimizedFidelity);
        }
        else
        {
            return GetFidelity();
        }
    }

    template <class Type>
    void LevelWrapperSpatial<Type>::
    DoStep(const RVecWrapper & ctrl)
    {
        try
        {
            switch(mControlState)
            {
                case ControlState::COMPLETE: // intentional fall-through
                case ControlState::INIT:
                {
                    SetError(ErrorCode::CONTROL_RECORDING_NOT_STARTED, "");
                    return;
                }

                case ControlState::RECORDING:
                {
                    break;
                }
            }
            /// Peform a step, update controls and perform a propagation step
            d->mCurrentControl = d->mControlScaling().Scale( ctrl.GetVec(), ScalingType::IDENTITY );
            d->mpPropagator()->step( d->mCurrentControl );
            d->mControlHistory.append(Control(d->mCurrentControl,2,d->mDt()));
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
        }
    }

    template <class Type>
    void LevelWrapperSpatial<Type>::
    DoOptimize(const int iterations)
    {
        try
        {
            switch (mControlState)
            {
                case ControlState::INIT: // intentional fall-through
                {
                    SetError(ErrorCode::CONTROL_INCOMPLETE, "");
                    return;
                }
                case ControlState::RECORDING:
                {
                    SetError(ErrorCode::CONTROL_STILL_RECORDING, "");
                    return;
                }
                case ControlState::COMPLETE:
                {
                    break;
                }
            }

            auto n_steps = d->mControlHistory.size();
            if(n_steps < 3)
            {
                SetError(ErrorCode::CONTROL_TOO_SHORT, "");
                return;
            }

            if(d && d->mpOptimizerMaker() && d->mControlHistory.size() > 2)
            {
                if((*d->mpOptimizerIsRunning) == false)
                {
                    if(d->mpOptimizerIteration && *d->mpOptimizerIteration > 0)
                    {
                        d->mControlHistory = *d->mpOptimizedControlHistory;
                    }
                    auto tup = d->mpOptimizerMaker()->operator()(d->mControlHistory,iterations, d->mGetOptimizationBoundaries());

                    d->mpOptimizer() = std::get<0>(tup);
                    auto optVars = std::get<1>(tup);

                    tieOptVars(d,optVars);
                    (*d->mpOptimizerIsRunning) = true;

                    std::thread t(
                    [this]()
                    {
                        try
                        {
                            d->mpOptimizer()->optimize();
                        }
                        catch (const std::exception & qengine_exception)
                        {
                            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
                            (*d->mpOptimizerIsRunning) = false;
                            (*d->mpOptimizerStopCondition) = OptimizerStopCondition::EXCEPTION;
                        }
                    });
                    t.detach();
                 }
            }
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
        }
    }

    template <class Type>
    void LevelWrapperSpatial<Type>::
    DoOptimizeStopRequest()
    {
        if(d->mpOptimizerStopSwitch) (*d->mpOptimizerStopSwitch) = true;
    }

    template<class Type>
    bool LevelWrapperSpatial<Type>::GetOptimizerIsRunning()
    {
        if(d->mpOptimizerIsRunning) return (*d->mpOptimizerIsRunning);
        else return false;
    };


    template <class Type>
    void LevelWrapperSpatial<Type>::
    DoResetLevel()
    {
        try
        {
            /// Restart and reinit the level
            SetControlState(ControlState::INIT);
            d->mCurrentControl = d->mInitialControl();
            d->mpPropagator()->reset(*d->mpInitialWaveFunction(), d->mInitialControl());

            if(d->mpOptimizerIteration){(*d->mpOptimizerIteration) = 0;}

            ResetControlHistory();
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
        }
    }

    template <class Type>
    void LevelWrapperSpatial<Type>::
    ResetControlHistory()
    {
        try
        {
            SetControlState(ControlState::INIT);
            d->mControlHistory = Control::zeros(1,2,d->mDt());
            d->mControlHistory.setFront(d->mInitialControl());
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
        }
    }


    template <class Type>
    RVec LevelWrapperSpatial<Type>::
    TransformWavefunction(const RVec& wavefunc, const RVec& potential, const double scaling)
    {
        try
        {
            return scaling * (d->mWaveFunctionScaling().Scale( wavefunc, utility::ScalingType::FORWARD )) + potential;
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetCurrentWavefunctionRE(const double scaling)
    {
        try
        {
            return TransformWavefunction(d->mpPropagator()->state().re().vec(), GetRescaledPotential(d->mCurrentControl), scaling);
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }

    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetCurrentWavefunctionIM(const double scaling)
    {
        try
        {
            return TransformWavefunction(d->mpPropagator()->state().im().vec(), GetRescaledPotential(d->mCurrentControl), scaling);
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetCurrentWavefunctionNORM(const double scaling)
    {
        try
        {
            return TransformWavefunction(d->mpPropagator()->state().absSquare().vec(), GetRescaledPotential(d->mCurrentControl), scaling);
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetCurrentWavefunctionPhase(const double scaling)
    {
        try
        {
            auto && vec = arma::Col<double> (arma::arg( d->mpPropagator()->state().vec()._vec()));
            vec.transform([] (auto item) { return item + arma::Datum<double>::pi; }); // shift by pi to land in (0, 2pi) range
            return makeVector( vec ) * scaling;
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetInitialWavefunctionRE(const double scaling)
    {
        try
        {
            return TransformWavefunction(d->mpInitialWaveFunction()->re().vec(), GetRescaledPotential( d->mInitialControl() ), scaling);
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetInitialWavefunctionIM(const double scaling)
    {
        try
        {
            return TransformWavefunction(d->mpInitialWaveFunction()->im().vec(),  GetRescaledPotential( d->mInitialControl() ), scaling);
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetInitialWavefunctionNORM(const double scaling)
    {
        try
        {
            return TransformWavefunction(d->mpInitialWaveFunction()->absSquare().vec(),  GetRescaledPotential( d->mInitialControl() ), scaling);
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetInitialWavefunctionPhase(const double scaling)
    {
        try
        {
            auto && vec = arma::Col<double> (arma::arg( d->mpInitialWaveFunction()->vec()._vec() ));
            vec.transform([] (auto item) { return item + arma::Datum<double>::pi; }); // shift by pi to land in (0, 2pi) range
            return makeVector( vec ) * scaling;
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetTargetWavefunctionRE(const double scaling)
    {
        try
        {
            return TransformWavefunction(d->mpTargetWaveFunction()->re().vec(), GetRescaledPotential( d->mFinalControl() ), scaling);
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetTargetWavefunctionIM(const double scaling)
    {
        try
        {
            return TransformWavefunction(d->mpTargetWaveFunction()->im().vec(), GetRescaledPotential( d->mFinalControl() ), scaling);
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetTargetWavefunctionNORM(const double scaling)
    {
        try
        {
            return TransformWavefunction(d->mpTargetWaveFunction()->absSquare().vec(), GetRescaledPotential( d->mFinalControl() ), scaling);
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetTargetWavefunctionPhase(const double scaling)
    {
        try
        {
            auto && vec = arma::Col<double> (arma::arg( d->mpTargetWaveFunction()->vec()._vec() ));
            vec.transform([] (auto item) { return item + arma::Datum<double>::pi; }); // shift by pi to land in (0, 2pi) range
            return makeVector( vec ) * scaling;
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetX()
    {
        return d->mX();
    }

    template <class Type>
    std::string LevelWrapperSpatial<Type>::
    GetOptimizerStopMessage()
    {
        return *(d->mpOptimizerStopMessage);
    };

    template <class Type>
    int LevelWrapperSpatial<Type>::
    GetOptimizerIteration()
    {
        if(d->mpOptimizerIteration)
        {
            return (*d->mpOptimizerIteration);
        }
        else return 0;
    }

    template <class Type>
    ControlWrapper LevelWrapperSpatial<Type>::
    GetOptimizedControlHistory()
    {
        try
        {
            if(d->mpOptimizedControlHistory && d->mpOptimizerMutex && d->mpControlObserver)
            {
                std::lock_guard<MutexType> lock(*d->mpOptimizerMutex);
                d->mpControlObserver->Set( utility::observer::DataState::UP_TO_DATE );
                return d->mControlScaling().MapScale((*d->mpOptimizedControlHistory), ScalingType::IDENTITY);
            }
            else
            {
                return GetControlHistory();
            }
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    ControlWrapper LevelWrapperSpatial<Type>::
    GetOptimizedControlHistoryPhysical()
    {
        try
        {
            if(d->mpOptimizedControlHistory && d->mpOptimizerMutex && d->mpControlObserver)
            {
                std::lock_guard<MutexType> lock(*d->mpOptimizerMutex);
                d->mpControlObserver->Set( utility::observer::DataState::UP_TO_DATE );
                return d->mControlScaling().MapScale((*d->mpOptimizedControlHistory), ScalingType::INVERSE);
            }
            else
            {
                return GetControlHistoryPhysical();
            }
        }
        catch (const std::runtime_error & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        } 
    }


    template <class Type>
    std::vector<RVecWrapper> LevelWrapperSpatial<Type>::
    GetOptimizerIterationFidelityHistory()
    {
        try
        {
            std::vector<RVecWrapper> history;
            if(*d->mpOptimizerIsRunning && d->mpOptimizerIterationFidelityHistory && d->mpOptimizerMutex)
            {
                std::lock_guard<MutexType> lock(*d->mpOptimizerMutex);
                for(auto i = 0u; i < d->mpOptimizerIterationFidelityHistory->size(); ++i)
                {
                    history.emplace_back( (*d->mpOptimizerIterationFidelityHistory).at( i ) );
                }
                return history;
            }
            else
            {
                history = {RVec{0.0,0.0}};
            }
            return history;
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template<class Type>
    RVecWrapper LevelWrapperSpatial<Type>::
    GetOptimizedFinalWavefunction(const double scaling)
    {
        try
        {
            if(d && d->mpOptimizedControlHistory && d->mpOptimizerFinalWaveFunctionAbsSquare)
            {
                return TransformWavefunction(*d->mpOptimizerFinalWaveFunctionAbsSquare, GetRescaledPotential( d->mFinalControl() ), scaling);
            }
            else
            {
                return GetCurrentWavefunctionNORM(scaling);
            }
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template<class Type>
    void LevelWrapperSpatial<Type>::SetControlHistory(const ControlWrapper & controlHistory)
    {
        try
        {
            if (d)
            {
                switch (mControlState)
                {
                    case ControlState::RECORDING:
                    {
                        SetError(ErrorCode::CONTROL_STILL_RECORDING, "");
                        return;
                    }
                    case ControlState::COMPLETE:
                    {
                        break;
                    }
                    case ControlState::INIT:
                    {
                        SetControlState(ControlState::COMPLETE);
                        break;
                    }
                }
                auto n_steps = controlHistory.size();
                if(n_steps < 3)
                {
                    SetError(ErrorCode::CONTROL_TOO_SHORT, "");
                    return;
                }

                qengine::Control u =  qengine::Control(controlHistory.getControl().mat(), d->mDt());

                // Fill out control, and ensure initial/final point is fixed
                u.setFront(d->mInitialControl());
                u.setBack(d->mFinalControl());

                // reset the internal state of the optimizer
                d->ResetOptimizerState();

                // overwrite both control containers
                d->mControlHistory = u;

                if (d->mpOptimizedControlHistory)
                {
                    *d->mpOptimizedControlHistory = u;
                }

                PropagateAlongControl( d->mControlHistory );
            }
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
        }
    }

    template <class Type>
    ControlWrapper LevelWrapperSpatial<Type>::
    GetControlHistory()
    {
        try
        {
            std::lock_guard<MutexType> lock(*d->mpOptimizerMutex);
            return d->mControlScaling().MapScale(d->mControlHistory, ScalingType::IDENTITY);
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }

    template <class Type>
    ControlWrapper LevelWrapperSpatial<Type>::
    GetControlHistoryPhysical()
    {
        try
        {
            std::lock_guard<MutexType> lock(*d->mpOptimizerMutex);
            return d->mControlScaling().MapScale(d->mControlHistory, utility::ScalingType::INVERSE );
        }
        catch (const std::runtime_error & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
            return {};
        }
    }


    template <class Type>
    bool LevelWrapperSpatial<Type>::
    IsNewControlDataAvailable()
    {
        if(d->mpControlObserver)
        {
            return (d->mpControlObserver->Get() == utility::observer::DataState::RECENTLY_UPDATED);
        }
        else return false;
    }

    template <class Type>
    void LevelWrapperSpatial<Type>::
    DoLoadLevel_Common()
    {
        try
        {
            if(d)
            {
                // Always tie optimizer variables, prevent crashes
                auto tup = d->mpOptimizerMaker()->operator()(qengine::Control::zeros(3,2,0.1),1, d->mGetOptimizationBoundaries());
                d->mpOptimizer() = std::get<0>(tup);
                auto optVars = std::get<1>(tup);
                tieOptVars(d,optVars);

                DoResetLevel();
            }
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
        }
    }
}
}


/// INSTANTIATE COMMON TEMPLATES AND SPECIALIZE DoLoadLevel
namespace qengine
{
namespace levelwrapper
{



    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel1()
    {
        try
        {
            using namespace gpe;
            d = std::make_shared<Data<GPETupleTypes>>(Data<GPETupleTypes>(Initialize_Level1()));
            DoLoadLevel_Common();
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
        }
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel2()
    {
        try
        {
            using namespace gpe;
            d = std::make_shared<Data<GPETupleTypes>>(Data<GPETupleTypes>(Initialize_Level2()));
            DoLoadLevel_Common();
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
        }
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel3()
    {
        try
        {
            using namespace gpe;
            d = std::make_shared<Data<GPETupleTypes>>(Data<GPETupleTypes>(Initialize_Level3()));
            DoLoadLevel_Common();
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
        }
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel4()
    {
        try
        {
            using namespace gpe;
            d = std::make_shared<Data<GPETupleTypes>>(Data<GPETupleTypes>(Initialize_Level4()));
            DoLoadLevel_Common();
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
        }

    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel5()
    {
        try
        {
            using namespace gpe;
            d = std::make_shared<Data<GPETupleTypes>>(Data<GPETupleTypes>(Initialize_Level5()));
            DoLoadLevel_Common();
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
        }

    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel6()
    {
        try
        {
            using namespace gpe;
            d = std::make_shared<Data<GPETupleTypes>>(Data<GPETupleTypes>(Initialize_Level6()));
            DoLoadLevel_Common();
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
        }

    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel7()
    {
        DoLoadLevel1();
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel_ShakeUp(const utility::LevelConfig & cfg)
    {
        try
        {
            using namespace gpe;
            d = std::make_shared<Data<GPETupleTypes>>(Data<GPETupleTypes>(Initialize_ShakeUp(cfg)));
            DoLoadLevel_Common();
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
        }
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel_BringHomeWater(const utility::LevelConfig & cfg)
    {
        try
        {
            using namespace gpe;
            d = std::make_shared<Data<GPETupleTypes>>(Data<GPETupleTypes>(Initialize_BringHomeWater(cfg)));
            DoLoadLevel_Common();
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
        }
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel_Splitting(const utility::LevelConfig & cfg)
    {
        try
        {
            using namespace gpe;
            d = std::make_shared<Data<GPETupleTypes>> (Data<GPETupleTypes> (Initialize_Splitting(cfg)));
            DoLoadLevel_Common();
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
        }
    }


    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel_BringHomeWater_0_0()
    {
        DoLoadLevel_BringHomeWater( utility::ConstructBHWConfig(0.0, 0) );
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel_BringHomeWater_1_0()
    {
        DoLoadLevel_BringHomeWater( utility::ConstructBHWConfig(1.0, 0) );
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel_BringHomeWater_0_1()
    {
        DoLoadLevel_BringHomeWater( utility::ConstructBHWConfig(0.0, 1) );
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel_BringHomeWater_1_1()
    {
        DoLoadLevel_BringHomeWater( utility::ConstructBHWConfig(1.0, 1) );
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel_ShakeUp_0()
    {
        DoLoadLevel_ShakeUp( utility::ConstructShakeUpConfig(0.0) );
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel_ShakeUp_1()
    {
        DoLoadLevel_ShakeUp( utility::ConstructShakeUpConfig(1.0) );
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel_ShakeUp_2()
    {
        DoLoadLevel_ShakeUp( utility::ConstructShakeUpConfig(2.0) );
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel_ShakeUp_3()
    {
        DoLoadLevel_ShakeUp( utility::ConstructShakeUpConfig(3.0) );
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel_Splitting_50()
    {
        DoLoadLevel_Splitting( utility::ConstructSplittingConfig(0.5, 0.0) );
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel_Splitting_50_d()
    {
        DoLoadLevel_Splitting( utility::ConstructSplittingConfig(0.5, 1.0) );
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel_Splitting_25_d()
    {
        DoLoadLevel_Splitting( utility::ConstructSplittingConfig(0.25, 1.0) );
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel_Splitting_10_d()
    {
        DoLoadLevel_Splitting( utility::ConstructSplittingConfig(0.10, 1.0) );
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel_Splitting_0_d()
    {
        DoLoadLevel_Splitting( utility::ConstructSplittingConfig(0.0, 1.0) );
    }

    template <>
    void LevelWrapperSpatial<gpe::GPETupleTypes>::DoLoadLevel()
    {
        try
        {
            DoLoadLevel_BringHomeWater( utility::ConstructBHWConfig(0.0, 0) );
        }
        catch (const std::exception & qengine_exception)
        {
            SetError(ErrorCode::QENGINE_EXCEPTION, qengine_exception.what());
        }
    }
}
}
