#pragma once
#include <tuple>
#include <atomic>
#include "LevelWrappers/OptimizerTypes.h"
#include "LevelWrappers/HelperMacros.h"
#include "Utilities/Scaling.h"
#include "LevelWrappers/LevelConfigs.h"

namespace qengine
{
namespace levelwrapper
{

namespace oneparticle
{
}

namespace gpe
{

    inline decltype(auto) Initialize_Level1() // Transport
    {
        using namespace qengine;

        const real TQSLApprox = 0.40;
        const auto n_steps_approx = 400;
        real dt  = TQSLApprox / n_steps_approx;
        if(dt > 0.001) dt = 0.001;
        const size_t dim = 256;

        const auto xLimitsPhysical = RVec{-2, 2};
        const auto yLimitsPhysical = RVec{-300, 100};

        const auto initialControlPhysical   = RVec{-1.0, -130};
        const auto finalControlPhysical     = RVec{+1.0, -130};

        const auto xScalingFunc = utility::GetCoordinateReferenceScalings({xLimitsPhysical.at(0)}, {xLimitsPhysical.at(1)}, 0);
        const auto yScalingFunc = utility::GetCoordinateReferenceScalings({yLimitsPhysical.at(0)}, {yLimitsPhysical.at(1)}, 0);

        const auto xLimits = utility::MapRVec(xScalingFunc.first, xLimitsPhysical);
        const auto yLimits = utility::MapRVec(yScalingFunc.first, yLimitsPhysical);

        const utility::Scaling<Control, RVec> wavefunctionScaling(
            [] (const RVec & input) {return input * 0.06;},
            [] (const RVec & input) {return input / 0.06; });

        const utility::Scaling<Control, RVec> potentialScaling(
            [yScalingFunc] (const RVec & input) { return utility::MapRVec(yScalingFunc.first, input); },
            [yScalingFunc] (const RVec & input) { return utility::MapRVec(yScalingFunc.second, input); });

        const utility::Scaling<Control, RVec> controlScaling(
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.first( ctrl.at(0) ),  yScalingFunc.first(ctrl.at(1))}; }, // SCALE FORWARD
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.second( ctrl.at(0) ), yScalingFunc.second(ctrl.at(1))}; }); // SCALE INVERSE

        const auto initialControl = controlScaling.Scale(initialControlPhysical, utility::ScalingType::FORWARD);
        const auto finalControl   = controlScaling.Scale(finalControlPhysical, utility::ScalingType::FORWARD);

        const auto leftBoundaryPhysical = RVec{xLimitsPhysical.at(0), yLimitsPhysical.at(0)};
        const auto rightBoundaryPhysical = RVec{xLimitsPhysical.at(1), yLimitsPhysical.at(1)};
        const auto leftBoundary = controlScaling.Scale({leftBoundaryPhysical},   utility::ScalingType::FORWARD);
        const auto rightBoundary = controlScaling.Scale({rightBoundaryPhysical}, utility::ScalingType::FORWARD);                
        constexpr auto boundaryStrength = 2e3;
        const OptimizationBoundaries optimizerBounds {leftBoundary, rightBoundary, boundaryStrength};

        const auto kinFactor = 0.5;
        const auto s = qengine::gpe::makeHilbertSpace(-3, 3, 256, kinFactor);
        const auto x = s.x();

        const auto xVecRescaled = utility::MapRVec(xScalingFunc.first, x.vec());

        const auto width = 0.5;

        const auto V = makePotentialFunction(wrap([x, width, initialControlPhysical, controlScaling](const RVec& pIn)
        {
            const RVec p = controlScaling.Scale(pIn, utility::ScalingType::INVERSE);            
            const auto x0 = p.at(0);
            const auto amplitude = initialControlPhysical.at(1);

            const auto x_disp = x - x0;
            return amplitude * exp(-2*x_disp*x_disp / (width*width));
        }), initialControl);


        constexpr ControlAxis controlAxis = ControlAxis::X;

        const auto g1D = 0;

        const auto psi_0 = makeWavefunction((s.T() + V(initialControl))[0]);    // ground state in static tweezer at t=0
        const auto psi_t = makeWavefunction((s.T() + V(finalControl  ))[0]); // groundstate in dynamic tweezer at t=T

        const auto H = s.T() + V + makeGpeTerm(g1D);

        BOILERPLATE_INIT
    };

    inline decltype(auto) Initialize_Level2() // Shake-X
    {
        using namespace qengine;

        const auto TQSLApprox =  2*0.15;
        const auto n_steps_approx = 400;
        real dt  = TQSLApprox / n_steps_approx;
        if(dt > 0.001) dt = 0.001;

        const size_t dim = 256;

        const auto xLimitsPhysical = RVec{-0.75, 0.75};
        const auto yLimitsPhysical = RVec{-50, 200};

        // scaling part
        const auto initialControlPhysical   = RVec{0, 0};
        const auto finalControlPhysical     = RVec{0, 0};

        const auto xScalingFunc = utility::GetCoordinateReferenceScalings({xLimitsPhysical.at(0)}, {xLimitsPhysical.at(1)}, 0);
        const auto yScalingFunc = utility::GetCoordinateReferenceScalings({yLimitsPhysical.at(0)}, {yLimitsPhysical.at(1)}, 0);

        const utility::Scaling<Control, RVec> wavefunctionScaling(
            [] (const RVec & input) {return input * 0.08;},
            [] (const RVec & input) {return input / 0.08; });

        const utility::Scaling<Control, RVec> potentialScaling(
            [yScalingFunc] (const RVec & input) { return utility::MapRVec(yScalingFunc.first, input); },
            [yScalingFunc] (const RVec & input) { return utility::MapRVec(yScalingFunc.second, input); });

        const utility::Scaling<Control, RVec> controlScaling(
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.first( ctrl.at(0) ),  yScalingFunc.first(ctrl.at(1))}; }, // SCALE FORWARD
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.second( ctrl.at(0) ), yScalingFunc.second(ctrl.at(1))}; }); // SCALE INVERSE


        const auto initialControl = controlScaling.Scale(initialControlPhysical, utility::ScalingType::FORWARD);
        const auto finalControl   = controlScaling.Scale(finalControlPhysical, utility::ScalingType::FORWARD); 

        const auto leftBoundaryPhysical = RVec{xLimitsPhysical.at(0), yLimitsPhysical.at(0)};
        const auto rightBoundaryPhysical = RVec{xLimitsPhysical.at(1), yLimitsPhysical.at(1)};
        const auto leftBoundary = controlScaling.Scale({leftBoundaryPhysical},   utility::ScalingType::FORWARD);
        const auto rightBoundary = controlScaling.Scale({rightBoundaryPhysical}, utility::ScalingType::FORWARD);                
        constexpr auto boundaryStrength = 2e3;
        const OptimizationBoundaries optimizerBounds {leftBoundary, rightBoundary, boundaryStrength};


        const auto xLimits = utility::MapRVec(xScalingFunc.first, xLimitsPhysical);
        const auto yLimits = utility::MapRVec(yScalingFunc.first, yLimitsPhysical);
        //    

        const auto kinFactor = 0.5;
        const auto s = qengine::gpe::makeHilbertSpace(-1,1 , 256, kinFactor);
        const auto x = s.x();

        const auto xVecRescaled = utility::MapRVec(xScalingFunc.first, x.vec());

        const auto frequency = 45.6;

        const auto amplitude = -130;
        const auto width= 0.50;

        const auto V_dynamic = makePotentialFunction(wrap([x, amplitude,width,frequency,initialControlPhysical, controlScaling](const RVec& pIn)
        {
            const RVec p = controlScaling.Scale(pIn, utility::ScalingType::INVERSE);
            const auto x0 = p.at(0);

            const auto x_disp = x - x0;
            return amplitude * exp(-2*x_disp*x_disp / (width*width)) - amplitude;
        }), initialControl);

        constexpr ControlAxis controlAxis = ControlAxis::X;        

        const auto g1D = 0;

        const auto V = makePotentialFunction(wrap([x, frequency,V_dynamic](const RVec& p)
        {
            return V_dynamic(p);
        }), initialControl);

        const auto H = s.T() + V + makeGpeTerm(g1D);

        const auto Hi = H(initialControl);
        const auto Hf = H(finalControl);

        const auto psi_0 = makeWavefunction(Hi[0] + Hi[1],NORMALIZATION::NORMALIZE);
        const auto psi_t = makeWavefunction(Hf[0]); // groundstate in dynamic tweezer at t=T

        BOILERPLATE_INIT
    }

    inline decltype(auto) Initialize_Level3() // Deep well transport
    {
        using namespace qengine;

        const real TQSLApprox = 0.40;
        const auto n_steps_approx = 400;
        real dt  = TQSLApprox / n_steps_approx;
        if(dt > 0.001) dt = 0.001;
        const size_t dim = 256;

        const auto xLimitsPhysical = RVec{-2, 2};
        const auto yLimitsPhysical = RVec{-450, 100};

        const auto initialControlPhysical   = RVec{-1.0, -130*3};
        const auto finalControlPhysical     = RVec{+1.0, -130*3};

        const auto xScalingFunc = utility::GetCoordinateReferenceScalings({xLimitsPhysical.at(0)}, {xLimitsPhysical.at(1)}, 0);
        const auto yScalingFunc = utility::GetCoordinateReferenceScalings({yLimitsPhysical.at(0)}, {yLimitsPhysical.at(1)}, 0);

        const auto xLimits = utility::MapRVec(xScalingFunc.first, xLimitsPhysical);
        const auto yLimits = utility::MapRVec(yScalingFunc.first, yLimitsPhysical);

        const utility::Scaling<Control, RVec> wavefunctionScaling(
            [] (const RVec & input) {return input * 0.06;},
            [] (const RVec & input) {return input / 0.06; });

        const utility::Scaling<Control, RVec> potentialScaling(
            [yScalingFunc] (const RVec & input) { return utility::MapRVec(yScalingFunc.first, input); },
            [yScalingFunc] (const RVec & input) { return utility::MapRVec(yScalingFunc.second, input); });

        const utility::Scaling<Control, RVec> controlScaling(
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.first( ctrl.at(0) ),  yScalingFunc.first(ctrl.at(1))}; }, // SCALE FORWARD
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.second( ctrl.at(0) ), yScalingFunc.second(ctrl.at(1))}; }); // SCALE INVERSE

        const auto initialControl = controlScaling.Scale(initialControlPhysical, utility::ScalingType::FORWARD);
        const auto finalControl   = controlScaling.Scale(finalControlPhysical, utility::ScalingType::FORWARD);

        const auto leftBoundaryPhysical = RVec{xLimitsPhysical.at(0), yLimitsPhysical.at(0)};
        const auto rightBoundaryPhysical = RVec{xLimitsPhysical.at(1), yLimitsPhysical.at(1)};
        const auto leftBoundary = controlScaling.Scale({leftBoundaryPhysical},   utility::ScalingType::FORWARD);
        const auto rightBoundary = controlScaling.Scale({rightBoundaryPhysical}, utility::ScalingType::FORWARD);                
        constexpr auto boundaryStrength = 2e3;
        const OptimizationBoundaries optimizerBounds {leftBoundary, rightBoundary, boundaryStrength};

        const auto kinFactor = 0.5;
        const auto s = qengine::gpe::makeHilbertSpace(-3, 3, 256, kinFactor);
        const auto x = s.x();

        const auto xVecRescaled = utility::MapRVec(xScalingFunc.first, x.vec());

        const auto width = 0.5;

        const auto V = makePotentialFunction(wrap([x, width, initialControlPhysical, controlScaling](const RVec& pIn)
        {
            const RVec p = controlScaling.Scale(pIn, utility::ScalingType::INVERSE);
            const auto x0 = p.at(0);
            const auto amplitude = initialControlPhysical.at(1);

            const auto x_disp = x - x0;
            return amplitude * exp(-2*x_disp*x_disp / (width*width));
        }), initialControl);

        constexpr ControlAxis controlAxis = ControlAxis::X;        

        const auto g1D = 0;

        const auto psi_0 = makeWavefunction((s.T() + V(initialControl))[0]);    // ground state in static tweezer at t=0
        const auto psi_t = makeWavefunction((s.T() + V(finalControl  ))[0]); // groundstate in dynamic tweezer at t=T

        const auto H = s.T() + V + makeGpeTerm(g1D);

        BOILERPLATE_INIT
    };

    inline decltype(auto) Initialize_Level4() // Shallow well transport
    {
        using namespace qengine;

        const real TQSLApprox = 0.40;
        const auto n_steps_approx = 400;
        real dt  = TQSLApprox / n_steps_approx;
        if(dt > 0.001) dt = 0.001;
        const size_t dim = 256;

        const auto xLimitsPhysical = RVec{-2, 2};
        const auto yLimitsPhysical = RVec{-100, 100};

        const auto initialControlPhysical   = RVec{-1.0, -130/3};
        const auto finalControlPhysical     = RVec{+1.0, -130/3};

        const auto xScalingFunc = utility::GetCoordinateReferenceScalings({xLimitsPhysical.at(0)}, {xLimitsPhysical.at(1)}, 0);
        const auto yScalingFunc = utility::GetCoordinateReferenceScalings({yLimitsPhysical.at(0)}, {yLimitsPhysical.at(1)}, 0);

        const auto xLimits = utility::MapRVec(xScalingFunc.first, xLimitsPhysical);
        const auto yLimits = utility::MapRVec(yScalingFunc.first, yLimitsPhysical);

        const utility::Scaling<Control, RVec> wavefunctionScaling(
            [] (const RVec & input) {return input * 0.06;},
            [] (const RVec & input) {return input / 0.06; });

        const utility::Scaling<Control, RVec> potentialScaling(
            [yScalingFunc] (const RVec & input) { return utility::MapRVec(yScalingFunc.first, input); },
            [yScalingFunc] (const RVec & input) { return utility::MapRVec(yScalingFunc.second, input); });

        const utility::Scaling<Control, RVec> controlScaling(
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.first( ctrl.at(0) ),  yScalingFunc.first(ctrl.at(1))}; }, // SCALE FORWARD
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.second( ctrl.at(0) ), yScalingFunc.second(ctrl.at(1))}; }); // SCALE INVERSE

        const auto initialControl = controlScaling.Scale(initialControlPhysical, utility::ScalingType::FORWARD);
        const auto finalControl   = controlScaling.Scale(finalControlPhysical, utility::ScalingType::FORWARD);

        const auto leftBoundaryPhysical = RVec{xLimitsPhysical.at(0), yLimitsPhysical.at(0)};
        const auto rightBoundaryPhysical = RVec{xLimitsPhysical.at(1), yLimitsPhysical.at(1)};
        const auto leftBoundary = controlScaling.Scale({leftBoundaryPhysical},   utility::ScalingType::FORWARD);
        const auto rightBoundary = controlScaling.Scale({rightBoundaryPhysical}, utility::ScalingType::FORWARD);                
        constexpr auto boundaryStrength = 2e3;
        const OptimizationBoundaries optimizerBounds {leftBoundary, rightBoundary, boundaryStrength};

        const auto kinFactor = 0.5;
        const auto s = qengine::gpe::makeHilbertSpace(-3, 3, 256, kinFactor);
        const auto x = s.x();

        const auto xVecRescaled = utility::MapRVec(xScalingFunc.first, x.vec());

        const auto width = 0.5;

        const auto V = makePotentialFunction(wrap([x, width, initialControlPhysical, controlScaling](const RVec& pIn)
        {
            const RVec p = controlScaling.Scale(pIn, utility::ScalingType::INVERSE);
            const auto x0 = p.at(0);
            const auto amplitude = initialControlPhysical.at(1);

            const auto x_disp = x - x0;
            return amplitude * exp(-2*x_disp*x_disp / (width*width));
        }), initialControl);

        constexpr ControlAxis controlAxis = ControlAxis::X;        

        const auto g1D = 0;

        const auto psi_0 = makeWavefunction((s.T() + V(initialControl))[0]);    // ground state in static tweezer at t=0
        const auto psi_t = makeWavefunction((s.T() + V(finalControl  ))[0]); // groundstate in dynamic tweezer at t=T

        const auto H = s.T() + V + makeGpeTerm(g1D);

        BOILERPLATE_INIT
    };


    inline decltype(auto) Initialize_Level5() // Shake-Y
    {
        using namespace qengine;

        const auto TQSLApprox =  2*0.15;
        const auto n_steps_approx = 400;
        real dt  = TQSLApprox / n_steps_approx;
        if(dt > 0.001) dt = 0.001;

        const size_t dim = 256;

        const auto xLimitsPhysical = RVec{-0.75, 0.75};
        const auto yLimitsPhysical = RVec{-50, 200};

        const auto initialControlPhysical   = RVec{0, 0};
        const auto finalControlPhysical     = RVec{0, 0};

        const auto xScalingFunc = utility::GetCoordinateReferenceScalings({xLimitsPhysical.at(0)}, {xLimitsPhysical.at(1)}, 0);
        const auto yScalingFunc = utility::GetCoordinateReferenceScalings({yLimitsPhysical.at(0)}, {yLimitsPhysical.at(1)}, 0);

        const utility::Scaling<Control, RVec> wavefunctionScaling(
            [] (const RVec & input) {return input * 0.08;},
            [] (const RVec & input) {return input / 0.08; });

        const utility::Scaling<Control, RVec> potentialScaling(
            [yScalingFunc] (const RVec & input) { return utility::MapRVec(yScalingFunc.first, input); },
            [yScalingFunc] (const RVec & input) { return utility::MapRVec(yScalingFunc.second, input); });

        const utility::Scaling<Control, RVec> controlScaling(
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.first( ctrl.at(0) ),  yScalingFunc.first(ctrl.at(1))}; }, // SCALE FORWARD
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.second( ctrl.at(0) ), yScalingFunc.second(ctrl.at(1))}; }); // SCALE INVERSE


        const auto initialControl = controlScaling.Scale(initialControlPhysical, utility::ScalingType::FORWARD);
        const auto finalControl   = controlScaling.Scale(finalControlPhysical, utility::ScalingType::FORWARD);

        const auto leftBoundaryPhysical = RVec{xLimitsPhysical.at(0), yLimitsPhysical.at(0)};
        const auto rightBoundaryPhysical = RVec{xLimitsPhysical.at(1), yLimitsPhysical.at(1)};
        const auto leftBoundary = controlScaling.Scale({leftBoundaryPhysical},   utility::ScalingType::FORWARD);
        const auto rightBoundary = controlScaling.Scale({rightBoundaryPhysical}, utility::ScalingType::FORWARD);                
        constexpr auto boundaryStrength = 2e3;
        const OptimizationBoundaries optimizerBounds {leftBoundary, rightBoundary, boundaryStrength};

        const auto xLimits = utility::MapRVec(xScalingFunc.first, xLimitsPhysical);
        const auto yLimits = utility::MapRVec(yScalingFunc.first, yLimitsPhysical);
        //

        const auto kinFactor = 0.5;
        const auto s = qengine::gpe::makeHilbertSpace(-1,1 , 256, kinFactor);
        const auto x = s.x();

        const auto xVecRescaled = utility::MapRVec(xScalingFunc.first, x.vec());

        const auto Voffset = 130;
        const auto width= 0.50;


        const auto V_dynamic = makePotentialFunction(wrap([x, Voffset,width,initialControlPhysical, controlScaling](const RVec& pIn)
        {
            const RVec p = controlScaling.Scale(pIn, utility::ScalingType::INVERSE);
            const auto amplitude = p.at(1);

            return (-Voffset+amplitude) * exp(-2*x*x / (width*width)) + Voffset;
        }), initialControl);

        constexpr ControlAxis controlAxis = ControlAxis::Y;        

        const auto g1D = 0;

        const auto V = makePotentialFunction(wrap([x,V_dynamic](const RVec& p)
        {
            return V_dynamic(p);
        }), initialControl);

        const auto H = s.T() + V + makeGpeTerm(g1D);

        const auto Hi = H(initialControl);
        const auto Hf = H(finalControl);

        const auto psi_0 = makeWavefunction(Hi[0] + Hi[2],NORMALIZATION::NORMALIZE);
        const auto psi_t = makeWavefunction(Hf[0]); // groundstate in dynamic tweezer at t=T

        BOILERPLATE_INIT
    }

    inline decltype(auto) Initialize_Level6() // Tunneling
    {
        using namespace qengine;

        const real TQSLApprox = 2.0;
        const auto n_steps_approx = 400;
        real dt  = TQSLApprox / n_steps_approx;
        if(dt > 0.001) dt = 0.001;
        const size_t dim = 256;

        const auto xLimitsPhysical = RVec{-1.0, 1.0};
        const auto yLimitsPhysical = RVec{-70, 30};

        const auto initialControlPhysical   = RVec{-0.42, 0};
        const auto finalControlPhysical     = RVec{-0.42, -45};

        const auto xScalingFunc = utility::GetCoordinateReferenceScalings({xLimitsPhysical.at(0)}, {xLimitsPhysical.at(1)}, 0);
        const auto yScalingFunc = utility::GetCoordinateReferenceScalings({yLimitsPhysical.at(0)}, {yLimitsPhysical.at(1)}, 0);

        const utility::Scaling<Control, RVec> wavefunctionScaling(
            [] (const RVec & input) {return input * 0.04;},
            [] (const RVec & input) {return input / 0.04; });

        const utility::Scaling<Control, RVec> potentialScaling(
            [yScalingFunc] (const RVec & input) { return utility::MapRVec(yScalingFunc.first, input); },
            [yScalingFunc] (const RVec & input) { return utility::MapRVec(yScalingFunc.second, input); });

        const utility::Scaling<Control, RVec> controlScaling(
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.first( ctrl.at(0) ),  yScalingFunc.first(ctrl.at(1))}; }, // SCALE FORWARD
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.second( ctrl.at(0) ), yScalingFunc.second(ctrl.at(1))}; }); // SCALE INVERSE

        const auto initialControl = controlScaling.Scale(initialControlPhysical, utility::ScalingType::FORWARD);
        const auto finalControl   = controlScaling.Scale(finalControlPhysical, utility::ScalingType::FORWARD);

        const auto leftBoundaryPhysical = RVec{xLimitsPhysical.at(0), yLimitsPhysical.at(0)};
        const auto rightBoundaryPhysical = RVec{xLimitsPhysical.at(1), yLimitsPhysical.at(1)};
        const auto leftBoundary = controlScaling.Scale({leftBoundaryPhysical},   utility::ScalingType::FORWARD);
        const auto rightBoundary = controlScaling.Scale({rightBoundaryPhysical}, utility::ScalingType::FORWARD);                
        constexpr auto boundaryStrength = 2e3;
        const OptimizationBoundaries optimizerBounds {leftBoundary, rightBoundary, boundaryStrength};

        const auto xLimits = utility::MapRVec(xScalingFunc.first, xLimitsPhysical);
        const auto yLimits = utility::MapRVec(yScalingFunc.first, yLimitsPhysical);        
  
        const auto kinFactor = 0.5;
        const auto s = qengine::gpe::makeHilbertSpace(-3,3, 256, kinFactor);
        const auto x = s.x();

        const auto xVecRescaled = utility::MapRVec(xScalingFunc.first, x.vec());


        const auto width = 0.5;
        const auto staticTweezerParams = RVec{0.42,-45};

        const auto V_static = staticTweezerParams.at(1) * exp(-2*pow(x - staticTweezerParams.at(0), 2) / (width*width));

        const auto V_dynamic = makePotentialFunction(wrap([x, width, controlScaling, initialControlPhysical](const RVec& pIn)
        {
            const RVec p = controlScaling.Scale(pIn, utility::ScalingType::INVERSE);
            const auto x0 = initialControlPhysical.at(0);
            const auto amplitude = p.at(1);

            const auto x_disp = x - x0;
            return amplitude * exp(-2*x_disp*x_disp / (width*width));
        }), initialControl);

        constexpr ControlAxis controlAxis = ControlAxis::Y;        

        const auto g1D = 0;

        const auto psi_0 = makeWavefunction((s.T() + V_static)[0]);    // ground state in static tweezer at t=0
        const auto psi_t = makeWavefunction((s.T() + V_dynamic(finalControl))[0]); // groundstate in dynamic tweezer at t=T


        const auto V = makePotentialFunction(wrap([x, width,V_static,V_dynamic](const RVec& p)
        {
            return V_dynamic(p) + V_static;
        }), initialControl);

        const auto H = s.T() + V + makeGpeTerm(g1D);

        BOILERPLATE_INIT
    };

    // Level 7 is Transport

    inline decltype(auto) Initialize_ShakeUp(const utility::LevelConfig & cfg)
    {
        using namespace qengine;

        const real TQSLApprox = 0.89; // from 24/11
        real dt  = 1e-3;
        const size_t dim = 256;


        const auto xLimitsPhysical = RVec{-1.25, 1.25};
        const auto yLimitsPhysical = RVec{-200, 100};

        const auto xScalingFunc = utility::GetCoordinateReferenceScalings({xLimitsPhysical.at(0)}, {xLimitsPhysical.at(1)}, 0);
        const auto yScalingFunc = utility::GetCoordinateReferenceScalings({yLimitsPhysical.at(0)}, {yLimitsPhysical.at(1)}, 0);

        constexpr auto Voffset = -130.0;

        // scaling part
        const auto initialControlPhysical   = RVec{0.0, Voffset};
        const auto finalControlPhysical     = RVec{0.0, Voffset};

        const utility::Scaling<Control, RVec> wavefunctionScaling(
            [] (const RVec & input) {return input * 0.06;},
            [] (const RVec & input) {return input / 0.06; });

        const utility::Scaling<Control, RVec> potentialScaling(
            [yScalingFunc] (const RVec & input) { return utility::MapRVec(yScalingFunc.first, input); },
            [yScalingFunc] (const RVec & input) { return utility::MapRVec(yScalingFunc.second, input); });

        const utility::Scaling<Control, RVec> controlScaling(
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.first( ctrl.at(0) ),  yScalingFunc.first(ctrl.at(1))}; }, // SCALE FORWARD
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.second( ctrl.at(0) ), yScalingFunc.second(ctrl.at(1))}; }); // SCALE INVERSE

        const auto initialControl = controlScaling.Scale(initialControlPhysical, utility::ScalingType::FORWARD);
        const auto finalControl   = controlScaling.Scale(finalControlPhysical, utility::ScalingType::FORWARD); 

        const auto leftBoundaryPhysical = RVec{-1, yLimitsPhysical.at(0)};
        const auto rightBoundaryPhysical = RVec{1, yLimitsPhysical.at(1)};
        const auto leftBoundary = controlScaling.Scale({leftBoundaryPhysical},   utility::ScalingType::FORWARD);
        const auto rightBoundary = controlScaling.Scale({rightBoundaryPhysical}, utility::ScalingType::FORWARD);                
        constexpr auto boundaryStrength = 2e3;
        const OptimizationBoundaries optimizerBounds {leftBoundary, rightBoundary, boundaryStrength};

        const auto xLimits = utility::MapRVec(xScalingFunc.first, xLimitsPhysical);
        const auto yLimits = utility::MapRVec(yScalingFunc.first, yLimitsPhysical);        

        const auto kinFactor = 0.36537;
        const auto s = qengine::gpe::makeHilbertSpace(-2,+2,256,kinFactor);
        const auto x = s.x();

        const auto xVecRescaled = utility::MapRVec(xScalingFunc.first, x.vec());

        const auto V = makePotentialFunction(wrap([x,p2{65.8392},p4{97.6349},p6{-15.3850},Voffset, controlScaling](const RVec& pIn)
        {
            const RVec p = controlScaling.Scale(pIn, utility::ScalingType::INVERSE);
            const auto u = p.at(0);
            const auto x_u = x - u;
            const auto x_uPow2 = x_u * x_u;
            const auto x_uPow4 = x_uPow2 * x_uPow2;
            const auto x_uPow6 = x_uPow2 * x_uPow4;

            return  p2*x_uPow2 + p4*x_uPow4 + p6*x_uPow6 + Voffset;
        }), initialControl);

        constexpr ControlAxis controlAxis = ControlAxis::X;        

        const auto g1D = mpark::get<double> ( cfg.at("g1D") );

        const auto H = s.T() + V +  makeGpeTerm(g1D);
        const auto psi_0 = makeWavefunction(H(initialControl)[0]);
        const auto psi_t = makeWavefunction(H(finalControl  )[1]);

        BOILERPLATE_INIT
    };
    using ShakeUpTypes = decltype(gpe::Initialize_ShakeUp({}));

    inline decltype(auto) Initialize_Splitting(const utility::LevelConfig & cfg)
    {
        using namespace qengine;

        const real TQSLApprox = 0.92; // from 27/11
        real dt  = 1e-3;
        const size_t dim = 256;

        const auto xLimitsPhysical = RVec{-2.25, 2.25};
        const auto yLimitsPhysical = RVec{-0.1, 1.3};

        const auto xScalingFunc = utility::GetCoordinateReferenceScalings({xLimitsPhysical.at(0)}, {xLimitsPhysical.at(1)}, 0);
        const auto yScalingFunc = utility::GetCoordinateReferenceScalings({yLimitsPhysical.at(0)}, {yLimitsPhysical.at(1)}, 0);

        const auto initialControlPhysical = RVec {0.0, 0.0};
        const auto finalControlPhysical   = RVec {0.0, 1.0};

        const utility::Scaling<Control, RVec> wavefunctionScaling(
            [] (const RVec & input) {return input * 0.24;},
            [] (const RVec & input) {return input / 0.24; });

        const utility::Scaling<Control, RVec> potentialScaling(
            [] (const RVec & input) {return input * 0.01 ;},
            [] (const RVec & input) {return input / 0.01 ; });

        const auto V_offset = 0.0028;


        const utility::Scaling<Control, RVec> controlScaling(
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.first( ctrl.at(0) ),  yScalingFunc.first(ctrl.at(1))}; }, // SCALE FORWARD
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.second( ctrl.at(0) ), yScalingFunc.second(ctrl.at(1))}; }); // SCALE INVERSE

        const auto initialControl = controlScaling.Scale(initialControlPhysical, utility::ScalingType::FORWARD);
        const auto finalControl   = controlScaling.Scale(finalControlPhysical,   utility::ScalingType::FORWARD);

        const auto leftBoundaryPhysical =  RVec{-1.0, 0.0};
        const auto rightBoundaryPhysical = RVec{+1.0, 1.0};
        const auto leftBoundary = controlScaling.Scale({leftBoundaryPhysical},   utility::ScalingType::FORWARD);
        const auto rightBoundary = controlScaling.Scale({rightBoundaryPhysical}, utility::ScalingType::FORWARD);                
        constexpr auto boundaryStrength = 2e3;
        const OptimizationBoundaries optimizerBounds {leftBoundary, rightBoundary, boundaryStrength};


        const auto xLimits = utility::MapRVec(xScalingFunc.first, xLimitsPhysical);
        const auto yLimits = utility::MapRVec(yScalingFunc.first, yLimitsPhysical);


        const auto kinFactor = 0.36537;
        const auto s = qengine::gpe::makeHilbertSpace(-3.5, +3.5, 256, kinFactor);
        const auto x = s.x();
        const auto xVecRescaled = utility::MapRVec(xScalingFunc.first, x.vec());

        const real Gr = 0.2;
        const real BI = 1.0;
        
        const real Bdet = 0.90024;
        const real pV = 8794.1;
        const double displacementMultiplier = mpark::get<double> ( cfg.at("Displacement") );

        const auto V = makePotentialFunction(wrap([x,controlScaling,V_offset,
                                                  Bdet,pV,BI, displacementMultiplier, Gr
                                                  ](const RVec & pIn)
        {
            const RVec p = controlScaling.Scale(pIn, utility::ScalingType::INVERSE);
            const auto ux = p.at( 0 ) * displacementMultiplier;
            const auto uy = p.at( 1 );

            const auto x_disp = x - ux;

            const auto Bs = sqrt(Gr*Gr*x_disp*x_disp + BI*BI);            
            const auto Brf  = 0.5 + 0.3 * uy;
            auto pot = sqrt((Bs - Bdet) * (Bs - Bdet) + ((0.5 * Brf * BI) / Bs) * ((0.5 * Brf * BI) / Bs));
            pot = pot - min(pot) + V_offset;
            return pV*pot;
        }),initialControl);

        const ControlAxis controlAxis = (displacementMultiplier == 0.0) ? ControlAxis::Y : ControlAxis::XY;

        const auto g1D = 1.8299;

        const auto H = s.T() + V +  makeGpeTerm(g1D);
        const auto psi_0 = makeWavefunction(H(initialControl)[0]);

        const auto spectrum = H(finalControl).makeSpectrum(1, GROUNDSTATE_ALGORITHM::MIXING, 1e-8, 10000);
        const auto L = spectrum.eigenFunction(0) + spectrum.eigenFunction(1);
        const auto R = spectrum.eigenFunction(0) - spectrum.eigenFunction(1);
        const auto PL = mpark::get<double>(cfg.at("FractionLeftWell"));
        const auto PR = 1-PL;
        const auto cL = std::sqrt(PL);
        const auto cR = std::sqrt(PR);
        const auto psi_t = normalize(cL*L + cR*R);

        BOILERPLATE_INIT
    };

    inline decltype(auto) Initialize_BringHomeWater(const utility::LevelConfig & cfg)
    {
        using namespace qengine;

        const auto TQSLApprox = 0.37; // Switched back into old T_QSL on 28.01.19
        real dt  = 3.5e-4;

        const size_t dim = 256;

        const auto xLimitsPhysical = RVec{-2, 2};
        const auto yLimitsPhysical = RVec{-150-130, 100};

        const auto initialControlPhysical   = RVec{-1.0, -130.0};
        const auto finalControlPhysical     = initialControlPhysical;

        
        const auto staticTweezerParams = RVec{1.0,-130.0};
        

        const auto xScalingFunc = utility::GetCoordinateReferenceScalings({xLimitsPhysical.at(0)}, {xLimitsPhysical.at(1)}, 0);
        const auto yScalingFunc = utility::GetCoordinateReferenceScalings({yLimitsPhysical.at(0)}, {yLimitsPhysical.at(1)}, 0);


        // scaling part
        const utility::Scaling<Control, RVec> wavefunctionScaling(
            [] (const RVec & input) {return input * 0.06;},
            [] (const RVec & input) {return input / 0.06; });

        const utility::Scaling<Control, RVec> potentialScaling(
            [yScalingFunc] (const RVec & input) { return utility::MapRVec(yScalingFunc.first, input); },
            [yScalingFunc] (const RVec & input) { return utility::MapRVec(yScalingFunc.second, input); });

        const utility::Scaling<Control, RVec> controlScaling(
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.first( ctrl.at(0) ),  yScalingFunc.first(ctrl.at(1))}; }, // SCALE FORWARD
            [xScalingFunc, yScalingFunc] (const RVec & ctrl) { return RVec{ xScalingFunc.second( ctrl.at(0) ), yScalingFunc.second(ctrl.at(1))}; }); // SCALE INVERSE

        const auto initialControl = controlScaling.Scale(initialControlPhysical, utility::ScalingType::FORWARD);
        const auto finalControl   = controlScaling.Scale(finalControlPhysical, utility::ScalingType::FORWARD); 
        
        const auto leftBoundaryPhysical = RVec{xLimitsPhysical.at(0), -150.0};
        const auto rightBoundaryPhysical = RVec{xLimitsPhysical.at(1), 0.0};
        const auto leftBoundary = controlScaling.Scale({leftBoundaryPhysical},   utility::ScalingType::FORWARD);
        const auto rightBoundary = controlScaling.Scale({rightBoundaryPhysical}, utility::ScalingType::FORWARD);                
        constexpr auto boundaryStrength = 2e3;
        const OptimizationBoundaries optimizerBounds {leftBoundary, rightBoundary, boundaryStrength};

        const auto xLimits = utility::MapRVec(xScalingFunc.first, xLimitsPhysical);
        const auto yLimits = utility::MapRVec(yScalingFunc.first, yLimitsPhysical);

        const auto kinFactor = 0.5;
        const auto s = qengine::gpe::makeHilbertSpace(-3, 3, 256, kinFactor);
        const auto x = s.x();
        const auto xVecRescaled = utility::MapRVec(xScalingFunc.first, x.vec());
        const auto width = 0.50;
        const auto V_static = staticTweezerParams.at(1) * exp(-2*pow(x - staticTweezerParams.at(0), 2) / (width*width));

        const auto V_dynamic = makePotentialFunction(wrap([x, width, controlScaling](const RVec& pIn)
        {
            const RVec p = controlScaling.Scale(pIn, utility::ScalingType::INVERSE);
            const auto x0 = p.at(0);
            const auto amplitude = p.at(1);

            const auto x_disp = x - x0;
            return amplitude * exp(-2*x_disp*x_disp / (width*width));
        }), initialControl);

        constexpr ControlAxis controlAxis = ControlAxis::XY;        

        const auto g1D = mpark::get<double> ( cfg.at("g1D") );

        const auto psi_0 = makeWavefunction((s.T() + V_static)[0]);    // ground state in static tweezer at t=0

        const int targetState = mpark::get<int> (cfg.at("TargetState") );
        const auto psi_t = makeWavefunction((s.T() + V_dynamic(finalControl))[ targetState ]); // groundstate in dynamic tweezer at t=T

        const auto V = makePotentialFunction(wrap([x, width,V_static,V_dynamic](const RVec& p)
        {
            return V_dynamic(p) + V_static;
        }), initialControl);

        const auto H = s.T() + V + makeGpeTerm(g1D);

        BOILERPLATE_INIT
    };

    using BHWTupleType = decltype(gpe::Initialize_BringHomeWater({}));
    using GPETupleTypes = decltype(gpe::Initialize_ShakeUp({}));
}




namespace twoparticle
{

}

namespace bosehubbard 
{
	// using WaveFunction = (...);
	// using Propagator = (...);
	// using Hamiltonian = (...);	
}

namespace nlevel
{
	// using WaveFunction = (...);
	// using Propagator = (...);
	// using Hamiltonian = (...);
}
}
}
