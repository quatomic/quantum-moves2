#pragma once
#include "Utilities/Utilities.h"
#include "Utilities/DataObserver.h"

namespace qengine
{
    namespace levelwrapper
    {
        using TSBool = std::atomic<bool>;
        using TSInt  = std::atomic<int>;

        enum class OptimizerStopCondition { NONE = 0, 
                                            FIDELITY = 1, 
                                            STEPSIZE = 2, 
                                            ITERATIONS = 3, 
                                            EXCEPTION = 4, 
                                            USER = 5};

        namespace detail 
        {
            template < typename T > struct deduce_type ;

            template < typename RETURN_TYPE, typename CLASS_TYPE, typename... ARGS >
            struct deduce_type< RETURN_TYPE(CLASS_TYPE::*)(ARGS...) const >
            { using type = std::function< RETURN_TYPE(ARGS...) > ; };
        }

        template < typename CLOSURE > auto wrap( const CLOSURE& fn )
        { return typename detail::deduce_type< decltype( &CLOSURE::operator() ) >::type(fn) ; }


        template<class Hamiltonian, class WaveFunction>
        inline decltype(auto) InitializeOptimizer(Hamiltonian & H, WaveFunction & psi_0, WaveFunction & psi_t)
        {
            using MutexType = utility::MutexType;
            auto pControlObserver = std::make_shared<utility::observer::DataObserver> ();
            auto pOptimizerIteration = std::make_shared<TSInt>(0);
            auto pOptimizedFidelity  = std::make_shared<qengine::real>(0.0);
            auto pOptimizedControlHistory = std::make_shared<qengine::Control> (qengine::Control::zeros(10,2,0.1));
            auto pOptimizerStopMessage = std::make_shared<std::string>("");
            auto pOptimizerIterationFidelityHistory = std::make_shared<std::vector<RVec>>();
            auto pOptimizerFinalWaveFunctionAbsSquare= std::make_shared<RVec>(0*psi_t.absSquare().vec());
            auto pOptimizerStopCondition = std::make_shared< OptimizerStopCondition > ( OptimizerStopCondition::NONE ); 
            auto pBFGSRestarts = std::make_shared<TSInt> (0);

            auto optimizerMaker = [ H, psi_0, psi_t, pControlObserver, 
                                    pOptimizerStopMessage, pOptimizerIteration, pOptimizedFidelity, 
                                    pOptimizedControlHistory, pOptimizerIterationFidelityHistory, pOptimizerFinalWaveFunctionAbsSquare, 
                                    pOptimizerStopCondition, pBFGSRestarts](
                    qengine::Control controlHistory,
                    size_t maxIteration, 
                    const OptimizationBoundaries bounds)
            {
                auto pStopSwitch  = std::make_shared<TSBool> (false);
                auto pIsRunning   = std::make_shared<TSBool> (false);
                std::shared_ptr<MutexType> Mutex = std::make_shared<MutexType>();

                (*pOptimizedControlHistory) = controlHistory;

                auto problem  = makeStateTransferProblem(H,psi_0,psi_t,controlHistory) + 1e-6 * Regularization(controlHistory) +  bounds.mBoundaryStrength * Boundaries(controlHistory, bounds.mLeftBoundary, bounds.mRightBoundary);
                const auto stopper = makeStopper([pStopSwitch,pIsRunning,pOptimizerStopMessage,maxIteration,pOptimizerFinalWaveFunctionAbsSquare, pOptimizerStopCondition, Mutex](auto& optimizer) -> bool
                {
                    std::lock_guard<decltype(*Mutex)> guard(*Mutex);
                    bool stop = false;
                    if (*pStopSwitch)                           { stop = true; (*pOptimizerStopMessage) = "Stopping by request"; (*pOptimizerStopCondition) = OptimizerStopCondition::USER; }
                    if (optimizer.problem().fidelity() > 0.999) { stop = true; (*pOptimizerStopMessage) = "Solution converged."; (*pOptimizerStopCondition) = OptimizerStopCondition::FIDELITY; }
                    if (optimizer.stepSize() < 1e-7)            { stop = true; (*pOptimizerStopMessage) = "Cannot improve further"; (*pOptimizerStopCondition) = OptimizerStopCondition::STEPSIZE; }
                    if (optimizer.iteration() == maxIteration)  { stop = true; (*pOptimizerStopMessage) = "Iteration limit reached"; (*pOptimizerStopCondition) = OptimizerStopCondition::ITERATIONS; }

                    if(stop) (*pIsRunning) = false;
                    return stop;
                });

                const auto collector = makeCollector([  pOptimizedControlHistory, Mutex, pOptimizedFidelity, 
                                                        pControlObserver, pOptimizerIteration, pOptimizerIterationFidelityHistory, 
                                                        pOptimizerFinalWaveFunctionAbsSquare, pBFGSRestarts](auto& optimizer)
                {
                    std::lock_guard<decltype(*Mutex)> guard(*Mutex);
                    if ( pOptimizedControlHistory )
                    {
                        using namespace utility::observer;
                        pControlObserver->Set( DataState::RECENTLY_UPDATED );

                        (*pOptimizedControlHistory) = optimizer.problem().control();

                        (*pOptimizedFidelity)   = optimizer.problem().fidelity();

                        ++(*pOptimizerIteration);

                        if (optimizer.didRestart())
                        {
                            ++(*pBFGSRestarts);
                        }

                        (*pOptimizerIterationFidelityHistory).push_back(RVec{double(*pOptimizerIteration),(*pOptimizedFidelity)}) ;

                        (*pOptimizerFinalWaveFunctionAbsSquare) = optimizer.problem()._dynamicStateForward()(optimizer.problem().control().size()-1).absSquare().vec();
                    }
                });
                const auto stepSizeFinder = makeInterpolatingStepSizeFinder(10.0,1.0);
                const auto restarter = makeStepTimesYBfgsRestarter(1e-14);
                auto GRAPE = makeGrape_bfgs_L2(problem,stopper,collector,stepSizeFinder, restarter);
                auto pGRAPE = std::make_shared<decltype(GRAPE)>(std::move(GRAPE));

                auto optVars = std::make_tuple( pStopSwitch, pIsRunning, Mutex, 
                                                pControlObserver, pOptimizerIteration, pOptimizedFidelity, 
                                                pOptimizedControlHistory, pOptimizerStopMessage, pOptimizerIterationFidelityHistory, 
                                                pOptimizerFinalWaveFunctionAbsSquare, pOptimizerStopCondition, pBFGSRestarts);
                return std::make_tuple(pGRAPE,optVars);
            };

            return optimizerMaker;
        };

    }
}
