#pragma once

#include "ObjectWrappers/RVecWrapper.h"

class ControlWrapper
{
	///
	///	 Serves as a simplified version of Controls that is exposed to both C++ and C# layers of code.
	///  Intended as a faster alterantive to std::vector<qengine::RVec> in terms of 
	///	 copying between C# and C++ layers.
	/// 
public:
	ControlWrapper() : mCols(0), mRows(0)
	{
		mControl = qengine::Control{};
	}

	ControlWrapper(const int cols, const int rows) : mCols(cols), mRows(rows) 
	{
		qengine_assert(rows >= 0 || cols >= 0, "Negative dimensions for ControlWrapper");
		mControl = qengine::Control::zeros(cols, rows, 0.0);
	}

	ControlWrapper(qengine::Control && ctrl) : mCols(ctrl.size()), mRows(ctrl.paramCount()), mControl( std::move(ctrl) ) { }

	double at(const int col, const int row) const
	{
		return mControl.at(col).at(row);
	}
	
	void set(const int col, const int row,  const double val) 
	{
		mControl.at(col).at(row) = val;
	}

	int count() const
	{
		return mCols * mRows;
	}

	int paramCount() const
	{
		return mRows;
	}

	int size() const
	{
		return mCols;
	}

	qengine::Control getControl() const
	{
		return mControl;
	}

	qengine::Control & getControlRef()
	{
		return mControl;
	}

private:
	size_t mCols;
	size_t mRows;
	qengine::Control mControl;
};