#pragma once
#include <cstdio>

#include "Utilities/Utilities.h"

class RVecWrapper
{
public:
	using RVec = qengine::Vector<double>;

	RVecWrapper() = default;

	RVecWrapper(const size_t vecSize);
	
	RVecWrapper(const int vecSize);

	RVecWrapper(const size_t vecSize, const double val);
	
	RVecWrapper(const int vecSize, const double val);

	RVecWrapper(const RVecWrapper & rhs) : mVec(rhs.mVec) { }

	RVecWrapper(const RVec & rhs) : mVec( rhs ) { }

	RVecWrapper(RVec && vec) : mVec( std::move(vec) ){ }

	double at(const size_t index) const;
	double at(const int index) const;
	
	void set(const size_t index, const double val);	
	void set(const int index, const double val);	

	size_t size() const;

	const RVec & GetVecRef() const;

	RVec GetVec() const;

	virtual ~RVecWrapper() { }

private:
	RVec mVec;
};

RVecWrapper & GetRVecRef(RVecWrapper & vec);

RVecWrapper GetRVecVal(RVecWrapper vec);

void CopyRVec(RVecWrapper & destination, const RVecWrapper & source);
