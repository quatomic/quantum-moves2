#pragma once
#include "Utilities/Utilities.h"

namespace qengine
{
namespace levelwrapper
{
namespace utility
{
namespace observer
{

	enum class DataState {UP_TO_DATE, RECENTLY_UPDATED, INITIAL};

	class DataObserver
	{
		DataState mState;
		std::mutex mMutex;
	public:	

		DataObserver() : mState( DataState::INITIAL ) {  }
		
		void Set(const DataState newState) noexcept
		{
			std::lock_guard<std::mutex> lock( mMutex );
			mState = newState;
		}

		DataState Get() noexcept
		{
			std::lock_guard<std::mutex> lock( mMutex );
			return mState;
		}
	};	

}
}
}
}