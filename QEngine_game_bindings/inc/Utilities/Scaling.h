#pragma once
#include <functional>
#include <utility>
#include "Utilities/Utilities.h"

namespace qengine
{
namespace levelwrapper
{
namespace utility
{

	
	using CoordinateScalingFunc = typename std::function<double(double)>;
	using CoordinateScalingFuncPair = std::pair<CoordinateScalingFunc, CoordinateScalingFunc>;
 
	enum class ScalingType {FORWARD, INVERSE, IDENTITY};

	template <typename Control, typename ControlPoint>
	class Scaling
	{
	public:
		using FuncType = typename std::function<ControlPoint(ControlPoint)>; // use closures to contain every required detail or make the function signatures more sophisticated?

		Scaling(const FuncType scale, const FuncType invScale);

		Scaling();

		Scaling(const Scaling & other);

		ControlPoint Scale(const ControlPoint &, const ScalingType) const;

		void ScaleInPlace(ControlPoint &, const ScalingType) const;

		Control MapScale(const Control &, const ScalingType) const;

		void MapScaleInPlace(Control &, const ScalingType) const;

		void SetScaling(const FuncType) noexcept;

		void SetInvScaling(const FuncType) noexcept;

		FuncType GetScalingFunc() const { return mfScale; }

		FuncType GetInvScalingFunc() const { return mfInvScale; }

		virtual ~Scaling() = default;

	private:
		
		const FuncType & GetScalingFunc(const ScalingType scalingType) const
		{
			switch (scalingType)
			{
				case ScalingType::FORWARD:
				{
					return mfScale;
				}
				case ScalingType::INVERSE:
				{
					return mfInvScale;
				}
				case ScalingType::IDENTITY:
				{
					return mfIdentity;
				}
				default:
				{
					// silence the compiler
					qengine_assert(false, "Scaling of this type is undefined.");
					return mfIdentity; // placeholder return variable
				}
			}
		}

		FuncType mfScale;
		FuncType mfInvScale;
		FuncType mfIdentity;
	};

	CoordinateScalingFuncPair GetRangeScalings(const RVec & rangeVec);
	CoordinateScalingFuncPair GetCoordinateReferenceScalings(const RVec & p0, const RVec & p1, const size_t coordinate);
}
}
}