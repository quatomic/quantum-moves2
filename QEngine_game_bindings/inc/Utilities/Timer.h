#pragma once
#include <chrono>
#include <iostream>
#include <cassert>
#include <string>

class Timer
{
public:	
	using Clock = std::chrono::high_resolution_clock;
	using TimePoint = std::chrono::time_point<Clock>;
	Timer(const std::string & msg) : mMessage( msg ), mStart( TimePoint{} ), mStop( TimePoint{} ) 
	{ 

	}

	void SetMessage(const std::string & msg)
	{
		mMessage = msg;
	}

	void SetStart(const std::string & msg)
	{
		SetMessage( msg );
		Start();
	}

	void Start()
	{
		mStart = GetTime();
	} 

	void Stop()
	{
		mStop = GetTime();
	}

	void StopPrint()
	{
		Stop();
		const auto durationUS = std::chrono::duration_cast<std::chrono::microseconds> (mStop - mStart);
		const auto durationMS = std::chrono::duration_cast<std::chrono::milliseconds> (mStop - mStart);
		std::cout << "Time elapsed for \"" << mMessage << "\": " << durationMS.count() << " ms (" << durationUS.count() << " us)" << std::endl;
	}

	void StopPrint(const size_t iters)
	{
		assert( iters );
		Stop();
		const auto durationUS = std::chrono::duration_cast<std::chrono::microseconds> (mStop - mStart);
		const auto durationMS = std::chrono::duration_cast<std::chrono::milliseconds> (mStop - mStart);
		std::cout << "Time elapsed for \"" << mMessage << "\": " << durationMS.count() / (double) iters << " ms (" << durationUS.count()/ (double) iters << " us) per iteration" << std::endl;
	}

	void Reset()
	{
		mStart = TimePoint{};
		mStop = TimePoint{};
	}

	TimePoint GetTime()
	{
		return std::chrono::high_resolution_clock::now();
	}

private:
	std::string mMessage;
	TimePoint mStart;
	TimePoint mStop;

};