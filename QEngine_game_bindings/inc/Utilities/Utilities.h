#pragma once
#include <mutex>
#include "qengine/qengine.h"


namespace qengine
{
namespace levelwrapper
{

enum class ControlAxis {X = 0, Y = 1, XY = 2}; // integer values allow to propagate the enum to the C# layer 
enum class WavePart {NORM_SQUARED, RE, IM, NONE};
enum class WavefuncType {TARGET, INIT, CURRENT};

using RVec = qengine::Vector<double>;

struct OptimizationBoundaries
{
    const RVec mLeftBoundary;
    const RVec mRightBoundary;
    const double mBoundaryStrength;
};

namespace utility
{


template <typename Func>
RVec MapRVec(const Func & foo, const RVec & input)
{
	RVec output(input.size());
	for (size_t i = 0 ; i < output.size() ; ++i)
	{
		output.at(i) = foo( input.at(i) );
	}
	return output;
}

using MutexType = std::recursive_mutex;

}
}
}

