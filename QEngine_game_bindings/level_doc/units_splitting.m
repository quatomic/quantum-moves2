%clc


% POTENTIAL FROM https://journals.aps.org/pra/pdf/10.1103/PhysRevA.73.033619
% EQ. 10 W. PHI=0

%% SI units and quantities
Hz = 1;    % [Hz]
KHz = 1e3; % [Hz]
MHz = 1e6; % [Hz]
micrometer = 1e-6; % [m]
nm = 1e-9; % [m]
ms = 1e-3; % [s]

T = 1; % [T]
mT = 1e-3*T; % [T]


GaussFromTesla = 1e4*T; % [Gauss]
TeslaFromGauss = 1/GaussFromTesla;
bohr_magneton = 9.274009e-24; % J/Tesla 

h = 6.626070040e-34; % [J*s]
hbar = h/(2*pi); % [J*s]
mRb = 86.9091835*1.660539040e-27; % [kg]
bohr_radius = 0.529177e-10; % [m]
kB = 1.38064852e-23; % [m^2 kg/(s^2 K)] boltzmann constant
%aRb = 110*bohr_radius; % [m] scattering length
aRb = 100.44*bohr_radius;% [m] Three-dimensional s-scattering length |1,-1>.                        
g3D = 4*pi*hbar^2*aRb/mRb;

% Conversion factors between SI units
HzFromJoule = 1/h;  JouleFromHz = 1/HzFromJoule;
KHzFromHz = 1e-3;   HzFromKHz = 1/KHzFromHz;
KHzFromJoule = KHzFromHz*HzFromJoule; JouleFromKHz = 1/KHzFromJoule;

MicrokelvinFromJoule = 1e6/kB;

mf  = 2;    % https://journals.aps.org/pra/pdf/10.1103/PhysRevA.75.023602
gf  = 1/2;  % https://steck.us/alkalidata/rubidium87numbers.1.6.pdf


%gf = 2;
%mf  = 0.5;

%gf = 1;
%mf = 1;

%gf = -0.5;
%mf = -1;


%% Quantities

w = 1.26*2*pi*MHz; % [Hz]

Gr    = TeslaFromGauss * 0.2*1/micrometer; % [G/m]

BI    = TeslaFromGauss * 1; % [G]
Bdet =  hbar*w/abs(gf*bohr_magneton); % [G] 
Bdet =  Bdet/2; % <---- FUDGE FACTOR!!

% '_u' means expression is evaluated for the u below
u_val=1;

Brf_u = TeslaFromGauss * (0.5+0.3*u_val); % [G] 
Bs_u  = TeslaFromGauss * sqrt((Gr*micrometer)^2 + BI^2); % [G]
Bu_u  = TeslaFromGauss * Brf_u*BI/(2*Bs_u); % [G]

pV = TeslaFromGauss*bohr_magneton*gf*mf; % [J], potential front factor after moving all units out of sqrt. This corresponds to JJ's scaling of the entire potential

% not so important:
%V0_u = mf*gf*bohr_magneton*TeslaFromGauss*Brf_u/(4*BI)*sqrt(4*BI^2 + BC^2-2*Gr^2*rho0_u^2);
%BC = 2*sqrt(BI*hbar*Delta/abs(gf*bohr_magneton));
%rho0_u = 1/(sqrt(2)*Gr)*sqrt(Brf_u^2-BC^2)
%Delta = gf*bohr_magneton*TeslaFromGauss*BI/hbar-w; % [Hz]


% 1D COUPLING BASED ON SHAKEUP
wx = 2*pi*16.3*Hz;       % Axial frequency of the trap (Hz).
wy = 2*pi*1.83*KHz;      % Frequency along the (transverse) y direction (Hz).
wz = 2*pi*2.58*KHz;      % Frequency along the (transverse) z direction (Hz).
wr = sqrt(wy*wz);        %           

ax = sqrt(hbar/mRb/wx);           % Axial oscillator length (m).
az = sqrt(hbar/mRb/wz);           % Oscillator length along the (transverse) z direction (m).
ar = sqrt(hbar/mRb/wr);           % Transverse mean oscillator length (m).

Natoms = 700;
falpha=@(alpha)alpha^3*(alpha+5)^2-(15*Natoms*ar*aRb)^2/ax^4 ;
alpha = fzero(falpha,0.5);
L = ax^2*sqrt(alpha)/ar;
Ix = alpha^2*L*(alpha^2 + 9*alpha + 21)/(315*(aRb*Natoms)^2);
g1D = g3D*Ix/(sqrt(2*pi)*az);
%g1D = g1D*Natoms;

x = linspace(-4*micrometer,4*micrometer,1000); 


%% Conversions to simulation units

convert = 1
if convert
    c = 0.5;
    SI = [w,BI,Bdet,Brf_u,Bs_u, Bu_u, Gr, pV, g1D,c];
    
    % natural units: denoted by e.g. x0
    % simulation quantities: denoted by primes, e.g. x'
    % si units: obtained by multiplying e.g. x=x0*x'

    % fundamental units
    x0 = micrometer;     % x = x0*x' (length) 
    t0 = ms;             % t = t0*t' (time)
    m0 = mRb;            % m = m0*m' (mass)
    b0 = 0.1*mT;         % B = b0*B' (magnetic field)
    
    
    % derived units
    c  = hbar*t0/(2*m0*x0^2);   % kinetic factor 
    E0 = hbar/t0;               % E = E0*E' (energy)
    w0 = 2*pi*MHz;              % w = w0*w' (ang. freq.)    
    k0 = 1/x0;                  % k = k0*k' (spat. freq.)   
    
    % Conversion to nondimensionalized simulation quantities (divide all eq.
    % above by x0, e.g. x' = x/x0) 
    % primes are dropped.

    x = x/x0;
%    rho0_u = rho0_u/x0;
    bohr_radius = bohr_radius/x0;
    aRb = aRb/x0;
    
    pV = pV/E0
%    V0_u = V0_u/E0;
    
    w = w/w0;
%    Delta = Delta/w0;
    
    BI = BI/b0;
%    BC = BC/b0;
    
    Brf_u = Brf_u/b0;
    Bs_u = Bs_u/b0;
    Bu_u = Bu_u/b0;
    Gr = Gr/(b0/micrometer);
    
    Bdet = Bdet/b0; 

    g1D = g1D*(Natoms/(E0*x0)); % psi = psi_0* psi' = 1/sqrt(x0) psi' gives the additional x0 scaling
    
    % Tables
    names = {'Length','Time','Energy','Ang. freq.','Mass'};
    conversion_factors = [x0,t0,E0,w0,m0];
    Table1 = table(conversion_factors','rowNames',names,'VariableNames',{'SIfromSim_Factors'})

    sim = [w,BI, Bdet, Brf_u,Bs_u,Bu_u, Gr, pV,g1D,c];
    names = {'w','BI','Bdet','Brf_1','Bs_1','Bu_1', 'Gr', 'pV','g1D','c'};
    Table2 = table(SI',sim','rownames',names,'VariableNames',{'SI','Simulation'})

    KHzFromE0 = KHzFromJoule*E0; E0FromKHz = 1/KHzFromE0;
    MicrokelvinFromE0 = MicrokelvinFromJoule*E0;
    

end

Brf = @(u) 0.5+0.3*u;
%Brf = @(u) 0.9;
%Brf = @(u) 1.2;
%Brf = @(u) 0.5+0.3*u;

Bs = sqrt((Gr*x).^2+1^2);
Bu = @(u) Brf(u)./(2*Bs);

%V = @(u) pV*sqrt( ( Bs - Bdet ) .^ 2 + ( 0.5 * Brf(u) * BI ./ Bs ) .^ 2 );
V = @(u) pV*sqrt( ( Bs - Bdet ) .^ 2 + Bu(u).^ 2 );

V_withV0 = @(u) V(u) - min(V(u));

hold off
plot(x,V_withV0(0),'DisplayName','u=0','linewidth',3)
hold on
%plot(x,MicrokelvinFromE0*V_withV0(0.5),'DisplayName','u=0.5','linewidth',3)
plot(x,V_withV0(1),'DisplayName','u=1','linewidth',3)
%ylim([0, 3])
xlim([-3,3])
xlabel('$x [\mu m]$','interpreter','latex','fontsize',20)
ylabel('$(V-V_0)/k_B [\mu K]$','interpreter','latex','fontsize',20)
legend show
leg = gca;
set(leg.Legend,'interpreter','latex','fontsize',20,'location','north')

