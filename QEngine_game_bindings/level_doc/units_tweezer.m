%% SI units and quantities
Hz = 1;    % [Hz]
KHz = 1e3; % [Hz]
micrometer = 1e-6; % [m]
nm = 1e-9; % [m]
ms = 1e-3; % [s]
microsecond = 1e-6; % [s]

h = 6.626070040e-34; % [J*s]
hbar = h/(2*pi); % [J*s]
mRb = 86.9091835*1.660539040e-27; % [kg]
bohr_radius = 0.529177e-10; % [m]

% Conversion factors between SI units
HzFromJoule = 1/h;  JouleFromHz = 1/HzFromJoule;
KHzFromHz = 1e-3;   HzFromKHz = 1/KHzFromHz;
MHzFromKHz = 1e-3;  KHzFromMHz = 1/MHzFromKHz;
KHzFromJoule = KHzFromHz*HzFromJoule; JouleFromKHz = 1/KHzFromJoule;
MHzFromJoule = MHzFromKHz*KHzFromHz*HzFromJoule; JouleFromMHz = 1/MHzFromJoule;

eVFromJoule = 6.2415091e18; JouleFromeV = 1/eVFromJoule; 


%% Quantities 

aLatt = 1064*nm/2; % Typical lattice spacing

Er = hbar^2*pi^2/(2*mRb*aLatt^2);  %[J]
FiveHundredEr = 500*Er;

%sigma = 710*nm; % Regal Optical waist of beam [m] 
%sigma = 133*nm; % Nature Optical waist of beam [m] 
sigma = aLatt/2; % Optical waist of beam [m] https://journals.aps.org/pra/pdf/10.1103/PhysRevA.84.032322
%sigma = aLatt/4; % Optical waist of beam [m] 

%L = 854*nm;  % Distance between gaussian centroids [m]
%A = JouleFromMHz*50; % Transport tweezer depth [J]
%A = JouleFromKHz*53.42; % Transport tweezer depth [J]
%A = JouleFromMHz*1; % Transport tweezer depth [J]
A = 500*Er; % Transport tweezer depth [J] https://journals.aps.org/pra/pdf/10.1103/PhysRevA.84.032322

wOsc = 2/sigma*sqrt(A/mRb); % Oscillator freq. of harmonic approx. to tweezer [J]
lOsc = sqrt(hbar/(mRb*wOsc)); % Oscillator length of harmonic approx. to tweezer [J]




x = linspace(-4*micrometer,4*micrometer,10000); 
dxC = 1.57*micrometer; % Tweezer separation during loading [m]
%% Conversions to simulation units

convert = 1
units = 0;
if convert
    c = 0.5;
    SI = [Er,FiveHundredEr,A,sigma,lOsc,wOsc,dxC,aLatt,c];
    
    % natural units: denoted by e.g. x0
    % simulation quantities: denoted by primes, e.g. x'
    % si units: obtained by multiplying e.g. x=x0*x'
%% NATURE UNITS
    if units==0
        % fundamental units
        x0 = aLatt;    % x = x0*x' (length) 
        m0 = mRb;      % m = m0*m' (mass)

        % derived units
        %c  = hbar*t0/(2*m0*x0^2);   % kinetic factor 
        t0 = 2*m0*c*x0^2/hbar;
        E0 = hbar/t0;               % E = E0*E' (energy)
        f0 = 1/t0;                  % f = f0*f' (frequency)
        w0 = 2*pi*f0;               % w = w0*w' (ang. freq.)    
        k0 = 1/x0;                  % k = k0*k' (spat. freq.)   
    end
%% ALTERNATIVE UNITS 1
    if units==1
        % fundamental units
        x0 = aLatt;    % x = x0*x' (length) 
        t0 = 0.3*ms;  % t = t0*t' (time)
        m0 = mRb;      % m = m0*m' (mass)

        % derived units
        c  = hbar*t0/(2*m0*x0^2);   % kinetic factor 
        E0 = hbar/t0;               % E = E0*E' (energy)
        f0 = 1/t0;                  % f = f0*f' (frequency)
        w0 = 2*pi*f0;               % w = w0*w' (ang. freq.)    
        k0 = 1/x0;                  % k = k0*k' (spat. freq.)   
    end
%% ALTERNATIVE UNITS 2
    if units == 2
        % fundamental units
        x0 = micrometer;        % x = x0*x' (length) 
        m0 = mRb;               % m = m0*m' (mass)
        E0 = A;    % E = E0*E' (energy)

        % derived units
        t0 = hbar/E0;               % t = t0*t' (time)
        c  = hbar*t0/(2*m0*x0^2);   % kinetic factor 
        f0 = 1/t0;                  % f = f0*f' (frequency)
        w0 = 2*pi*f0;               % w = w0*w' (ang. freq.)    
        k0 = 1/x0;                  % k = k0*k' (spat. freq.)   
    end
%%     
    % Conversion to nondimensionalized simulation quantities (divide all eq.
    % above by x0, e.g. x' = x/x0) 
    % primes are dropped.

    
    x = x/x0;
    sigma = sigma/x0;
    dxC = dxC/x0;
    bohr_radius = bohr_radius/x0;
    aLatt = aLatt/x0;
    lOsc = lOsc/x0;
    
    A = A/E0;
    Er = Er/E0;
    FiveHundredEr = FiveHundredEr/E0;
    
    wOsc = wOsc/w0;
    
    % Tables
    names = {'Length','Time','Energy','Freq.','Ang. freq.','Mass'};
    conversion_factors = [x0,t0,E0,f0,w0,m0];
    Table1 = table(conversion_factors','rowNames',names,'VariableNames',{'SIfromSim_Factors'})

     sim = [Er,FiveHundredEr,A,sigma,lOsc,wOsc,dxC,aLatt,c];
     names = {'Erecoil','FiveHundredEr','A','sigma','lOsc','wOsc','dxC','aLatt','kin factor'};
     Table2 = table(SI',sim','rownames',names,'VariableNames',{'SI','Simulation'})
 
     KHzFromE0 = KHzFromJoule*E0; E0FromKHz = 1/KHzFromE0;
     MHzFromE0 = MHzFromJoule*E0; E0FromMHz = 1/MHzFromE0;

    
    quantities = [KHzFromE0*A,lOsc/sigma,A/FiveHundredEr,KHzFromHz*w0*wOsc/(2*pi),1/nm*x0*lOsc];
    names = {'AKHz','lOsc/sigma','A/500Er','wOscKHz','lOscnm'};
    Table3 = table(quantities','rowNames',names,'VariableNames',{'Quantities_of_Interest'})

     
     

end