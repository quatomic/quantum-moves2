using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Threading;
using System.Threading.Tasks;

public class InvalidLengthException : Exception
{
	private int mInSize;
	private int mExpectedSize;

	public InvalidLengthException(int inSize, int expectedSize, string message) : base( message )
	{
		mInSize = inSize;
		mExpectedSize = expectedSize;
	}

	public int GetInputSize()
	{
		return mInSize;
	}

	public int GetExpectedSize()
	{
		return mExpectedSize;
	}
}

class RVecConversion
{
	/// Conversion from RVecWrapper to the C# container of choice and backwards
	public static double[] ToDArray(ref RVecWrapper rvec)
	{
		uint size = rvec.size();
		double[] output = new double[ size ];
		for (uint i = 0 ; i < size ; ++i)
		{
			output[ i ] = rvec.at( i );
		}
		return output;
	}

	public static double[] ToDArray(RVecWrapper rvec)
	{
		uint size = rvec.size();
		double[] output = new double[ size ];
		for (uint i = 0 ; i < size ; ++i)
		{
			output[ i ] = rvec.at( i );
		}
		return output;
	}

	public static RVecWrapper FromDArray(ref double[] array)
	{
		int size = array.Length;
		RVecWrapper wrapper = new RVecWrapper( (uint) size );
		for (uint i = 0 ; i < size ; ++i)
		{
			wrapper.set(i, array[ i ]);
		}
		return wrapper;
	}		

	public static RVecWrapper FromDArray(double[] array)
	{
		int size = array.Length;
		RVecWrapper wrapper = new RVecWrapper( (uint) size );
		for (uint i = 0 ; i < size ; ++i)
		{
			wrapper.set(i, array[ i ]);
		}
		return wrapper;
	}

	public static void ToDArray(ref RVecWrapper rvec, ref double[] output)
	{
		int size = (int) rvec.size();
		if (output.Length != size)
		{
			throw new InvalidLengthException(output.Length, size, "Invalid output array length for RVec conversion.");
		}
		for (uint i = 0 ; i < size ; ++i)
		{
			output[ i ] = rvec.at( i );
		}
	}

	public static void FromDArray(ref double[] array, ref RVecWrapper wrapper)
	{
		int size = array.Length;
		if (wrapper.size() != size)
		{
			throw new InvalidLengthException((int) wrapper.size(), size, "Invalid output wrapper length for RVec conversion.");
		}
		for (uint i = 0 ; i < size ; ++i)
		{
			wrapper.set(i, array[ i ]);
		}
	}	

	public static List<double> ToDList(ref RVecWrapper rvec)
	{
		int size = (int) rvec.size();
		List<double> output = new List<double>( size );
		for (int i = 0 ; i < size ; ++i)
		{
			output.Add( rvec.at( i ) );
		}
		return output;
	}

	public static List<double> ToDList(RVecWrapper rvec)
	{
		int size = (int) rvec.size();
		List<double> output = new List<double>( size );
		for (int i = 0 ; i < size ; ++i)
		{
			output.Add( rvec.at( i ) );
		}
		return output;
	}

	public static RVecWrapper FromDList(ref List<double> list)
	{
		int size = list.Count;
		RVecWrapper wrapper = new RVecWrapper( (uint) size );
		for (int i = 0 ; i < size ; ++i)
		{
			wrapper.set(i, list[ i ]);
		}
		return wrapper;
	}

	public static RVecWrapper FromDList(List<double> list)
	{
		int size = list.Count;
		RVecWrapper wrapper = new RVecWrapper( (uint) size );
		for (int i = 0 ; i < size ; ++i)
		{
			wrapper.set(i, list[ i ]);
		}
		return wrapper;
	}

	public static void ToDList(ref RVecWrapper rvec, ref List<double> output)
	{
		int size = (int) rvec.size();
		if (output.Capacity != size)
		{
			throw new InvalidLengthException(output.Capacity, size, "Invalid output list capacity for RVec conversion.");
		}
		// erase stuff from the list
		output.Clear();

		for (int i = 0 ; i < size ; ++i)
		{
			output.Add( rvec.at( i ) );
		}
	}

	public static void FromDList(ref List<double> list, ref RVecWrapper wrapper)
	{
		int size = list.Count;
		if (wrapper.size() != size)
		{
			throw new InvalidLengthException((int) wrapper.size(), size, "Invalid output wrapper length for RVec conversion.");
		}
		for (int i = 0 ; i < size ; ++i)
		{
			wrapper.set(i, list[ i ]);
		}
	}

	public static Vector2 ToVector2(ref RVecWrapper rvec)
	{
		if (rvec.size() != 2)
		{
			throw new InvalidLengthException((int) rvec.size(), 2, "Invalid input wrapper length for RVec conversion.");
		}
		return new Vector2((float) rvec.at( 0 ),(float)  rvec.at( 1 ));
	}

	public static Vector2 ToVector2( RVecWrapper rvec)
	{
		if (rvec.size() != 2)
		{
			throw new InvalidLengthException((int) rvec.size(), 2, "Invalid input wrapper length for RVec conversion.");
		}
		return new Vector2((float) rvec.at( 0 ),(float)  rvec.at( 1 ));
	}

	public static void ToVector2(ref RVecWrapper rvec, ref Vector2 output)
	{
		if (rvec.size() != 2)
		{
			throw new InvalidLengthException((int) rvec.size(), 2, "Invalid input wrapper length for RVec conversion.");
		}
		output.x = (float) rvec.at( 0 );
		output.y = (float) rvec.at( 1 );
	}	

	public static RVecWrapper FromVector2(ref Vector2 input)
	{
		RVecWrapper newWrap = new RVecWrapper(2);
		newWrap.set( 0, input.x);
		newWrap.set( 1, input.y);
		return newWrap;
	}	

	public static RVecWrapper FromVector2(Vector2 input)
	{
		RVecWrapper newWrap = new RVecWrapper(2);
		newWrap.set( 0, input.x);
		newWrap.set( 1, input.y);
		return newWrap;
	}		

	public static void FromVector2(ref Vector2 input, ref RVecWrapper rvec)
	{
		if (rvec.size() != 2)
		{
			throw new InvalidLengthException((int) rvec.size(), 2, "Invalid output wrapper length for RVec conversion.");
		}
		rvec.set( 0, input.x);
		rvec.set( 1, input.y);
	}


    public static List<Vector2> ToVector2List(ref stdVec_RVecWrapper input)
	{
		List<Vector2> newList = new List<Vector2>(input.Count);
		for (int i = 0 ; i < input.Count ; ++i)
		{
			newList.Add( new Vector2((float) input[i].at( 0 ), (float) input[i].at( 1 )) );
		}
		return newList;
	}

    public static List<Vector2> ToVector2List(stdVec_RVecWrapper input)
	{
		List<Vector2> newList = new List<Vector2>(input.Count);
		foreach (RVecWrapper element in input) 
		{
      		newList.Add( new Vector2((float) element.at( 0 ), (float) element.at( 1 )) );
		}
		return newList;
	}	

	public static List<Vector2> ToVector2ListFromRVecWrapper(ref RVecWrapper input)
	{
		List<Vector2> newList = new List<Vector2>((int) input.size());

		int i = 0;
		while (i + 1 < (int)  input.size())
		{
			newList.Add(new Vector2((float) input.at( i ), (float) input.at( i + 1)));
			i += 2;
		}
		return newList;		
	}

	public static List<Vector2> ToVector2List(ControlWrapper input)
	{
		if (input.paramCount() != 2)
		{
			throw new InvalidLengthException((int) input.paramCount(), 2, "Invalid ControlWrapper for fast control vector passing.");
		}
		List<Vector2> newList = new List<Vector2>((int) input.size());
		for (int i = 0 ; i < input.size() ; ++i)
		{
			newList.Add(new Vector2((float) input.at( i, 0 ), (float) input.at(i, 1)));
		}
		return newList;		
	}

	public static List<Vector2> ToVector2List(ref ControlWrapper input)
	{
		if (input.paramCount() != 2)
		{
			throw new InvalidLengthException((int) input.paramCount(), 2, "Invalid ControlWrapper for fast control vector passing.");
		}
		List<Vector2> newList = new List<Vector2>((int) input.size());
		for (int i = 0 ; i < input.size() ; ++i)
		{
			newList.Add(new Vector2((float) input.at( i, 0 ), (float) input.at(i, 1)));
		}
		return newList;		
	}


    public static void ToVector2List(ref stdVec_RVecWrapper input, ref List<Vector2> output)
	{
		if (input.Count == 0)
		{
			return;
		}

		if (output.Count != input.Count && (output.Count > 0))
		{
			throw new InvalidLengthException((int) output.Count, 2, "Invalid output RVec vector length for RVec conversion.");
		}

		int i = 0;
		foreach(RVecWrapper element in input)
		{
			Vector2 tmp = new Vector2((float) element.at( 0 ),(float) element.at( 1 ));
			if (output.Count > 0)
			{
				output[ i++ ] = tmp;
			}
			else
			{
				output.Add( tmp );
			}
		}
	}

    public static void ToVector2List(ref ControlWrapper input, ref List<Vector2> output)
	{
		if (input.size() == 0)
		{
			return;
		}

		if (output.Count != input.size())
		{
			throw new InvalidLengthException((int) output.Count, 2, "Invalid output RVec vector length for RVec conversion.");
		}

		for (int i = 0 ; i < input.size() ; ++i)
		{
			output[ i ] = new Vector2((float) input.at( i,  0 ), (float) input.at( i, 1 ));			
		}
	}

    public static stdVec_RVecWrapper FromVector2List(ref List<Vector2> input)
	{
                stdVec_RVecWrapper wrapper = new stdVec_RVecWrapper();
		foreach (Vector2 element in input)
		{
			RVecWrapper tmp = new RVecWrapper(2);
			tmp.set( 0, element.x);
			tmp.set( 1, element.y);
			wrapper.Add( tmp );		
		}
		return wrapper;
	}

    public static stdVec_RVecWrapper FromVector2List(List<Vector2> input)
	{
        stdVec_RVecWrapper wrapper = new stdVec_RVecWrapper();
		foreach (Vector2 element in input)
		{
			RVecWrapper tmp = new RVecWrapper(2);
			tmp.set( 0, element.x);
			tmp.set( 1, element.y);
			wrapper.Add( tmp );		
		}
		return wrapper;
	}

    public static ControlWrapper FromVector2ListToControlWrapper(ref List<Vector2> input)
	{
        ControlWrapper wrapper = new ControlWrapper(input.Count, 2);
		for (int i = 0 ; i < input.Count; ++i)
		{
			wrapper.set(i, 0, input[i].x);
			wrapper.set(i, 1, input[i].y);
		}
		return wrapper;
	}

    public static ControlWrapper FromVector2ListToControlWrapper(List<Vector2> input)
	{
        ControlWrapper wrapper = new ControlWrapper(input.Count, 2);
		for (int i = 0 ; i < input.Count; ++i)
		{
			wrapper.set(i, 0, input[i].x);
			wrapper.set(i, 1, input[i].y);
		}
		return wrapper;
	}

    public static void FromVector2List(ref List<Vector2> input, ref ControlWrapper output)
	{
		if (input.Count == 0)
		{
			return;
		}

		if (output.size() != input.Count)
		{
			throw new InvalidLengthException((int) output.size(), 2, "Invalid output ControlWrapper length for conversion.");
		}

		for (int i = 0 ; i < input.Count ; ++i)
		{
			output.set(i, 0, input[i].x);
			output.set(i, 1, input[i].y);
		}
	}		

    public static void FromVector2List(ref List<Vector2> input, ref stdVec_RVecWrapper output)
	{
		if (input.Count == 0)
		{
			return;
		}

		if (output.Count != input.Count && (output.Count > 0))
		{
			throw new InvalidLengthException((int) output.Count, 2, "Invalid output RVec vector length for RVec conversion.");
		}

		int i = 0;
		foreach (Vector2 element in input)
		{
			RVecWrapper tmp = new RVecWrapper(2);
			tmp.set( 0, element.x);
			tmp.set( 1, element.y);
			if (output.Count > 0)
			{
				output[ i++ ] = tmp;
			}
			else
			{
				output.Add( tmp );
			}
		}
	}			

}


