using System;
using System.Collections.Generic;

// NOTE ALL CURVES ARE RETURNED IN LOG-FIDELITY

public static class FidelityCurveEngine
{
    public static List<double> FidelityListToOLogFidelityList(List<double> curve)
    {
        var mappedCurve = new List<double>();
        foreach (var curveValue in curve)
        {
            #if WRAPPER_TESTS
                    mappedCurve.Add(curveValue);
            #else
                    mappedCurve.Add(GameManager.Instance.CurrentLevel.GetFidelityLogscale(curveValue));
            #endif            
        }
        return mappedCurve;
    }

    public static List<double> ScaleCurve(List<double> referenceCurve, double scalingFactor)
    {
        var scaledCurve = new List<double>();

        foreach (var bucketValue in referenceCurve)
        {
            scaledCurve.Add(scalingFactor * bucketValue);
        }

        return scaledCurve;
    }

    public static List<double> GetChallengeCurveBottom() // NOTE ALL CURVES ARE RETURNED IN LOG-FIDELITY
    {
        return FidelityListToOLogFidelityList(new List<double> { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 });
    }

    public static List<double> GetChallengeCurveTop() // NOTE ALL CURVES ARE RETURNED IN LOG-FIDELITY
    {
        return FidelityListToOLogFidelityList(new List<double> { 0.999, 0.999, 0.999, 0.999, 0.999, 0.999, 0.999, 0.999, 0.999, 0.999, 0.999, 0.999 });
    }

    public static List<double> GetChallengeCurve1_ShakeUp()
    {
        return ScaleCurve((GetChallengeCurve3_ShakeUp()), 0.33);
    }

    public static List<double> GetChallengeCurve2_ShakeUp()
    {
        return ScaleCurve((GetChallengeCurve3_ShakeUp()), 0.66);
    }

    public static List<double> GetChallengeCurve3_ShakeUp() // NOTE ALL CURVES ARE RETURNED IN LOG-FIDELITY
    {
        return FidelityListToOLogFidelityList(new List<double> { 0.4381, 0.51455, 0.61513, 0.75831, 0.81177, 0.86384, 0.92944, 0.95007, 0.96731, 0.98502, 0.99162, 0.99604 }); // from 25/11
    }

    public static List<double> GetChallengeCurve1_BringHomeWater()
    {
        return ScaleCurve((GetChallengeCurve3_BringHomeWater()), 0.33);
    }

    public static List<double> GetChallengeCurve2_BringHomeWater()
    {
        return ScaleCurve((GetChallengeCurve3_BringHomeWater()), 0.66);
    }

    public static List<double> GetChallengeCurve3_BringHomeWater() // NOTE ALL CURVES ARE RETURNED IN LOG-FIDELITY
    {
        return FidelityListToOLogFidelityList(new List<double>  {2.56E-19, 1.93E-15, 0.000350065, 0.041546773, 0.327977306, 0.753296277, 0.964024702, 0.998436476, 0.999103966, 0.999103966, 0.999103966, 0.999103966} );  // Switched back into old reference T_QSL = 0.37 (REGAME T_QSL) but having interpolated F(T) points based on T_QSL = 0.255 (results from 17.01.19))
    }

    public static List<double> GetChallengeCurve1_Splitting()
    {
        return ScaleCurve((GetChallengeCurve3_Splitting()), 0.33);
    }

    public static List<double> GetChallengeCurve2_Splitting()
    {
        return ScaleCurve((GetChallengeCurve3_Splitting()), 0.66);
    }

    public static List<double> GetChallengeCurve3_Splitting() // NOTE ALL CURVES ARE RETURNED IN LOG-FIDELITY
    {
        return FidelityListToOLogFidelityList(new List<double> { 0.00067875, 0.0068207, 0.10319, 0.37767, 0.50773, 0.64942, 0.76883, 0.84273, 0.90533, 0.97803, 0.99714, 0.99905 });  // from 28/11
    }

}
