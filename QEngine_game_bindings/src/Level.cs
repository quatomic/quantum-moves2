using System;
using System.Collections.Generic;
using UnityEngine;

public class Level
{
        private LevelGPE mLevel;
        private double mFidelityLinThreshold = 0.25;
        private double mFidelityLogThreshold = 0.999;

        public Level()
        {
                mLevel = new LevelGPE();
#if WRAPPER_TESTS
                if ( Object.ReferenceEquals(mLevel, null) )
#else
                if ( UnityEngine.Object.ReferenceEquals(mLevel, null) )
#endif
                {
                        Console.WriteLine("Level constructor failed.");
                }
                mLevel.DoLoadLevel();
                CheckError();
        }

        public bool DidOptimizerRestart() 
        {
            bool output = mLevel.DidOptimizerRestart();
            CheckError();
            return output;
        }

        public int GetBFGSRestarts()
        {
            int output = mLevel.GetBFGSRestarts();
            CheckError();
            return output;
        }

        public int GetFPP()
        {
            int output = mLevel.GetFPP();
            CheckError();
            return output;
        }

        public void StartRecordingSteps()
        {
            mLevel.StartRecordingSteps();
            CheckError();
        }

        public void StopRecordingSteps()
        {
            mLevel.StopRecordingSteps();
            CheckError();
        }

        public OptimizerStopCondition GetOptimizerStopCondition()
        {
            return (OptimizerStopCondition) mLevel.GetOptimizerStopCondition();
        }

        public ControlAxis GetControlAxis()
        {
            return (ControlAxis) mLevel.GetControlAxis(); 
        }

        private void CheckError()
        {
            // Console.WriteLine("Checking for errors:" + mLevel.GetErrorState());
            switch((ErrorCode) mLevel.GetErrorState())
            {
                case ErrorCode.CONTROL_INCOMPLETE:
                {
                    mLevel.MarkErrorHandled();
                    throw new LevelException("Controls are incomplete. Can't change Level state.");
                }
                case ErrorCode.CONTROL_STILL_RECORDING:
                {
                    mLevel.MarkErrorHandled();
                    throw new LevelException("Controls are still being recorded. Can't change Level state.");
                }
                case ErrorCode.CONTROL_RECORDING_NOT_STARTED:
                {
                    mLevel.MarkErrorHandled();
                    throw new LevelException("Controls recording is not started. Can't change Level state.");
                }
                case ErrorCode.CONTROL_TOO_SHORT:
                {
                    mLevel.MarkErrorHandled();
                    throw new LevelException("Controls are too short. Can't change Level state.");
                }
                case ErrorCode.QENGINE_EXCEPTION:
                {
                    string errorMessage = mLevel.GetErrorBuffer();
                    mLevel.MarkErrorHandled();
                    throw new LevelException("QEngine exception: " + errorMessage);
                }
                default:
                {
                    break;
                }
            }
        }

        // Setters / void

        public void DoStep(Vector2 CursorPoint)
        {
            mLevel.DoStep( RVecConversion.FromVector2(ref CursorPoint) );
            CheckError();
        }




        // Level list
        public void DoLoadLevel1()
        {
                mLevel.DoLoadLevel1();
                CheckError();
        }

        public void DoLoadLevel2()
        {
                mLevel.DoLoadLevel2();
                CheckError();
        }

        public void DoLoadLevel3()
        {
                mLevel.DoLoadLevel3();
                CheckError();
        }

        public void DoLoadLevel4()
        {
                 mLevel.DoLoadLevel4();
                 CheckError();
        }

        public void DoLoadLevel5()
        {
                 mLevel.DoLoadLevel5();
                 CheckError();
        }

        public void DoLoadLevel6()
        {
                mLevel.DoLoadLevel6();
                CheckError();
        }

        public void DoLoadLevel7()
        {
                mLevel.DoLoadLevel7();
                CheckError();
        }

        public void DoLoadLevel()
        {
            mLevel.DoLoadLevel();
            CheckError();
        }

        /// "Special" levels

        public void DoLoadLevel_ShakeUp()
        {
                mLevel.DoLoadLevel_ShakeUp_1();
                CheckError();
        }

        public void DoLoadLevel_ShakeUp_1()
        {
                mLevel.DoLoadLevel_ShakeUp_0();
                CheckError();
        }

        public void DoLoadLevel_ShakeUp_2()
        {
                mLevel.DoLoadLevel_ShakeUp_2();
                CheckError();
        }

        public void DoLoadLevel_ShakeUp_3()
        {
                mLevel.DoLoadLevel_ShakeUp_3();
                CheckError();
        }


        public void DoLoadLevel_BringHomeWater()
        {
                mLevel.DoLoadLevel_BringHomeWater_0_0();
                CheckError();
        }

        public void DoLoadLevel_BringHomeWater_1()
        {
                mLevel.DoLoadLevel_BringHomeWater_1_0();
                CheckError();
        }

        public void DoLoadLevel_BringHomeWater_2() 
        {
                mLevel.DoLoadLevel_BringHomeWater_0_1();
                CheckError();
        }

        public void DoLoadLevel_BringHomeWater_3()
        {
                mLevel.DoLoadLevel_BringHomeWater_1_1();
                CheckError();
        }                        

        public void DoLoadLevel_Splitting()
        {
            mLevel.DoLoadLevel_Splitting_50();
            CheckError();
        }

        public void DoLoadLevel_Splitting_1()
        {
            mLevel.DoLoadLevel_Splitting_50_d();
            CheckError();
        }

        public void DoLoadLevel_Splitting_2()
        {
            mLevel.DoLoadLevel_Splitting_25_d();
            CheckError();
        }

        public void DoLoadLevel_Splitting_3()
        {
            mLevel.DoLoadLevel_Splitting_10_d();
            CheckError();
        }                        

        public void DoLoadLevel_Splitting_4()
        {
            mLevel.DoLoadLevel_Splitting_0_d();
            CheckError();
        }        

        ///

        public void DoResetLevel()
        {
                mLevel.DoResetLevel();
                CheckError();
        }

        public void DoOptimize(int iterations=10)
        {
            mLevel.DoOptimize(iterations);
            CheckError();
        }

        public void DoOptimizeStopRequest()
        {
            mLevel.DoOptimizeStopRequest();
            CheckError();
        }


        // Getters
        public int GetDim()
        {
            int output = (int) mLevel.GetDim();
            CheckError();
            return output;                  
        }

        public double GetFidelity()
        {
            double output = mLevel.GetFidelity();
            CheckError();
            return output;          
        }

        public double GetOptimizedFidelity()
        {
            double output = mLevel.GetOptimizedFidelity();
            CheckError();
            return output;                   
        }

        public double GetOptimizedFidelityLogscale()
        {
            double F = GetOptimizedFidelity();
            if(F <= mFidelityLinThreshold) return F;

            CheckError();
            double N = (mFidelityLogThreshold - mFidelityLinThreshold)/(Math.Log10(1-mFidelityLinThreshold) - Math.Log10(1-mFidelityLogThreshold));
            double M = mFidelityLinThreshold + N*Math.Log10(1-mFidelityLinThreshold);

            double retVal = -N * Math.Log10(1-F) + M;
            return retVal <= 1 ? retVal : 1;
        }

        public double GetFidelityLogscale(double F)
        {
            if ( (F >= 1) || Double.IsNaN( F ))
            {
                throw new ArgumentException("Invalid fidelity passed to GetFidelityLogscale");
            }
            if(F <= mFidelityLinThreshold) return F;

            double N = (mFidelityLogThreshold - mFidelityLinThreshold)/(Math.Log10(1-mFidelityLinThreshold) - Math.Log10(1-mFidelityLogThreshold));
            double M = mFidelityLinThreshold + N*Math.Log10(1-mFidelityLinThreshold);

            double retVal = -N * Math.Log10(1-F) + M;
            return retVal <= 1 ? retVal : 1;
        }

        public double GetDt()
        {
            double output = mLevel.GetDt();
            CheckError();
            return output;            
        }

        public double GetT()
        {
            double output = mLevel.GetT();
            CheckError();
            return output;
        }

        public double GetNormalizedT()
        {
            double output = mLevel.GetNormalizedT();
            CheckError();
            return output;
        }

        public Vector2 GetInitialCursorPoint()
        {
            Vector2 output = RVecConversion.ToVector2( mLevel.GetInitialControl() );
            CheckError();
            return output;
        }

        public Vector2 GetTargetCursorPoint()
        {
            Vector2 output = RVecConversion.ToVector2( mLevel.GetFinalControl() );
            CheckError();
            return output;                
        }

        public Vector2 GetLeftLowerOptimizationBound()
        {
            Vector2 output = RVecConversion.ToVector2( mLevel.GetLeftLowerOptimizationBound() );
            CheckError();
            return output;
        }

        public Vector2 GetLeftLowerOptimizationBoundPhysical()
        {
            Vector2 output = RVecConversion.ToVector2( mLevel.GetLeftLowerOptimizationBoundPhysical() );
            CheckError();
            return output;
        }

        public Vector2 GetRightUpperOptimizationBound()
        {
            Vector2 output = RVecConversion.ToVector2( mLevel.GetRightUpperOptimizationBound() );
            CheckError();
            return output;
        }

        public Vector2 GetRightUpperOptimizationBoundPhysical()
        {
            Vector2 output = RVecConversion.ToVector2( mLevel.GetRightUpperOptimizationBoundPhysical() );
            CheckError();
            return output;
        }        


        public Vector2 GetXLimits()
        {
            Vector2 output = RVecConversion.ToVector2( mLevel.GetXLimits() );
            CheckError();
            return output;                
        }

        public Vector2 GetYLimits()
        {
            Vector2 output = RVecConversion.ToVector2( mLevel.GetYLimits() );
            CheckError();
            return output;                
        }

        public List<double> GetX()
        {
            List<double> output = RVecConversion.ToDList( mLevel.GetX() );
            CheckError();
            return output;                
        }

        public List<double> GetInitialWavefunction(double scaling = 1.0)
        {
            List<double> outputList = RVecConversion.ToDList( mLevel.GetInitialWavefunctionNORM(scaling ) ) ; 
            CheckError();
            return outputList;        
        }

        public List<double> GetTargetWavefunction(double scaling = 1.0)
        {
            List<double> outputList = RVecConversion.ToDList( mLevel.GetTargetWavefunctionNORM( scaling ) ) ; 
            CheckError();
            return outputList;        
        }

        public List<double> GetCurrentWavefunction(double scaling = 1.0)
        {
            List<double> outputList = RVecConversion.ToDList( mLevel.GetCurrentWavefunctionNORM( scaling) ) ; 
            CheckError();
            return outputList;        
        }

        public List<double> GetOptimizedFinalWavefunction(double scaling = 1.0)
        {
            List<double> outputList = RVecConversion.ToDList( mLevel.GetOptimizedFinalWavefunction( scaling) ) ; 
            CheckError();
            return outputList;        
        }

        public List<double> GetInitialWavefunctionPhase(double scaling = 1.0)
        {
            List<double> outputList = RVecConversion.ToDList( mLevel.GetInitialWavefunctionPhase(scaling ) ) ; 
            CheckError();
            return outputList;        
        }

        public List<double> GetTargetWavefunctionPhase(double scaling = 1.0)
        {
            List<double> outputList = RVecConversion.ToDList( mLevel.GetTargetWavefunctionPhase( scaling ) ) ; 
            CheckError();
            return outputList;                
        }

        public List<double> GetCurrentWavefunctionPhase(double scaling = 1.0)
        {
            List<double> outputList = RVecConversion.ToDList( mLevel.GetCurrentWavefunctionPhase( scaling) ) ; 
            CheckError();
            return outputList;       
        }

        public List<double> GetCurrentPotential()
        {
            List<double> outputList = RVecConversion.ToDList( mLevel.GetPotential() ) ; 
            CheckError();
            return outputList;
        }

        public List<Vector2> GetControlHistory()
        {
            List<Vector2> outputList = RVecConversion.ToVector2List( mLevel.GetControlHistory() ); 
            CheckError();
            return outputList;
        }

        public List<Vector2> GetOptimizedControlHistory()
        {
            List<Vector2> outputList = RVecConversion.ToVector2List( mLevel.GetOptimizedControlHistory() ); 
            CheckError();
            return outputList;
        }

        public List<Vector2> GetControlHistoryPhysical()
        {
            List<Vector2> outputList = RVecConversion.ToVector2List( mLevel.GetControlHistoryPhysical() ); 
            CheckError();
            return outputList;
        }

        public List<Vector2> GetOptimizedControlHistoryPhysical()
        {
            List<Vector2> outputList = RVecConversion.ToVector2List( mLevel.GetOptimizedControlHistoryPhysical() ); 
            CheckError();
            return outputList;
        }

        public List<Vector2> GetOptimizerIterationFidelityHistory()
        {
            List<Vector2> outputList = RVecConversion.ToVector2List( mLevel.GetOptimizerIterationFidelityHistory() ); 
            CheckError();
            return outputList;
        }

        public bool GetOptimizerIsRunning()
        {
            return mLevel.GetOptimizerIsRunning();
        }

        public int GetOptimizerIteration()
        {
            return mLevel.GetOptimizerIteration();
        }

        public string GetOptimizerStopMessage()
        {
            return mLevel.GetOptimizerStopMessage();
        }

        public bool IsNewControlDataAvailable()
        {
            return mLevel.IsNewControlDataAvailable();
        }

        public void SetControlHistory(List<Vector2> controlHistory)
        {
            mLevel.SetControlHistory(RVecConversion.FromVector2ListToControlWrapper(controlHistory));
            CheckError();
        }

        public double GetOptimizedFidelityLogscale(double fidelity)
        {
            double F = fidelity;
            if (F <= mFidelityLinThreshold) return F;

            CheckError();
            double N = (mFidelityLogThreshold - mFidelityLinThreshold) / (Math.Log10(1 - mFidelityLinThreshold) - Math.Log10(1 - mFidelityLogThreshold));
            double M = mFidelityLinThreshold + N * Math.Log10(1 - mFidelityLinThreshold);

            double retVal = -N * Math.Log10(1 - F) + M;
            return retVal <= 1 ? retVal : 1;
        }        

}
