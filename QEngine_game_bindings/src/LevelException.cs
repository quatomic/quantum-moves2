using System;

[Serializable()]
public class LevelException : System.Exception
{
    public LevelException() : base() { }
    public LevelException(string message) : base(message) { }
    public LevelException(string message, System.Exception inner) : base(message, inner) { }

    protected LevelException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context) { }
}

// 1 to 1 correspondence to the C++ equivalent (within Level class)
enum ErrorCode { 	CONTROL_INCOMPLETE = -10, 
                	CONTROL_STILL_RECORDING = -11, 
                	CONTROL_RECORDING_NOT_STARTED = -12, 
                	CONTROL_TOO_SHORT = -13,
                	QENGINE_EXCEPTION = -14,
                	NONE = 0 };
