#include "LevelWrappers/LevelConfigs.h"

namespace qengine
{
namespace levelwrapper
{
namespace utility
{
	constexpr double g1D = 1.8299;

	LevelConfig ConstructShakeUpConfig(const double g1DMultiplier)
	{
		return { 	{"g1D", g1D * g1DMultiplier},
				  	{"LevelDescriptor", "ShakeUp g1D: " + std::to_string(g1D * g1DMultiplier)} };
	}

	LevelConfig ConstructSplittingConfig(const double fractionLeftWell, const double displacement)
	{
		return { 	{"FractionLeftWell", fractionLeftWell},
					{"Displacement", displacement},
				 	{"LevelDescriptor", "Splitting Fraction(left): " + std::to_string(fractionLeftWell) + " Displacement: " + std::to_string(displacement)} };
	}

	LevelConfig ConstructBHWConfig(const double g1DMultiplier, const int targetState)
	{
		return { 	{"g1D", g1DMultiplier * g1D}, 
					{"TargetState", targetState}, 
					{"LevelDescriptor", "BHW g1D: " + std::to_string(g1DMultiplier * g1D) + " TargetState: " + std::to_string(targetState)}};
	}

}
}
}