#include <functional>
#include <type_traits>
#include <iostream>
#include <random>

#include <qengine/qengine.h>
#include <gtest/gtest.h>
#include <Utilities/Scaling.h>

using namespace qengine;
using namespace levelwrapper;
using namespace utility;

constexpr size_t elements = 100;
constexpr double dt = 0.001;
constexpr size_t paramCount = 25;
constexpr size_t testVecSize = 50;
constexpr double distributionRange = 100.0;
constexpr double distributionLeft = 1.0;

class Scaling_Control_Fixture : public ::testing::Test
{
public:
	Scaling_Control_Fixture()
	{
		size_t seed = std::chrono::system_clock::now().time_since_epoch().count();
		mGen = std::default_random_engine(static_cast<unsigned int>(seed));
		mDist = std::uniform_real_distribution<double> {distributionLeft, distributionLeft + distributionRange};
	}

	Control makeCurrentControl(const count_t controllength, const real dtin, const count_t pCount = 1) const
	{
		auto controls = std::vector<RVec>();

		for (auto i = 0u; i < pCount; ++i)
		{
			controls.push_back(linspace(i, 2 * (i + 1), controllength));
		}

		return makeControl(controls, dtin);
	}

	Control makeRandControl(const count_t controllength, const real dtin, const count_t pCount = 1)
	{
		auto controls = std::vector<RVec>();

		for (auto i = 0u; i < pCount; ++i)
		{
			controls.push_back( GetRandRVec(controllength) );
		}

		return makeControl(controls, dtin);
	}

	template <typename vector, typename point>
	Scaling<vector, point> makeScaling() const
	{
		return Scaling<vector, point>{};
	}

	template <typename vector, typename point>
	Scaling<vector, point> makeRandomLinearScaling()
	{
		double a = 0.0;
		double b = mDist(mGen);
		while (0.0 == a) // pick as long as it takes to not get a = 0.0
		{
			a = mDist(mGen);
		}

		auto scaling = makeScaling<vector, point>();
		auto linearElementFunc = [a, b] (const auto i) { return a * i + b; };
		auto invElementFunc = [a, b] (const auto i) { return (i - b)/a; };
		scaling.SetScaling(
			[this, linearElementFunc] (const auto vec) 
			{ 
				typename std::remove_const<decltype(vec)>::type outVec = GetZeroRVec(vec.size());
				for (size_t i = 0 ; i < vec.size() ; ++i)
				{
					outVec.at( i ) = linearElementFunc( vec.at(i) ); 
				}
				return outVec;
			});
		scaling.SetInvScaling(
			[this, invElementFunc] (const auto vec) 
			{ 
				typename std::remove_const<decltype(vec)>::type outVec = GetZeroRVec(vec.size());
				for (size_t i = 0 ; i < vec.size() ; ++i)
				{
					outVec.at( i ) = invElementFunc( vec.at(i) ); 
				}
				return outVec;
			});
		return scaling;
	}

	RVec GetRandRVec(const count_t elems)
	{
		RVec vec(elems);
		for (size_t i =  0 ; i < vec.size() ; ++i)
		{
			vec.at( i ) = mDist( mGen );
		}
		return vec;
	}

	RVec GetZeroRVec(const count_t elems)
	{
		RVec vec(elems);
		for (size_t i =  0 ; i < vec.size() ; ++i)
		{
			vec.at( i ) = 0;
		}
		return vec;
	}

	std::default_random_engine mGen;
	std::uniform_real_distribution<double> mDist;
};

TEST_F(Scaling_Control_Fixture, FixtureControlTest)
{
	auto ctrl = makeCurrentControl(elements, dt, paramCount);
	ASSERT_EQ(ctrl.size(), elements);
	ASSERT_EQ(ctrl.dt(), dt);
	ASSERT_EQ(ctrl.paramCount(), paramCount);
}

TEST_F(Scaling_Control_Fixture, FixtureRandControlTest)
{
	auto ctrl = makeRandControl(elements, dt, paramCount);
	ASSERT_EQ(ctrl.size(), elements);
	ASSERT_EQ(ctrl.dt(), dt);
	ASSERT_EQ(ctrl.paramCount(), paramCount);
}

TEST_F(Scaling_Control_Fixture, DefaultIdentityScalingTest)
{
	auto scaling = makeScaling<Control, RVec>();
	RVec p = GetRandRVec( testVecSize );
	auto pIdentityScaled = scaling.Scale(p, ScalingType::FORWARD);
	ASSERT_EQ(p, pIdentityScaled);
	auto pIdentityReScaled = scaling.Scale(p, ScalingType::INVERSE);
	ASSERT_EQ(p, pIdentityReScaled);
}

TEST_F(Scaling_Control_Fixture, DefaultIdentityInPlaceScalingTest)
{
	auto scaling = makeScaling<Control, RVec>();
	RVec p = GetRandRVec( testVecSize );
	const auto pCopy = p;
	scaling.ScaleInPlace(p, ScalingType::FORWARD);
	ASSERT_EQ(p, pCopy);

	p = pCopy;
	scaling.ScaleInPlace(p, ScalingType::INVERSE);
	ASSERT_EQ(p, pCopy);
}

TEST_F(Scaling_Control_Fixture, DefaultIdentityMapScalingTest)
{
	auto scaling = makeScaling<Control, RVec>();
	auto ctrl = makeRandControl(elements, dt, paramCount);
	auto ctrlForwardScaled = scaling.MapScale(ctrl, ScalingType::FORWARD);
	ASSERT_EQ(ctrl, ctrlForwardScaled);

	auto ctrlInvScaled = scaling.MapScale(ctrl, ScalingType::INVERSE);
	ASSERT_EQ(ctrl, ctrlInvScaled);
}

TEST_F(Scaling_Control_Fixture, DefaultIdentityMapInPlaceScalingTest)
{
	auto scaling = makeScaling<Control, RVec>();
	auto ctrl = makeRandControl(elements, dt, paramCount);
	const auto ctrlCopy = ctrl;
	scaling.MapScaleInPlace(ctrl, ScalingType::FORWARD);
	ASSERT_EQ(ctrl, ctrlCopy);

	ctrl = ctrlCopy;
	scaling.MapScaleInPlace(ctrl, ScalingType::INVERSE);
	ASSERT_EQ(ctrl, ctrlCopy);
}


TEST_F(Scaling_Control_Fixture, ScaleTest)
{
	auto scaling = makeRandomLinearScaling<Control, RVec>();
	RVec p = GetRandRVec( testVecSize );

	RVec pScaled = scaling.Scale(p, ScalingType::FORWARD);
	ASSERT_NE(pScaled, p);

	RVec pInvScaled = scaling.Scale(p, ScalingType::INVERSE);
	ASSERT_NE(pInvScaled, p);
	ASSERT_NE(pScaled, pInvScaled);

	RVec pIdentityReScaled = scaling.Scale(pInvScaled, ScalingType::FORWARD);
	for (size_t i = 0 ; i < pIdentityReScaled.size() ; ++i)
	{
        ASSERT_NEAR(p.at(i), pIdentityReScaled.at(i),1e-10);
	}
}

TEST_F(Scaling_Control_Fixture, ScaleInPlaceTest)
{
	auto scaling = makeRandomLinearScaling<Control, RVec>();
	RVec p = GetRandRVec( testVecSize );
	const RVec pCopy = p;
	scaling.ScaleInPlace(p, ScalingType::FORWARD);
	ASSERT_NE(p, pCopy);

	p = pCopy;
	scaling.ScaleInPlace(p, ScalingType::INVERSE);
	ASSERT_NE(p, pCopy);

	scaling.ScaleInPlace(p, ScalingType::FORWARD);
	for (size_t i = 0 ; i < p.size() ; ++i)
	{
		ASSERT_NEAR(p.at(i), pCopy.at(i), 1e-10);	
	}
}

TEST_F(Scaling_Control_Fixture, MapScaleTest)
{
	auto scaling = makeRandomLinearScaling<Control, RVec>();

	auto ctrl = makeRandControl(elements, dt, paramCount);
	auto ctrlForwardScaled = scaling.MapScale(ctrl, ScalingType::FORWARD);
	ASSERT_NE(ctrl, ctrlForwardScaled);

	auto ctrlInvScaled = scaling.MapScale(ctrl, ScalingType::INVERSE);
	ASSERT_NE(ctrl, ctrlInvScaled);

	auto ctrlIdentityScaled = scaling.MapScale(ctrlInvScaled, ScalingType::FORWARD);
	for (size_t i = 0 ; i < ctrlIdentityScaled.size() ; ++i)
	{
		auto itemId= ctrlIdentityScaled.at(i);
		auto item = ctrl.at(i);
		for (size_t j = 0 ; j < itemId.size() ; ++j)
		{
			ASSERT_NEAR(item.at(j), itemId.at(j), 1e-10);	
		}
	}
}

TEST_F(Scaling_Control_Fixture, MapScaleInPlaceTest)
{
	auto scaling = makeRandomLinearScaling<Control, RVec>();

	auto ctrl = makeRandControl(elements, dt, paramCount);
	const auto ctrlCopy = ctrl;
	scaling.MapScaleInPlace(ctrl, ScalingType::FORWARD);
	ASSERT_NE(ctrl, ctrlCopy);

	ctrl = ctrlCopy;

	scaling.MapScaleInPlace(ctrl, ScalingType::INVERSE);
	ASSERT_NE(ctrl, ctrlCopy);

	scaling.MapScaleInPlace(ctrl, ScalingType::FORWARD);
	for (size_t i = 0 ; i < ctrl.size() ; ++i)
	{
		auto itemId= ctrl.at(i);
		auto item = ctrlCopy.at(i);
		for (size_t j = 0 ; j < itemId.size() ; ++j)
		{
			ASSERT_NEAR(item.at(j), itemId.at(j), 1e-10);	
		}
	}
}

TEST_F(Scaling_Control_Fixture, TestGetCoordinateReferenceScalings)
{
	RVec init = GetRandRVec(testVecSize);
	RVec final = GetRandRVec(testVecSize);

	for (size_t coordinate = 0 ; coordinate < testVecSize ; ++coordinate)
	{
		auto func = GetCoordinateReferenceScalings(init, final, coordinate);
		RVec testCoord = GetRandRVec(testVecSize);
		const size_t fractions = 1000;
		const double diff = final.at(coordinate) - init.at(coordinate);

		// run across the range of values
		for (size_t frac = 1 ; frac < fractions ; ++frac)
		{
			// test for values between
			testCoord.at(coordinate) = init.at(coordinate) + frac * diff / fractions;
			testCoord.at(coordinate) = func.first(testCoord.at(coordinate));
			ASSERT_GE(testCoord.at(coordinate), 0.0);
			ASSERT_LE(testCoord.at(coordinate), 1.0);
		
			// test for values on the left side of the range
			testCoord.at(coordinate) = init.at(coordinate) - frac * diff / fractions;
			testCoord.at(coordinate) = func.first(testCoord.at(coordinate));
			ASSERT_LE(testCoord.at(coordinate), 0.0);

			// test for values on the right side of the range
			testCoord.at(coordinate) = init.at(coordinate) + diff + frac * diff / fractions;
			testCoord.at(coordinate) = func.first(testCoord.at(coordinate));
			ASSERT_GE(testCoord.at(coordinate), 1.0);
		}		
	}
}
