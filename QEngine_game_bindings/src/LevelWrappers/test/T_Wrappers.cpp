#include <qengine/qengine.h>
#include <LevelWrappers/LevelWrapperSpatial.h>
#include <LevelWrappers/LevelWrapperTypes.h>
#include <gtest/gtest.h>
#include <iostream>

#include "LevelWrappers/Data.h"



#include <functional>
#include <type_traits>


using namespace qengine;
using namespace levelwrapper;
using namespace qengine::levelwrapper::gpe;

class GPE_Fixture : public ::testing::Test {
    public:
    GPE_Fixture()
    {
        l.DoLoadLevel_ShakeUp( utility::ConstructShakeUpConfig(1.0) );
        auto t = makeTimeControl(10, l.d->mDt());

        // remember to always check the dimensionality of the controls passed in. 
        // qengine won't let you know that something went wrong when appending the controls to ControlHistory
        auto u = 1.0*sin(PI*makeControl({t, t})); 
        l.StartRecordingSteps();
        for (size_t i = 0 ; i < u.size() ; ++i)
        {
            l.DoStep(u.get(i));
        }
        l.StopRecordingSteps(); 
    }

    LevelWrapperSpatial<levelwrapper::gpe::GPETupleTypes> l;
};

class BHW_Fixture: public ::testing::Test 
{
    public:
    BHW_Fixture()
    {
        l.DoLoadLevel_BringHomeWater(utility::ConstructBHWConfig(1.0, 0));
        auto t = makeTimeControl(10, l.d->mDt());

        // remember to always check the dimensionality of the controls passed in. 
        // qengine won't let you know that something went wrong when appending the controls to ControlHistory
        auto u = 1.0*sin(PI*makeControl({t, t})); 
        l.StartRecordingSteps();
        for (size_t i = 0 ; i < u.size() ; ++i)
        {
            l.DoStep(u.get(i));
        }
        l.StopRecordingSteps(); 
    }

    LevelWrapperSpatial<levelwrapper::gpe::GPETupleTypes> l;   
};

TEST_F(GPE_Fixture,debug1)
{


    auto d1 = Data<ShakeUpTypes>(Initialize_ShakeUp( utility::ConstructShakeUpConfig(1.0) ));
    auto d2 = Data<BHWTupleType>(Initialize_BringHomeWater(utility::ConstructBHWConfig(1.0, 0)));

    if(!std::is_same<decltype(d1.mX()),decltype(d2.mX())>::value)    {        std::cout << "x" << std::endl;    }
    if(!std::is_same<decltype(d1.mpTargetWaveFunction()),decltype(d2.mpTargetWaveFunction())>::value)    {        std::cout << "ini" << std::endl;    }
    if(!std::is_same<decltype(d1.mpInitialWaveFunction()),decltype(d2.mpInitialWaveFunction())>::value)    {        std::cout << "tar" << std::endl;    }
    if(!std::is_same<decltype(d1.mpPropagator()),decltype(d2.mpPropagator())>::value)    {        std::cout << "prop" << std::endl;    }
    if(!std::is_same<decltype(d1.mpHamiltonian()),decltype(d2.mpHamiltonian())>::value)    {        std::cout << "Ham" << std::endl;    }
    if(!std::is_same<decltype(d1.mYLimits()),decltype(d2.mYLimits())>::value)    {        std::cout << "ylim" << std::endl;    }
    if(!std::is_same<decltype(d1.mXLimits()),decltype(d2.mXLimits())>::value)    {        std::cout << "xlim" << std::endl;    }
    if(!std::is_same<decltype(d1.mpOptimizerMaker()),decltype(d2.mpOptimizerMaker())>::value)    {        std::cout << "optim maker" << std::endl;    }
    if(!std::is_same<decltype(d1.mpOptimizer()),decltype(d2.mpOptimizer())>::value)    {        std::cout << "optim" << std::endl;    }

    auto om1 = d1.mpOptimizerMaker();
    auto om2 = d2.mpOptimizerMaker();
//    std::cout << typeid(d1.mpOptimizer()).name() << std::endl;
//    std::cout << std::endl;
//    std::cout << typeid(d2.mpOptimizer()).name() << std::endl;

//    std::cout << typeid(d1.mpOptimizerMaker()).name() << std::endl;
//    std::cout << std::endl;
//    std::cout << typeid(d2.mpOptimizerMaker()).name() << std::endl;
    //    if(!std::is_same<decltype(d1.mDataTuple),decltype(d2.mDataTuple)>::value)    {        std::cout << "Whole tup." << std::endl;    }



//    t1 = t2;
//    Data<decltype(f1())> d1(t1);
//    Data<decltype(f1())> d2(t2);

}

TEST_F(GPE_Fixture,iterationIncrement)
{
    using namespace levelwrapper;

    int increment = 2;
    for(int i = 0; i<= 5; ++i)
    {
        l.DoOptimize(increment);
        while(l.GetOptimizerIsRunning()){}
    }

    auto& hist = l.d->mpOptimizerIterationFidelityHistory;
    for(int i = 1; i<hist->size();++i)
    {
        auto F1 = hist->at(i-1).at(1);
        auto F2 = hist->at(i).at(1);
        ASSERT_TRUE(F2-F1 > 0);
   }

   // const auto & wfR = l.GetCurrentWavefunctionRE(1);
   // const auto & wfI = l.GetCurrentWavefunctionIM(1);
   // const auto & wfP = l.GetCurrentWavefunctionPhase(1);
   // const auto & wfNORM = l.GetCurrentWavefunctionRE(1);
   // const auto & pot = l.GetPotential();
}

TEST_F(BHW_Fixture,iterationIncrement)
{
    using namespace levelwrapper;

    int increment = 2;
    for(int i = 0; i<= 5; ++i)
    {
        l.DoOptimize(increment);
        while(l.GetOptimizerIsRunning()){}
    }

    auto& hist = l.d->mpOptimizerIterationFidelityHistory;
    for(int i = 1; i<hist->size();++i)
    {
        auto F1 = hist->at(i-1).at(1);
        auto F2 = hist->at(i).at(1);
        ASSERT_TRUE(F2-F1 > 0);
   }
}

TEST_F(GPE_Fixture,stopRequest)
{

    int stopRequestAtIteration = 3;

    if (l.GetControlHistory().size() < 3)
    {
        FAIL() << "Test controls are too short";
    }

    l.DoOptimize(10);

    while(l.GetOptimizerIsRunning())
    {
        if(l.GetOptimizerIteration() > stopRequestAtIteration)
        {
            l.DoOptimizeStopRequest();
            while(l.GetOptimizerIsRunning()) {}
            ASSERT_GT(l.GetOptimizerIteration(), stopRequestAtIteration); 
            return;           
        }
    }

    FAIL() << "Optimizer stopped at iteration " << l.GetOptimizerIteration() << " before reaching the (stopRequest) iteration " 
            << stopRequestAtIteration
            << "\nStopMessage: " << l.GetOptimizerStopMessage() 
            << "\nError message: " << l.GetErrorBuffer() 
            << "\nError code: " << l.GetErrorState();
    
}
