#include "ObjectWrappers/RVecWrapper.h"

// RVecWrapper
RVecWrapper::RVecWrapper(const size_t vecSize): mVec( qengine::Vector<double> ( vecSize ) ) {}

RVecWrapper::RVecWrapper(const int vecSize): mVec( qengine::Vector<double> ( vecSize ) ) {}

RVecWrapper::RVecWrapper(const size_t vecSize, const double val): mVec( qengine::Vector<double> ( vecSize, val ) ) { }

RVecWrapper::RVecWrapper(const int vecSize, const double val): mVec( qengine::Vector<double> ( vecSize, val ) ) { }

double RVecWrapper::at(const size_t index) const
{
	assert(index < size() && size() > 0);
    return mVec.at( index );
}

double RVecWrapper::at(const int index) const
{
	assert(index >= 0 && (uint) index < size());
    return mVec.at( index );
}

void RVecWrapper::set(const size_t index, const double val)
{
	assert(index < size() && size() > 0);
    mVec.at( index ) = val;
}

void RVecWrapper::set(const int index, const double val)
{
	assert(index >= 0 && (uint) index < size());
    mVec.at( index ) = val;
}

size_t RVecWrapper::size() const
{
	return mVec.size();
}

const qengine::Vector<double> & RVecWrapper::GetVecRef() const
{
	return mVec;
}

qengine::Vector<double> RVecWrapper::GetVec() const
{
	return mVec;
}

RVecWrapper & GetRVecRef(RVecWrapper & vec)
{
	return vec;
}

RVecWrapper GetRVecVal(RVecWrapper vec)
{
	return vec;
}

void CopyRVec(RVecWrapper & destination, const RVecWrapper & source)
{
	assert(source.size() == destination.size());

	// copy using the wrapper
	for (size_t i = 0 ; i < source.size() ; ++i)
	{
		destination.set( i , source.at( i ) );
	}
}