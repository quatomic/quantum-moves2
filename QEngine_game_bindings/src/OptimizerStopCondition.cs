// 1 to 1 correspondence to the C++ equivalent (within Level class)
public enum OptimizerStopCondition {	NONE = 0, 
										FIDELITY = 1, 
										STEPSIZE = 2, 
										ITERATIONS = 3, 
										EXCEPTION = 4, 
										USER = 5};
