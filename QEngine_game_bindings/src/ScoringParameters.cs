using System;
using System.Collections.Generic;

public class ScoringParameters
{
    public ScoringParameters(List<double> qs, List<double> gs)
    {
        mqs = qs;
        mgs = gs;
        mK  = qs.Count - 1;

        mQs = makeQs(qs,gs,mK);
    }

    private List<double> mqs;
    private List<double> mgs;
    private List<double> mQs;
    private int mK;

    public List<double> qs(){ return mqs;}
    public List<double> gs(){ return mgs;}
    public List<double> Qs(){ return mQs;}
    public int K(){ return mK;}


    private List<double> makeQs(List<double> qs, List<double> gs, int K)
    {
        var Qs = new List<double>();

        Qs.Add(0);

        for (int j = 0; j < K; j++)
        {
            Qs.Add(qs[j + 1] + gs[j + 1] + Qs[j]);
        }

        return Qs;
    }
}


public static class ScoringEngine
{

    private static ScoringParameters mIntroLevelParameters;
    private static ScoringParameters mIntroOptimizeLevelParameters;
    private static ScoringParameters mScienceLevelParameters;
    private static ScoringParameters mScienceVariantLevelParameters;

    public const int nBuckets = 12;

    public const int nIntroLevels = 6;
    public const int nIntroOptimizeLevels = 1;
    public const int nScienceLevels = 3;
    public const int nScienceVariantLevels = 10;

    public const double TotalPointsIntroLevels           = 3000;
    public const double TotalPointsIntroOptimizeLevels        = 2000;
    public const double TotalPointsScienceLevels         = 10500;
    public const double TotalPointsScienceVariantLevel = 7500;

    public const double PointsPerIntroLevel            = TotalPointsIntroLevels/nIntroLevels;
    public const double PointsPerIntroOptimizeLevel         = TotalPointsIntroOptimizeLevels/nIntroOptimizeLevels;
    public const double PointsPerScienceLevel          = TotalPointsScienceLevels/nScienceLevels;
    public const double PointsPerScienceVariantLevel = TotalPointsScienceVariantLevel/nScienceVariantLevels;

    public const double PointsPerIntroLevelBucket            = PointsPerIntroLevel/nBuckets;
    public const double PointsPerIntroOptimizeLevelBucket         = PointsPerIntroOptimizeLevel/nBuckets;
    public const double PointsPerScienceLevelBucket          = PointsPerScienceLevel/nBuckets;
    public const double PointsPerScienceVariantLevelBucket = PointsPerScienceVariantLevel/nBuckets;


    static ScoringEngine()
    {

        InitializeIntroLevelParameters();
        InitializeIntroOptimizeLevelParameters();
        InitializeScienceLevelParameters();
        InitializeScienceVariantLevelParameters();
    }

    private static void InitializeIntroLevelParameters()
    {
        List<double> qs = new List<double>() {0, PointsPerIntroLevelBucket };
        List<double> gs = new List<double>() {0, 0 };

        mIntroLevelParameters = new ScoringParameters(qs,gs);
    }

    private static void InitializeIntroOptimizeLevelParameters()
    {
        List<double> qs = new List<double>() {0, PointsPerIntroOptimizeLevelBucket };
        List<double> gs = new List<double>() {0, 0 };

        mIntroOptimizeLevelParameters = new ScoringParameters(qs,gs);
    }

    private static void InitializeScienceLevelParameters()
    {

        // cumulative percentages of full scores: Qi = pi*Q4
        double p0 = 0.00;
        double p1 = 0.25;
        double p2 = 0.50;
        double p3 = 0.90;
        double p4 = 1.00;

        // gs as a percentage of qs: qi = ci*bi
        double c0 = 0.00;
        double c1 = 1.0/5.0;
        double c2 = 1.0/5.0;
        double c3 = 1.0/2.0;
        double c4 = 0.00;

        // Now calculate
        double Q4 = p4 * PointsPerScienceLevelBucket;
        double Q3 = p3 * Q4;
        double Q2 = p2 * Q4;
        double Q1 = p1 * Q4;
        double Q0 = p0 * Q4;

        double q0 = 0.0;
        double q1 = (Q1-Q0)/(1+c1);
        double q2 = (Q2-Q1)/(1+c2);
        double q3 = (Q3-Q2)/(1+c3);
        double q4 = (Q4-Q3)/(1+c4);

        double g0 = c0*q0;
        double g1 = c1*q1;
        double g2 = c2*q2;
        double g3 = c3*q3;
        double g4 = c4*q4;


        List<double> qs = new List<double>() { q0, q1, q2, q3, q4 };
        List<double> gs = new List<double>() { g0, g1, g2, g3, g4 };

        mScienceLevelParameters = new ScoringParameters(qs,gs);
    }

    private static void InitializeScienceVariantLevelParameters()
    {
        List<double> qs = new List<double>() { 0, PointsPerScienceVariantLevelBucket };
        List<double> gs = new List<double>() { 0, 0 };

        mScienceVariantLevelParameters = new ScoringParameters(qs,gs);
    }

    public static ScoringParameters GetIntroLevelParameters()
    {
        return mIntroLevelParameters;
    }

    public static ScoringParameters GetIntroOptimizeLevelParameters()
    {
        return mIntroOptimizeLevelParameters;
    }

    public static ScoringParameters GetScienceLevelParameters()
    {
        return mScienceLevelParameters;
    }

    public static ScoringParameters GetScienceVariantLevelParameters()
    {
        return mScienceVariantLevelParameters;
    }
}
