using System;

namespace UnityEngine
{
#if WRAPPER_TESTS
    public struct Vector2
    {
        public float x;
        public float y;

        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }
    }
#endif // WRAPPER_TESTS
}