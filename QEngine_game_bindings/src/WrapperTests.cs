using System;
using System.Collections.Generic;
using UnityEngine;

#if NUNIT_TESTS
namespace WrapperTesting
{
	using NUnit.Framework;

        public delegate void LoadLevelDelegate();

        [TestFixture( 10 )]
        // [TestFixture( 50 )]
        public class LevelWrapperTests
        {
                // double dt;
                int size;
                Level lvl;
                int propagationSteps;
                int optimizerIters;
                Random randGen;
                int durationsLengths;
		
                private float GetRandD()
                {
                        return (float) randGen.NextDouble();
                }

                private Vector2 GetRandVec2()
                {
                        return new Vector2(GetRandD(), GetRandD());
                }

                public LevelWrapperTests(int testPropagationSteps)
                {

                        durationsLengths = 50;

                        size = 256;
                        propagationSteps = testPropagationSteps;
                        optimizerIters = 20;
                        randGen = new Random();
                }

                public List<Vector2> ProduceSetControls()
                {
                        List<Vector2> controlList = new List<Vector2>();
			
                        for (int i = 0 ; i < propagationSteps ; ++i)
                        {
                                controlList.Add( GetRandVec2() );
                        }
                        lvl.SetControlHistory(controlList );
                        return controlList;
                }

                public List<Vector2> ProduceSetControls(int stepCount)
                {
                        List<Vector2> controlList = new List<Vector2>();
			
                        for (int i = 0 ; i < stepCount ; ++i)
                        {
                                controlList.Add( GetRandVec2() );
                        }
                        lvl.SetControlHistory(controlList );
                        return controlList;
                }

                public void ProduceDoStep()
                {
                        lvl.StartRecordingSteps();
                        for (int i = 0 ; i < propagationSteps ; ++i)
                        {
                                lvl.DoStep( GetRandVec2() );
                        }
                        lvl.StopRecordingSteps();
                }

                public void ProduceDoStep(int stepCount)
                {
                        lvl.StartRecordingSteps();
                        for (int i = 0 ; i < stepCount ; ++i)
                        {
                                lvl.DoStep( GetRandVec2() );
                        }
                        lvl.StopRecordingSteps();
                }

                public void OptimizerTestRoutine(string levelName)
                {
                        ProduceDoStep();
                        lvl.DoOptimize(optimizerIters);
                        int prevIter = 0;
                        int currentIter = 0;
                        int restarts = 0;
                        bool didRestart = false;
                        while(lvl.GetOptimizerIsRunning())
                        {
                                currentIter = lvl.GetOptimizerIteration();
                                didRestart = lvl.DidOptimizerRestart();
                                if (prevIter != currentIter)
                                {
                                        prevIter = currentIter;
                                        if (didRestart)
                                        {
                                                restarts += 1;
                                        }
                                }
                        }
                        TestFPP();
                        // uncomment in order to show the amount of restarts done by the optimizer during the optimization routine
                        Console.WriteLine(levelName + " BFGS restarts: " + restarts + " out of " + currentIter + " iterations. \nStop Message: " + lvl.GetOptimizerStopMessage() + "\nFPP: " + lvl.GetFPP());
                        Assert.AreEqual(lvl.GetBFGSRestarts(), restarts);
                        OptimizerStopRequest();
                        OptimizerStopIterations();
                }

                public void TestFPP()
                {
                        Assert.GreaterOrEqual(lvl.GetFPP(), 0);
                }

                // general tests of level-by-level functionality that should obey certain constraints
                // xLimits, yLimits scaling tests
                public void TestVisualLimits()
                {
                        TestXLimits();
                        TestYLimits();
                }

                public void TestXLimits()
                {
                        Vector2 limitX = lvl.GetXLimits();
                        if (limitX.x < 0 || limitX.y > 1)
                        {
                                NearOutOfBoundsCheck( limitX );
                                return;
                        }
                        Assert.GreaterOrEqual(limitX.x, 0);
                        Assert.LessOrEqual(limitX.y, 1);
                }

                public void TestLeftLowerOptimizationBound()
                {
                    Vector2 limitX = lvl.GetXLimits();
                    Vector2 limitY = lvl.GetYLimits();
                    Vector2 llob = lvl.GetLeftLowerOptimizationBound();
                    Assert.GreaterOrEqual(llob.x, limitX.x);
                    Assert.GreaterOrEqual(limitX.y, llob.x);

                    Assert.GreaterOrEqual(llob.y, limitY.x);
                    Assert.GreaterOrEqual(limitY.y, llob.y);                    
                }

                public void TestLeftLowerOptimizationBoundPhysical()
                {
                    Vector2 llob = lvl.GetLeftLowerOptimizationBoundPhysical();
                    Assert.That(llob.x, Is.Not.NaN);
                    Assert.That(llob.y, Is.Not.NaN);               
                }

                public void TestRightUpperOptimizationBound()
                {
                    Vector2 limitX = lvl.GetXLimits();
                    Vector2 limitY = lvl.GetYLimits();
                    Vector2 ruob = lvl.GetRightUpperOptimizationBound();
                    Assert.GreaterOrEqual(ruob.x, limitX.x);
                    Assert.GreaterOrEqual(limitX.y, ruob.x);
                    
                    Assert.GreaterOrEqual(ruob.y, limitY.x);
                    Assert.GreaterOrEqual(limitY.y, ruob.y);                    
                }

                public void TestRightUpperOptimizationBoundPhysical()
                {
                    Vector2 ruob = lvl.GetRightUpperOptimizationBoundPhysical();
                    Assert.That(ruob.x, Is.Not.NaN);
                    Assert.That(ruob.y, Is.Not.NaN);                   
                }

                public void TestYLimits()
                {
                        Vector2 limitY = lvl.GetYLimits();
                        if (limitY.x < 0 || limitY.y > 1)
                        {
                                NearOutOfBoundsCheck( limitY );
                                return;
                        }
                        Assert.GreaterOrEqual(limitY.x, 0);
                        Assert.LessOrEqual(limitY.y, 1);
                }

                public void NearOutOfBoundsCheck(Vector2 lim)
                {
                        Assert.That(lim.x, Is.EqualTo(0).Within(1e-10));
                        Assert.That(lim.y, Is.EqualTo(1).Within(1e-10));
                }

                // control scaling tests
                public void TestGetInitialCursorPoint()
                {
                        var control = lvl.GetInitialCursorPoint();
                        TestXControlScaling(control);
                        TestYControlScaling(control);
                }

		
                public void TestGetTargetCursorPoint()
                {
                        var control = lvl.GetTargetCursorPoint();
                        TestXControlScaling(control);
                        TestYControlScaling(control);
                }


                public void TestXControlScaling(Vector2 ctrl)
                {
                        Assert.GreaterOrEqual(ctrl.x, 0);
                        Assert.LessOrEqual(ctrl.x, 1);
                }

                public void TestYControlScaling(Vector2 ctrl)
                {
                        Assert.GreaterOrEqual(ctrl.y, 0);
                        Assert.LessOrEqual(ctrl.y, 1);
                }


                public void TestControlAxis(ControlAxis expected)
                {
                    Assert.AreEqual(expected, lvl.GetControlAxis());
                }

                public void TestGetPotential()
                {
                        List<double> pot = lvl.GetCurrentPotential();
                        for (int i = 0 ; i < pot.Count ; ++i)
                        {
                                // we should see the bottom of the potential well on the screen
                                Assert.GreaterOrEqual(pot[i], 0);
                        }
                }

                public void TestGetTSetControlHistory()
                {
                    for (int i = 3 ; i < durationsLengths ;)
                    {
                        i += 100;
                        ProduceSetControls(i);
                        Assert.AreEqual((i - 1) * lvl.GetDt(), lvl.GetT());                         
                    }
                }

                // default test level behaviour
                [SetUp]
                public void Init()
                {
                    lvl = new Level();
                }

                [Test]
                public void GetFidelityLogscale([Range(0.01, 0.999999, 0.05)]  double fid)
                {
                        Assert.LessOrEqual(lvl.GetFidelityLogscale(fid), 1);
                        Assert.GreaterOrEqual(lvl.GetFidelityLogscale(fid), 0);
                }

                [Test]
                public void GetOptimizedFidelityLogscale()
                {
                        ProduceSetControls();
                        lvl.DoOptimize(optimizerIters);
                        while(lvl.GetOptimizerIsRunning()) {}
                        Assert.GreaterOrEqual(lvl.GetOptimizedFidelityLogscale(), 0);
                        Assert.LessOrEqual(lvl.GetOptimizedFidelityLogscale(), 1);
                }

                [Test]
                public void TestLoadLevel1()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel1), "Level1");
                        // level specific tests
                        TestControlAxis(ControlAxis.X);                        
                }

                [Test]
                public void TestLoadLevel2()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel2), "Level2");
                        // level specific tests
                        TestControlAxis(ControlAxis.X);
                }

                [Test]
                public void TestLoadLevel3()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel3), "Level3");
                        // level specific tests
                        TestControlAxis(ControlAxis.X);
                }

                [Test]
                public void TestLoadLevel4()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel4), "Level4");
                        // level specific tests
                        TestControlAxis(ControlAxis.X);
                }

                [Test]
                public void TestLoadLevel6()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel6), "Level6");
                        // level specific tests
                        TestControlAxis(ControlAxis.Y);
                }

                [Test]
                public void TestLoadLevel7()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel7), "Level7");
                        // level specific tests
                        TestControlAxis(ControlAxis.X);
                }

                [Test]
                public void TestLoadLevelBHW_0_0()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel_BringHomeWater), "BHW_0_0");
                        // level specific tests
                        TestControlAxis(ControlAxis.XY);
                }

                [Test]
                public void TestLoadLevelBHW_1_0()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel_BringHomeWater_1), "BHW_1_0");
                        // level specific tests
                        TestControlAxis(ControlAxis.XY);
                }

                [Test]
                public void TestLoadLevelBHW_0_1()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel_BringHomeWater_2), "BHW_0_1");
                        // level specific tests
                        TestControlAxis(ControlAxis.XY);
                }

                [Test]
                public void TestLoadLevelBHW_1_1()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel_BringHomeWater_3), "BHW_1_1");
                        // level specific tests
                        TestControlAxis(ControlAxis.XY);
                }

                [Test]
                public void TestLoadLevelShakeUp_1()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel_ShakeUp_1), "ShakeUp_0");
                        // level specific tests
                        TestControlAxis(ControlAxis.X);
                }

                [Test]
                public void TestLoadLevelShakeUp()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel_ShakeUp), "ShakeUp_1");
                        // level specific tests
                        TestControlAxis(ControlAxis.X);
                }

                [Test]
                public void TestLoadLevelShakeUp_2()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel_ShakeUp_2), "ShakeUp_2");
                        // level specific tests
                        TestControlAxis(ControlAxis.X);
                }

                [Test]
                public void TestLoadLevelShakeUp_3()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel_ShakeUp_3), "ShakeUp_3");
                        // level specific tests
                        TestControlAxis(ControlAxis.X);
                }

                [Test]
                public void DoLoadLevel_Splitting_50()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel_Splitting), "Splitting_50");
                        // level specific tests
                        TestControlAxis(ControlAxis.Y);
                }

                [Test]
                public void DoLoadLevel_Splitting_50_d()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel_Splitting_1), "Splitting_50_d");
                        // level specific tests
                        TestControlAxis(ControlAxis.XY);
                }

                [Test]
                public void DoLoadLevel_Splitting_25_d()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel_Splitting_2), "Splitting_25_d");
                        // level specific tests
                        TestControlAxis(ControlAxis.XY);
                }

                [Test]
                public void DoLoadLevel_Splitting_10_d()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel_Splitting_3), "Splitting_10_d");
                        // level specific tests
                        TestControlAxis(ControlAxis.XY);
                }

                [Test]
                public void DoLoadLevel_Splitting_0_d()
                {
                        StandardLevelTest(new LoadLevelDelegate(lvl.DoLoadLevel_Splitting_4), "Splitting_0_d");
                        // level specific tests
                        TestControlAxis(ControlAxis.XY);
                }

                public void StandardLevelTest(LoadLevelDelegate loader, string levelName)
                {
                        // Console.WriteLine("Executing: " + levelName);
                        loader();
                        TestVisualLimits();
                        TestLeftLowerOptimizationBound();
                        TestLeftLowerOptimizationBoundPhysical();
                        TestRightUpperOptimizationBound();
                        TestRightUpperOptimizationBoundPhysical();
                        TestGetInitialCursorPoint();
                        TestGetTargetCursorPoint();
                        TestGetPotential();
                        TestGetTSetControlHistory();
                        OptimizerTestRoutine(levelName);
                }

                [Test]
                public void GetDim()
                {
                        Assert.AreEqual(size, lvl.GetDim());
                }

                [Test]
                public void GetFidelity()
                {
                        Assert.GreaterOrEqual(lvl.GetFidelity(), 0);
                }

                [Test]
                public void DoResetLevel()
                {
                        lvl.StartRecordingSteps();
                        for (int i = 0 ; i < 100 ; ++i)
                        {
                                lvl.DoStep( new Vector2(0, 0) );
                        }
                        lvl.DoResetLevel();
                        Assert.AreEqual(0, lvl.GetT());
                }

                [Test]
                public void OptimizeSingleIterUpdateAvailable()
                {
                        // WARNING: std::runtime_error is thrown when there is less than 2 items in control history!
                        // (NaN in controls)
                        ProduceDoStep();
			
                        lvl.DoOptimize();

                while(lvl.GetOptimizerIsRunning())
                {
                    lvl.DoOptimizeStopRequest();
                }


                // there must have been at least one iteration of the optimization algorithm!
                Assert.AreEqual(true, lvl.IsNewControlDataAvailable());
                Assert.AreEqual("Stopping by request", lvl.GetOptimizerStopMessage());
                }

                [Test]
                public void OptimizeSingleIterUpdateGet()
                {
                        ProduceDoStep();

                        lvl.DoOptimize();

                while(lvl.GetOptimizerIsRunning())
                {
                    lvl.DoOptimizeStopRequest();
                }

                // there must have been at least one iteration of the optimization algorithm!
                Assert.AreEqual(true, lvl.IsNewControlDataAvailable());
                lvl.GetOptimizedControlHistory();
                Assert.AreEqual(false, lvl.IsNewControlDataAvailable());
                }
		
                [Test]
                public void DoOptimize()
                {
                        ProduceDoStep();

                        lvl.DoOptimize( optimizerIters );

                        while(lvl.GetOptimizerIsRunning()) { }

                        Assert.GreaterOrEqual(optimizerIters, lvl.GetOptimizerIteration());
                }

                public void OptimizerStopRequest()
                {
                        ProduceDoStep();
                        lvl.DoOptimize( 10000 );

                        Assert.AreEqual(lvl.GetOptimizerStopCondition(), OptimizerStopCondition.NONE);
                        lvl.DoOptimizeStopRequest();

                        while(lvl.GetOptimizerIsRunning())
                        { }
                        Assert.AreEqual(lvl.GetOptimizerStopCondition(), OptimizerStopCondition.USER);
                }

                public void OptimizerStopIterations()
                {
                        ProduceDoStep();
                        lvl.DoOptimize( optimizerIters );

                        Assert.AreEqual(lvl.GetOptimizerStopCondition(), OptimizerStopCondition.NONE);
                        while(lvl.GetOptimizerIsRunning())
                        { }
                        Assert.AreEqual(lvl.GetOptimizerStopCondition(), OptimizerStopCondition.ITERATIONS);
                }

                [Test]
                public void DoOptimizeSetControl()
                {
                        List<Vector2> newHistory = new List<Vector2>();
                        for (int i = 0 ; i < propagationSteps * 2 ; ++i)
                        {
                                newHistory.Add(new Vector2(i, i));
                        }
                        lvl.SetControlHistory(newHistory);
                        lvl.DoOptimize( optimizerIters );

                        while(lvl.GetOptimizerIsRunning()) { }

                        Assert.AreEqual(optimizerIters, lvl.GetOptimizerIteration());
                }

                [Test]
                public void DoOptimizeCycle()
                {
                        for (int k = 1 ; k < 10 ; ++k)
                        {
                                List<Vector2> newHistory = ProduceSetControls();
                                int num = newHistory.Count;
                                lvl.SetControlHistory(newHistory);
                                Assert.AreEqual(lvl.GetOptimizerIteration(), 0);
                                lvl.DoOptimize( optimizerIters );
                                while(lvl.GetOptimizerIsRunning()) { }
                                newHistory = lvl.GetOptimizedControlHistory();
                                Assert.AreEqual(newHistory.Count, num);

                                Assert.LessOrEqual(lvl.GetOptimizerIteration(), optimizerIters);

                                ProduceDoStep();
                                Assert.AreEqual(lvl.GetOptimizerIteration(), 0);

                                lvl.DoOptimize( optimizerIters );
                                Assert.LessOrEqual(lvl.GetOptimizerIteration(), optimizerIters);

                                while(lvl.GetOptimizerIsRunning()) { }
                        }
                }

                [Test]
                public void OptimizerResetState()
                {
                        int optimizerIters = 10;
                        ProduceSetControls();

                        Assert.AreEqual(lvl.GetOptimizerIteration(), 0);

                        lvl.DoOptimize( optimizerIters );
                        while(lvl.GetOptimizerIsRunning()) { }

                        Assert.LessOrEqual(lvl.GetOptimizerIteration(), optimizerIters);

                        ProduceDoStep();
                        Assert.AreEqual(lvl.GetOptimizerIteration(), 0);

                        lvl.DoOptimize( optimizerIters );
                        Assert.LessOrEqual(lvl.GetOptimizerIteration(), optimizerIters);

                        while(lvl.GetOptimizerIsRunning()) { }
                }

                public void DoStepInvalid0()
                {
                        lvl.DoStep(new Vector2(1, 2));
                }

                public void DoStepInvalid1()
                {
                        lvl.StartRecordingSteps();
                        lvl.StopRecordingSteps();
                        lvl.DoStep(new Vector2(1, 2));
                }

                public void DoStepInvalid2()
                {
                        ProduceSetControls();
                        lvl.DoStep(new Vector2(1, 2));
                }

                [Test]
                public void DoStepInvalid()
                {
                        Assert.Throws(typeof(LevelException), delegate { DoStepInvalid0(); });
                        Assert.Throws(typeof(LevelException), delegate { DoStepInvalid1(); });
                        Assert.Throws(typeof(LevelException), delegate { DoStepInvalid2(); });

                }

                public void SetControlHistoryInvalid0()
                {
                        lvl.StartRecordingSteps();
                        lvl.DoStep(new Vector2(1, 2));
                        lvl.SetControlHistory(new List<Vector2>());
                }

                public void SetControlHistoryInvalid1()
                {
                        lvl.SetControlHistory(new List<Vector2>()); // should result in too short controls
                }

                [Test]
                public void SetControlHistoryInvalid()
                {
                        Assert.Throws(typeof(LevelException), delegate { SetControlHistoryInvalid0(); });
                        Assert.Throws(typeof(LevelException), delegate { SetControlHistoryInvalid1(); });
                }

                public void DoOptimizeInvalid0()
                {
                        lvl.StartRecordingSteps();
                        lvl.DoOptimize(1);
                }

                public void DoOptimizeInvalid1()
                {
                        lvl.DoOptimize(1);
                }

                public void DoOptimizeInvalid2()
                {
                        lvl.StartRecordingSteps();
                        lvl.StopRecordingSteps();
                        lvl.DoOptimize(1); // should result in too short controls
                }

                [Test]
                public void DoOptimizeInvalid()
                {
                        Assert.Throws(typeof(LevelException), delegate { DoOptimizeInvalid0(); });
                        Assert.Throws(typeof(LevelException), delegate { DoOptimizeInvalid1(); });
                        Assert.Throws(typeof(LevelException), delegate { DoOptimizeInvalid2(); });
                }

                public void DoRecordInvalid0()
                {
                        lvl.StopRecordingSteps();
                        lvl.StartRecordingSteps();
                }

                public void DoRecordInvalid1()
                {
                        lvl.StopRecordingSteps();
                        lvl.StopRecordingSteps();
                }

                public void DoRecordInvalid2()
                {
                        lvl.StartRecordingSteps();
                        lvl.StartRecordingSteps();
                }

                [Test]
                public void DoRecordInvalid()
                {
                        Assert.Throws(	typeof(LevelException), delegate { DoRecordInvalid0(); } );
                        Assert.Throws(	typeof(LevelException), delegate { DoRecordInvalid1(); } );
                        Assert.Throws(	typeof(LevelException), delegate { DoRecordInvalid2(); } );
			
                }

                [Test]
                public void StopRecordingSteps()
                {
                        lvl.StartRecordingSteps();
                        for (int i = 0 ; i < 10 ; ++i)
                        {
                                lvl.StopRecordingSteps();
                        }
                }

                [Test]
                public void DoOptimizeStepsSetControlHistory()
                {
                        ProduceDoStep();

                        lvl.DoOptimize( optimizerIters );
                        while(lvl.GetOptimizerIsRunning()) { }

                        Assert.GreaterOrEqual(optimizerIters, lvl.GetOptimizerIteration());

                        // get the history
                        List<Vector2> oldOptimizedHistory = lvl.GetOptimizedControlHistory();
                        Assert.AreNotEqual(0, oldOptimizedHistory.Count);

                        // create some new history
                        List<Vector2> newHistory = ProduceSetControls();
                        lvl.SetControlHistory(newHistory);
                        lvl.DoOptimize( optimizerIters );
                        Assert.AreNotEqual(newHistory.Count, oldOptimizedHistory.Count);
                        while(lvl.GetOptimizerIsRunning()) {}

                        // obtain the new history
                        newHistory = lvl.GetOptimizedControlHistory();

                        // check if the sizes are different
                        Assert.AreNotEqual(newHistory.Count, oldOptimizedHistory.Count);
                }

                [Test]
                public void GetOptimizedControlHistory()
                {
                        ProduceDoStep();

                        lvl.DoOptimize( optimizerIters );

                        while(lvl.GetOptimizerIsRunning()) { }
                        List<Vector2> history = lvl.GetOptimizedControlHistory();

                        for (int i = 0 ; i < history.Count ; ++i)
                        {
                                Assert.That(history[ i ], Is.Not.NaN);
                        }
                }


                [Test]
                public void GetOptimizedControlHistoryPhysical()
                {
                        ProduceDoStep();

                        lvl.DoOptimize( optimizerIters );

                        while(lvl.GetOptimizerIsRunning()) { }
                        List<Vector2> history = lvl.GetOptimizedControlHistoryPhysical();

                        for (int i = 0 ; i < history.Count ; ++i)
                        {
                                Assert.That(history[ i ], Is.Not.NaN);
                        }
                }

                [Test]
                public void GetControlHistoryPhysicalTest()
                {
                        ProduceDoStep();

                        while(lvl.GetOptimizerIsRunning()) { }
                        List<Vector2> history = lvl.GetControlHistoryPhysical();

                        for (int i = 0 ; i < history.Count ; ++i)
                        {
                                Assert.That(history[ i ], Is.Not.NaN);
                        }
                }


                [Test]
                public void GetControlHistoryTest()
                {
                        ProduceDoStep();

                        while(lvl.GetOptimizerIsRunning()) { }
                        List<Vector2> history = lvl.GetControlHistory();

                        for (int i = 0 ; i < history.Count ; ++i)
                        {
                                Assert.That(history[ i ], Is.Not.NaN);
                        }
                }

                [Test]
                public void CheckFidelityUpdateSetControlHistory()
                {
                        ProduceDoStep();
                        lvl.DoOptimize( optimizerIters );
                        while(lvl.GetOptimizerIsRunning()) { }

                        double newFidelity = lvl.GetFidelity();
                        double newOptimizedFidelity = lvl.GetOptimizedFidelity();

                        Assert.GreaterOrEqual(optimizerIters, lvl.GetOptimizerIteration());

                        // create some new history
                        List<Vector2> newHistory = ProduceSetControls();
                        lvl.SetControlHistory(newHistory);
			
                        Assert.AreNotEqual(lvl.GetFidelity(), newFidelity);
                        Assert.AreNotEqual(lvl.GetOptimizedFidelity(), newOptimizedFidelity);
                }

                // Control tests

                void InitTargetBoundTest(Vector2 init, Vector2 target)
                {
                        Assert.AreEqual(init.x, lvl.GetInitialCursorPoint().x);
                        Assert.AreEqual(init.y, lvl.GetInitialCursorPoint().y);
                        Assert.AreEqual(target.x, lvl.GetTargetCursorPoint().x);
                        Assert.AreEqual(target.y, lvl.GetTargetCursorPoint().y);
                }

                [Test]
                public void CheckControlBoundsDoStepOptimize()
                {
                        Vector2 initControls = lvl.GetInitialCursorPoint();
                        Vector2 finalControls = lvl.GetTargetCursorPoint();

                        ProduceDoStep();

                        lvl.DoOptimize( optimizerIters );

                        while(lvl.GetOptimizerIsRunning()) { }

                        InitTargetBoundTest(initControls, finalControls);
                }

                [Test]
                public void CheckControlBoundsSetControlHistoryOptimize()
                {
                        Vector2 initControls = lvl.GetInitialCursorPoint();
                        Vector2 finalControls = lvl.GetTargetCursorPoint();

                        ProduceSetControls();

                        lvl.DoOptimize( optimizerIters );

                        while(lvl.GetOptimizerIsRunning()) { }

                        InitTargetBoundTest(initControls, finalControls);
                }

                [Test]
                public void CheckControlBounds()
                {
                        Vector2 initControls = lvl.GetInitialCursorPoint();
                        Vector2 finalControls = lvl.GetTargetCursorPoint();
                        ProduceSetControls();
                        lvl.DoOptimize( optimizerIters );
                        while(lvl.GetOptimizerIsRunning()) { }
                        InitTargetBoundTest(initControls, finalControls);
                        ProduceDoStep();
                        lvl.DoOptimize( optimizerIters );
                        while(lvl.GetOptimizerIsRunning()) { }
                        InitTargetBoundTest(initControls, finalControls);
                }

                [Test]
                public void GetControlHistory()
                {
                        Vector2 vec2;
                        List<Vector2> referenceList = new List<Vector2>();
                        referenceList.Add(lvl.GetInitialCursorPoint());
                        lvl.StartRecordingSteps();
                        for (int i = 0 ; i < propagationSteps ; ++i)
                        {
                                vec2 = new Vector2(i % 5, i % 5);
                                lvl.DoStep(vec2);
                                referenceList.Add( vec2 );
                        }
                        lvl.StopRecordingSteps();

                        List<Vector2> outList = lvl.GetControlHistory();
                        for (int i = 0 ; i < propagationSteps ; ++i)
                        {
                                Assert.AreEqual(outList[i].x, referenceList[i].x);
                                Assert.AreEqual(outList[i].y, referenceList[i].y);
                        }
                }

                [Test]
                public void FinalControlAppended()
                {
                        ProduceDoStep( propagationSteps ); // produce random steps, expecting the final control to be appended to the controls after we stop recording
                        List<Vector2> ctrl = lvl.GetControlHistory();
                        int count = ctrl.Count;
                        Assert.AreNotEqual(propagationSteps, count); // check that the nubmer of steps we took explicitly is not equal to the total number of steps taken
                        Assert.AreEqual(lvl.GetTargetCursorPoint().x, ctrl[count - 1].x);
                        Assert.AreEqual(lvl.GetTargetCursorPoint().y, ctrl[count - 1].y);
                }

                [Test]
                public void SetControlHistory()
                {
                        List<Vector2> referenceList = ProduceSetControls();

                        List<Vector2> outList = lvl.GetControlHistory();

                        // we expect matches between 2nd and (n-1)th element
                        for (int i = 1 ; i < propagationSteps - 1 ; ++i)
                        {
                                Assert.AreEqual(referenceList[i].x, outList[i].x);
                                Assert.AreEqual(referenceList[i].y, outList[i].y);
                        }
                }

                [Test]
                public void GetDtSetControlHistory()
                {
                        double dt = lvl.GetDt();
                        ProduceSetControls();
                        Assert.AreEqual(lvl.GetDt(), dt);
                }

                [Test]
                public void GetTDoStep()
                {
                        lvl.StartRecordingSteps();
                        for (int i = 0 ; i < propagationSteps ; ++i)
                        {
                                lvl.DoStep( new Vector2(i, i) );
                        }
                        lvl.StopRecordingSteps();
                        Assert.AreEqual( (propagationSteps + 1) * lvl.GetDt(), lvl.GetT());
                }


                [Test]
                public void GetTSetControlHistoryDoStep()
                {
                        ProduceSetControls();

                        lvl.StartRecordingSteps();
                        for (int i = 0 ; i < propagationSteps ; ++i)
                        {
                                lvl.DoStep( new Vector2(i, i) );
                        }
                        lvl.StopRecordingSteps();
                        Assert.AreEqual( (2 * (propagationSteps ) ) * lvl.GetDt(), lvl.GetT());
                }


                // Wavefunction tests

                [Test]
                public void GetInitialWavefunctionNorm()
                {
                        List<double> wavefunc = lvl.GetInitialWavefunction();
                        for (int i = 0 ; i < wavefunc.Count ; ++i)
                        {
                                Assert.GreaterOrEqual(1, wavefunc[ i ]);
                                Assert.That(wavefunc[ i ], Is.Not.NaN);
                        }
                }

                [Test]
                public void GetTargetWavefunctionNorm()
                {
                        List<double> wavefunc = lvl.GetTargetWavefunction();
                        for (int i = 0 ; i < wavefunc.Count ; ++i)
                        {
                                Assert.That(wavefunc[ i ], Is.Not.NaN);
                                Assert.GreaterOrEqual(1, wavefunc[ i ]);
                        }
                }

                [Test]
                public void GetCurrentWavefunctionNorm()
                {
                        lvl.StartRecordingSteps();
                        for (int i = 0 ; i < propagationSteps ; ++i)
                        {
                                Vector2 vec2 = new Vector2(i % 5, i % 5);
                                lvl.DoStep( vec2 );
                        }

                        List<double> wavefunc = lvl.GetCurrentWavefunction();
                        for (int i = 0 ; i < wavefunc.Count ; ++i)
                        {
                                Assert.That(wavefunc[ i ], Is.Not.NaN);
                                Assert.GreaterOrEqual(1, wavefunc[ i ]);
                        }
                }

                [Test]
                public void GetInitialWavefunctionPhase()
                {
                        List<double> phase = lvl.GetInitialWavefunctionPhase();
                        for (int i = 0 ; i < phase.Count ; ++i)
                        {
                                Assert.That(phase[ i ], Is.Not.NaN);
                                Assert.GreaterOrEqual(2 * Math.PI, phase[i]);
                                Assert.LessOrEqual(0, phase[i]);
                        }
                }

                [Test]
                public void GetTargetWavefunctionPhase()
                {
                        List<double> phase = lvl.GetTargetWavefunctionPhase();
                        for (int i = 0 ; i < phase.Count ; ++i)
                        {
                                Assert.That(phase[ i ], Is.Not.NaN);
                                Assert.GreaterOrEqual(2 * Math.PI, phase[i]);
                                Assert.LessOrEqual(0, phase[i]);
                        }
                }

                [Test]
                public void GetCurrentWavefunctionPhase()
                {
                        lvl.StartRecordingSteps();
                        for (int i = 0 ; i < propagationSteps ; ++i)
                        {
                                Vector2 vec2 = new Vector2(i % 5, i % 5);
                                lvl.DoStep( vec2 );
                        }

                        List<double> phase = lvl.GetCurrentWavefunctionPhase();
                        for (int i = 0 ; i < phase.Count ; ++i)
                        {
                                Assert.That(phase[ i ], Is.Not.NaN);
                                Assert.GreaterOrEqual(2 * Math.PI, phase[i]);
                                Assert.LessOrEqual(0, phase[i]);
                        }
                }

                public void DoOptimizeQEngineException()
                {
                        Vector2 vec2;
                        List<Vector2> controlList = new List<Vector2>();
			
                        for (int i = 0 ; i < propagationSteps ; ++i)
                        {
                                vec2 = new Vector2((float) Double.NaN, (float) Double.NaN);
                                controlList.Add( vec2 );
                        }
                        lvl.SetControlHistory(controlList );
                        lvl.DoOptimize(50);
                        while(lvl.GetOptimizerIsRunning()) {}
                }

                [Test]
                public void QEngineExceptions()
                {
                        Assert.Throws(	typeof(LevelException), delegate { DoOptimizeQEngineException(); } );
                }
        }

        [TestFixture( 1 )]
        [TestFixture( 10 )]
        [TestFixture( 100 )]
        [TestFixture( 1000 )]
        public class RVecWrapperTests
        {
                // RVecWrapper methods
                RVecWrapper wrapper;
                uint vecSize;
                int initVal;

                public RVecWrapperTests(int length)
                {
                        vecSize = (uint) length;
                }

                [SetUp]
                public void Init()
                {
                        vecSize = 100;
                        initVal = 999;
                        wrapper = new RVecWrapper( vecSize, initVal );
                }

                [Test]
                public void size()
                {
                        Assert.AreEqual(vecSize, wrapper.size());
                }

                [Test]
                public void at()
                {
                        for (uint i = 0 ; i < wrapper.size() ; ++i)
                        {
                                Assert.AreEqual(initVal, wrapper.at( i ));
                        }
                }

                [Test]
                public void set()
                {
                        for (uint i = 0 ; i < wrapper.size() ; ++i)
                        {
                                wrapper.set(i, i * 5);
                        }

                        for (uint i = 0 ; i < wrapper.size() ; ++i)
                        {
                                Assert.AreEqual(i * 5, wrapper.at( i ));
                        }
                }

                [Test]
                public void copy()
                {
                        RVecWrapper newVec = new RVecWrapper(vecSize, initVal + 1);
                        csqenginewrapper.CopyRVec(newVec, wrapper);
                        for (uint i = 0 ; i < vecSize ; ++i)
                        {
                                Assert.AreEqual(newVec.at( i ), wrapper.at( i ));
                        }
                }
        }

        [TestFixture]
        public class RVecWrapperVector2ConversionTests
        {
                Vector2 vec2;
                RVecWrapper wrapper;

                int size;
                double initVal;

                [SetUp]
                public void Init()
                {
                        size = 2;
                        initVal = 0;
                        vec2 = new Vector2(0, 0);
                        wrapper = new RVecWrapper(size, initVal);
                        Assert.AreEqual(size, 2);
                }

                [Test]
                public void RVecConversionVector2Alloc()
                {
                        vec2 = RVecConversion.ToVector2( wrapper );
                        Assert.AreEqual(vec2.x, wrapper.at( 0 ));
                        Assert.AreEqual(vec2.y, wrapper.at( 1 ));
                }


                [Test]
                public void RVecConversionVector2RefAlloc()
                {
                        vec2 = RVecConversion.ToVector2( ref wrapper );
                        Assert.AreEqual(vec2.x, wrapper.at( 0 ));
                        Assert.AreEqual(vec2.y, wrapper.at( 1 ));
                }

                [Test]
                public void RVecConversionVector2Ref()
                {
                        RVecConversion.ToVector2( ref wrapper, ref vec2 );
                        Assert.AreEqual(vec2.x, wrapper.at( 0 ));
                        Assert.AreEqual(vec2.y, wrapper.at( 1 ));
                }

                [Test]
                public void Vector2ConversionRVecRefAlloc()
                {

                        wrapper = RVecConversion.FromVector2( ref vec2 );
                        Assert.AreEqual(vec2.x, wrapper.at( 0 ));
                        Assert.AreEqual(vec2.y, wrapper.at( 1 ));
                }

                [Test]
                public void Vector2ConversionRVecAlloc()
                {

                        wrapper = RVecConversion.FromVector2( vec2 );
                        Assert.AreEqual(vec2.x, wrapper.at( 0 ));
                        Assert.AreEqual(vec2.y, wrapper.at( 1 ));
                }

                [Test]
                public void Vector2ConversionRVecRef()
                {

                        RVecConversion.FromVector2( ref vec2, ref wrapper );
                        Assert.AreEqual(vec2.x, wrapper.at( 0 ));
                        Assert.AreEqual(vec2.y, wrapper.at( 1 ));
                }

                [Test]
                public void DListInvalidConversionRVec()
                {
                        Assert.Throws<InvalidLengthException> ( () =>
                                {
                                        Vector2 newVec = new Vector2(1, 1);
                                        RVecWrapper newWrap = new RVecWrapper(size + 1);
                                        RVecConversion.FromVector2( ref newVec, ref newWrap );
                                });
                }

                [Test]
                public void RVecInvalidConversionDList()
                {
                        Assert.Throws<InvalidLengthException> ( () =>
                                {
                                        Vector2 newVec = new Vector2(1, 1);
                                        RVecWrapper newWrap = new RVecWrapper(size + 1);
                                        RVecConversion.ToVector2( ref newWrap, ref newVec );
                                });
                }

                [TearDown]
                public void AlterWrapperValues()
                {
                        for (int i = 0 ; i < wrapper.size() ; ++i)
                        {
                                wrapper.set(i, wrapper.at(i) + 1);
                        }
                }

                [TearDown]
                public void AlterVector2Values()
                {
                        vec2.x = (float) wrapper.at( 0 ) + 1;
                        vec2.y = (float) wrapper.at( 1 ) + 2;
                }
        }


        [TestFixture( 1 )]
        [TestFixture( 10 )]
        [TestFixture( 100 )]
        [TestFixture( 1000 )]
        public class RVecWrapperDListConversionTests
        {
                List<double> dlist;
                RVecWrapper wrapper;

                int size;
                double initVal;

                public RVecWrapperDListConversionTests(int length)
                {
                        size = length;
                }

                [SetUp]
                public void Init()
                {
                        initVal = 0;
                        dlist = new List<double> (size);
                        for (int i = 0 ; i < size ; ++i)
                        {
                                dlist.Add( initVal );
                        }
                        wrapper = new RVecWrapper(size, initVal);
                }

                [Test]
                public void RVecConversionDListAlloc()
                {
                        dlist = RVecConversion.ToDList( wrapper );
                        CheckEqualVals();
                }

                [Test]
                public void RVecConversionDListRefAlloc()
                {
                        dlist = RVecConversion.ToDList( ref wrapper );
                        CheckEqualVals();
                }

                [Test]
                public void RVecConversionDListRef()
                {
                        RVecConversion.ToDList( ref wrapper, ref dlist );
                        CheckEqualVals();
                }

                [Test]
                public void DListConversionRVecRefAlloc()
                {
                        wrapper = RVecConversion.FromDList( ref dlist );
                        Assert.AreEqual(wrapper.size(), dlist.Count);
                        CheckEqualVals();
                }

                [Test]
                public void DListConversionRVecAlloc()
                {

                        wrapper = RVecConversion.FromDList( dlist );
                        CheckEqualVals();
                }

                [Test]
                public void DListConversionRVecRef()
                {

                        RVecConversion.FromDList( ref dlist, ref wrapper );
                        CheckEqualVals();
                }

                [Test]
                public void DListInvalidConversionRVec()
                {
                        Assert.Throws<InvalidLengthException> ( () =>
                                {
                                        List<double> newList= new List<double>(size + 1);
                                        for (int i = 0 ; i < size + 1 ; ++i)
                                        {
                                                newList.Add( i );
                                        }
                                        RVecConversion.FromDList( ref newList, ref wrapper );
                                });
                }

                [Test]
                public void RVecInvalidConversionDList()
                {
                        Assert.Throws<InvalidLengthException> ( () =>
                                {
                                        List<double> newList= new List<double>(size + 1);
                                        for (int i = 0 ; i < size + 1 ; ++i)
                                        {
                                                newList.Add( i );
                                        }
                                        RVecConversion.ToDList( ref wrapper, ref newList );
                                });
                }

                [TearDown]
                public void AlterWrapperValues()
                {
                        for (int i = 0 ; i < wrapper.size() ; ++i)
                        {
                                wrapper.set(i, wrapper.at(i) + 1);
                        }
                }

                [TearDown]
                public void AlterDListValues()
                {
                        for (int i = 0 ; i < size ; ++i)
                        {
                                dlist[ i ] = wrapper.at( i ) + 1;
                        }
                }

                public void CheckEqualVals()
                {
                        Assert.AreEqual(wrapper.size(), dlist.Count);
                        for (int i = 0 ; i < size ; ++i)
                        {
                                Assert.AreEqual(wrapper.at( i ), dlist[ i ]);
                        }
                }
        }

        [TestFixture( 1 )]
        [TestFixture( 10 )]
        [TestFixture( 100 )]
        [TestFixture( 1000 )]
        public class RVecWrapperDArrayConversionTests
        {
                double[] darray;
                RVecWrapper wrapper;

                int size;
                double initVal;

                public RVecWrapperDArrayConversionTests(int length)
                {
                        size = length;
                }

                [SetUp]
                public void Init()
                {
                        initVal = 0;
                        darray = new double[size];
                        for (int i = 0 ; i < size ; ++i)
                        {
                                darray[ i ] = initVal;
                        }
                        wrapper = new RVecWrapper(size, initVal);
                }

                [Test]
                public void RVecConversionDArrayAlloc()
                {
                        darray = RVecConversion.ToDArray( wrapper );
                        CheckEqualVals();
                }

                [Test]
                public void RVecConversionDArrayRefAlloc()
                {
                        darray = RVecConversion.ToDArray( ref wrapper );
                        CheckEqualVals();
                }

                [Test]
                public void RVecConversionDArrayRef()
                {
                        RVecConversion.ToDArray( ref wrapper, ref darray );
                        CheckEqualVals();
                }

                [Test]
                public void DArrayConversionRVecRefAlloc()
                {
                        wrapper = RVecConversion.FromDArray( ref darray );
                        CheckEqualVals();
                }

                [Test]
                public void DArrayConversionRVecAlloc()
                {

                        wrapper = RVecConversion.FromDArray( darray );
                        CheckEqualVals();
                }

                [Test]
                public void DArrayConversionRVecRef()
                {

                        RVecConversion.FromDArray( ref darray, ref wrapper );
                        CheckEqualVals();
                }

                [Test]
                public void DArrayInvalidConversionRVec()
                {
                        Assert.Throws<InvalidLengthException> ( () =>
                                {
                                        double[] array = new double[size + 1];
                                        RVecConversion.FromDArray( ref array, ref wrapper );
                                });
                }

                [Test]
                public void RVecInvalidConversionDArray()
                {
                        Assert.Throws<InvalidLengthException> ( () =>
                                {
                                        double[] array = new double[size + 1];
                                        RVecConversion.ToDArray( ref wrapper, ref array );
                                });
                }

                [TearDown]
                public void AlterWrapperValues()
                {
                        for (int i = 0 ; i < wrapper.size() ; ++i)
                        {
                                wrapper.set(i, wrapper.at(i) + 1);
                        }
                }

                [TearDown]
                public void AlterDArrayValues()
                {
                        for (int i = 0 ; i < size ; ++i)
                        {
                                darray[ i ] = wrapper.at( i ) + 1;
                        }
                }

                public void CheckEqualVals()
                {
                        Assert.AreEqual(wrapper.size(), darray.Length);
                        for (int i = 0 ; i < size ; ++i)
                        {
                                Assert.AreEqual(wrapper.at( i ), darray[ i ]);
                        }
                }
        }

        [TestFixture( 1 )]
        [TestFixture( 10 )]
        [TestFixture( 100 )]
        [TestFixture( 1000 )]
    public class stdVec_RVecWrapperVector2ListConversionTests
        {
        stdVec_RVecWrapper wrapper;
                List<Vector2> vlist;

                int size;
                double initVal;

                public stdVec_RVecWrapperVector2ListConversionTests(int length)
                {
                        size = length;
                }

                [SetUp]
                public void Init()
                {
                        initVal = 1.0;
                        vlist = new List<Vector2>();
                        for (int i = 0 ; i < size ; ++i)
                        {
                                vlist.Add( new Vector2((float) (initVal + 1.0), (float) (initVal + 1.0)) );
                        }
                        wrapper = new stdVec_RVecWrapper();
                        for (int i = 0 ; i < size ; ++i)
                        {
                                wrapper.Add( new RVecWrapper(2, initVal) );
                        }
                }

                [Test]
        public void stdVec_RVecWrapperConversionVector2ListAlloc()
                {
                        vlist = RVecConversion.ToVector2List( wrapper );
                        CheckEqualVals();
                }

                [Test]
        public void stdVec_RVecWrapperConversionVector2ListRefAlloc()
                {
                        vlist = RVecConversion.ToVector2List( ref wrapper );
                        CheckEqualVals();
                }

                [Test]
        public void stdVec_RVecWrapperConversionVector2ListRef()
                {
                        RVecConversion.ToVector2List( ref wrapper, ref vlist );
                        CheckEqualVals();
                }

                [Test]
        public void Vector2ListConversionstdVec_RVecWrapperRefAlloc()
                {
                        wrapper = RVecConversion.FromVector2List( ref vlist );
                        CheckEqualVals();
                }

                [Test]
        public void Vector2ListConversionstdVec_RVecWrapperAlloc()
                {

                        wrapper = RVecConversion.FromVector2List( vlist );
                        CheckEqualVals();
                }

                [Test]
        public void Vector2ListConversionstdVec_RVecWrapperRef()
                {

                        RVecConversion.FromVector2List( ref vlist, ref wrapper );
                        CheckEqualVals();
                }

                [Test]
        public void Vector2ListInvalidConversionstdVec_RVecWrapper()
                {
                        Assert.Throws<InvalidLengthException> ( () =>
                                {
                                        List<Vector2> vtmp = new List<Vector2>();
                                        for (int i = 0 ; i < size + 1 ; ++i)
                                        {
                                                vtmp.Add(new Vector2(1, 1));
                                        }
                                        RVecConversion.FromVector2List( ref vtmp, ref wrapper );
                                });
                }

                [Test]
        public void stdVec_RVecWrapperInvalidConversionVector2List()
                {
                        Assert.Throws<InvalidLengthException> ( () =>
                                {
                                        List<Vector2> vtmp = new List<Vector2>();
                                        for (int i = 0 ; i < size + 1 ; ++i)
                                        {
                                                vtmp.Add(new Vector2(1, 1));
                                        }
                                        RVecConversion.ToVector2List( ref wrapper, ref vtmp );
                                });
                }

                [TearDown]
                public void AlterWrapperValues()
                {
                        for (int i = 0 ; i < wrapper.Count ; ++i)
                        {
                                RVecWrapper tmp = new RVecWrapper(2, 2 * (i + 1));
                                wrapper[ i ] = tmp;
                        }
                }

                [TearDown]
                public void AlterVector2ListValues()
                {
                        for (int i = 0 ; i < size ; ++i)
                        {
                                vlist[ i ] = new Vector2((float) wrapper[ i ].at( 0 ) + 1, (float) wrapper[ i ].at( 1 ) + 1);
                        }
                }

                public void CheckEqualVals()
                {
                        Assert.AreEqual(wrapper.Count, vlist.Count);
                        for (int i = 0 ; i < size ; ++i)
                        {
                                RVecWrapper lhs = wrapper[ i ];
                                Vector2 rhs = vlist[ i ];
                                Assert.AreEqual(lhs.at( 0 ), rhs.x);
                                Assert.AreEqual(lhs.at( 1 ), rhs.y);
                        }
                }
        }


        [TestFixture( 1 )]
        [TestFixture( 10 )]
        [TestFixture( 100 )]
        [TestFixture( 1000 )]
        [TestFixture( 10000 )]
        [TestFixture( 100000 )]
        public class ControlWrapperVector2ListConversionTests
        {
                ControlWrapper wrapper;
                List<Vector2> vlist;

                int size;
                double initVal;

                public ControlWrapperVector2ListConversionTests(int length)
                {
                        size = length;
                }

                [SetUp]
                public void Init()
                {
                        initVal = 1.0;
                        vlist = new List<Vector2>();
                        for (int i = 0 ; i < size ; ++i)
                        {
                                vlist.Add( new Vector2((float) (initVal + 1.0), (float) (initVal + 1.0)) );
                        }
                        wrapper = new ControlWrapper(size, 2);
                        for (int i = 0 ; i < size ; ++i)
                        {
                                wrapper.set( i, 2, initVal);
                        }
                }

                [Test]
        public void ControlWrapperConversionVector2ListAlloc()
                {
                        vlist = RVecConversion.ToVector2List( wrapper );
                        CheckEqualVals();
                }

                [Test]
        public void ControlWrapperConversionVector2ListRefAlloc()
                {
                        vlist = RVecConversion.ToVector2List( ref wrapper );
                        CheckEqualVals();
                }

                [Test]
        public void ControlWrapperConversionVector2ListRef()
                {
                        RVecConversion.ToVector2List( ref wrapper, ref vlist );
                        CheckEqualVals();
                }

                [Test]
        public void Vector2ListConversionControlWrapperRefAlloc()
                {
                        wrapper = RVecConversion.FromVector2ListToControlWrapper( ref vlist );
                        CheckEqualVals();
                }

                [Test]
        public void Vector2ListConversionControlWrapperAlloc()
                {

                        wrapper = RVecConversion.FromVector2ListToControlWrapper( vlist );
                        CheckEqualVals();
                }

                [Test]
        public void Vector2ListConversionControlWrapperRef()
                {

                        RVecConversion.FromVector2List( ref vlist, ref wrapper );
                        CheckEqualVals();
                }

                [Test]
        public void Vector2ListInvalidConversionControlWrapper()
                {
                        Assert.Throws<InvalidLengthException> ( () =>
                                {
                                        List<Vector2> vtmp = new List<Vector2>();
                                        for (int i = 0 ; i < size + 1 ; ++i)
                                        {
                                                vtmp.Add(new Vector2(1, 1));
                                        }
                                        RVecConversion.FromVector2List( ref vtmp, ref wrapper );
                                });
                }

                [Test]
        public void ControlWrapperInvalidConversionVector2List()
                {
                        Assert.Throws<InvalidLengthException> ( () =>
                                {
                                        List<Vector2> vtmp = new List<Vector2>();
                                        for (int i = 0 ; i < size + 1 ; ++i)
                                        {
                                                vtmp.Add(new Vector2(1, 1));
                                        }
                                        RVecConversion.ToVector2List( ref wrapper, ref vtmp );
                                });
                }

                [TearDown]
                public void AlterWrapperValues()
                {
                        for (int i = 0 ; i < wrapper.size() ; ++i)
                        {
                                RVecWrapper tmp = new RVecWrapper(2, 2 * (i + 1));
                                wrapper.set(i, 0, tmp.at(0));
                                wrapper.set(i, 1, tmp.at(1));
                        }
                }

                [TearDown]
                public void AlterVector2ListValues()
                {
                        for (int i = 0 ; i < size ; ++i)
                        {
                                vlist[ i ] = new Vector2((float) wrapper.at( i, 0 ) + 1, (float) wrapper.at(i, 1) + 1);
                        }
                }

                public void CheckEqualVals()
                {
                        Assert.AreEqual(wrapper.size(), vlist.Count);
                        for (int i = 0 ; i < size ; ++i)
                        {
                                Assert.That( wrapper.at(i, 0 ), Is.EqualTo( vlist[ i ].x ).Within( 1e-16 ));
                                Assert.That( wrapper.at(i, 1 ), Is.EqualTo( vlist[ i ].y ).Within( 1e-16 ));
                        }
                }
        }

        [TestFixture( 1000000 )]
    public class RVecCopySpeedTests
        {
                // Tests the time of passing RVec by value OR by reference.
                // The purpose of this test is to determine whether SWIG really passes objects by reference,
                // or if there are some implicit COPY operations going on.

                int size;
                int attempts;

                public RVecCopySpeedTests(int length)
                {
                        size = length;
                        attempts = 10;
                }

                [Test]
                public void RVecTest()
                {
                        double val = 0.0;
                        RVecWrapper testObj0 = new RVecWrapper(size, val); // 'warm-up' the armadillo allocator?
                        var watch0 = System.Diagnostics.Stopwatch.StartNew();

                        RVecWrapper testObj = new RVecWrapper(size, val);
                        watch0.Stop();
                        var elapsedMs0 = watch0.ElapsedMilliseconds;


                        var watch = System.Diagnostics.Stopwatch.StartNew();
                        csqenginewrapper.GetRVecRef(testObj);
                        watch.Stop();
                        var elapsedMs = watch.ElapsedMilliseconds;


                        var watch2 = System.Diagnostics.Stopwatch.StartNew();
                        csqenginewrapper.GetRVecVal(testObj);
                        watch2.Stop();
                        var elapsedMs2 = watch2.ElapsedMilliseconds;
                }


                [Test]
                public void stdVecListTest1()
                {
                        double val = 0.0;
                        RVecWrapper testObj0 = new RVecWrapper(2, val); // 'warm-up' the armadillo allocator?
                        RVecWrapper testObj = new RVecWrapper(2, val);
                        stdVec_RVecWrapper testStdVec = new stdVec_RVecWrapper(size);
                        for (int i = 0 ; i < size ; ++i)
                        {
                            testStdVec.Add( testObj );
                        }

                        var watch0 = System.Diagnostics.Stopwatch.StartNew();

                        for (int i = 0 ; i < attempts ; ++i)
                        {
                            var output0 = RVecConversion.ToVector2List( testStdVec );
                        }

                        watch0.Stop();
                        var elapsedMs0 = watch0.ElapsedMilliseconds;
                } 

                [Test]
                public void ToVector2ListFromRVecWrapper()
                {
                        double val = 0.0;
                        RVecWrapper testStdVec = new RVecWrapper(size * 2);
                        for (int i = 0 ; i < size * 2 ; ++i)
                        {
                            testStdVec.set( i, val);
                        }

                        var watch2 = System.Diagnostics.Stopwatch.StartNew();
                        for (int i = 0 ; i < attempts ; ++i)
                        {
                            var output2 = RVecConversion.ToVector2ListFromRVecWrapper(ref testStdVec );
                        }                        
                        
                        watch2.Stop();
                        var elapsedMs2 = watch2.ElapsedMilliseconds;
                        var output3 = RVecConversion.ToVector2ListFromRVecWrapper( ref testStdVec );
                }            
 
                 [Test]
                public void ToVector2ListFromRVecWrapperP()
                {
                        double val = 0.0;
                        RVecWrapper testStdVec = new RVecWrapper(size * 2);
                        for (int i = 0 ; i < size * 2 ; ++i)
                        {
                            testStdVec.set( i, val);
                        }

                        var watch2 = System.Diagnostics.Stopwatch.StartNew();
                        for (int i = 0 ; i < attempts ; ++i)
                        {
                            var output2 = RVecConversion.ToVector2ListFromRVecWrapper( ref testStdVec );
                        }                        
                        
                        watch2.Stop();
                        var elapsedMs2 = watch2.ElapsedMilliseconds;
                        var output3 = RVecConversion.ToVector2ListFromRVecWrapper( ref testStdVec );
                }     

                 [Test]
                public void ToVector2ListFromControlWrapper()
                {
                        double val = 0.0;
                        ControlWrapper testCW = new ControlWrapper(size, 2);
                        for (int j = 0 ; j < 2 ; ++j)
                        {
                            for (int i = 0 ; i < size ; ++i)
                            {
                                testCW.set(i, j, val);    
                            }
                        }

                        var watch2 = System.Diagnostics.Stopwatch.StartNew();
                        for (int i = 0 ; i < attempts ; ++i)
                        {
                            var output2 = RVecConversion.ToVector2List( testCW );
                        }                        
                        watch2.Stop();
                        var elapsedMs2 = watch2.ElapsedMilliseconds;
                        
                        var output3 = RVecConversion.ToVector2List( testCW );
                }                    
                
                [Test]
                public void ToVector2ListFromControlWrapperR()
                {
                        double val = 0.0;
                        ControlWrapper testCW = new ControlWrapper(size, 2);
                        for (int j = 0 ; j < 2 ; ++j)
                        {
                            for (int i = 0 ; i < size ; ++i)
                            {
                                testCW.set(i, j, val);    
                            }
                        }

                        var watch2 = System.Diagnostics.Stopwatch.StartNew();
                        for (int i = 0 ; i < attempts ; ++i)
                        {
                            var output2 = RVecConversion.ToVector2List(ref testCW );
                        }                        
                        watch2.Stop();
                        var elapsedMs2 = watch2.ElapsedMilliseconds;
                        
                        var output3 = RVecConversion.ToVector2List( testCW );
                }         

        }

	public class ScoringParametersTests
	{
            [Test]
            public void GeneralTest()
            {
                ScoringEngine.GetIntroLevelParameters();
                ScoringEngine.GetIntroOptimizeLevelParameters();
                ScoringEngine.GetScienceLevelParameters();
                ScoringEngine.GetScienceVariantLevelParameters();
            }

            [Test]
            public void PointsTest()
            {

                Assert.AreEqual(6,  ScoringEngine.nIntroLevels);
                Assert.AreEqual(1,  ScoringEngine.nIntroOptimizeLevels);
                Assert.AreEqual(3,  ScoringEngine.nScienceLevels);
                Assert.AreEqual(10, ScoringEngine.nScienceVariantLevels);


                Assert.AreEqual(3000, ScoringEngine.TotalPointsIntroLevels);
                Assert.AreEqual(2000, ScoringEngine.TotalPointsIntroOptimizeLevels);
                Assert.AreEqual(10500, ScoringEngine.TotalPointsScienceLevels);
                Assert.AreEqual(7500, ScoringEngine.TotalPointsScienceVariantLevel);


                Assert.AreEqual(500,  ScoringEngine.PointsPerIntroLevel);
                Assert.AreEqual(2000,  ScoringEngine.PointsPerIntroOptimizeLevel);
                Assert.AreEqual(3500,  ScoringEngine.PointsPerScienceLevel);
                Assert.AreEqual(750,  ScoringEngine.PointsPerScienceVariantLevel);
             }

            [Test]
            public void ParameterTest()
            {

                // K
                Assert.AreEqual(1, ScoringEngine.GetIntroLevelParameters().K());
                Assert.AreEqual(1, ScoringEngine.GetIntroOptimizeLevelParameters().K());
                Assert.AreEqual(4, ScoringEngine.GetScienceLevelParameters().K());
                Assert.AreEqual(1, ScoringEngine.GetScienceVariantLevelParameters().K());

                // Intro
                var Qintro = ScoringEngine.GetIntroLevelParameters().Qs();
                Assert.AreEqual(0,Qintro[0]);
                Assert.AreEqual(ScoringEngine.PointsPerIntroLevel,ScoringEngine.nBuckets*Qintro[1]);

                // Intro opt
                var Qintroopt = ScoringEngine.GetIntroOptimizeLevelParameters().Qs();
                Assert.AreEqual(0,Qintroopt[0]);
                Assert.AreEqual(ScoringEngine.PointsPerIntroOptimizeLevel,ScoringEngine.nBuckets*Qintroopt[1]);

                // Science
                var qs = ScoringEngine.GetScienceLevelParameters().qs();
                var gs = ScoringEngine.GetScienceLevelParameters().gs();
                var Qscience = ScoringEngine.GetScienceLevelParameters().Qs();

                Assert.AreEqual(gs[0], 0.00*qs[0]);
                Assert.AreEqual(gs[1], 0.20*qs[1]);
                Assert.AreEqual(gs[2], 0.20*qs[2]);
                Assert.AreEqual(gs[3], 0.50*qs[3]);
                Assert.AreEqual(gs[4], 0.00*qs[4]);


                Assert.AreEqual(Qscience[0], 0.00*Qscience[4]);
                Assert.AreEqual(Qscience[1], 0.25*Qscience[4]);
                Assert.AreEqual(Qscience[2], 0.50*Qscience[4]);
                Assert.AreEqual(Qscience[3], 0.90*Qscience[4]);
                Assert.AreEqual(Qscience[4], 1.00*Qscience[4]);

                Assert.AreEqual(ScoringEngine.PointsPerScienceLevel, ScoringEngine.nBuckets*Qscience[4]);

                // Science variant
                var Qsciencevar = ScoringEngine.GetScienceVariantLevelParameters().Qs();
                Assert.AreEqual(0,Qsciencevar[0]);
                Assert.AreEqual(ScoringEngine.PointsPerScienceVariantLevel,ScoringEngine.nBuckets*Qsciencevar[1]);

            }
	}

	public class FidelityCurveTests
	{
            [Test]
            public void GeneralTest()
            {
                FidelityCurveEngine.GetChallengeCurveBottom();
                FidelityCurveEngine.GetChallengeCurveTop();
                FidelityCurveEngine.GetChallengeCurve1_ShakeUp();
                FidelityCurveEngine.GetChallengeCurve2_ShakeUp();
                FidelityCurveEngine.GetChallengeCurve3_ShakeUp();
                FidelityCurveEngine.GetChallengeCurve1_BringHomeWater();
                FidelityCurveEngine.GetChallengeCurve2_BringHomeWater();
                FidelityCurveEngine.GetChallengeCurve3_BringHomeWater();
                FidelityCurveEngine.GetChallengeCurve1_Splitting();
                FidelityCurveEngine.GetChallengeCurve2_Splitting();
                FidelityCurveEngine.GetChallengeCurve3_Splitting();
            }
	}
}
#endif
