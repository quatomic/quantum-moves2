%module csqenginewrapper

%{	
#include "ObjectWrappers/RVecWrapper.h"
#include "ObjectWrappers/ControlWrapper.h"
#include "LevelWrappers/LevelWrapperSpatial.h"
#include "LevelWrappers/LevelWrapperTypes.h"
#include "LevelWrappers/LevelConfigs.h"
%}

%include "std_string.i"
%include "std_vector.i"
%include "ObjectWrappers/RVecWrapper.h"
%include "ObjectWrappers/ControlWrapper.h"
%include "LevelWrappers/LevelWrapperSpatial.h"
%template(LevelGPE) qengine::levelwrapper::LevelWrapperSpatial<qengine::levelwrapper::gpe::GPETupleTypes>;
%template(stdVec_RVecWrapper) std::vector<RVecWrapper>;