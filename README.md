# Quantum Moves 2 - results analysis

This repository contains the code used for the optimisation and analysis of solutions from the Quantum Moves 2 game. 
It is a supplementary material for our pre-print paper: https://arxiv.org/abs/2004.03296

The paper analyses three "scientific interest" levels: *Bring Home Water*, *Splitting* and *Shake Up*.

## Disclaimer

The code is not intended to be runnable as is, since there are some external dependencies, file path dependencies and missing data (we cannot include the raw player data due to GDPR). 
The code is meant to document the procedure of optimising and anylysing the data as to arrive at the results presented in the paper.

## Content

The code is a join of five repositories that we used while working on the analysis of the results.

1. **QEngine_game_bindings**: the code binding the Quantum Moves 2 game (C# language) to the nummerical engine which is available at https://gitlab.com/quatomic/qengine.git
2. **Conventional_optimisation_algorithms**: the C++ code used for optimisation of the randomly generated solutions (links to QEngine as well).
3. **Player_opt_and_method_sampling**: python code for dispatching the player seeds for optimisation in the QEngine. Also included is the code for estimating T_QSL based on sampling a fininte number of optimised solutions from the different methods, both random and player seeds and the code for setting up the walltime comparison between SGA and GRAPE (on random seeds).
4. **Data_analysis**: python code for analysing the game solutions: clustering of optimised seeds and solution density calculation for the different methods.
5. **figs**: python and Matlab code for making the figures in the article.
