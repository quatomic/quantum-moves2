function [monotonically_best_T, monotonically_best_F] = convertToMonotonic(durations,F)



    monotonically_best_F = zeros(size(durations)); % make sure arrays are large enough, truncate later
    monotonically_best_T = zeros(size(durations));


    monotonically_best_T(1) = durations(1);
    monotonically_best_F(1) = F(1);

    best_index = 1;
    for k = 2:numel(durations)
        if F(k) >= monotonically_best_F(best_index)
            best_index = best_index + 1;
            monotonically_best_F(best_index) = F(k);
            monotonically_best_T(best_index) = durations(k);
        end
    end

    monotonically_best_F =  monotonically_best_F(1:best_index); % truncate 
    monotonically_best_T =  monotonically_best_T(1:best_index);
    
    monotonically_best_T(end+1) = durations(end);
    monotonically_best_F(end+1) = monotonically_best_F(end);
end