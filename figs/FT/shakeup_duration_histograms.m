clear all
close all
clc

cd('/figs/FT')
helperscipts =  '/figs/helperscripts';
addpath(genpath(helperscipts))
defaults()


ps = load('grape_ps_shakeup.mat')
rs = load('grape_rs_shakeup.mat')

%binEdges = 0:0.015:ps.durations(end);
binEdges = 0:0.05:ps.durations(end);

%unique_Ts = linspace(0.7,1.07,40);


figure(1)
H_ps = histogram(ps.durations,binEdges,'displayname','grape ps','facecolor',colors(1,:))
hold on
H_rs = histogram(rs.durations,H_ps.BinEdges,'displayname','grape rs','facecolor',colors(2,:))

leg  = legend;



seed_amount_difference = H_rs.Values - H_ps.Values;
figure(2)
%bar(seed_amount_difference)
p = plot(rs.durations, 1-rs.F,'.','displayname','grape rs','color',colors(2,:));
hold on
plot(ps.durations, 1-ps.F,'.','displayname','grape ps','color',colors(1,:))
set(gca,'yscale','log')

xlim([0, 1.12])
leg = legend;
leg.Location = 'southwest';

plot(ps.monotonically_best_T,1-ps.monotonically_best_F ,'displayname','grape ps','color',colors(1,:))
plot(rs.unique_durations,1-rs.best_F, 'displayname','grape rs subset','color',colors(2,:))

%% remove durations at random such that there is an equal amount of seeds
n_subsets = 100;
rs_duration_indices_in_bin_func = @(t1,t2)  t1 <= rs.durations & rs.durations < t2;

rs_duration_subsets = [];
rs_F_subsets = [];
monotonically_best_subsets = {};
best_subsets = {};

for j = 1:n_subsets    
    rs_duration_subset_j = [];
    rs_F_subset_j = [];

    for i = 1:numel(H_ps.Values)-1
        n_ps_in_bin = H_ps.Values(i);

        if n_ps_in_bin <= 1 || n_ps_in_bin > H_rs.Values(i)
            continue; 
        end

        t1 = H_ps.BinEdges(i);
        t2 = H_ps.BinEdges(i+1);

        rs_in_bin_idxs_bool = rs_duration_indices_in_bin_func(t1,t2);

        rs_in_bin_idxs = find(rs_in_bin_idxs_bool); % pick out the indices corresponding to the current bin
        random_shuffled_in_bin_idxs = rs_in_bin_idxs(randperm(length(rs_in_bin_idxs))); % pick a random subset of these

        subset_in_bin_idxs = sort(random_shuffled_in_bin_idxs(1:n_ps_in_bin));
        %subset_in_bin_idxs = sort(random_shuffled_in_bin_idxs);

        rs_in_bin = rs.durations(subset_in_bin_idxs);
        Fs_in_bin = rs.F(subset_in_bin_idxs);
                
        rs_duration_subset_j = [rs_duration_subset_j; rs_in_bin];
        rs_F_subset_j = [rs_F_subset_j; Fs_in_bin];
        
    end
    
    [monotonically_best_T_j, monotonically_best_F_j] = convertToMonotonic(rs_duration_subset_j, rs_F_subset_j);    
    monotonically_best_subsets{end+1} = [monotonically_best_T_j, monotonically_best_F_j];

    
    rs_duration_subsets = [rs_duration_subsets, rs_duration_subset_j];
    rs_F_subsets = [rs_F_subsets, rs_F_subset_j];
    
    
    
    
    
end

figure(1)
H_rs_subset = histogram(rs_duration_subset_j,H_ps.BinEdges,'displayname','grape rs subset','facealpha',0.2);


monotonically_best_final_fidelities = cellfun(@(x) x(end,2) ,monotonically_best_subsets');
quantile(1-monotonically_best_final_fidelities, [0.25, 0.50, 0.75])


% FT 
figure(3)
plot(rs_duration_subset_j, 1-rs_F_subset_j,'.','displayname','grape rs subset','color',colors(2,:))
set(gca,'yscale','log')
hold on
plot(ps.durations, 1-ps.F,'.','displayname','grape ps','color',colors(1,:))
leg = legend();
leg.Location = 'southwest';

plot(ps.monotonically_best_T,1-ps.monotonically_best_F, 'displayname','grape ps','color',colors(1,:))
plot(monotonically_best_T_j,1-monotonically_best_F_j, 'displayname','grape rs subset','color',colors(2,:))

xlim([0, 1.12])

figure(4)
semilogy(ps.monotonically_best_T,1-ps.monotonically_best_F, 'displayname','grape ps')
hold on
semilogy(rs.unique_durations,1-rs.best_F, 'displayname','grape rs')
%semilogy(monotonically_best_T_j,1-monotonically_best_F_j, 'displayname','grape rs subset')
for j = 1:n_subsets
    bestTs = monotonically_best_subsets{j}(:,1);
    bestFs = monotonically_best_subsets{j}(:,2);
    p = semilogy(bestTs,1-bestFs,'color',colors(3,:),'HandleVisibility',"off");
   % p.Color = [p.Color 3/n_subsets];
    p.Color = [p.Color 0.10];
end
p.HandleVisibility = 'on';
p.DisplayName = 'grape rs subset';
xlim([0.7, 1.07])

leg = legend();

%% Kernel density estimation

bandwidth = 0.025;%bandwidth = 0.0315;
IF_log_sampling  = linspace(-3,0,50); % mireks parameters %IF_log_sampling = -3: 5e-2:0;
%outlierDensityThreshold = 0.002; 
outlierDensityThreshold = 0.002;

%unique_Ts = unique(rs.durations);
unique_Ts = linspace(0.7,1.07,40);
%unique_Ts = linspace(0,2,2);


Ts = rs_duration_subset_j;
Fs = rs_F_subset_j;

% 
% densities = [];
% for j=1:n_subsets
%     Ts = rs_duration_subsets(:,j);
%     Fs = rs_F_subsets(:,j);
%     
%     [densities(:,:,j), outliers] = kerneldensities(Ts, Fs, unique_Ts, IF_log_sampling, bandwidth);
% end
% densities = mean(densities,3);

% rs seeds full 
%Ts = rs.durations;
%Fs = rs.F;
% ps seeds full
%Ts = ps.durations;
%Fs = ps.F;

[densities, outliers] = kerneldensities(Ts, Fs, unique_Ts, IF_log_sampling, bandwidth, outlierDensityThreshold);


load('/figs/solutiondensity/cmap_gist_stern_imitation_newcol.mat')
level_time = shakeup_time;

f1 = figure
im = imagesc(level_time.ms_from_sim * unique_Ts,IF_log_sampling,densities);
xlim([0.7 1.07])
colormap(cm)
colorbar

ax = gca;
ax.YDir = 'normal';
ax.CLim = [0, 0.1];
ax.YTick = [-3 -2 -1 0];
ax.YTickLabel = 10.^(ax.YTick);
ax.YMinorTick = 'on';
ax.YAxis.MinorTickValues = unique([log10(1e-3:1e-3:1e-2) log10(1e-2:1e-2:1e-1) log10(1e-1:1e-1:1e-0)]);

xlabel('$T\;[\mathrm{ms}]$')            
ylabel('$1-F$')

hold on
plot(level_time.ms_from_sim * ps.monotonically_best_T, log10(1-ps.monotonically_best_F))
plot(level_time.ms_from_sim * rs.unique_durations,log10(1-rs.best_F))
plot(level_time.ms_from_sim * monotonically_best_T_j, log10(1-monotonically_best_F_j))


nOutliersTotal = 0;
for k = 1:numel(outliers)
    outliers_Tk = outliers{k};
    nOutliers_tk = size(outliers_Tk,1);
    nOutliersTotal = nOutliersTotal + nOutliers_tk;
    for i = 1:nOutliers_tk
        T = outliers_Tk(i,1);
        IF = outliers_Tk(i,3);
        hold on
        plot(T,IF,'x','color',colors(4,:),'markersize',8,'handlevisibility','off')
    end
end

unique_Ts_diff = unique_Ts(2) - unique_Ts(1);
nOutliersTotal = nOutliersTotal
nTotalPoints = sum(unique_Ts(1) -unique_Ts_diff/2  <= Ts &  Ts < unique_Ts(end) + unique_Ts_diff/2)

f2 = figure;
ax2 = copyobj(ax,f2);
ax2.Children(end).CData = densities <= outlierDensityThreshold;
%imagesc(level_time.ms_from_sim * unique_Ts,IF_log_sampling,densities <= outlierDensityThreshold);


pause(0.001)

autoArrangeFigures(2,3,2)

%save('shakeup_seed_equalized_data.mat') 

