Figure scripts were created in MATLAB 2019a.

Some figures include additional data analysis performed in-situ on the serialized data sets (in folder QM2_Serialized_Datasets_Algorithm_Seeding_v15). The actual data itself cannot be shared here due to GDPR compliance. The data sets include player generated data through the Quantum Moves 2 game (stored in an online database), and the data we generated on the computer cluster. Note that the naming convention on the data set files follows an older naming scheme, which has since been changed in the paper, but is kept here for backwards compatability.

Most scripts assume that this folder (the 'figure root') is the current folder in MATLAB.
Each individual script usually requires the user to specify certain paths, usually at least the location of the 'helperscripts' folder in the figure root (relative paths are not used).
The 'helperscripts/defaults.m' is a script run by every script, that sets up several common variables to ensure e.g. same style across plots.

Here is a listing of which scripts produce which figures in the paper (as per arxiv v1):

Fig 1: 			controllandscape/controllandscape.m 	(note that this is a bit special, as an interactive figure with callbacks are programmed to 'paint' the landscape)
Fig 3, 8, 12:  	solutiondensity/SolutionDensity.mlx
Fig 4, 9, 13:  	FT/FT.mlx
Fig 5, 10, 14: 	SAvGRAPE/SAvGRAPE.mlx
Fig 6: 			clustering/bhw/clustering_bhw.m
Fig 7: 			bhwSeedSegmentation/bhwseedsegmentation.m
Fig 11: 		clustering/splitting/clustering_splitting.m
Fig 15: 		clustering/shakeup/clustering_shakeup.m