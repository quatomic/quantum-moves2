function [binEdges, Fs_quantiles, ts_mean, Fs_mean] = statistics(ts,Fs)
            
    global nBinsForMedian nSeeds
            
    ts_order = cellfun(@(x) x(end),ts)'; % for each element, find the last time value
    [~,idxs] = sort(ts_order); 

    ts = ts(idxs);
    Fs = Fs(idxs);

    max_ts = ts{end}(end);
    binEdges = linspace(0,max_ts,nBinsForMedian); 

    ts_indices_in_bin_func = @(t1,t2) cellfun(@(t) t1 < t & t < t2 ,ts ,'UniformOutput' ,false);
    %figure

    ts_mean   = zeros(numel(binEdges)-1,1);
    Fs_mean   = zeros(numel(binEdges)-1,1);

    Fs_quantiles = zeros(numel(binEdges)-1, 3);


    for i = 2:numel(binEdges)
        tmin = binEdges(i-1); tmax = binEdges(i);

        ts_indices_in_bin = ts_indices_in_bin_func(tmin,tmax);

        %ts_indices_in_bin_include_backward = ts_indices_in_bin_func(ts(1),tmax);


        ts_in_bin_total = [];
        Fs_in_bin_total = [];
        
        for n=1:nSeeds


            ts_in_bin = ts{n}(ts_indices_in_bin{n});
            Fs_in_bin = Fs{n}(ts_indices_in_bin{n});

            ts_in_bin_total = [ts_in_bin_total; ts_in_bin];
            Fs_in_bin_total = [Fs_in_bin_total; Fs_in_bin]; 

            %plot(ts{1}(ts_bin{1}), mean(Fs{1}(ts_bin{1}))); hold on

        end
        ts_mean(i) = mean(ts_in_bin_total);
        Fs_mean(i) = mean(Fs_in_bin_total);

        Fs_quantiles(i,:) = quantile(Fs_in_bin_total,[.25 .50 .75]);
                
    end
end