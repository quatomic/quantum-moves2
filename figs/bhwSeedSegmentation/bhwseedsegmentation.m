clc
clear all
close all

helperscipts =  '/figs/helperscripts';
addpath(genpath(helperscipts))
defaults()

marker = '.';
markercol = colors(3,:);
markersize = 6;
save_plots       = 0;
paper_submission = 0;

boxcolor = 'k';

T = 0.27;
load('cmap_gist_stern_imitation_newcol.mat')

%% grape rs (nb)
rs_density = jsondecode(fileread('dens_data_segmented_seeding.json'))
        
%

yLimInterp_rs  = linspace(rs_density.extent(3), rs_density.extent(4), size(rs_density.dens_ar,1));

imagesc(rs_density.x_grid, yLimInterp_rs, flipud(rs_density.dens_ar))
   
colormap(cm)

yticks(flipud(rs_density.y_tic_pos))
yticklabels(flipud(rs_density.y_tic_lab))   

xticks(rs_density.x_grid)
xticklabels(rs_density.piec_mesh)

ax = gca;
ax.YDir = 'normal';
ax.CLim = [0, 0.10];

ax.FontSize = articleFormat.fontSize;


ylabel('$1-F$')
xlabel('$n_b$')
colorbar

title(['GRAPE RS at $T=' num2str(round(bhw_time.ms_from_sim * T, 4)) ' \lessim T_\mathrm{QSL}^{F=0.999} $']) 

backswingIdxs_rs = (rs_density.infid_log < rs_density.infid_family(1));
hold on
plot(rs_density.x_grid_bin(backswingIdxs_rs), rs_density.infid_log(backswingIdxs_rs ),marker,'color',markercol,'markersize',markersize)

%% grape ps 
ps_density = jsondecode(fileread('/figs/solutiondensity/FT_density/dens_plot_bhw_Upso.json'))

xLimInterp_ps  = linspace(ps_density.extent(1), ps_density.extent(2), size(ps_density.solution_density,2));
yLimInterp_ps  = linspace(ps_density.extent(3), ps_density.extent(4), size(ps_density.solution_density,1));

[~, Tidx] = min(abs(xLimInterp_ps - T));


figure
imagesc(xLimInterp_ps, yLimInterp_ps, flipud(ps_density.solution_density));
yticks(flipud(ps_density.y_tic_pos))
yticklabels(flipud(ps_density.y_tic_lab))
                
colormap(cm)

ax = gca;
ax.YDir = 'normal';
ax.CLim = [0, 0.10];


xLimInterp_diff =  (xLimInterp_ps(2) - xLimInterp_ps(1));
hold on
xline(T- xLimInterp_diff/2 , 'linewidth',2);
xline(T+ xLimInterp_diff/2 , 'linewidth',2);;
ax.XTick = sort(unique([ax.XTick T]))

set(gcf,'position',[1922 365 1559 400])
xlim([xLimInterp_ps(1) xLimInterp_ps(end)]) 


ps_density_strip = (ps_density.solution_density(:, Tidx));

ps_density_strip = mean([ps_density.solution_density(:, Tidx), ps_density.solution_density(:, Tidx+1)],2);

ps_density_strip_x = rs_density.x_grid(end) + 1;
ps_density_strip_label = 'PS';

figure
imagesc(-1, yLimInterp_ps, flipud(ps_density_strip));

xticklabels({round(xLimInterp_ps(Tidx),4)})

set(gcf,'position', [3400 357 80 407])

yticks(flipud(ps_density.y_tic_pos))
yticklabels(flipud(ps_density.y_tic_lab))
                
colormap(cm)

ax = gca;
ax.YDir = 'normal';
ax.CLim = [0, 0.10];


%% combined

sum(yLimInterp_ps - yLimInterp_rs)

figure

title(['GRAPE RS at $T_\mathrm{QSL}^{F=0.99} \approx ' num2str(round(bhw_time.ms_from_sim * T, 4)) ' $']) 


combined_density = [rs_density.dens_ar, ps_density_strip];
combined_xaxis = [rs_density.x_grid; ps_density_strip_x];

imagesc(combined_xaxis, yLimInterp_rs, flipud(combined_density))
   
colormap(cm)

yticks(flipud(rs_density.y_tic_pos))
yticklabels(flipud(rs_density.y_tic_lab))

xticks([rs_density.x_grid; ps_density_strip_x])

meshlabels = {rs_density.piec_mesh(1:6);'$2^4$'; '$2^5$'; '$2^6$'; '$2^7$'; '$2^8$'; rs_density.piec_mesh(end-1); '$n_t$'; ps_density_strip_label};
xticklabels(meshlabels)

ax = gca;
ax.YDir = 'normal';
ax.CLim = [0, 0.10];

ax.FontSize = articleFormat.fontSize;

ylabel('$1-F$')
xlabel('$n_b$')
colorbar

title(['GRAPE RS at $T=' num2str(round(bhw_time.ms_from_sim * T, 4)) ' $ ms']) 

backswingIdxs_rs = (rs_density.infid_log < rs_density.infid_family(1));
hold on

r=rectangle('Position',[(ps_density_strip_x-0.48) -3 1 3.2],'linewidth',2.5,'edgecolor',boxcolor,'Linestyle','-')

set(gcf,'position',[3600         529         266         191])

scatter(rs_density.x_grid_bin(backswingIdxs_rs), rs_density.infid_log(backswingIdxs_rs ),marker,'MarkerEdgeColor',markercol,'MarkerEdgeAlpha',0.25)
if save_plots
    
            if paper_submission
                outRoot = texroot;
            else
                outRoot = [figroot '/bhwSeedSegmentation'];
            end
            
            file_out = [outRoot '/seedsegmentation_density_bhw'];
            save2pdf(file_out);
            
end


rs_density_belowF09_nb2 = sum(rs_density.dens_ar(1:17,2))
ps_density_belowF09 = sum(ps_density_strip(1:17))

%% histogram
rs_histogram = jsondecode(fileread('hist_data_segmented_seeding.json'))

f_hist = figure;
H = histogram(rs_histogram.x_grid_bin);
%yyaxis right

rescaledValues = H.Values / rs_histogram.n_seeds;


algorithmResults = player_computer_metadata_load('upso_serialized.csv');
[algorithmResultTable, legendName ] = player_computer_data_load(algorithmResults,'bhw');

algorithmResultTable.Duration_sim = algorithmResultTable.Duration * bhw_time.sim_from_norm;
figure
scatter(algorithmResultTable.Duration_sim , 1-algorithmResultTable.Fidelity,'.')
set(gca,'yscale','log')
xlim([xLimInterp_ps(1) xLimInterp_ps(end)]) 

set(gcf,'position',[1921 50 1565 241])


resultTableInT= algorithmResultTable(T - xLimInterp_diff/2  <= algorithmResultTable.Duration_sim & ...
                                     T + xLimInterp_diff/2  >= algorithmResultTable.Duration_sim , :);
ps_bar_x = rs_histogram.x_grid(end) + 1;
ps_bar_label = 'PS';
hold on
scatter(resultTableInT.Duration_sim , 1-resultTableInT.Fidelity,'x')


xline(T);
xline(T - xLimInterp_diff/2);
xline(T + xLimInterp_diff/2);
backswingIdxs_ps = log10(1-resultTableInT.Fidelity) < rs_density.infid_family(1);

hold on 


n_ps_seeds = size(resultTableInT,1);
figure
bar(rs_histogram.x_grid,rescaledValues,'facecolor',colors(3,:),'displayname', ['RS (' num2str(rs_histogram.n_seeds) ' / bin)'])
hold on
bar(ps_bar_x, sum(backswingIdxs_ps)/ n_ps_seeds,'facecolor',colors(1,:),'displayname', ['PS (' num2str(numel(backswingIdxs_ps)) ')']);

xticks(sort([ps_bar_x; rs_histogram.x_grid]))
xticklabels(meshlabels)

ax = gca;
ax.FontSize = articleFormat.fontSize;

xlabel('$n_b$')
title('Fraction in back-swing strategy')

leg = legend();
leg.Location = 'northwest';

ylim([0, 0.15])

set(gcf,'position',[1660 397 231 146])


if save_plots
    
            if paper_submission
                outRoot = texroot;
            else
                outRoot = [figroot '/bhwSeedSegmentation'];
            end
            
            file_out = [outRoot '/seedsegmentation_histogram_bhw'];
            save2pdf(file_out);
            
end