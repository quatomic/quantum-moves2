clc
clear all
close all


helperscipts =  '/figs/helperscripts';
addpath(genpath(helperscipts))
defaults()



linewidth = 1;
markersize = 15;
markertype = '-.';

save_plots       = 0;
paper_submission = 0;



%% mean controls

data = jsondecode(fileread('bhw_clustering_data.json'));
figure

subplot(2,1,2)
hold on;
cluster_1 = data.x_1; cluster_1.col = colors(3,:);
plot(cluster_1.x_ar, cluster_1.u_mean,'color',cluster_1.col)
xx = [cluster_1.x_ar, flipud(cluster_1.x_ar)];
y1 = cluster_1.u_mean - cluster_1.u_std;
y2 = cluster_1.u_mean + cluster_1.u_std;
plot(cluster_1.x_ar, y1,'color',cluster_1.col)
plot(cluster_1.x_ar, y2,'color',cluster_1.col)

shade(cluster_1.x_ar, y1, cluster_1.x_ar, y2,'Filltype',[1 2; 2 1],'color',cluster_1.col,'linewidth',linewidth); % shade is a matlab fileexchange dependency


cluster0 = data.x0; cluster0.col = colors(1,:);
plot(cluster0.x_ar, cluster0.u_mean,'color',cluster0.col)
xx = [cluster0.x_ar, flipud(cluster0.x_ar)];
y1 = cluster0.u_mean - cluster0.u_std;
y2 = cluster0.u_mean + cluster0.u_std;
plot(cluster0.x_ar, y1,'color',cluster0.col)
plot(cluster0.x_ar, y2,'color',cluster0.col)

shade(cluster0.x_ar, y1, cluster0.x_ar, y2,'Filltype',[1 2; 2 1],'color',cluster0.col,'linewidth',linewidth); % shade is a matlab fileexchange dependency

cluster1 = data.x1; cluster1.col = colors(2,:);
plot(cluster1.x_ar, cluster1.u_mean,'color',cluster1.col)
xx = [cluster1.x_ar, flipud(cluster1.x_ar)];
y1 = cluster1.u_mean - cluster1.u_std;
y2 = cluster1.u_mean + cluster1.u_std;
plot(cluster1.x_ar, y1,'color',cluster1.col)
plot(cluster1.x_ar, y2,'color',cluster1.col)

shade(cluster1.x_ar, y1, cluster1.x_ar, y2,'Filltype',[1 2; 2 1],'color',cluster1.col,'linewidth',linewidth); % shade is a matlab fileexchange dependency

title('Cluster means')
xlabel('$t/T$')
ylabel('$u_1(t)$')

ax = gca;
ax.FontSize = articleFormat.fontSize;

%% clusters

subplot(2,1,1)
hold on;

alphaVal = 0.7;
legmarkersize = 10;
s=scatter(cluster_1.dur * bhw_time.ms_from_sim, 1-cluster_1.fid, '.','markeredgecolor',cluster_1.col,'markerfacealpha',alphaVal ,'markeredgealpha',alphaVal,'handlevisibility','off'); 
plot(nan,nan,'.', 'Color',cluster_1.col,'DisplayName',['-1 (' num2str(numel(cluster_1.fid)) ')'],'markersize',legmarkersize)

set(gca,'yscale','log')

set(gca,'ColorOrderIndex',1)
scatter(cluster0.dur * bhw_time.ms_from_sim, 1-cluster0.fid, '.','markeredgecolor',cluster0.col,'markerfacealpha',alphaVal,'markeredgealpha',alphaVal,'handlevisibility','off'); 
plot(nan,nan,'.', 'Color',cluster0.col,'DisplayName',['0 (' num2str(numel(cluster0.fid)) ')'],'markersize',legmarkersize)

set(gca,'ColorOrderIndex',2)
scatter(cluster1.dur * bhw_time.ms_from_sim, 1-cluster1.fid, '.','markeredgecolor',cluster1.col,'markerfacealpha',alphaVal,'markeredgealpha',alphaVal,'handlevisibility','off'); 
plot(nan,nan,'.', 'Color',cluster1.col,'DisplayName',['1 (' num2str(numel(cluster1.fid)) ')'],'markersize',legmarkersize)


fit0 = @(T_in) 10.^(-1.45 - 19.4*(T_in - 0.24));
fit1 = @(T_in) 10.^(-1.50 - 45.4*(T_in - 0.24));

fit0_param1 = 19.4 / bhw_time.ms_from_sim
fit0_param2 = 0.24 * bhw_time.ms_from_sim

fit1_param1 = 45.4 / bhw_time.ms_from_sim
fit1_param2 = fit0_param2;

fit0_si = @(T_in) 10.^(-1.45 - fit0_param1 *(T_in - fit0_param2));
fit1_si = @(T_in) 10.^(-1.50 - fit1_param1 *(T_in - fit1_param2));


plot(cluster0.x_fit * bhw_time.ms_from_sim, fit0_si(cluster0.x_fit * bhw_time.ms_from_sim), '--' ,'color', cluster0.col, 'Displayname','$(1-F)_0$')
plot(cluster0.x_fit * bhw_time.ms_from_sim, fit1_si(cluster0.x_fit * bhw_time.ms_from_sim), '--' ,'color', cluster1.col, 'Displayname','$(1-F)_1$')

ylabel('$1-F$')
xlabel('$T$ (ms)')

ylim([5e-4 1e-1])
xlim([0.078 0.125])

ax = gca;
ax.FontSize = articleFormat.fontSize;

leg = legend(); 
leg.Location = 'southwest';

set(gcf,'position',[3110 479 236 393])

if save_plots
    
            if paper_submission
                outRoot = texroot;
            else
                outRoot = [figroot '/clustering/bhw'];
            end
            
            file_out = [outRoot '/bhw_clustering'];
            save2pdf(file_out);
            
            command = ['/anaconda/bin/ps2pdf ' file_out '.pdf ' file_out '_compressed.pdf'];
            system(command)
end
