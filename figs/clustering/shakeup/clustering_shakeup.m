clc
clear all
close all


helperscipts =  '/figs/helperscripts';
addpath(genpath(helperscipts))
defaults()

figure

%% clusters
subplot(3,1,1)

linewidth = 2;
markersize = 15;
markertype = '-.';

save_plots       = 0;
paper_submission = 1;

data_ck = jsondecode(fileread('clust_shakeup_data.json'))

x_1 = data_ck.x_1;
x0 = data_ck.x0;
x1 = data_ck.x1;
x2 = data_ck.x2;
x3 = data_ck.x3;

hold on
errorbar(x2.u_mean, x2.u_std,markertype,'linewidth',linewidth,'markersize',markersize,'displayname','2')
errorbar(x1.u_mean, x1.u_std,markertype,'linewidth',linewidth,'markersize',markersize,'displayname','3')
errorbar(x0.u_mean, x0.u_std,markertype,'linewidth',linewidth,'markersize',markersize,'displayname','4');


errorbar(x3.u_mean, x3.u_std,markertype,'linewidth',linewidth,'markersize',markersize,'displayname','5')

xticklabels({'0','1','2','3','4','5'})
xlabel('$k$')
ylabel('$c_k$')

leg = legend;
leg.Location = 'northoutside';
leg.Orientation = 'horizontal';

ax = gca;
ax.FontSize = articleFormat.fontSize;


%% means
subplot(3,1,2)
data_means = jsondecode(fileread('interpretation_data.json'))

plot(data_means.t_aux, data_means.mean_rec(3,:))
hold on
plot(data_means.t_aux, data_means.mean_rec(2,:))
plot(data_means.t_aux, data_means.mean_rec(1,:))
plot(data_means.t_aux, data_means.mean_rec(4,:))

ax = gca;
ax.FontSize = articleFormat.fontSize;

title('Cluster means')
xlabel('t/T')
ylabel('$u(t) - \langle x(t)\rangle$')

yline(0,'k','--');

set(gcf,'position',[3355 322 273 624])

%% IF

alphaVal = 0.1;
legmarkersize = 10;

subplot(3,1,3)
hold on

scatter(x_1.dur * shakeup_time.ms_from_norm, 1-x_1.fid, 'k.','markerfacealpha',alphaVal,'markeredgealpha',alphaVal,'handlevisibility','off'); 
plot(nan,nan,'.', 'Color','k','DisplayName','unclassified','markersize',legmarkersize)

set(gca,'ColorOrderIndex',1)
scatter(x2.dur * shakeup_time.ms_from_norm, 1-x2.fid, '.','markerfacealpha',alphaVal,'markeredgealpha',alphaVal,'handlevisibility','off'); 
plot(nan,nan,'.', 'Color',colors(1,:),'DisplayName','$k=2$','markersize',legmarkersize)

set(gca,'ColorOrderIndex',2)
scatter(x1.dur * shakeup_time.ms_from_norm, 1-x1.fid, '.','markerfacealpha',alphaVal,'markeredgealpha',alphaVal,'handlevisibility','off'); 
plot(nan,nan,'.', 'Color',colors(2,:),'DisplayName','$k=3$','markersize',legmarkersize)

set(gca,'ColorOrderIndex',3)
scatter(x0.dur * shakeup_time.ms_from_norm, 1-x0.fid, '.','markerfacealpha',alphaVal,'markeredgealpha',alphaVal,'handlevisibility','off'); 
plot(nan,nan,'.', 'Color',colors(3,:),'DisplayName','$k=4$','markersize',legmarkersize)

set(gca,'ColorOrderIndex',4)
scatter(x3.dur * shakeup_time.ms_from_norm, 1-x3.fid, '.','markerfacealpha',alphaVal,'markeredgealpha',alphaVal,'handlevisibility','off'); 
plot(nan,nan,'.', 'Color',colors(4,:),'DisplayName','$k=5$','markersize',legmarkersize)



leg = legend();
leg.Location = 'southwest';

ax = gca;
ax.YScale = 'log';
ax.FontSize = articleFormat.fontSize;

ylabel('$1-F$')

xlabel('$T$ (ms)')



xmin_SI = 0.3 * shakeup_time.ms_from_norm;
xmax_SI = 1.2 * shakeup_time.ms_from_norm;
xlim([xmin_SI xmax_SI])
xticks(linspace(xmin_SI,xmax_SI,5))


if save_plots
    
            if paper_submission
                outRoot = [texroot '/fig_'];
            else
                outRoot = [figroot '/clustering/shakeup/'];
            end
            
            file_out = [outRoot 'shakeup_clustering'];
            save2pdf(file_out);
            
            command = ['/anaconda/bin/ps2pdf ' file_out '.pdf ' file_out '_compressed.pdf'];
            system(command)
end