clc
clear all
close all


helperscipts =  '/figs/helperscripts';
addpath(genpath(helperscipts))
defaults()


save_plots       = 0;
paper_submission = 1;

data = jsondecode(fileread('Splitting_control.json'))

xLimInterp  = linspace(data.extent(1), data.extent(2), size(data.img,2));
yLimInterp  = linspace(data.extent(3), data.extent(4), size(data.img,1));

imagesc(xLimInterp, yLimInterp * splitting_time.ms_from_sim , data.img)

ax = gca;
ax.YDir = 'normal';

ax.YTick = linspace(ax.YLim(1), ax.YLim(2),5);
ax.YTickLabel = round(ax.YTick,2)

load('cmap_gist_stern_imitation_newcol.mat')

colormap(brewermap([],'RdYlBu'))
colorbar

set(gcf,'position',[1450 473 258 154])


ax.FontSize = articleFormat.fontSize;

xlabel('$t/T$')
ylabel('$T$ (ms)')
title('Mean of optimal controls')  %$u_2(t/T)$


if save_plots
    
            if paper_submission
                outRoot = [texroot '/fig_'];
            else
                outRoot = [figroot '/clustering/splitting/'];
            end
            
            file_out = [outRoot 'splitting_clustering'];
            save2pdf(file_out);
            
            command = ['/anaconda/bin/ps2pdf ' file_out '.pdf ' file_out '_compressed.pdf'];
            system(command)
end