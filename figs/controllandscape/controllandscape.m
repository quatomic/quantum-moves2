clear all
close all
clc

% Note: this script allows to 'paint' the landscape by left+right clicking on the 2d view 

helperscipts =  '/figs/helperscripts';
addpath(genpath(helperscipts))
defaults()

%loadFromFile = 'landscape.fig';
%openfig(loadFromFile,'visible');

loadFromFile = 'landspace';

load(loadFromFile)



opengl('OpenGLLineSmoothingBug',1)
areaHeight = -1.5;


xlim([-8,8])
ylim([-8,8])
zlim([areaHeight,1.01])


%% Uniform


X = [-1,1,1,-1] * u1(end);
Y = [-1,-1,1,1] * u2(end);

uniformStrat = fill3(X,Y, areaHeight + 0*X, colors(2,:) );
uniformStrat.FaceAlpha=0.5;

uniformStrat.FaceColor = colors(4,:);



%% Player
% data = drawfreehand
% save('player_strat.mat','data')
p1 = load('player_strat_1.mat');
coords = p1.data.Position;
X = coords(:,1);
Y = coords(:,2);

playerStrat1 = fill3(X,Y, areaHeight + 0.01 + 0*X, colors(3,:) );
playerStrat1.FaceAlpha=0.4;
playerStrat1.LineWidth = 2;
playerStrat1.EdgeColor = colors(3,:);
playerStrat1.FaceColor = colors(3,:);


%getPosition(data)
view(-78,26)


xlabel('$u_1$')
ylabel('$u_2$')
zlabel('$F$')

zticks([0,0.5,1])

xticklabels('')
yticklabels('')

hold on

s = findall(f2,'Type','Surface');
s.FaceAlpha = 0.9;
colormap(flipud(brewermap([],'RdYlBu')))

points = { {4.3, 3.7,  playerStrat1.FaceColor}, ...
           {4.4, 2.06, playerStrat1.FaceColor}, ...
           {2.29, 3.867, playerStrat1.FaceColor}, ...
           {2.26, 1.0741, playerStrat1.FaceColor}, ...
           {-6.87, 4.857, uniformStrat.FaceColor}, ...
           {-6.39, -4.280, uniformStrat.FaceColor}, ...
           {3.09, -6.1082,  uniformStrat.FaceColor}, ...
           {3.5752, 3.895, uniformStrat.FaceColor}, ...
    }; 


pointOffset = [-0.05 -0.05 0];
for i = 1:length(points)

    point = points{i};
    
    [~, Ix ] = min(abs(point{1}-s.XData));
    [~, Iy ] = min(abs(point{2}-s.YData));
    point_val = s.ZData(Iy,Ix);
    plot3([point{1} point{1}] + pointOffset(1), [point{2} point{2}] + pointOffset(2),[areaHeight, point_val],'--','color',point{3})  ;
    plot3([point{1} point{1}] + pointOffset(1), [point{2} point{2}] + pointOffset(2),[point_val , point_val ] + pointOffset(3),'.','markersize',25,'color',point{3})  ;

end

f2.Children.XLabel.Position = [ -3.431998332834723   1.795931253198674  -1.924667004371095];
f2.Children.YLabel.Position = [ -4.293248898720208 -10.607487812129605  -1.043163438706664];
f2.Children.ZLabel.Position = [-6.438801712005898  10.100485869209152   0.386638816167178];

set(f1,'position',[-1772 555 560 420])
set(f2,'position',[-1877 50 560 420])


save2pdf('landscape',gcf,150);





%shading interp
%set(uniformStrat,'color',colors(1,:))