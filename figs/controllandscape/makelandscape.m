 function makelandscape()
clc;clear; close all;
global u1 u2 addGauss U1 U2 landscape imageHandle sideView

nu = 500;
u1 = linspace(-8,8,nu);
u2 = linspace(-8,8,nu);

[U1, U2] = meshgrid(u1,u2);

w = 0.2;
addGauss = @(coordinates) 0.15*exp(-(U1-coordinates(1)).^2/w - (U2-coordinates(2)).^2/w);

addGaussWithWidths = @(coordinates,widths) exp(-(U1-coordinates(1)).^2/widths(1) - (U2-coordinates(2)).^2/widths(2));

% add background of small bumps
nbumps = 35; % per axis
edgeOffset = 0.5;
bumps_u1 = linspace(u1(1) + edgeOffset, u1(end) - edgeOffset,nbumps);
bumps_u2 = linspace(u2(1) + edgeOffset, u2(end) - edgeOffset,nbumps);

background = zeros(length(u1),length(u2));
for i = 1:length(bumps_u1)
    for j = 1:length(bumps_u2)
        coordShift = randn(1,2);
        coordinates = [bumps_u1(i), bumps_u2(j)] + coordShift;
        
        widthShift = rand(1,2);
        widths = [w, w] + widthShift;
        
        background = background + addGaussWithWidths(coordinates,widths);
        
    end
end
background_maxval = 0.25;
background = background / max(max(background));
background = background_maxval * background;


%loadFromFile = 'landscape.fig';
loadFromFile = '';

if isfile(loadFromFile)
    openfig(loadFromFile,'visible');
    
    FigList = findobj(allchild(0), 'flat', 'Type', 'figure');

    landscape = background;

else
    landscape = zeros(length(u1),length(u2));
    %landscape = abs(sin(U1).*cos(U2) .* exp(abs(1 - sqrt(U1.^2 + U2.^2)/pi)));

    f1 = figure;
    hAxes = axes();
    %imageHandle = imshow(imObj);
    imageHandle = surf(u1,u2,landscape);
    zlim([0,1])
    %colormap summer
    shading interp

    %hold on;
    view(2)
    set(imageHandle,'ButtonDownFcn',@ImageClickCallback);
    set(f1,'windowscrollWheelFcn', @ImageClickCallback);
    set(f1,'position',[-2479 528 560 420])

    f2 = figure;
    sideView = surf(u1,u2,landscape);
    zlim([0,1])
    set(f2,'position',[-2480 36 560 420])
    %colormap summer
    shading interp

    landscape = landscape + background;
    
    set(imageHandle,'zdata',landscape)
    set(sideView,'zdata',landscape)
end


hold on
disp('')
%surf(u1,u2,p);

function ImageClickCallback ( objectHandle , eventData )

    %global addGauss
    
    leftClick  = 1;
    middleClick = 2;
    rightClick = 3;
    
    axesHandle  = get(objectHandle,'Parent');
    coordinates = get(axesHandle,'CurrentPoint'); 
    coordinates = coordinates(1,1:2);
 
    if eventData.Button == leftClick
        landscape = landscape + addGauss(coordinates);
        landscape(landscape > 1) = 1;
    end
    
    if eventData.Button == rightClick
        landscape = landscape - 0.4*addGauss(coordinates);
        landscape(0 > landscape) = 0;
    end
    
    if eventData.Button == middleClick
        FolderName = pwd;
        FigList = findobj(allchild(0), 'flat', 'Type', 'figure');
        filename = 'landscape';
        disp('Saving...')
        savefig(FigList,filename)
        save('landspace')
        disp('Saved!')
%         for iFig = 1:length(FigList)
%           FigHandle = FigList(iFig);
%           FigName   = get(FigHandle, 'Name');
%           savefig(FigHandle, fullfile(FolderName, FigName, '.fig'));
%         end
    end
        
    set(imageHandle,'zdata',landscape)
    set(sideView,'zdata',landscape)
    
end



end