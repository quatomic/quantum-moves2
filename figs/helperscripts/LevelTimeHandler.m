classdef LevelTimeHandler
    
    properties
        
        tqsl_sim
        tqsl_ms
        
        ms_from_sim
        sim_from_norm
        
        ms_from_norm
        norm_from_ms;
        
    end
    
    methods
        function obj = LevelTimeHandler(ms_from_sim,tqsl_sim)
    
            % fundamentals
            obj.tqsl_sim = tqsl_sim;
            obj.ms_from_sim = ms_from_sim;
            
            
            % derived
            obj.tqsl_ms       = ms_from_sim*tqsl_sim;
            obj.sim_from_norm = tqsl_sim;
            obj.ms_from_norm  = obj.ms_from_sim*obj.sim_from_norm;
            obj.norm_from_ms  = 1/obj.ms_from_norm;

        end
        
    end
end

