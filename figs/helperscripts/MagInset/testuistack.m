clear all
close all
clc


x = linspace(0,1.2,100);
y = exp(-4*x.^2);

h1 = semilogy(x,y)
ylim([1e-3, 1])

hold on
h2 = semilogy(x,y)
h3 = semilogy(x,y)

yline(1e-2)

MagInset(gcf, -1, [0.9 1.1 5e-3 5e-2], [0.1 0.5 0.1 0.5], {'NW','NW';'SE','SE'});
