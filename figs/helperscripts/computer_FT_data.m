function [durations, F, unique_durations, best_F] = computer_FT_data(algorithmResultTable)

    
    global xaxis_timeunits level_time

    durations = algorithmResultTable.Duration;
    F         = algorithmResultTable.FinalFidelity;
    
    
    % computer durations are stored in sim time
    if strcmp(xaxis_timeunits,'ms');         durations = level_time.ms_from_sim * durations ; end;
    if strcmp(xaxis_timeunits,'normalized'); durations = level_time.norm_from_ms * level_time.ms_from_sim * durations ; end;
    
    
    [durations I] = sort(durations); % sort by duration
    F = F(I);
    
    % now sort by fidelity within each duration
    unique_durations = unique(durations);
    best_F = zeros(size(unique_durations));
    for k = 1:numel(unique_durations)
        duration_k = unique_durations(k);
        duration_k_idxs = find(durations == duration_k,1e9,'last'); % find occurences of a unique duration 

        best_F(k) = max(F(duration_k_idxs));
    end

end