function [algorithmResultTable,legendName] = computer_data_load(algorithmResultFileStruct,Level)

    global legendFirstColWidth
    
    defaults();
    
    algorithmResultFile = [algorithmResultFileStruct.folder '/' algorithmResultFileStruct.name]


    importopts           = detectImportOptions(algorithmResultFile);
    importopts.Delimiter = '|';
    importopts.VariableNames = {'Duration','InitialFidelity','FinalFidelity','dt','filename','levelid','optimizerType','seedingType','stopCode'};
    importopts.VariableTypes = {'double','double','double','double','char','char','char','char','double'};
    
    algorithmResultTable = readtable(algorithmResultFile,importopts);
    preview(algorithmResultFile,importopts);

    
    algTypeName  = algorithmResultTable.optimizerType{1}; algTypeName(regexp(algTypeName,'['']')) = []; % remove "'" from  
    algTypeName(regexp(algTypeName,'[_]')) = ['-']; % remove "_" 
    
    seedTypeName = algorithmResultTable.seedingType{1};   seedTypeName(regexp(seedTypeName,'['']')) = []; % remove "'" from  
    seedTypeName(regexp(seedTypeName,'[_]')) = ['-']; % remove "_" 
    
    seedTypeName = strrep(seedTypeName,'PS-RSS-DE','Pr-RSS-DE');
    

    algTypeName = strrep(algTypeName,'SGA-BUCKET','SA $(n_b = 40)$');
    algTypeName = strrep(algTypeName,'SGA','SA $(n_b = n_t)$');
    
    
    legendName = ['\begin{tabular}{p{' legendFirstColWidth '}r}' algTypeName ' ' seedTypeName '&' num2str(size(algorithmResultTable,1)) '\end{tabular}'];
    
    
end