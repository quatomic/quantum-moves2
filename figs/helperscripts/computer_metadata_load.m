function [algorithmResultsFiles] = computer_metadata_load(Level,seedDataType)

global dataroot

fileext = 'csv';

datafolder = [dataroot '/' seedDataType '/' Level ];
algorithmResultsFiles = dir([datafolder '/*.' fileext]);

for k=1:numel(algorithmResultsFiles)
   algorithmResultsFiles(k).seedtype = 'computer';
end




end