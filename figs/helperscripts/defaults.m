global dataroot helperscipts 

texroot      =  '/Users/au446513/projects/quantum-moves2_paper';
dataroot     = '/figs/QM2_Serialized_Datasets_Algorithm_Seeding_v15';

figroot      =  '/Users/au446513/projects/quantum-moves2_paper/figs';
helperscipts =  '/figs/helperscripts';
addpath(genpath(helperscipts))

colors = brewermap(15,'Set1');
colors(6,:) = []; % remove yellow color
 
msFroms  = 1e3;
sFromms  = 1/msFroms;

bhw_ms_from_sim       = msFroms*0.000387131;
shakeup_ms_from_sim   = msFroms*0.001;
splitting_ms_from_sim = msFroms*0.001;


bhw_tqsl_sim      = 0.37;   
shakeup_tqsl_sim  = 0.89;     
spliting_tqsl_sim = 0.92;     


bhw_time       = LevelTimeHandler(bhw_ms_from_sim, bhw_tqsl_sim);
shakeup_time   = LevelTimeHandler(shakeup_ms_from_sim, shakeup_tqsl_sim);
splitting_time = LevelTimeHandler(splitting_ms_from_sim, spliting_tqsl_sim);


articleFormat.legendSize = 9;
articleFormat.fontSize = 9;
articleFormat.titleSize = 12;

