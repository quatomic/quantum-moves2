function [densities, outliers] = kerneldensities(Ts, Fs, Ts_sampling, IF_log_sampling, bandwidth, outlierDensityThreshold)


outliers = cell(1,numel(Ts_sampling));

indices_from_v1_to_v2_func= @(v1,v2,values)  v1 <= values & values < v2;

unique_Ts_diff = Ts_sampling(2) - Ts_sampling(1);
IF_log_sampling_diff = IF_log_sampling(2) - IF_log_sampling(1);

densities = [];


for k = 1 : numel(Ts_sampling)
    
    t1 = Ts_sampling(k) - unique_Ts_diff/2;
    t2 = Ts_sampling(k) + unique_Ts_diff/2;
        
    current_Ts_idxs = indices_from_v1_to_v2_func(t1, t2 , Ts);
    if sum(current_Ts_idxs) == 0; continue; end;
    
    current_Ts = Ts(current_Ts_idxs);
    current_Fs = Fs(current_Ts_idxs);
    current_IFs = log10(1-current_Fs); 
    
    pd = fitdist(current_IFs,'kernel','Kernel','epanechnikov','Bandwidth',bandwidth);
    density = pdf(pd,IF_log_sampling);
    normalization = sum(density);
    normalizedDensity = density'/normalization;
    densities = [densities, normalizedDensity];
    
    % find outliers
    outlierDensityIndices = normalizedDensity <= outlierDensityThreshold;
    for i = 1:numel(current_IFs)
        [~, closestIdx] = min(abs(IF_log_sampling - current_IFs(i)));
        if outlierDensityIndices(closestIdx) == 1
            outliers{k}(end+1,:) = [current_Ts(i), current_Fs(i), current_IFs(i)];
        end
    end
    

end

   %% 
   plotExample = 0;
   if plotExample 
   figure
    
    pd = fitdist(log10(1-current_Fs),'kernel','Kernel','epanechnikov','Bandwidth',bandwidth)
    %x = 1e-3:1e-4:0.999;
    %x = 1e-3: 1e-4:1;
    x = -3: 1e-2:0;
    y = pdf(pd,x);
    hold off
    plot(x,y)
    hold on
    %histogram(current_Fs)
    plot(log10(1-current_Fs), 0*current_Fs,'o')
%    plot(current_Fs, 0*current_Fs,'o')
    
    
    %semilogy(current_Ts,1-current_Fs,'.')
    %semilogy(current_Ts,1-current_Fs)
   end


end