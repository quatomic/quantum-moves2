function [algorithmResultTable,legendName] = player_computer_data_load(algorithmResultFileStruct,Level)

    global legendFirstColWidth

    defaults();

       
    algorithmResultFile = [algorithmResultFileStruct.folder '/' algorithmResultFileStruct.name]

    importopts           = detectImportOptions(algorithmResultFile);
    
    importopts.VariableNames{2} = 'Fidelity'; % change table names for later use


    algorithmResultTable = readtable(algorithmResultFile,importopts);
    preview(algorithmResultFile,importopts);



    if     strcmp(Level,'bhw');           level_id = 16; 
    elseif strcmp(Level,'gpe_shakeup');   level_id = 7;
    elseif strcmp(Level,'gpe_split');     level_id = 11;
    else;  error('no such level'); end

    algorithmResultTable = algorithmResultTable(algorithmResultTable.levelId == level_id,:); % truncate table to only the relevant level

    
    if     strcmp(algorithmResultFileStruct.name,'upso_serialized.csv')
                prefix = 'GRAPE PS';
    elseif strcmp(algorithmResultFileStruct.name,'preOpso_serialized_1_cycle.csv')
                prefix = 'GRAPE Pr-PO (1)';
    elseif strcmp(algorithmResultFileStruct.name,'preUpso_serialized_1_cycle.csv')
                prefix = 'GRAPE Pr-PS (1)';
    elseif strcmp(algorithmResultFileStruct.name,'preOpso_serialized_2_cycles.csv')
                prefix = 'GRAPE Pr-PO (2)';
    elseif strcmp(algorithmResultFileStruct.name,'preUpso_serialized_2_cycles.csv')
                prefix = 'GRAPE Pr-PS (2)';
    else
            error('No such file')
    end
            


    
    legendName = ['\begin{tabular}{p{' legendFirstColWidth '}r}' prefix '&' num2str(size(algorithmResultTable,1)) '\end{tabular}'];

    
end