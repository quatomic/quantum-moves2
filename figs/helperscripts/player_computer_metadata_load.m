function [algorithmResultsFiles] = player_computer_metadata_load(filename)

global dataroot

seedDataType = 'playerData';
fileext = 'csv';

datafolder = [dataroot '/' seedDataType ];

%  need to join the two databases (done in data load)
algorithmResultsFiles.folder   = datafolder; 
algorithmResultsFiles.seedtype = 'player_computer';
algorithmResultsFiles.name = filename;


% dummy fields to be able to concatenate with computer structs
algorithmResultsFiles.date = '';
algorithmResultsFiles.bytes = 0;
algorithmResultsFiles.datenum =  0;
algorithmResultsFiles.isdir = 1;


end