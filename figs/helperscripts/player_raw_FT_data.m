function [durations, F, iterations, monotonically_best_T, monotonically_best_F] = player_raw_FT_data(algorithmResultTable)

    global xaxis_timeunits level_time

    defaults();
    
    durations  = algorithmResultTable.Duration;
    F          = algorithmResultTable.Fidelity;
    
    % only set 'iterations' if this is raw player data
    if ismember('optimization_iterations', algorithmResultTable.Properties.VariableNames)
        iterations = algorithmResultTable.optimization_iterations;
    else
        iterations = [];
    end
    
    
    % player durations are stored in normalized time 
    if strcmp(xaxis_timeunits,'ms'); durations = level_time.ms_from_norm * durations ; end;
    
    
    [durations I] = sort(durations); % sort by duration
    F = F(I);
    
    if ismember('optimization_iterations', algorithmResultTable.Properties.VariableNames)
        iterations(I);
    end
    

    % now find monotonically best fidelity 
    monotonically_best_F = zeros(size(durations)); % make sure arrays are large enough, truncate later
    monotonically_best_T = zeros(size(durations));


    monotonically_best_T(1) = durations(1);
    monotonically_best_F(1) = F(1);

    best_index = 1;
    for k = 2:numel(durations)
        if F(k) >= monotonically_best_F(best_index)
            best_index = best_index + 1;
            monotonically_best_F(best_index) = F(k);
            monotonically_best_T(best_index) = durations(k);
        end
    end

    monotonically_best_F =  monotonically_best_F(1:best_index); % truncate 
    monotonically_best_T =  monotonically_best_T(1:best_index);

end