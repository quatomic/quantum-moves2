function [joinedAlgorithmResultTable,legendName] = player_raw_data_load(algorithmResultFileStruct,Level)

    global legendFirstColWidth

    defaults();

    databases = {'ps_db.csv'};
    
    joinedAlgorithmResultTable = [];
    
    for k = 1:length(databases)
       
        algorithmResultFile = [algorithmResultFileStruct.folder '/' databases{k}]

        importopts           = detectImportOptions(algorithmResultFile);
        
        importopts.VariableNames{4} = 'Fidelity'; % change table names for later use
        importopts.VariableNames{6} = 'Duration'; 
        
    

        algorithmResultTable = readtable(algorithmResultFile,importopts);
        preview(algorithmResultFile,importopts);


        if     strcmp(Level,'bhw');           level_id = 16; 
        elseif strcmp(Level,'gpe_shakeup');   level_id = 7;
        elseif strcmp(Level,'gpe_split');     level_id = 11;
        else;  error('no such level'); end

        algorithmResultTable = algorithmResultTable(algorithmResultTable.level_id == level_id,:); % truncate table to only the relevant level
        
        algorithmResultTable = algorithmResultTable(algorithmResultTable.Duration <= 1.2,:);       % remove duration larger than 1.2

        
        joinedAlgorithmResultTable = [joinedAlgorithmResultTable; algorithmResultTable];
        
    end
        
    algTypeName  = 'PGRAPE';
    seedTypeName = 'PS+PO';
        
    legendName = ['\begin{tabular}{p{' legendFirstColWidth '}r}' algTypeName ' ' seedTypeName '&' num2str(size(joinedAlgorithmResultTable,1)) '\end{tabular}'];


    
end