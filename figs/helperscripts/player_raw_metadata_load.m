function [algorithmResultsFiles] = player_raw_metadata_load()

global dataroot

seedDataType = 'playerData';
fileext = 'csv';

datafolder = [dataroot '/' seedDataType ];

%  need to join the two databases (done in data load)
algorithmResultsFiles.folder   = datafolder; 
algorithmResultsFiles.seedtype = 'player_raw';

% dummy fields to be able to concatenate with computer structs
algorithmResultsFiles.date = '';
algorithmResultsFiles.bytes = 0;
algorithmResultsFiles.datenum =  0;
algorithmResultsFiles.isdir = 1;
algorithmResultsFiles.name = '';


end