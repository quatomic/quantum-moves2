function saveFigAsFile(name,varargin)

p = inputParser;
p.addParameter('clean',true,@islogical)

p.parse(varargin{:});

fig = gcf;
set(fig,'renderer','Painters')
print(fig,name,'-depsc')
%saveas(fig,name,'epsc')
if p.Results.clean
    epsclean([name,'.eps'],'groupSoft',true)
end
