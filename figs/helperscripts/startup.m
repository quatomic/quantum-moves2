disp(['Settings and default scripts runs from /Users/au446513/Documents/MATLAB/startup.m'])

startupFolder = '/Users/au446513/Documents/MATLAB/';

tickFontSize   = 18;
legendFontSize = 18;
axisFontSize   = 18;
txtFontSize    = 18;
%figureDefaultPosition = [680 632 598 466]+[-200 -200 200 200];

set(groot, 'defaulttextinterpreter','latex');  
set(groot, 'defaultAxesTickLabelInterpreter','latex');  
set(groot, 'defaultLegendInterpreter','latex');  


set(groot, 'DefaultLineLineWidth', 2, ...
           'DefaultTextInterpreter', 'LaTeX', ...
           'DefaultAxesTickLabelInterpreter', 'LaTeX', ...
           'DefaultAxesFontName', 'LaTeX', ...
           'DefaultLegendInterpreter', 'LaTeX', ...
           'DefaultAxesLineWidth', 1.5, ...
           'DefaultAxesBox', 'on')

       
set(groot, ...
   'DefaultTextInterpreter', 'LaTeX', ...
   'DefaultAxesTickLabelInterpreter', 'LaTeX', ...
   'DefaultAxesFontName', 'LaTeX', ...
   'DefaultLegendInterpreter', 'LaTeX')


set(0, 'DefaultAxesFontSize',axisFontSize)
set(0, 'DefaultLegendFontSize',legendFontSize)


colors = brewermap(15,'Set1');
colors(6,:) = []; % remove yellow color

set(groot,'defaultAxesColorOrder',colors)

%files = dir(startupFolder);
%dirFlags = [files.isdir];
%subFolders = files(dirFlags)

addpath([startupFolder '/freezeColors'])
addpath([startupFolder '/export_fig'])