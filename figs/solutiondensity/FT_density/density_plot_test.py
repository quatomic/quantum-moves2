# -*- coding: utf-8 -*-


import matplotlib.pyplot as plt
import numpy as np
import json, os

levels = ['bhw', 'splitting', 'shakeup']

for l_ in levels:
    data_files = [f_ for f_ in os.listdir() if 'dens_plot_' + l_ in f_]
    
    for f_ in data_files:
        with open(f_, 'r') as fid:
            plt_data = json.load(fid)
        
        ###########
        plt.figure()
        aspect = np.abs((plt_data['extent'][1] - plt_data['extent'][0])/(plt_data['extent'][3] - plt_data['extent'][2]))
        
        plt.imshow(plt_data['solution_density'], cmap = plt.get_cmap(plt_data['c_map']), vmin = 0.0, vmax = plt_data['vmax'], interpolation='none', extent=plt_data['extent'], aspect = aspect)
        
        plt.yticks(plt_data['y_tic_pos'], plt_data['y_tic_lab'])
        
        plt.plot(plt_data['best_player_outline_x'], plt_data['best_player_outline_y'], color="orange")
        
        plt.plot(plt_data['low_chance_x'], plt_data['low_chance_y'], 'xw', markersize = 4)
        
        plt.xlim(plt_data['extent'][:2])
        plt.ylim(plt_data['extent'][2:])
        
        plt.title(plt_data['title'])
        
        plt.xlabel(plt_data['x_label'])
        plt.ylabel(plt_data['y_label'])
        
        plt.colorbar()      
    
            